<!doctype html>
<html lang="en" class="no-focus">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Setting | MYTA</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

    <!-- END Icons -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/js/plugins/select2/select2.css'?>">




</head>

<body>
    <!-- Page Container -->

    <div id="page-container"
        class="sidebar-o side-scroll main-content-boxed side-trans-enabled page-header-fixed <?php if($dark === 'dark'): ?>page-header-inverse sidebar-inverse <?php endif; ?>">


        <?php echo $this->load->view('admin/v_sidemenu.php');?>


        <!-- Header -->
        <?php echo $this->load->view('admin/header.php');?>
        <!-- END Header -->

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Settings</h3>
                                <div class="block-options">
                                </div>
                            </div>
                            <div class="block-content block-content-full">
                               <form action="<?php echo base_url().'admin/setting/update_user';?>" method="post" enctype="multipart/form-data">
                                   <div class="row">
                                       <?php foreach($dataUser->result() as $user): ?>
                                       <div class="col-md-6">
                                           <div class="form-group">
                                               <input type="hidden" name="xkode" value="<?php echo $user->pengguna_id ?>" required>
                                               <input class="form-control" type="text" placeholder="Name" value="<?php echo $user->pengguna_nama; ?>" readonly required>
                                           </div>
                                       </div>
                                       <div class="col-md-6">
                                           <div class="form-group">
                                               <input class="form-control" type="email" placeholder="Email" value="<?php echo $user->pengguna_email; ?>" readonly required>
                                           </div>
                                       </div>
                                       <div class="col-md-12">
                                           <div class="form-group">
                                               <input class="form-control" type="password" name="xoldpass" placeholder="Old Password">
                                           </div>
                                       </div>
                                       <div class="col-md-6">
                                           <div class="form-group">
                                               <input class="form-control" type="password" name="xpass1" placeholder="New Password">
                                           </div>
                                       </div>
                                       <div class="col-md-6">
                                           <div class="form-group">
                                               <input class="form-control" type="password" name="xpass2" placeholder="Confirm Password">
                                           </div>
                                       </div>
                                       <div class="col-md-12">
                                           <div class="form-group">
                                               <input class="dropify" type="file" name="filefoto" data-default-file="<?php echo base_url().'assets/images/'.$user->pengguna_photo; ?>">
                                           </div>
                                       </div>
                                       <?php endforeach; ?>
                                       <div class="col-md-6">
                                           <div class="form-group">
                                               <button type="submit" class="btn btn-md btn-primary btn-square"> Update</button>
                                           </div>
                                       </div>
                                   </div>
                               </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Project</h3>
                                <div class="block-options">
                                </div>
                            </div>
                            <div class="block-content block-content-full">
                               <form action="<?php echo base_url().'admin/setting/update_prior';?>" method="post">
                               <div class="row">
                                   <div class="col-md-12">
                                       <div class="form-group">
                                           <label>Priority</label>
                                           <?php $idUser = $this->session->userdata('idadmin'); ?>
                                           <input type="hidden" name="xkode" value="<?php echo $idUser; ?>" required>
                                           <!-- <select class="js-select2 form-control" name="xproject" id="example-select2" onchange="this.form.submit()" >
                                                <option value="">- No Priority -</option>
                                                <?php 
                                                $kategori = $this->db->query("SELECT * FROM kategori");
                                                foreach($kategori->result() as $kat): ?>
                                                <?php if($prior === $kat->kategori_id):?>
                                                <option value="<?php echo $kat->kategori_id;?>" selected>
                                                    <?php echo $kat->kategori_nama; ?></option>
                                                <?php else: ?>
                                                <option value="<?php echo $kat->kategori_id;?>"><?php echo $kat->kategori_nama; ?>
                                                </option>
                                                <?php endif; ?>
                                                <?php endforeach; ?>
                                            </select> -->
                                            <select class="js-select2 form-control" id="example-select2"
                                            onchange="this.form.submit()" name="xproject" required>
                                            <option value=""> Choose Project</option>
                                            <?php
                                            $userId = $this->session->userdata('idadmin');
                                            if($akses === '5'):
                                                $userId = $this->session->userdata('idadmin');?>
                                               <?php $kategori = $this->db->query("SELECT * FROM kategori where kategori_tl_id='$userId'");
                                                        foreach($kategori->result() as $kat): ?>
                                                    <?php if($prior === $kat->kategori_id):?>
                                                        <option value="<?php echo $kat->kategori_id;?>" selected>
                                                            <?php echo $kat->kategori_nama; ?></option>
                                                        <?php else: ?>
                                                        <option value="<?php echo $kat->kategori_id;?>">
                                                            <?php echo $kat->kategori_nama; ?></option>
                                                        <?php endif; ?>
                                                <?php endforeach; ?>
                                                <?php else: ?>
                                           <?php $tim = $this->db->query("SELECT * FROM tim where tim_user_id='$userId' AND tim_acc='VERIFIED'");
                                            foreach($tim->result() as $tm):
                                                $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$tm->tim_kategori_id'");
                                                    foreach($kategori->result() as $kat): ?>
                                                    <?php if($prior === $kat->kategori_id):?>
                                                    <option value="<?php echo $kat->kategori_id;?>" selected>
                                                        <?php echo $kat->kategori_nama; ?></option>
                                                    <?php else: ?>
                                                    <option value="<?php echo $kat->kategori_id;?>">
                                                        <?php echo $kat->kategori_nama; ?></option>
                                                    <?php endif; ?>
                                            <?php endforeach; ?>
                                            <?php endforeach; ?>
                                            <?php endif; ?>

                                        </select>
                                       </div>
                                   </div>
                               </div>
                               </form>

                            </div>
                        </div>
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Theme</h3>
                                <div class="block-options">
                                </div>
                            </div>
                            <div class="block-content block-content-full">
                               <form id="theme" action="<?php echo base_url().'admin/setting/update_dark';?>" method="post">
                               <div class="row">
                                   <div class="col-md-6">
                                        <label class="css-control css-control-sm css-control-secondary css-radio">
                                            <input type="radio" class="css-control-input" value="dark"
                                                name="xdark" checked>
                                            <span class="css-control-indicator"></span>
                                            <img alt="dark" src="<?php echo base_url().'theme/images/screen_dark.png';?>" width="200px">
                                        </label>
                                   </div>
                                   <div class="col-md-6">
                                         <label class="css-control css-control-sm css-control-info css-radio">
                                            <input type="radio" class="css-control-input" value=""
                                                name="xdark">
                                            <span class="css-control-indicator"></span>
                                            <img alt="light" src="<?php echo base_url().'theme/images/screen_light.png';?>" width="200px">
                                        </label>
                                   </div>
                                   <div class="col-md-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-sm btn-primary">Change Theme</button>
                                        </div>
                                   </div>
                                </div>
                               </form>

                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
        <!-- Footer -->
        <?php echo $this->load->view('admin/v_footer');?>
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <!-- Codebase Core JS -->
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/select2/select2.js'?>"></script>
     <script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $( ".themeBtn" ).click(function() {
            $( "#theme" ).submit();
        });
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Profile Photo',
                replace: 'Change',
                remove: 'Delete',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });

        //Show Modal Update Kategori
        $('.btn-edit').on('click', function() {
            var kategori_id = $(this).data('id');
            var kategori_nama = $(this).data('kategori');
            var kategori_deskripsi = $(this).data('deskripsi');
            $('#ModalUpdate').modal('show');
            $('[name="xkode"]').val(kategori_id);
            $('[name="xkategori2"]').val(kategori_nama);
            $('[name="xdeskripsi"]').val(kategori_deskripsi);
        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var kategori_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(kategori_id);
        });

    });
    </script>
    
    <script type="text/javascript">
    $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
    $(function() {
        CKEDITOR.replace('ckeditor', {
            height: '240px',
            toolbar: [{
                    name: 'clipboard',
                    groups: ['clipboard', 'undo'],
                    items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo',
                        'Redo'
                    ]
                },
                {
                    name: 'editing',
                    groups: ['find', 'selection', 'spellchecker'],
                    items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']
                },
                '/',
                {
                    name: 'basicstyles',
                    groups: ['basicstyles', 'cleanup'],
                    items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript',
                        '-', 'CopyFormatting', 'RemoveFormat'
                    ]
                },
                {
                    name: 'paragraph',
                    groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                    items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-',
                        'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter',
                        'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language'
                    ]
                },
                {
                    name: 'links',
                    items: ['Link', 'Unlink', 'Anchor']
                },
                {
                    name: 'insert',
                    items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar',
                        'PageBreak', 'Iframe'
                    ]
                },
                '/',
                {
                    name: 'styles',
                    items: ['Styles', 'Format', 'Font', 'FontSize']
                },
                {
                    name: 'colors',
                    items: ['TextColor', 'BGColor']
                },
            ]
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });



        $('.btn-detail').on('click', function() {
            var work_deskripsi = $(this).data('deskripsi');
            $('#ModalDetail').modal('show');
            $('[name="deskripsi"]').val(work_deskripsi);
        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var work_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(work_id);
        });

    });
    </script>
  
    <?php if($this->session->flashdata('msg')=='warning'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Warning',
        text: "Image upload error",
        showHideTransition: 'slide',
        icon: 'error',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#FF4859'
    });
    </script>
    <?php elseif($this->session->flashdata('msg')=='wrong-pass'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Warning',
        text: "Wrong old Password!",
        showHideTransition: 'slide',
        icon: 'error',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#FF4859'
    });
    </script>
    <?php elseif($this->session->flashdata('msg')=='nothing'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Warning',
        text: "Nothing changes",
        showHideTransition: 'slide',
        icon: 'warning',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#7EC857'
    });
    </script>
    <?php elseif($this->session->flashdata('msg')=='sukses'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Success',
        text: "Profile Updated",
        showHideTransition: 'slide',
        icon: 'success',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#7EC857'
    });
    
    </script>
    <?php elseif($this->session->flashdata('msg')=='prior'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Success',
        text: "Priority Updated",
        showHideTransition: 'slide',
        icon: 'success',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#7EC857'
    });
    
    </script>
    <?php elseif($this->session->flashdata('msg')=='password'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Update Failed',
        text: "Wrong Confirmation password",
        showHideTransition: 'slide',
        icon: 'error',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#FF4859'
    });
    </script>
    <?php elseif($this->session->flashdata('msg')=='show-modal'):?>
    <script type="text/javascript">
    $('#ModalResetPassword').modal('show');
    </script>
    <?php else:?>

    <?php endif;?>
    <script>
    jQuery(function() {
        Codebase.helpers(['select2']);
    });
    </script>

</body>

</html>