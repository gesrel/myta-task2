<!doctype html>
<html lang="en" class="no-focus">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Assignor</title>

    <meta name="description" content="">
    <meta name="author" content="M Fikri Setiadi">
    <meta name="robots" content="noindex, nofollow">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

    <!-- END Icons -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">
</head>

<body>
    <!-- Page Container -->

    <div id="page-container"
        class="sidebar-o side-scroll main-content-boxed side-trans-enabled page-header-fixed ">

        <?php echo $this->load->view('admin/v_sidemenu.php');?>
        <!-- Header -->
        <?php echo $this->load->view('admin/header.php');?>
        <!-- END Header -->

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Assignor List</h3>
                                <div class="block-options">
                                    <button class="btn btn-primary" id="btn-add-new"><span class="fa fa-plus"></span>
                                        Add New</button>
                                </div>
                            </div>
                            <div class="block-content block-content-full">
                                <table id="mytable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 120px;text-align: left;">No</th>
                                            <th>Img</th>
                                            <th>Nama</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            <th style="text-align:center;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $no=0;
                                            foreach ($data->result() as $row) :
                                            $no++;
                                        ?>
                                        <tr>
                                            <td><?php echo $no;?></td>
                                            <td><img class="img"
                                                    src="<?php echo base_url().'assets/images/assignor/'.$row->assignor_gambar;?>"
                                                    width="45px" alt=""></td>
                                            <td><?php echo $row->assignor_nama;?></td>
                                            <td><?php echo $row->assignor_email; ?></td>
                                            <td>Active (Aktif)</td>
                                            <td style="width: 90px;text-align: center;">
                                                <a href="javascript:void(0);"
                                                    class="btn btn-sm btn-secondary btn-circle btn-edit"
                                                    data-id="<?php echo $row->assignor_id;?>"
                                                    data-nama="<?php echo $row->assignor_nama;?>"
                                                    data-email="<?php echo $row->assignor_email;?>"><span
                                                        class="fa fa-pencil"></span></a>
                                                <a href="javascript:void(0);"
                                                    class="btn btn-sm btn-secondary btn-circle btn-hapus"
                                                    data-id="<?php echo $row->assignor_id;?>"><span
                                                        class="fa fa-trash"></span></a>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
        <!-- Footer -->
        <?php echo $this->load->view('admin/v_footer');?>
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <!-- Modal Hapus -->
    <form action="<?php echo base_url().'admin/assignor/simpan_assignor'?>" method="post" enctype="multipart/form-data">
        <div class="modal" id="ModalAddNew" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Add New</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="file" name="filefoto" class="dropify" data-height="140" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="xnama" class="form-control" placeholder="Nama" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="xemail" class="form-control" placeholder="Email" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-square">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END Normal Modal -->

    <!-- Modal Hapus -->
    <form action="<?php echo base_url().'admin/assignor/update_assignor'?>" method="post" enctype="multipart/form-data">
        <div class="modal" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Update assignor</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="form-group">
                                <input type="file" name="filefoto2" class="dropify" data-height="140">
                            </div>
                            <div class="form-group">
                                <input type="text" name="xnama" class="form-control" placeholder="Nama" required>
                            </div>
                            <div class="form-group">
                                <input type="email" name="xemail" class="form-control" placeholder="Email" required>
                            </div>
                            <input type="hidden" name="xkode" id="kode" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-square">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END Normal Modal -->

    <!-- Modal Hapus -->
    <form action="<?php echo base_url().'admin/assignor/hapus_assignor'?>" method="post">
        <div class="modal" id="Modalhapus" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Info</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <p>Anda yakin mau menghapus assignor ini?</p>
                            <input type="hidden" name="kode" id="kode" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-primary btn-square">Ya</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END Normal Modal -->


    <!-- Codebase Core JS -->
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });

        //Show Modal Update assignor
        $('.btn-edit').on('click', function() {
            var assignor_id = $(this).data('id');
            var assignor_nama = $(this).data('nama');
            var assignor_email = $(this).data('email');
            $('#ModalUpdate').modal('show');
            $('[name="xkode"]').val(assignor_id);
            $('[name="xnama"]').val(assignor_nama);
            $('[name="xemail"]').val(assignor_email);
        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var assignor_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(assignor_id);
        });

    });
    </script>

</body>

</html>