<!doctype html>
<html lang="en" class="no-focus">
<!--<![endif]-->

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>MYTA</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

    <!-- END Icons -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>" />
        <style>
    .cover-spin {
        position:fixed;
        width:100%;
        left:0;right:0;top:0;bottom:0;
        <?php if($dark === 'dark'): ?>
            background-color: rgba(0,0,0,0.8);
        <?php else:?>
            background-color: rgba(255,255,255,0.8);
        <?php endif;?>
        z-index:9999;
        display:none;
    }
    @-webkit-keyframes spin {
        from {-webkit-transform:rotate(0deg);}
        to {-webkit-transform:rotate(360deg);}
    }

    @keyframes spin {
        from {transform:rotate(0deg);}
        to {transform:rotate(360deg);}
    }
    .cover-spin::after {
        content:'';
        display:block;
        position:absolute;
        left:50%;top:40%;
        width:40px;height:40px;
        border-style:solid;
        <?php if($dark === 'dark'): ?>
            border-color:white;
        <?php else:?>
            border-color:black;
        <?php endif;?>
        border-top-color:transparent;
        border-width: 4px;
        border-radius:50%;
        -webkit-animation: spin .8s linear infinite;
        animation: spin .8s linear infinite;
    }
    .cover-spin p{
        margin: auto;
        width: 100%;
        top: 48%;
        left:49.5%;
        position: absolute;
        font-size: 16px;
        color: #ccc;
    }
    </style>



</head>

<body>
    <!-- Page Container -->
    <div id="cover-spin" class="cover-spin"></div>

    <div id="page-container"
    class="sidebar-o side-scroll main-content-boxed side-trans-enabled page-header-fixed page-header-glass <?php if($dark === 'dark'): ?>page-header-inverse sidebar-inverse <?php endif; ?>">

        <?php echo $this->load->view('admin/v_sidemenu.php');?>


        <!-- Header -->
        <?php echo $this->load->view('admin/header.php');?>
        <!-- END Header -->
        <?php if($dark === 'dark'){
                $slide = 'https://ifabula.com/images/slider1.jpg';
                $fontColor = 'text-white';
            }else{
                $slide = base_url().'theme/images/slide2.jpg';
                $fontColor = 'text-black';
            }?>
        <!-- Main Container -->
        <main id="main-container" class="gutters-tiny invisible" data-toggle="appear">
            <!-- Page Content -->
            <div id="banner" class="bg-image"
                style="margin-top:-100:100;background-color:#f1edec;background-size:100%;background-position:cover;background-repeat:no-repeat;background-image: url(<?php echo $slide; ?>);filter:opacity(100%)">
                <div class="content content-top text-left overflow-hidden"><br>
                    <div class="pt-30 pb-20">
                        <h2 class="font-w700 <?php echo $fontColor; ?> mb-5 invisible" data-toggle="appear"
                            data-class="animated fadeInDown">Hello, <?php echo $this->session->userdata('nama');?>
                        </h2>
                        <h3 class="h4 font-w300 <?php echo $fontColor; ?>-op invisible" data-toggle="appear"
                            data-class="animated fadeInDown">Welcome to Management desk, do the best for today <span class="si si-emoticon-smile"></span></h3>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="js-filter" data-speed="400">
                    <div class="p-10 bg-white push">
                        <form action="<?php echo base_url().'admin/dashboard/index_core2';?>" method="GET">
                        <ul class="nav nav-pills" id="listPM">
                            <li class="nav-item">
                            <?php $active = '';
                            if($idSelectedPM == ''){
                                $active = 'active';
                            }else{
                                $active = '';
                            } ?>
                                <a class="nav-link <?php echo $active; ?>" href="<?php echo base_url().'admin/dashboard';?>">
                                    <i class="fa fa-fw fa-folder-open-o mr-5"></i> All
                                </a>
                            </li>
                        </ul>
                        </form>
                    </div>
                    <div class="row">
                        <?php if($listProject->num_rows() > 0): ?>
                        <?php foreach($listProject->result() as $project): ?>
                        <div class="col-md-6 col-xl-3">
                            <a href="<?php echo base_url().'admin/dashboard/project/'.$project->kategori_slug;?>" class="block block-link-pop text-center projectItem">
                                <div class="block-content block-content-full block-sticky-options pt-30 bg-lighter ribbon ribbon-top ribbon-bookmark ribbon-success">
                                    <div class="ribbon-box">
                                        <?php echo $project->kategori_status; ?>
                                    </div>
                                    <img width="40%" style="border-radius:30px" src="<?php echo base_url().'assets/images/business/'.$project->kategori_gambar;?>">
                                </div>
                                <div class="block-content block-content-full block-content-sm <?php if($dark === 'dark'):?> bg-primary-dark <?php else: ?> bg-corporate<?php endif;?>">
                                    <div class="font-w600 mb-5 text-white"><?php echo $project->kategori_nama;?></div>
                                    <div class="font-size-sm text-white"><span class="si si-control-play"></span> <?php echo date("l, d-m-y", strtotime($project->kategori_tanggal));?> <hr> <span class="si si-user"></span> <?php echo $project->pengguna_nama; ?></div>
                                </div>
                                <div class="block-content bg-info text-white">
                                    <div class="row items-push">
                                        <div class="col-4">
                                            <?php $sprint = $this->db->query("SELECT * FROM sprint where sprint_kategori_id='$project->kategori_id'")->num_rows();?>
                                            <div class="mb-5"><i class="si si-notebook fa-2x"></i></div>
                                            <div class="font-size-sm text-white"><span data-toggle="countTo" data-to="<?php echo $sprint; ?>" data-speed="2000"><?php echo $sprint; ?> </span> Sprint</div>
                                        </div>
                                        <div class="col-4">
                                            <?php $task = $this->db->query("SELECT * FROM work where work_kategori_id='$project->kategori_id'")->num_rows();?>
                                            <div class="mb-5"><i class="si si-layers fa-2x"></i></div>
                                            <div class="font-size-sm text-white"><span data-toggle="countTo" data-to="<?php echo $task; ?>" data-speed="2000"> <?php echo $task; ?></span> Task</div>
                                        </div>
                                        <div class="col-4">
                                            <?php $tim = $this->db->query("SELECT * FROM tim where tim_kategori_id='$project->kategori_id'")->num_rows();?>
                                            <div class="mb-5"><i class="si si-users fa-2x"></i></div>
                                            <div class="font-size-sm text-white"><span data-toggle="countTo" data-to="<?php echo $tim; ?>" data-speed="2000"> <?php echo $tim; ?></span> Tim</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php endforeach; ?>
                        <?php else: ?>
                        <div class="col-md-12">
                            <div class="content content-full">
                                <div class="py-30 text-center">
                                    <div class="display-3 text-corporate">
                                        <i class="si si-ban"></i>
                                    </div>
                                    <h1 class="h2 font-w700 mt-30 mb-10">Oops.. There's no Project Available..</h1>
                                    <h2 class="h3 font-w400 text-muted mb-50">We are sorry but this Project Manager don't set up any project yet..</h2>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
        <!-- Footer -->
        <?php echo $this->load->view('admin/v_footer');?>
        <!-- END Footer -->
    </div>


    <!-- Codebase Core JS -->
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
     <script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
     <script>jQuery(function(){ Codebase.helpers('content-filter'); });</script>
    <script type="text/javascript">
    
    $(document).ready(function() {
        $('.projectItem').click(function(){
            $('#cover-spin').show();
        });
    //    setTimeout(() => {
    //     $('#banner').slideUp();
    //    }, 5000);
       $.ajax({
            url: "<?php echo base_url().'admin/dashboard/get_pm_list';?>",
            type: "POST",
            beforeSend: function() {
                $('#cover-spin').show();
            },
            success: function(hsl) {
                $('#cover-spin').hide();
                let hasilPM = JSON.parse(hsl);
                console.log('hasil PM : ', hasilPM);
                for(let i=0;i<hasilPM.length;i++){
                    let namaPM = hasilPM[i].pm_nama;
                    let idPM = hasilPM[i].pm_id;
                    let emailPM = hasilPM[i].pm_email;
                    let gambarPM = hasilPM[i].pm_gambar;

                    let active = '';
                    let selectedPM = '<?php echo $idSelectedPM;?>';
                    if(idPM == selectedPM){
                        active = 'active';
                    }else{
                        active = '';
                    }
                    $('#listPM').append('<li class="nav-item"><a class="nav-link '+active+'" href="http://myta.bacotlu.com/admin/dashboard/index_core2?pm='+emailPM+'" data-category-link="'+namaPM+'"> <img style="border-radius:30px" width="18px" src="http://myta.bacotlu.com/assets/images/'+gambarPM+'"> '+namaPM+'</a> </li>');
                }
            }
        });
        
       
    });
   

    
    </script>

</body>

</html>