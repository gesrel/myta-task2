<!doctype html>
<html lang="en" class="no-focus">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Todo List | bacotLu assignment</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

    <!-- END Icons -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>" />




</head>

<body>
    <!-- Page Container -->

    <div id="page-container"
        class="main-content-boxed page-header-fixed <?php if($dark === 'dark'): ?>page-header-inverse sidebar-inverse <?php endif; ?>">




        <!-- Header -->
        <!-- END Header -->

        <!-- Main Container -->
        <main id="main-container">
          <div class="content">
            <div class="block block-rounded">
                <div class="block-content bg-pattern" style="background-image: url('assets/media/various/bg-pattern-inverse.png');">
                    <div class="py-20 text-center">
                        <h1 class="h3 mb-5">Personal To Do List</h1>
                        <p class="mb-10 text-muted">
                            <em>Welcome <?php echo $this->session->userdata('nama');?> to Personal To Do List</em>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 col-xl-3">
                    <button type="button" class="btn btn-block btn-primary d-md-none mb-10" data-toggle="class-toggle" data-target=".js-tasks-nav" data-class="d-none d-md-block">Menu</button>
                    <div class="js-tasks-nav d-none d-md-block">
                        <div class="block block-rounded">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Tasks</h3>
                            </div>
                            <div class="block-content">
                                <ul class="list-group push">
                                    <li class="list-group-item">
                                        <span class="js-task-badge badge badge-primary float-right animated bounceIn"></span>
                                        <i class="fa fa-fw fa-tasks mr-5"></i> Active
                                    </li>
                                    <li class="list-group-item">
                                        <span class="js-task-badge-starred badge badge-warning float-right animated bounceIn"></span>
                                        <i class="fa fa-fw fa-star mr-5"></i> Starred
                                    </li>
                                    <li class="list-group-item">
                                        <span class="js-task-badge-completed badge badge-success float-right animated bounceIn"></span>
                                        <i class="fa fa-fw fa-check mr-5"></i> Completed
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="block block-rounded">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">List Ongoing Task</h3>
                                <div class="block-options">
                                    <div class="dropdown ">
                                        <button type="button" class="btn-block-option" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="si si-settings"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="javascript:void(0)">
                                                <i class="fa fa-fw fa-eye mr-5"></i>Make Private
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="javascript:void(0)">
                                                <i class="fa fa-fw fa-pencil mr-5"></i>Edit People
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block-content">
                                <ul class="nav-users push">
                                    <?php foreach($work->result() as $wrk): ?>
                                    <li>
                                        <a data-toggle="modal" data-target="#ModalDetail<?php echo $wrk->work_id;?>" href="#!">
                                            <?php $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$wrk->work_kategori_id'")->row_array(); ?>
                                            <img class="img-avatar" src="<?php echo base_url().'assets/images/business/'.$kategori['kategori_gambar'];?>" alt="">
                                            <i class="fa fa-circle text-danger"></i> <?php echo $wrk->work_nama; ?>
                                             <?php $user = $this->db->query("SELECT * FROM pengguna where pengguna_id='$wrk->work_user_id'")->row_array(); ?>
                                            <div class="font-w400 font-size-xs text-muted"><i class="fa fa-location-arrow"></i> <?php echo $user['pengguna_nama'];?></div>
                                        </a>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-xl-9">
                    <div class="js-tasks">
                        <form id="js-task-form" action="be_pages_generic_todo.php" method="post">
                            <div class="input-group input-group-lg">
                                <input class="form-control" type="text" id="js-task-input" name="js-task-input" placeholder="Add a task and press enter..">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                    </span>
                                </div>
                            </div>
                        </form>
                        <h2 class="content-heading mb-10">Active</h2>
                        <div class="js-task-list">
                            <?php foreach($work->result() as $wrks): ?>
                             <div class="js-task block block-rounded mb-5 animated fadeIn" data-task-id="<?php echo $wrks->work_id; ?>" data-task-completed="false" data-task-starred="false">
                                <table class="table table-borderless table-vcenter mb-0">
                                    <tr>
                                        <td class="text-center" style="width: 50px;">
                                            <label class="js-task-status css-control css-control-primary css-checkbox py-0">
                                                <input type="checkbox" class="css-control-input">
                                                <span class="css-control-indicator"></span>
                                            </label>
                                        </td>
                                        <td class="js-task-content font-w600">
                                            <?php echo $wrks->work_nama; ?><br>
                                             <div class="js-task-content font-w300">
                                                <small><?php echo $wrks->work_deskripsi; ?></small>
                                            </div>
                                        </td>
                                       
                                        <td class="text-right" style="width: 100px;">
                                            <button class="js-task-star btn btn-sm btn-alt-warning" type="button">
                                                <i class="fa fa-star-o"></i>
                                            </button>
                                            <button class="js-task-remove btn btn-sm btn-alt-danger" type="button">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <?php endforeach; ?>
                        
                        </div>
                        <h2 class="content-heading mb-10">Starred</h2>
                        <div class="js-task-list-starred">
                            
                            
                        </div>
                        <h2 class="content-heading mb-10">Completed</h2>
                        <div class="js-task-list-completed">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
                   
        </main>
        <!-- END Main Container -->
        
    <?php foreach($work->result() as $wrkD): ?>
    <div class="modal fade in" id="ModalDetail<?php echo $wrkD->work_id;?>" tabindex="-1" role="dialog"
        aria-labelledby="modal-normal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Detail Assignment</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-4">
                                <?php 
                                $busD = $this->db->query("SELECT * FROM kategori where kategori_id='$wrkD->work_kategori_id'");
                                foreach($busD->result() as $busDe): ?>
                                <img src="<?php echo base_url().'assets/images/business/'.$busDe->kategori_gambar;?>"
                                    width="100%" alt=""><br>
                                <small><?php echo $busDe->kategori_slug; ?></small>
                                <?php endforeach; ?>
                            </div>
                            <div class="col-8">
                                <h3><?php echo $wrkD->work_nama; ?></h3>
                                <?php 
                                $assignD2 = $this->db->query("SELECT * FROM pengguna where pengguna_id='$wrkD->work_assignor_id'");
                                foreach($assignD2->result() as $assigD2): ?>
                                Assign : <?php echo $assigD2->pengguna_nama; ?>
                                <?php endforeach; ?>
                                <br>
                                Deadline :
                                <b><?php echo date('l, d M Y H:i', strtotime($wrkD->work_deadline));?></b></br>
                                Device :<?php if($wrkD->work_device === 'ANDROIOS'): ?>
                                <img src="<?php echo base_url().'theme/images/ios.png';?>" width="40px" alt="">
                                <img src="<?php echo base_url().'theme/images/android.png';?>" width="30px" alt="">
                                <?php elseif($wrkD->work_device === 'IOS'): ?>
                                <img src="<?php echo base_url().'theme/images/ios.png';?>" width="40px" alt="">
                                <?php elseif($wrkD->work_device === 'ANDROID'): ?>
                                <img src="<?php echo base_url().'theme/images/android.png';?>" width="30px" alt="">
                                <?php else: ?>
                                <?php endif; ?>
                            </div>
                        </div><br>
                        <div style="width:100%"><?php echo $wrkD->work_deskripsi;?></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
    </div>
    <!-- END Page Container -->

    <!-- Codebase Core JS -->
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
     <script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/pages/be_pages_generic_todo.js';?>"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });

        //Show Modal Update Kategori
        $('.btn-edit').on('click', function() {
            var kategori_id = $(this).data('id');
            var kategori_nama = $(this).data('kategori');
            var kategori_deskripsi = $(this).data('deskripsi');
            $('#ModalUpdate').modal('show');
            $('[name="xkode"]').val(kategori_id);
            $('[name="xkategori2"]').val(kategori_nama);
            $('[name="xdeskripsi"]').val(kategori_deskripsi);
        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var kategori_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(kategori_id);
        });

    });
    </script>
    
    <script type="text/javascript">
    $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
    $(function() {
        CKEDITOR.replace('ckeditor', {
            height: '240px',
            toolbar: [{
                    name: 'clipboard',
                    groups: ['clipboard', 'undo'],
                    items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo',
                        'Redo'
                    ]
                },
                {
                    name: 'editing',
                    groups: ['find', 'selection', 'spellchecker'],
                    items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']
                },
                '/',
                {
                    name: 'basicstyles',
                    groups: ['basicstyles', 'cleanup'],
                    items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript',
                        '-', 'CopyFormatting', 'RemoveFormat'
                    ]
                },
                {
                    name: 'paragraph',
                    groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                    items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-',
                        'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter',
                        'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language'
                    ]
                },
                {
                    name: 'links',
                    items: ['Link', 'Unlink', 'Anchor']
                },
                {
                    name: 'insert',
                    items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar',
                        'PageBreak', 'Iframe'
                    ]
                },
                '/',
                {
                    name: 'styles',
                    items: ['Styles', 'Format', 'Font', 'FontSize']
                },
                {
                    name: 'colors',
                    items: ['TextColor', 'BGColor']
                },
            ]
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });



        $('.btn-detail').on('click', function() {
            var work_deskripsi = $(this).data('deskripsi');
            $('#ModalDetail').modal('show');
            $('[name="deskripsi"]').val(work_deskripsi);
        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var work_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(work_id);
        });

    });
    </script>
  
    <?php if($this->session->flashdata('msg')=='gagal-hapus'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Error',
        text: "There is still assignment attached in that project",
        showHideTransition: 'slide',
        icon: 'error',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#FF4859'
    });
    </script>
    <?php elseif($this->session->flashdata('msg')=='success-hapus'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Success',
        text: "Pengguna Berhasil dihapus.",
        showHideTransition: 'slide',
        icon: 'success',
        hideAfter: false,
        position: 'bottom-right',
        bgColor: '#7EC857'
    });
    </script>
    <?php elseif($this->session->flashdata('msg')=='show-modal'):?>
    <script type="text/javascript">
    $('#ModalResetPassword').modal('show');
    </script>
    <?php else:?>

    <?php endif;?>

</body>

</html>