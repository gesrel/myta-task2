<!doctype html>
<html lang="en" class="no-focus">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Sprint | Myta My Task</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

    <!-- END Icons -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>" />
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/flatpickr/flatpickr.min.css';?>">

</head>

<body>
    <!-- Page Container -->

    <div id="page-container"
        class="sidebar-o side-scroll main-content-boxed side-trans-enabled page-header-fixed <?php if($dark === 'dark'): ?>page-header-inverse sidebar-inverse <?php endif; ?>">

        <?php echo $this->load->view('admin/v_sidemenu.php');?>
        <!-- Header -->
        <?php echo $this->load->view('admin/header.php');?>
        <!-- END Header -->

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <div class="row">
                    <div class="col-md-8">
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title"><?php echo $project_nama; ?> Sprint</h3>
                                <div class="block-options">
                                    <button class="btn btn-primary" id="btn-add-new"><span class="fa fa-plus"></span>
                                        Add New</button>
                                </div>
                            </div>
                            <div class="block-content block-content-full">
                                <table id="mytable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 120px;text-align: left;">No</th>
                                            <th style="width: 220px;">Sprint</th>
                                            <th>Period</th>
                                            <th style="text-align:center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $no=0;
                                            foreach ($data->result() as $row) :
                                            $no++;
                                        ?>
                                        <tr>
                                            <td><?php echo $no;?></td>
                                            <td><?php echo $row->sprint_nama;?></td>
                                            <td>
                                                <span class="badge badge-primary">
                                                    <?php echo date("l, d M Y", strtotime($row->sprint_start)); ?>
                                                </span>
                                                <span class="badge badge-info">
                                                    <?php echo date("l, d M Y", strtotime($row->sprint_end)); ?>
                                                </span>
                                            </td>
                                            <td style="width: 90px;text-align: center;">
                                                <a href="javascript:void(0);"
                                                    class="btn btn-sm btn-secondary btn-circle btn-edit"
                                                    data-id="<?php echo $row->sprint_id;?>"
                                                    data-nama="<?php echo $row->sprint_nama;?>"
                                                    data-start="<?php echo $row->sprint_start;?>"
                                                    data-end="<?php echo $row->sprint_end;?>"><span
                                                        class="fa fa-pencil"></span></a>
                                                <a href="javascript:void(0);"
                                                    class="btn btn-sm btn-secondary btn-circle btn-hapus"
                                                    data-id="<?php echo $row->sprint_id;?>"><span
                                                        class="fa fa-trash"></span></a>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
        <!-- Footer -->
        <?php echo $this->load->view('admin/v_footer');?>
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <!-- Modal Hapus -->
    <form action="<?php echo base_url().'admin/sprint/simpan_sprint'?>" method="post" enctype="multipart/form-data">
        <div class="modal" id="ModalAddNew" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Add New</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row">
                                <div class="col-md-12">
                                <div class="form-group">
                                        <input type="text" name="xnama" class="form-control" placeholder="Sprint Title" required>
                                        <input type="hidden" name="xslug" value="<?php echo $this->uri->segment(4);?>" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Sprint Start</label>
                                        <input type="date" name="xstart" class="form-control" placeholder="Nama" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Sprint End</label>
                                        <input type="date" name="xend" class="form-control" placeholder="Nama" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-square">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END Normal Modal -->

    <!-- Modal Hapus -->
    <form action="<?php echo base_url().'admin/sprint/update_sprint'?>" method="post" enctype="multipart/form-data">
        <div class="modal" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Update sprint</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row">
                                <div class="col-md-12">
                                <div class="form-group">
                                        <input type="text" name="xnama" class="form-control" placeholder="Sprint Title" required>
                                        <input type="hidden" name="xkode" id="kode" required>
                                        <input type="hidden" name="xslug" value="<?php echo $this->uri->segment(4);?>" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Sprint Start</label>
                                        <input type="text" name="xstart" class="form-control" placeholder="Nama" readonly required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Sprint End</label>
                                        <input type="text" name="xend" class="form-control" placeholder="Nama" readonly required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">New Start</label>
                                        <input type="date" name="xstartnew" class="form-control" placeholder="Nama">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">New End</label>
                                        <input type="date" name="xendnew" class="form-control" placeholder="Nama">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-square">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END Normal Modal -->

    <!-- Modal Hapus -->
    <form action="<?php echo base_url().'admin/sprint/hapus_sprint'?>" method="post">
        <div class="modal" id="Modalhapus" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Info</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <p>Anda yakin mau menghapus sprint ini?</p>
                            <input type="hidden" name="kode" id="kode" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-primary btn-square">Ya</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END Normal Modal -->


    <!-- Codebase Core JS -->
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/flatpickr/flatpickr.min.js';?>"></script>
    <script>jQuery(function(){ Codebase.helpers(['flatpickr', 'datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']); });</script>

    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });

        //Show Modal Update sprint
        $('.btn-edit').on('click', function() {
            var sprint_id = $(this).data('id');
            var sprint_nama = $(this).data('nama');
            var sprint_start = $(this).data('start');
            var sprint_end = $(this).data('end');
            $('#ModalUpdate').modal('show');
            $('[name="xkode"]').val(sprint_id);
            $('[name="xnama"]').val(sprint_nama);
            $('[name="xstart"]').val(sprint_start);
            $('[name="xend"]').val(sprint_end);

        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var sprint_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(sprint_id);
        });

    });
    </script>
    <?php if($this->session->flashdata('msg')=='date'):?>
    <script type="text/javascript">
        $.toast({
            heading: 'Error',
            text: "Starting date cannot be above End date",
            showHideTransition: 'slide',
            icon: 'error',
            hideAfter: 5000,
            position: 'bottom-right',
            bgColor: '#FF4859'
        });
    </script>
    <?php elseif($this->session->flashdata('msg')=='invalid-date'):?>
    <script type="text/javascript">
        $.toast({
            heading: 'Error',
            text: "Invalid Date! Sprint date is already taken",
            showHideTransition: 'slide',
            icon: 'error',
            hideAfter: 5000,
            position: 'bottom-right',
            bgColor: '#FF4859'
        });
    </script>
    <?php endif; ?>

</body>

</html>