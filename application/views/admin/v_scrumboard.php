<!doctype html>
<html lang="en" class="no-focus" >
<!--<![endif]-->

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=2.0, user-scalable=0">

    <title>Scrum Board | MYTA</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

    <!-- END Icons -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/scrumboard.css'?>">
    <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/js/plugins/select2/select2.css'?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>" />
        <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/magnific-popup/magnific-popup.css'?>" />

    <style>
   .cover-spin {
        position:fixed;
        width:100%;
        left:0;right:0;top:0;bottom:0;
        <?php if($dark === 'dark'): ?>
            background-color: rgba(0,0,0,0.8);
        <?php else:?>
            background-color: rgba(255,255,255,0.8);
        <?php endif;?>
        z-index:9999;
        display:none;
    }
    @-webkit-keyframes spin {
        from {-webkit-transform:rotate(0deg);}
        to {-webkit-transform:rotate(360deg);}
    }

    @keyframes spin {
        from {transform:rotate(0deg);}
        to {transform:rotate(360deg);}
    }
    .cover-spin::after {
        content:'';
        display:block;
        position:absolute;
        left:50%;top:40%;
        width:40px;height:40px;
        border-style:solid;
        <?php if($dark === 'dark'): ?>
            border-color:white;
        <?php else:?>
            border-color:black;
        <?php endif;?>
        border-top-color:transparent;
        border-width: 4px;
        border-radius:50%;
        -webkit-animation: spin .8s linear infinite;
        animation: spin .8s linear infinite;
    }
    .cover-spin p{
        margin: auto;
        width: 100%;
        top: 48%;
        left:49.5%;
        position: absolute;
        font-size: 16px;
        color: #ccc;
    }
    </style>
<script type="text/JavaScript">
    function AutoRefresh( t ) {
        setTimeout("location.reload(true);", t);
    }
</script>
</head>


<body onload="JavaScript:AutoRefresh(300000);">
<div id="cover-spin"></div>


    <!-- Page Container --> 

    <div id="page-container"
        class="sidebar-o side-scroll side-trans-enabled page-header-fixed <?php if($dark === 'dark'): ?>page-header-inverse sidebar-inverse <?php endif; ?>">
        <?php echo $this->load->view('admin/v_sidemenu.php');?>


        <!-- Header -->
        <?php echo $this->load->view('admin/header.php');?>
        <!-- END Header -->
        <main>
        <div id="ModalDetail" style="margin-top:8%;width:100%">
            <div class="js-chat-container row no-gutters content content-full" style="margin-top:-30px">
                <div class="js-chat-options d-none d-md-block col-md-6 col-lg-4 bg-white border-right">
                    <div class="js-chat-logged-user m-15">
                        <div class="d-flex align-items-center">
                            <a class="img-link img-status" href="javascript:void(0)">
                                <img class="img-avatar img-avatar" style="width:50px;height:50px" id="kategori_gambar1" alt="Avatar">
                                <div class="img-status-indicator bg-success"></div>
                            </a>
                            <div class="ml-10">
                                <a class="font-w600" href="javascript:void(0)" id="nama_task"></a>
                                <div class="font-size-sm text-muted" id="namaSprint"></div>
                            </div>
                        </div>
                    </div>
                    <div class="block block-transparent mb-0">
                        <ul class="js-chat-tabs nav nav-tabs nav-tabs-alt nav-justified px-15" data-toggle="tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#chat-tabs-chats">
                                    <i class="si si-user text-muted font-size-lg"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#chat-tabs-timeline">
                                    <i class="si si-clock text-muted font-size-lg"></i>
                                </a>
                            </li>
                        </ul>
                        <div class="js-chat-tabs-content block-content tab-content p-0">
                            <div style="overflow:auto;height:350px;width:100%" class="tab-pane active p-15" id="chat-tabs-chats" role="tabpanel" data-simplebar>
                                <div class="push">
                                    <div class="d-flex align-items-center">
                                        <a class="img-link img-status" href="javascript:void(0)">
                                            <img class="img-avatar img-avatar" style="width:50px;height:50px    " id="work_assignor_gambar" alt="Avatar">
                                            <div class="img-status-indicator bg-success"></div>
                                        </a>
                                        <div class="ml-10">
                                            <a class="font-w600" href="javascript:void(0)" id="work_assignor"></a>
                                            <div class="font-size-sm text-muted">PIC / DIC</div>
                                        </div>
                                    </div><br>
                                    <div class="d-flex align-items-center justify-content-between mb-5">
                                        <span class="font-w600 font-size-xs text-muted text-uppercase">Description</span>
                                    
                                    </div>
                                    <div id="deskripsi_task"></div>
                                </div>
                            </div>
                            <div style="overflow:auto;height:350px;width:600px" class="tab-pane p-15" id="chat-tabs-timeline" role="tabpanel" data-simplebar>
                            </div>
                        </div>
                    </div>
                    <div class="d-md-none py-5 bg-body-dark"></div>
                </div>
                <div class="col-md-6 col-lg-8 bg-white d-flex flex-column">
                    <div class="js-chat-active-user p-15 d-flex align-items-center justify-content-between bg-white">
                        <div class="d-flex align-items-center">
                            
                        </div>
                        <div class="ml-10">
                            
                            <a href="javascript:void(0)" id="btnCancelModalDetail" data-toggle="dropdown">
                                <i class="fa fa-remove"></i>
                            </a>
                            <button type="button" class="d-md-none btn btn-sm btn-circle btn-alt-success ml-5" data-toggle="class-toggle" data-target=".js-chat-options" data-class="d-none">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>
                    </div>
                    <div id="scrollDiv" class="js-chat-window p-15 bg-light flex-grow-1 text-wrap-break-word overflow-y-auto" style="overflow:auto;height:450px">
                        <div class="d-flex mb-20">
                            <div>
                                <a class="img-link img-status" href="javascript:void(0)">
                                    <img class="img-avatar img-avatar" style="width:40px;height:30px"  src="<?php echo base_url().'theme/images/favicon.png';?>" alt="Avatar">
                                    <div class="img-status-indicator bg-success"></div>
                                </a>
                            </div>
                            <div class="mx-10">
                                <div>
                                    <p class="bg-body-dark text-dark rounded px-15 py-10 mb-5">
                                    <label for="">Myta</label><br>
                                        Hello there! if you had couple question about this task, ask your team mates
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div id="commentContainer" class="items-push js-gallery img-fluid-100"></div>
                        <div class="d-flex flex-row-reverse mb-20 " >
                            <div class="onlineContainer">
                                <a class="img-link img-status" href="javascript:void(0)">
                                    <img id="onlineUserGambar" class="img-avatar img-avatar32" alt="Avatar">
                                </a>
                            </div>
                            <div class="mx-10 text-right onlineContainer">
                                <div>
                                    <p id="onlineUserNama" class="bg-primary-lighter text-primary-darker rounded px-15 py-10 mb-5 d-inline-block"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="js-chat-message p-10 mt-auto">
                        <form id="formComment" action="<?php echo base_url().'admin/comment/tambah_comment';?>" method="post" enctype="multipart/form-data">
                            <div style="margin-bottom:10px;margin-top:10px;width:100%" class="conEmot">
                                <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="smile.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/smile.gif'?>"></button>
                                <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="laugh.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/laugh.gif'?>"></button>
                                <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="cry.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/cry.gif'?>"></button>
                                <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="tears.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/tears.gif'?>"></button>
                                <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="shock.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/shock.gif'?>"></button>
                                <!--<button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="tang.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/tang.gif'?>"></button>-->
                                <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="think.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/think.gif'?>"></button>
                                <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="cool.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/cool.gif'?>"></button>
                                <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="love.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/love.gif'?>"></button>
                                <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="sweat.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/sweat.gif'?>"></button>
                                <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="ok.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/ok.gif'?>"></button>
                                <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="clap.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/clap.gif'?>"></button>
                            </div>
                            <div class="row">
                                <div class="col-md-3 attachment">
                                    <input type="file" data-height="70" name="attach1" class="form-control dropify" data-max-file-size-preview="1M">
                                </div>
                                <div class="col-md-3 attachment">
                                    <input name="attach2" data-height="70"  type="file" class="form-control dropify">
                                </div>
                                <div class="col-md-3 attachment">
                                    <input name="attach3" data-height="70" type="file" class="form-control dropify" >
                                </div>
                                <div class="col-md-3 attachment">
                                    <input name="attach4" data-height="70" type="file" class="form-control dropify">
                                </div>
                                <div style="margin-left:20px" class="attachment">
                                    <small>2MB Max Size / Attachment</small>
                                </div>
                                <input type="hidden" id="idForNewComment">
                            </div>
                            <div class="d-flex align-items-center">
                                <a href="javascript:void(0)" onclick="$('.conEmot').fadeIn()" class="btn btn-alt-secondary btn-circle mr-5">
                                    <i class="fa fa-fw fa-smile-o font-size-lg"></i>
                                </a>
                                <a href="javascript:void(0)" id="btnAttach" onclick="$('.attachment').fadeIn()" class="btn btn-alt-secondary btn-circle mr-5">
                                    <i class="fa fa-fw fa-paperclip font-size-lg"></i>
                                </a>
                                <input type="hidden" name="taskId" id="taskId" required>
                                <input type="hidden" name="screen" value="scrumboard" required>
                                <textarea style="height:40px" type="text" class="form-control flex-grow mr-5" placeholder="Type a message.." name="isi" id="xcomment"></textarea>
                                <button type="submit" class="btn btn-circle btn-alt-primary">
                                    <i class="fa fa-share-square"></i>
                                </button>    
                            </div><br>
                        </form>
                    </div>
                </div>
            </div>
        </main>
        <main class="" id="main-container">
            <?php if($task->num_rows() > 0): ?>
          
        <div style="width:100%;<?php if($akses === '4' || $akses === '5'): ?> zoom:84%<?php endif;?>;" id="html" class="scrumboard js-scrumboard otherCon">
                <div class="scrumboard-col block block-themed" style="<?php echo $width;?>;border-radius:20px">
                    <div class="block-header bg-info" style="border-radius:20px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;">
                        <h3 class="block-title font-w600"><span class="si si-cup"></span> In Progress Task</h3>
                    </div>
                    <div id="dropOngoing" class="scrumboard-items block-content" style="<?php echo $heightContainer ?>;overflow:auto;">
                        <div class="itemsWork">
                        </div>
                        <?php if($ongoing->num_rows() === 0): ?>
                        <div id="wordingKosongOpen" style="visible:block;">
                                <center><img src="<?php echo base_url().'theme/images/favicon.png';?>" width="50px" alt="">
                                <p style="color:#ddd;text-align:center;padding:10px">No Task available.</p></center>
                            </div>
                        <?php else: ?>
                        <?php foreach($ongoing->result() as $work): ?>
                        <div class="scrumboard-item itemsWork" style="<?php if($work->work_status === 'F'):?> background-color:#ddd<?php endif; ?>;border-radius:20px" data-id="<?php echo $work->work_id; ?>"
                            data-nama="<?php echo $work->work_nama; ?>">
                            <div class="scrumboard-item-options">
                                <?php if($work->work_status === 'A'): ?>
                                    <?php if($akses === '3'): ?>
                                        <a class="scrumboard-item-handler btn btn-sm btn-alt-warning" href="javascript:void(0)">
                                            <i class="fa fa-hand-grab-o"></i>
                                        </a>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <i class="fa fa-spinner fa-spin" style="color:#000" data-toggle="tooltip" data-placement="top"
                                            title="Waiting Approval"></i>
                                <?php endif; ?>
                            </div>
                            
                            <div class="scrumboard-item-content" style="width:130%">
                                <img style="border-radius:50px" src="<?php echo base_url().'assets/images/business/'.$work->kategori_gambar;?>"
                                    width="30px" alt="">
                                <small><?php echo $work->kategori_nama; ?></small>
                                <br>
                                <a data-id="<?php echo $work->work_id; ?>" href="#!" class="btnDetail"><?php echo $work->work_nama; ?></a>
                                <div class="scrumboard-item-options" style="margin-top:15%;margin-right:8px">
                                    <a class="font-w600" data-toggle="collapse" data-parent="#accordion" href="#accordion_<?php echo $work->work_id; ?>" aria-expanded="true" aria-controls="accordion_q1"><span class="si si-arrow-down pull-right"></span></a>
                                </div>
                                <div id="accordion_<?php echo $work->work_id; ?>" class="collapse" role="tabpanel" aria-labelledby="accordion_h1" data-parent="#accordion">
                                        <?php if($work->work_device === 'ANDROIOS'): ?>
                                            <img src="<?php echo base_url().'theme/images/ios.png';?>" width="20px" alt=""> 
                                            <img src="<?php echo base_url().'theme/images/android.png';?>" width="15px" alt=""> 
                                            <span class="badge badge-secondary">iOS & Android</span>
                                        <?php elseif($work->work_device === 'IOS'): ?>
                                            <img src="<?php echo base_url().'theme/images/ios.png';?>"
                                                width="20px" alt=""> 
                                                <span class="badge badge-secondary">iOS</span>
                                        <?php elseif($work->work_device === 'ANDROID'): ?>
                                            <img src="<?php echo base_url().'theme/images/android.png';?>"
                                                width="15px" alt=""> 
                                                <span class="badge badge-secondary">Android</span>
                                        <?php elseif($work->work_device === 'CMS'): ?>
                                            <img src="<?php echo base_url().'theme/images/cms.png';?>"
                                                width="20px" alt=""><span class="badge badge-secondary">CMS</span>
                                        <?php elseif($work->work_device === 'BACKEND'): ?>
                                            <img src="<?php echo base_url().'theme/images/backend.png';?>"
                                                width="20px" alt=""> <span class="badge badge-secondary">Backend</span>
                                        <?php elseif($work->work_device === 'UI & UX'): ?>
                                            <img src="<?php echo base_url().'theme/images/frontend.png';?>"
                                                width="20px" alt=""> <span class="badge badge-secondary">UI & UX</span>
                                        <?php else: ?>
                                            <img src="https://images.vexels.com/media/users/3/128132/isolated/preview/fa3b9aad78a9db81459bd03294a0f985-flat-laptop-icon-design-by-vexels.png"
                                                width="20px" alt="">
                                        <?php endif; ?>
                                        <br><hr></hr>
                                        <?php echo $pic; ?> 
                                        <?php $linkImg = $work->pengguna_photo;
                                            if($linkImg === ''){
                                                if($work->pengguna_jenkel === 'L'){
                                                    $linkImg = 'user_blank.png';
                                                }else{
                                                    $linkImg = 'user_blank2.png';
                                                }
                                            }
                                        ?>
                                            <small><img width="30px" style="border-radius:50px" src="<?php echo base_url().'assets/images/'.$linkImg; ?>" alt=""><?php echo $work->pengguna_nama; ?></small><br>
                                        Deadline : 
                                            <small><?php echo date("l, d M Y H:i A", strtotime($work->work_deadline)); ?></small>
                                </div>
                               
                            </div>
                        </div>
                        
                        <?php endforeach; ?>
                        
                        <?php endif; ?>
                    </div>

                </div>
                <div class="scrumboard-col block block-themed" style="<?php echo $width;?>;border-radius:20px">
                    <div class="block-header bg-secondary" style="border-radius:20px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;">
                        <h3 class="block-title font-w600"><span class="fa fa-android"></span> <span
                                class="fa fa-apple"></span> Ready Test Deployment</h3>
                        <div class="block-options">

                        </div>
                    </div>
                    <div id="dropAPK" class="scrumboard-items block-content" style="<?php echo $heightContainer ?>;overflow:auto">
                        <div class="itemsWork">

                        </div>
                        <?php if($readytoapk->num_rows() === 0): ?>
                        <div id="wordingKosongAPK" style="visible:block;">
                                <center><img src="<?php echo base_url().'theme/images/favicon.png';?>" width="50px" alt="">
                                <p style="color:#ddd;text-align:center;padding:10px">No Task available.</p></center>
                            </div>
                        <?php else:
                        foreach($readytoapk->result() as $work3): ?>
                        <div class="scrumboard-item itemsWork" style="border-radius:20px;" data-id="<?php echo $work3->work_id; ?>"
                            data-nama="<?php echo $work3->work_nama; ?>">
                            <div class="scrumboard-item-options">

                                <a class="scrumboard-item-handler btn btn-sm btn-alt-warning" href="javascript:void(0)">
                                    <i class="fa fa-hand-grab-o" title="Drag to update"></i>
                                </a>
                            </div>
                            <div class="scrumboard-item-content" style="width:130%">
                                <img style="border-radius:50px" src="<?php echo base_url().'assets/images/business/'.$work3->kategori_gambar;?>"
                                    width="30px" alt="">
                                <small><?php echo $work3->kategori_nama; ?></small>
                                <br>
                                <a data-id="<?php echo $work3->work_id; ?>" href="#!" class="btnDetail"><?php echo $work3->work_nama; ?></a>
                                <div class="scrumboard-item-options" style="margin-top:15%;margin-right:8px">
                                    <a class="font-w600" data-toggle="collapse" data-parent="#accordion" href="#accordion_<?php echo $work3->work_id; ?>" aria-expanded="true" aria-controls="accordion_q1"><span class="si si-arrow-down pull-right"></span></a>
                                </div>
                                <div id="accordion_<?php echo $work3->work_id; ?>" class="collapse" role="tabpanel" aria-labelledby="accordion_h1" data-parent="#accordion">
                                        <?php if($work3->work_device === 'ANDROIOS'): ?>
                                            <img src="<?php echo base_url().'theme/images/ios.png';?>" width="20px" alt=""> 
                                            <img src="<?php echo base_url().'theme/images/android.png';?>" width="15px" alt=""> 
                                            <span class="badge badge-secondary">iOS & Android</span>
                                        <?php elseif($work3->work_device === 'IOS'): ?>
                                            <img src="<?php echo base_url().'theme/images/ios.png';?>"
                                                width="20px" alt=""> 
                                                <span class="badge badge-secondary">iOS</span>
                                        <?php elseif($work3->work_device === 'ANDROID'): ?>
                                            <img src="<?php echo base_url().'theme/images/android.png';?>"
                                                width="15px" alt=""> 
                                                <span class="badge badge-secondary">Android</span>
                                        <?php elseif($work3->work_device === 'CMS'): ?>
                                            <img src="<?php echo base_url().'theme/images/cms.png';?>"
                                                width="20px" alt=""><span class="badge badge-secondary">CMS</span>
                                        <?php elseif($work3->work_device === 'BACKEND'): ?>
                                            <img src="<?php echo base_url().'theme/images/backend.png';?>"
                                                width="20px" alt=""> <span class="badge badge-secondary">Backend</span>
                                        <?php elseif($work3->work_device === 'UI & UX'): ?>
                                            <img src="<?php echo base_url().'theme/images/frontend.png';?>"
                                                width="20px" alt=""> <span class="badge badge-secondary">UI & UX</span>
                                        <?php else: ?>
                                            <img src="https://images.vexels.com/media/users/3/128132/isolated/preview/fa3b9aad78a9db81459bd03294a0f985-flat-laptop-icon-design-by-vexels.png"
                                                width="20px" alt="">
                                        <?php endif; ?>
                                        <br><hr></hr>
                                        <?php echo $pic; ?> 
                                        <?php $linkImg = $work3->pengguna_photo;
                                            if($linkImg === ''){
                                                if($work->pengguna_jenkel === 'L'){
                                                    $linkImg = 'user_blank.png';
                                                }else{
                                                    $linkImg = 'user_blank2.png';
                                                }
                                            }
                                        ?>
                                            <small><img width="30px" style="border-radius:50px" src="<?php echo base_url().'assets/images/'.$linkImg; ?>" alt=""><?php echo $work3->pengguna_nama; ?></small><br>
                                        Deadline : 
                                            <small><?php echo date("l, d M Y H:i A", strtotime($work3->work_deadline)); ?></small>
                                </div>
                            </div>
                        </div>
                        
                        <?php endforeach; ?>
                       
                        <?php endif; ?>
                    </div>
                </div>
                <div class="scrumboard-col block block-themed" style="<?php echo $width;?>;border-radius:20px">
                    <div class="block-header bg-success" style="border-radius:20px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;">
                        <h3 class="block-title font-w600"><span class="si si-screen-smartphone"></span> Ready to Test (Live)
                        </h3>
                    </div>
                    <div id="dropReady" class="scrumboard-items block-content" style="<?php echo $heightContainer ?>;overflow:auto">
                        <div class="itemsWork">

                        </div>
                        <?php 
                        if($readytotest->num_rows() === 0):?>
                            
                            <div id="wordingKosongReady" style="visible:block;">
                                <center><img src="<?php echo base_url().'theme/images/favicon.png';?>" width="50px" alt="">
                                <p style="color:#ddd;text-align:center;padding:10px">No Task available.</p></center>
                            </div>
                        <?php else:
                        foreach($readytotest->result() as $work2): ?>

                        <div class="scrumboard-item itemsWork" style="border-radius:20px" data-id="<?php echo $work2->work_id; ?>"
                            data-nama="<?php echo $work2->work_nama; ?>">
                            <div class="scrumboard-item-options">

                                <a class="scrumboard-item-handler btn btn-sm btn-alt-warning" href="javascript:void(0)">
                                    <i class="fa fa-hand-grab-o" title="Drag to update"></i>
                                </a>
                            </div>
                            <div class="scrumboard-item-content" style="width:130%">
                                <img style="border-radius:50px" src="<?php echo base_url().'assets/images/business/'.$work2->kategori_gambar;?>"
                                    width="30px" alt="">
                                <small><?php echo $work2->kategori_nama; ?></small>
                                <br>
                                <a data-id="<?php echo $work2->work_id; ?>" href="#!" class="btnDetail"><?php echo $work2->work_nama; ?></a>
                                <div class="scrumboard-item-options" style="margin-top:15%;margin-right:8px">
                                    <a class="font-w600" data-toggle="collapse" data-parent="#accordion" href="#accordion_<?php echo $work2->work_id; ?>" aria-expanded="true" aria-controls="accordion_q1"><span class="si si-arrow-down pull-right"></span></a>
                                </div>
                                <div id="accordion_<?php echo $work2->work_id; ?>" class="collapse" role="tabpanel" aria-labelledby="accordion_h1" data-parent="#accordion">
                                        <?php if($work2->work_device === 'ANDROIOS'): ?>
                                            <img src="<?php echo base_url().'theme/images/ios.png';?>" width="20px" alt=""> 
                                            <img src="<?php echo base_url().'theme/images/android.png';?>" width="15px" alt=""> 
                                            <span class="badge badge-secondary">iOS & Android</span>
                                        <?php elseif($work2->work_device === 'IOS'): ?>
                                            <img src="<?php echo base_url().'theme/images/ios.png';?>"
                                                width="20px" alt=""> 
                                                <span class="badge badge-secondary">iOS</span>
                                        <?php elseif($work2->work_device === 'ANDROID'): ?>
                                            <img src="<?php echo base_url().'theme/images/android.png';?>"
                                                width="15px" alt=""> 
                                                <span class="badge badge-secondary">Android</span>
                                        <?php elseif($work2->work_device === 'CMS'): ?>
                                            <img src="<?php echo base_url().'theme/images/cms.png';?>"
                                                width="20px" alt=""><span class="badge badge-secondary">CMS</span>
                                        <?php elseif($work2->work_device === 'BACKEND'): ?>
                                            <img src="<?php echo base_url().'theme/images/backend.png';?>"
                                                width="20px" alt=""> <span class="badge badge-secondary">Backend</span>
                                        <?php elseif($work2->work_device === 'UI & UX'): ?>
                                            <img src="<?php echo base_url().'theme/images/frontend.png';?>"
                                                width="20px" alt=""> <span class="badge badge-secondary">UI & UX</span>
                                        <?php else: ?>
                                            <img src="https://images.vexels.com/media/users/3/128132/isolated/preview/fa3b9aad78a9db81459bd03294a0f985-flat-laptop-icon-design-by-vexels.png"
                                                width="20px" alt="">
                                        <?php endif; ?>
                                        <br><hr></hr>
                                        <?php echo $pic; ?> 
                                        <?php $linkImg = $work2->pengguna_photo;
                                            if($linkImg === ''){
                                                if($work2->pengguna_jenkel === 'L'){
                                                    $linkImg = 'user_blank.png';
                                                }else{
                                                    $linkImg = 'user_blank2.png';
                                                }
                                            }
                                        ?>
                                            <small><img width="30px" style="border-radius:50px" src="<?php echo base_url().'assets/images/'.$linkImg; ?>" alt=""><?php echo $work2->pengguna_nama; ?></small><br>
                                        Deadline : 
                                            <small><?php echo date("l, d M Y H:i A", strtotime($work2->work_deadline)); ?></small>
                                            </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                        
                       
                        <?php endif; ?>
                    </div>
                </div>

                <?php if($akses === '4' || $akses === '5'): ?>

                <div class="scrumboard-col block block-themed" style="<?php echo $width;?>;border-radius:20px">
                    <div class="block-header bg-primary" style="border-radius:20px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;">
                        <h3 class="block-title font-w600"><span class="si si-check"></span> Done / Finish
                        </h3>
                    </div>
                    <div id="dropDone" class="scrumboard-items block-content" style="<?php echo $heightContainer ?>;overflow:auto">
                        <div class="itemsWork">

                        </div>
                        <?php 
                        if($done->num_rows() === 0):?>
                           
                           <div id="wordingKosongDone" style="visible:block;">
                                <center><img src="<?php echo base_url().'theme/images/favicon.png';?>" width="50px" alt="">
                                <p style="color:#ddd;text-align:center;padding:10px">No Task available.</p></center>
                            </div>
                        <?php else:
                        foreach($done->result() as $work4): ?>

                        <div class="scrumboard-item itemsWork" style="border-radius:20px" data-id="<?php echo $work4->work_id; ?>"
                            data-nama="<?php echo $work4->work_nama; ?>">
                            <div class="scrumboard-item-options">

                                <a class="scrumboard-item-handler btn btn-sm btn-alt-warning" href="javascript:void(0)">
                                    <i class="fa fa-hand-grab-o" title="Drag to update"></i>
                                </a>
                            </div>
                            <div class="scrumboard-item-content" style="width:130%">
                                <img style="border-radius:50px" src="<?php echo base_url().'assets/images/business/'.$work4->kategori_gambar;?>"
                                    width="30px" alt="">
                                <small><?php echo $work4->kategori_nama; ?></small>
                                <br>
                                <a data-id="<?php echo $work4->work_id; ?>" href="#!" class="btnDetail"><?php echo $work4->work_nama; ?></a>
                                <div class="scrumboard-item-options" style="margin-top:15%;margin-right:8px">
                                    <a class="font-w600" data-toggle="collapse" data-parent="#accordion" href="#accordion_<?php echo $work4->work_id; ?>" aria-expanded="true" aria-controls="accordion_q1"><span class="si si-arrow-down pull-right"></span></a>
                                </div>
                                <div id="accordion_<?php echo $work4->work_id; ?>" class="collapse" role="tabpanel" aria-labelledby="accordion_h1" data-parent="#accordion">
                                        <?php if($work4->work_device === 'ANDROIOS'): ?>
                                            <img src="<?php echo base_url().'theme/images/ios.png';?>" width="20px" alt=""> 
                                            <img src="<?php echo base_url().'theme/images/android.png';?>" width="15px" alt=""> 
                                            <span class="badge badge-secondary">iOS & Android</span>
                                        <?php elseif($work4->work_device === 'IOS'): ?>
                                            <img src="<?php echo base_url().'theme/images/ios.png';?>"
                                                width="20px" alt=""> 
                                                <span class="badge badge-secondary">iOS</span>
                                        <?php elseif($work4->work_device === 'ANDROID'): ?>
                                            <img src="<?php echo base_url().'theme/images/android.png';?>"
                                                width="15px" alt=""> 
                                                <span class="badge badge-secondary">Android</span>
                                        <?php elseif($work4->work_device === 'CMS'): ?>
                                            <img src="<?php echo base_url().'theme/images/cms.png';?>"
                                                width="20px" alt=""><span class="badge badge-secondary">CMS</span>
                                        <?php elseif($work4->work_device === 'BACKEND'): ?>
                                            <img src="<?php echo base_url().'theme/images/backend.png';?>"
                                                width="20px" alt=""> <span class="badge badge-secondary">Backend</span>
                                        <?php elseif($work4->work_device === 'UI & UX'): ?>
                                            <img src="<?php echo base_url().'theme/images/frontend.png';?>"
                                                width="20px" alt=""> <span class="badge badge-secondary">UI & UX</span>
                                        <?php else: ?>
                                            <img src="https://images.vexels.com/media/users/3/128132/isolated/preview/fa3b9aad78a9db81459bd03294a0f985-flat-laptop-icon-design-by-vexels.png"
                                                width="20px" alt="">
                                        <?php endif; ?>
                                        <br><hr></hr>
                                        <?php echo $pic; ?> 
                                        <?php $linkImg = $work4->pengguna_photo;
                                            if($linkImg === ''){
                                                if($work4->pengguna_jenkel === 'L'){
                                                    $linkImg = 'user_blank.png';
                                                }else{
                                                    $linkImg = 'user_blank2.png';
                                                }
                                            }
                                        ?>
                                            <small><img width="30px" style="border-radius:50px" src="<?php echo base_url().'assets/images/'.$linkImg; ?>" alt=""><?php echo $work4->pengguna_nama; ?></small><br>
                                        Deadline : 
                                            <small><?php echo date("l, d M Y H:i A", strtotime($work4->work_deadline)); ?></small>
                                            </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                        
                       
                        <?php endif; ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <?php else: ?>
            <div class="block-content block-content-full text-center">
                <div class="gutters-tiny invisible" data-toggle="appear" style="margin:100px">
                    <h2 style="color:#ABAAAA;font-weight:lighter">Sorry, You dont have available
                        task. <span style="font-size:15px;font-weight:lighter"> <a href="<?php echo base_url().'admin/dashboard';?>"> go back</a></span>
                    </h2>
                </div>
            </div>
            <?php endif; ?>
        </main>

    </div>

    <!-- END Main Container -->
    <!-- Footer -->
    <!-- <?php echo $this->load->view('admin/v_footer');?> -->
    <!-- END Footer -->
 
    <audio>
        <source src="<?php echo base_url().'assets/notif_sound.mp3' ?>" type="audio/ogg">
    </audio>
    
    <!-- END Page Container -->

    <!-- Codebase Core JS -->
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/pages/be_pages_generic_scrumboard.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/jquery-ui/jquery-ui.min.js';?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/magnific-popup/magnific-popup.min.js'?>"></script>


    <script>
    jQuery(function() {
            Codebase.helpers(['magnific-popup']);
    });
    $('.dropify').dropify({ //overate input type file
        messages: {
            default: '<small>.jpg | .png | .pdf </small>',
            replace: 'Change',
            remove: 'Delete',
            error: 'error'
        }
    });
     $('.conEmot').hide();
    var objDiv = document.getElementById("scrollDiv");
        $('#ModalDetail').hide();

    
    $('#deskripsiTitle').on('click',function(){
            var title = $('#deskripsiTitle').text();
            if(title === 'Show Description'){
                $('#deskripsiTitle').text('Hide Description');
            }else if(title === 'Hide Description'){
                $('#deskripsiTitle').text('Show Description');
            }
        });
        $('#btnCancelModalDetail').on('click', function(){
            $('#ModalDetail').hide();
            $('.otherCon').show();
        });
    setInterval(function(){
            var $audio = $('audio');
            var audio = $audio[0];
            $.ajax({
                url: "<?php echo base_url().'admin/comment/hit_newComment_dashboard';?>",
                success: function(hsl) {
                    if(hsl === 'noNotif'){
                    }else{
                        let result = JSON.parse(hsl);
                        if(result){
                            audio.play();
                            for(let k =0; k<result.length;k++){
                                var content = '<b>'+result[k].task_nama+'</b><br>'+result[k].comment_user+' : '+result[k].comment_isi;
                                if(result[k].comment_isi === ''){
                                    content = '<b>'+result[k].task_nama+'</b><br>'+result[k].comment_user+' : Attachment';
                                }
                                $.toast({
                                    heading: result[k].kategori_nama,
                                    text: content,
                                    showHideTransition: 'fade',
                                    icon: 'info',
                                    loader:false,
                                    hideAfter: 3000,
                                    position: 'bottom-right',
                                    // bgColor: '#00C9E6',
                                    afterHidden: function () {
                                        $.ajax({
                                            url: "<?php echo base_url().'admin/comment/read_comment';?>",
                                            type: "POST",
                                            data: {
                                                kode: result[k].task_id,
                                            },
                                            success: function(hsl) {
                                                console.log('updated', hsl);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                }
            });
        },4000);
        $('#xcomment').keypress(function(e){
            console.log("e : ", e);
            if(e.keyCode == 13 && e.shiftKey) {
            }else if(e.keyCode == 13){
                $('#formComment').submit();
            }
        });
        $('#xcomment').blur(function(){
            var taskId = $('#taskId').val();
            $.ajax({
                url: "<?php echo base_url().'admin/comment/unset_userOnline';?>",
                type: "POST",
                data: {
                    kode: taskId,
                },
                beforeSend: function() {
                },
                success: function(hsl) {
                },
                fail: function(){
                }
            });
           
        });
        $('#xcomment').keyup(function(e){
            var taskId = $('#taskId').val();
            $.ajax({
                url: "<?php echo base_url().'admin/comment/set_userOnline';?>",
                type: "POST",
                data: {
                    kode: taskId,
                },
                beforeSend: function() {
                },
                success: function(hsl) {
                    console.log('updated onilne');
                },
                fail: function(){
                }
            });
        });

       $('.btnDetail').on('click', function() {
            $('#commentContainer').html('');
            $('.otherCon').hide();
            var work_id = $(this).data('id');
            $.ajax({
                url: "<?php echo base_url().'admin/dashboard/get_detail_work';?>",
                type: "POST",
                data: {
                    kode: work_id
                },
                beforeSend: function() {
                    $('#cover-spin').show();
                },
                success: function(hsl) {
                    console.log("hasil : ", hsl);
                    if(hsl === false){
                        alert(hsl);
                    }else{
                        $('#cover-spin').fadeOut();
                        let result = JSON.parse(hsl);
                        var deadline = result.work_deadline;
                        $('#namaKategori').text(result.kategori_nama);
                        $('#namaSprint').text(result.work_sprint);
                        $('#nama_task').text(result.work_nama);
                        $('#work_assignor').text(result.work_assignor);
                        $('#work_assignor_gambar').attr('src','<?php echo base_url();?>assets/images/'+result.work_assignor_gambar);
                        $('#kategori_gambar1').attr('src','<?php echo base_url();?>assets/images/business/'+result.kategori_gambar);
                        $('#deskripsi_task').html(result.work_deskripsi);
                        $('#taskId').attr("value",work_id);
                        $('#device').attr('src','<?php echo base_url();?>theme/images/'+result.work_device);
                        $('#device_desc').text(result.work_device_deskripsi);
                        $('#dic').text(result.work_assignor);
                        $('#deadline_task').text(deadline);
                        $('#idForNewComment').attr("value", work_id);
                        var id_task = result.work_id;
                        for(let i=0;i<result.listComment.length;i++){
                            var attach = '';
                            var idUser = <?php echo $this->session->userdata('idadmin')?>;
                                var userId = result.listComment[i].comment_user_id;
                            var attach1 = result.listComment[i].comment_attach1;
                            var attach2 = result.listComment[i].comment_attach2;
                            var attach3 = result.listComment[i].comment_attach3;
                            var attach4 = result.listComment[i].comment_attach4;

                            if(userId == idUser){
                                if(attach1 === '' && attach2 === '' && attach3 === '' && attach4 === ''){
                                    attach = '';
                                }else{
                                    attach = '<div class="row"><div class="col-sm-6 col-xl-3"> '+attach4+'</div><div class="col-sm-6 col-xl-3"> '+attach3+'</div><div class="col-sm-6 col-xl-3"> '+attach2+'</div><div class="col-sm-6 col-xl-3"> '+attach1+'</div></div>';
                                }
                                $('#commentContainer').append('<div class="d-flex flex-row-reverse mb-20"> <div> <a class="img-link img-status" href="javascript:void(0)"> <img class="img-avatar img-avatar" style="width:40px;height:35px" src="http://myta.bacotlu.com/assets/images/'+result.listComment[i].comment_user_gambar+'" alt="Avatar"> <div class="img-status-indicator bg-success"></div> </a> </div> <div class="mx-10 text-right"> <div> <div class="bg-primary-lighter text-dark rounded px-15 py-10 mb-5"> <label for="">'+result.listComment[i].comment_user+'</label> <br>'+result.listComment[i].comment_isi+'<br>'+attach+'</div></div> <div class="text-right text-muted font-size-xs font-italic">'+result.listComment[i].comment_created+'</div> </div></div>')
                            }else{
                                if(attach1 === '' && attach2 === '' && attach3 === '' && attach4 === ''){
                                    attach = '';
                                }else{
                                    attach = '<div class="row"><div class="col-sm-6 col-xl-3"> '+attach1+'</div><div class="col-sm-6 col-xl-3"> '+attach2+'</div><div class="col-sm-6 col-xl-3"> '+attach3+'</div><div class="col-sm-6 col-xl-3"> '+attach4+'</div></div>';
                                }
                                $('#commentContainer').append('<div class="d-flex mb-20"><div> <a class="img-link img-status" href="javascript:void(0)"> <img class="img-avatar img-avatar" style="width:40px;height:35px" src="http://myta.bacotlu.com/assets/images/'+result.listComment[i].comment_user_gambar+'" alt="Avatar"> <div class="img-status-indicator bg-success"></div> </a> </div> <div class="mx-10"> <div> <div class="bg-body-dark text-dark rounded px-15 py-10 mb-5"> <label for="">'+result.listComment[i].comment_user+'</label><br>'+result.listComment[i].comment_isi+'<br>'+attach+'</div></div> <div class="text-muted font-size-xs font-italic">'+result.listComment[i].comment_created+'</div> </div></div>');
                            }
                        }
                        setTimeout(() => {
                            objDiv.scrollTop = objDiv.scrollHeight;
                        }, 0);
                        $('#ModalDetail').fadeIn();
                        $('.attachment').hide()
                        $('.onlineContainer').hide();
                        $.ajax({
                                url: "<?php echo base_url().'admin/comment/unset_userOnline';?>",
                                type: "POST",
                                data: {
                                    kode: work_id
                                },
                                beforeSend: function() {
                                },
                                success: function(hsl) {
                                }
                            });
                            setInterval(function(){
                                $.ajax({
                                    url: "<?php echo base_url().'admin/comment/check_online';?>",
                                    type: "POST",
                                    data: {
                                        kode: work_id
                                    },
                                    beforeSend: function() {
                                    },
                                    success: function(hsl) {
                                        if(hsl === 'noOnline'){
                                            $('.onlineContainer').hide();
                                        }else{
                                            setTimeout(() => {
                                                objDiv.scrollTop = objDiv.scrollHeight;
                                            }, 0);
                                            $('.onlineContainer').show();
                                            let result = JSON.parse(hsl);
                                            console.log('hasil :', result);
                                            for(let p=0;p<result.length;p++){
                                                $('#onlineUserGambar').attr("src", result[p].online_user_gambar);
                                                $('#onlineUserNama').text(result[p].online_user+' is typing...');
                                            }
                                        }
                                    }
                                });
                            },1000);


                        setInterval(function(){
                            $.ajax({
                                url: "<?php echo base_url().'admin/comment/hit_newComment';?>",
                                type: "POST",
                                data: {
                                    kode: $('#idForNewComment').val()
                                },
                                beforeSend: function() {
                                },
                                success: function(hsl) {
                                    $('#cover-spin').hide();
                                    var $audio = $('audio');
                                    var audio = $audio[0];
                                    let result = JSON.parse(hsl);
                                    if(result.length > 0){
                                        for(let i=0;i<result.length;i++){
                                            var attach = '';
                                            var attach1 = result[i].comment_attach1;
                                            var attach2 = result[i].comment_attach2;
                                            var attach3 = result[i].comment_attach3;
                                            var attach4 = result[i].comment_attach4;
                                            if(attach1 === "" && attach2 === "" && attach3 === "" && attach4 === ""){
                                                attach = "";
                                            }else{
                                                attach = '<div class="col-sm-6 col-xl-3"> '+attach1+'</div><div class="col-sm-6 col-xl-3"> '+attach2+'</div><div class="col-sm-6 col-xl-3"> '+attach3+'</div><div class="col-sm-6 col-xl-3"> '+attach4+'</div>';
                                            }
                                            $('#commentContainer').append('<div class="d-flex mb-20"><div> <a class="img-link img-status" href="javascript:void(0)"> <img class="img-avatar img-avatar" style="width:40px;height:35px" src="http://myta.bacotlu.com/assets/images/'+result[i].comment_user_gambar+'" alt="Avatar"> <div class="img-status-indicator bg-success"></div> </a> </div> <div class="mx-10"> <div> <div class="bg-body-dark text-dark rounded px-15 py-10 mb-5"> <label for="">'+result[i].comment_user+'</label><br>'+result[i].comment_isi+'<br>'+attach+'</div></div> <div class="text-muted font-size-xs font-italic">'+result[i].comment_created+'</div> </div></div>');
                                            setTimeout(() => {
                                                objDiv.scrollTop = objDiv.scrollHeight;
                                            }, 0);
                                            $.ajax({
                                                url: "<?php echo base_url().'admin/comment/read_comment';?>",
                                                type: "POST",
                                                data: {
                                                    kode: work_id,
                                                },
                                                success: function(hsl) {
                                                    console.log('updated', hsl);
                                                }
                                            }); 
                                        }
                                    }
                                }
                            });
                        },3000);
                    }
                },
                fail: function(){
                    $('#cover-spin').fadeOut();
                    alert('fail');
                }
            });
        });
    </script>
    <script>
    //READY
    $(document).ready(function() {
        $("#dropReady").droppable({
            accept: ".itemsWork",
            drop: function(ev, ui) {

                var work_id = $(ui.draggable).data('id');
                var work_nama = $(ui.draggable).data('nama');
                $.ajax({
                    url: "<?php echo base_url().'admin/scrumboard/update_task_ready';?>",
                    type: "POST",
                    data: {
                        kode: work_id
                    },
                    beforeSend: function() {
                        $('#cover-spin').show();
                    },
                    success: function(msg) {
                        $('#cover-spin').hide();
                        $('#wordingKosongReady').hide();
                            if(msg === 'ok'){
                                $.toast({
                                heading: work_nama + ' Ready to Test!',
                                text: "The task updated to ready to test",
                                showHideTransition: 'slide',
                                icon: 'success',
                                hideAfter: 5000,
                                position: 'bottom-right',
                                bgColor: '#00C9E6'
                            });
                        }
                            
                    },
                    fail: function(){
                        $.toast({
                            heading: 'Updating Task Fail',
                            text: "The task fail to update",
                            showHideTransition: 'slide',
                            icon: 'error',
                            hideAfter: 5000,
                            position: 'bottom-right',
                            bgColor: '#FF4859'
                        });
                    }
                });
            }
        });
    });
    </script>

    <script>
    //OPEN
    $(document).ready(function() {
        $("#dropOngoing").droppable({
            accept: ".itemsWork",
            drop: function(ev, ui) {
                $('body').css('pointer-events', 'all') 
                var work_id = $(ui.draggable).data('id');
                var work_nama = $(ui.draggable).data('nama');
                $.ajax({
                    url: "<?php echo base_url().'admin/scrumboard/update_task_ongoing';?>",
                    type: "POST",
                    data: {
                        kode: work_id
                    },
                    beforeSend: function() {
                        $('#cover-spin').show();
                    },
                    success: function(msg) {
                        $('#cover-spin').hide();
                        $('#wordingKosongOpen').hide();
                        if(msg === 'ok'){
                            $.toast({
                                heading: work_nama + ' Revert to In Progress',
                                text: "The task is reverted to In Progress",
                                showHideTransition: 'slide',
                                icon: 'success',
                                hideAfter: 5000,
                                position: 'bottom-right',
                                bgColor: '#00C9E6'
                            });
                        }
                    },
                    fail: function(){
                        $.toast({
                            heading: 'Updating Task Fail',
                            text: "The task fail to update",
                            showHideTransition: 'slide',
                            icon: 'error',
                            hideAfter: 5000,
                            position: 'bottom-right',
                            bgColor: '#FF4859'
                        });
                    }
                });
            }
        });
    });
    </script>

    <script>
    //APK
    $(document).ready(function() {
        $("#dropAPK").droppable({
            accept: ".itemsWork",
            drop: function(ev, ui) {
                var work_id = $(ui.draggable).data('id');
                var work_nama = $(ui.draggable).data('nama');
                $.ajax({
                    url: "<?php echo base_url().'admin/scrumboard/update_task_apk';?>",
                    type: "POST",
                    data: {
                        kode: work_id
                    },
                    beforeSend: function() {
                        $('#cover-spin').show();
                    },
                    success: function(msg) {
                        $('#cover-spin').hide();
                        $('#wordingKosongAPK').hide();
                        if(msg === 'ok'){
                            $.toast({
                                heading: work_nama + ' wait to new APK/IPA',
                                text: "The task is ready to test in the new Deployment!",
                                showHideTransition: 'slide',
                                icon: 'success',
                                hideAfter: 5000,
                                position: 'bottom-right',
                                bgColor: '#00C9E6'
                            });
                        }
                       
                    },
                    fail: function(){
                        $.toast({
                            heading: 'Updating Task Fail',
                            text: "The task fail to update",
                            showHideTransition: 'slide',
                            icon: 'error',
                            hideAfter: 5000,
                            position: 'bottom-right',
                            bgColor: '#FF4859'
                        });
                    }
                });
            }
        });
    });
    </script>
     <?php if($akses === '4' || $akses === '5'): ?>
    <script>
    //DONE
    $(document).ready(function() {
        $("#dropDone").droppable({
            accept: ".itemsWork",
            drop: function(ev, ui) {
                var work_id = $(ui.draggable).data('id');
                var work_nama = $(ui.draggable).data('nama');
                $.ajax({
                    url: "<?php echo base_url().'admin/scrumboard/update_task_done';?>",
                    type: "POST",
                    data: {
                        kode: work_id
                    },
                    beforeSend: function() {
                        $('#cover-spin').show();
                    },
                    success: function(msg) {
                        $('#cover-spin').hide();
                        $('#wordingKosongDone').hide();
                        if(msg === 'ok'){
                            $.toast({
                                heading: work_nama + ' Finished',
                                text: "The task is complete",
                                showHideTransition: 'slide',
                                icon: 'success',
                                hideAfter: 5000,
                                position: 'bottom-right',
                                bgColor: '#00C9E6'
                            });
                        }
                    },
                    fail: function(){
                        $.toast({
                            heading: 'Updating Task Fail',
                            text: "The task fail to update",
                            showHideTransition: 'slide',
                            icon: 'error',
                            hideAfter: 5000,
                            position: 'bottom-right',
                            bgColor: '#FF4859'
                        });
                    }
                });
            }
        });
    });
    </script>

    <script>
    //CANCEL
    $(document).ready(function() {
        $("#dropCancel").droppable({
            accept: ".itemsWork",
            drop: function(ev, ui) {
                var work_id = $(ui.draggable).data('id');
                var work_nama = $(ui.draggable).data('nama');
                $.ajax({
                    url: "<?php echo base_url().'admin/scrumboard/update_task_cancel';?>",
                    type: "POST",
                    data: {
                        kode: work_id
                    },
                    beforeSend: function() {
                        $('#cover-spin').show();
                    },
                    success: function(msg) {
                        $('#cover-spin').hide();
                        $('#wordingKosongCancel').hide();
                        if(msg === 'ok'){
                            $.toast({
                                heading: work_nama + ' Canceled',
                                text: "The task is canceled",
                                showHideTransition: 'slide',
                                icon: 'success',
                                hideAfter: 5000,
                                position: 'bottom-right',
                                bgColor: '#00C9E6'
                            });
                        }
                    },
                    fail: function(){
                        $.toast({
                            heading: 'Updating Task Fail',
                            text: "The task fail to update",
                            showHideTransition: 'slide',
                            icon: 'error',
                            hideAfter: 5000,
                            position: 'bottom-right',
                            bgColor: '#FF4859'
                        });
                    }
                });
            }
        });
    });
    </script>
    <?php endif; ?>

    <?php if($this->session->flashdata('msg')=='gagal-hapus'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Error',
        text: "There is still assignment attached in that project",
        showHideTransition: 'slide',
        icon: 'error',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#FF4859'
    });
    </script>

    <?php elseif($this->session->flashdata('msg')=='show-modal'):?>
    <script type="text/javascript">
    $('#ModalResetPassword').modal('show');
    </script>
    <?php endif ?>
    <?php if($this->session->userdata('succesComment')=='success_add_comment'):?>
        
<script type="text/javascript">
     
            var work_id = "<?php echo $this->session->userdata('idtask'); ?>";
            var objDiv = document.getElementById("scrollDiv");
            $('.otherCon').hide();
            $('.otherCon').attr('style', 'height:0px');
            setTimeout(() => {
                $.ajax({
                    url: "<?php echo base_url().'admin/dashboard/get_detail_work';?>",
                    type: "POST",
                    data: {
                        kode: work_id
                    },
                    beforeSend: function() {
                        $('#cover-spin').show();
                    },
                    success: function(hsl) {
                         $('#ModalDetail').fadeIn();

                        console.log("hasil : ", hsl);
                        if(hsl === false){
                            alert(hsl);
                        }else{
                            $('#cover-spin').fadeOut();
                            let result = JSON.parse(hsl);
                            var deadline = result.work_deadline;
                            $('#namaKategori').text(result.kategori_nama);
                            $('#namaSprint').text(result.work_sprint);
                            $('#nama_task').text(result.work_nama);
                             $('#work_assignor').text(result.work_assignor);
                            $('#work_assignor_gambar').attr('src','<?php echo base_url();?>assets/images/'+result.work_assignor_gambar);
                            $('#kategori_gambar1').attr('src','<?php echo base_url();?>assets/images/business/'+result.kategori_gambar);
                            $('#deskripsi_task').html(result.work_deskripsi);
                            $('#taskId').attr("value",work_id);
                            $('#device').attr('src','<?php echo base_url();?>theme/images/'+result.work_device);
                            $('#device_desc').text(result.work_device_deskripsi);
                            $('#dic').text(result.work_assignor);
                            $('#deadline_task').text(deadline);
                            $('#idForNewComment').attr("value", work_id);
                            

                            var id_task = result.work_id;
                            for(let i=0;i<result.listComment.length;i++){
                                var attach = '';
                                var idUser = <?php echo $this->session->userdata('idadmin')?>;
                                var userId = result.listComment[i].comment_user_id;
                                var attach1 = result.listComment[i].comment_attach1;
                                var attach2 = result.listComment[i].comment_attach2;
                                var attach3 = result.listComment[i].comment_attach3;
                                var attach4 = result.listComment[i].comment_attach4;

                                if(userId == idUser){
                                    if(attach1 === '' && attach2 === '' && attach3 === '' && attach4 === ''){
                                        attach = '';
                                    }else{
                                        attach = '<div class="row"><div class="col-sm-6 col-xl-3"> '+attach4+'</div><div class="col-sm-6 col-xl-3"> '+attach3+'</div><div class="col-sm-6 col-xl-3"> '+attach2+'</div><div class="col-sm-6 col-xl-3"> '+attach1+'</div></div>';
                                    }
                                    $('#commentContainer').append('<div class="d-flex flex-row-reverse mb-20"> <div> <a class="img-link img-status" href="javascript:void(0)"> <img class="img-avatar img-avatar" style="width:40px;height:35px" src="http://myta.bacotlu.com/assets/images/'+result.listComment[i].comment_user_gambar+'" alt="Avatar"> <div class="img-status-indicator bg-success"></div> </a> </div> <div class="mx-10 text-right"> <div> <div class="bg-primary-lighter text-dark rounded px-15 py-10 mb-5"> <label for="">'+result.listComment[i].comment_user+'</label> <br>'+result.listComment[i].comment_isi+'<br>'+attach+'</div></div> <div class="text-right text-muted font-size-xs font-italic">'+result.listComment[i].comment_created+'</div> </div></div>')
                                }else{
                                    if(attach1 === '' && attach2 === '' && attach3 === '' && attach4 === ''){
                                        attach = '';
                                    }else{
                                        attach = '<div class="row"><div class="col-sm-6 col-xl-3"> '+attach1+'</div><div class="col-sm-6 col-xl-3"> '+attach2+'</div><div class="col-sm-6 col-xl-3"> '+attach3+'</div><div class="col-sm-6 col-xl-3"> '+attach4+'</div></div>';
                                    }
                                    $('#commentContainer').append('<div class="d-flex mb-20"><div> <a class="img-link img-status" href="javascript:void(0)"> <img class="img-avatar img-avatar" style="width:40px;height:35px" src="http://myta.bacotlu.com/assets/images/'+result.listComment[i].comment_user_gambar+'" alt="Avatar"> <div class="img-status-indicator bg-success"></div> </a> </div> <div class="mx-10"> <div> <div class="bg-body-dark text-dark rounded px-15 py-10 mb-5"> <label for="">'+result.listComment[i].comment_user+'</label><br>'+result.listComment[i].comment_isi+'<br>'+attach+'</div></div> <div class="text-muted font-size-xs font-italic">'+result.listComment[i].comment_created+'</div> </div></div>');
                                }
                            }
        
                            // $('#ModalDetail').fadeIn();
                            $('.attachment').hide()

                           var taskId = $('#taskId').val();
                            setTimeout(() => {
                                objDiv.scrollTop = objDiv.scrollHeight;
                            }, 0);
                            $.ajax({
                                url: "<?php echo base_url().'admin/comment/unset_userOnline';?>",
                                type: "POST",
                                data: {
                                    kode:id_task
                                },
                                beforeSend: function() {
                                },
                                success: function(hsl) {
                                }
                            });
                            setInterval(function(){
                                $.ajax({
                                    url: "<?php echo base_url().'admin/comment/check_online';?>",
                                    type: "POST",
                                    data: {
                                        kode: id_task
                                    },
                                    beforeSend: function() {
                                    },
                                    success: function(hsl) {
                                        if(hsl === 'noOnline'){
                                            $('.onlineContainer').hide();
                                        }else{
                                            setTimeout(() => {
                                                objDiv.scrollTop = objDiv.scrollHeight;
                                            }, 0);
                                            $('.onlineContainer').show();
                                            let result = JSON.parse(hsl);
                                            console.log('hasil :', result);
                                            for(let p=0;p<result.length;p++){
                                                $('#onlineUserGambar').attr("src", result[p].online_user_gambar);
                                                $('#onlineUserNama').text(result[p].online_user+' is typing...');
                                            }
                                        }
                                    }
                                });
                            },1500);
                            console.log("response : ",result);
                                setInterval(function(){
                                $.ajax({
                                    url: "<?php echo base_url().'admin/comment/hit_newComment';?>",
                                    type: "POST",
                                    data: {
                                        kode: work_id
                                    },
                                    beforeSend: function() {
                                    },
                                    success: function(hsl) {
                                        $('#cover-spin').hide();
                                        var $audio = $('audio');
                                        var audio = $audio[0];
                                        let result = JSON.parse(hsl);
                                        if(result.length > 0){
                                            for(let i=0;i<result.length;i++){
                                                
                                                var attach = '';
                                                var attach1 = result[i].comment_attach1;
                                                var attach2 = result[i].comment_attach2;
                                                var attach3 = result[i].comment_attach3;
                                                var attach4 = result[i].comment_attach4;

                                                if(attach1 === "" && attach2 === "" && attach3 === "" && attach4 === ""){
                                                    attach = "";
                                                }else{
                                                    attach = '<div class="row items-push js-gallery img-fluid-100"><div class="col-sm-6 col-xl-3"> '+attach1+'</div><div class="col-sm-6 col-xl-3"> '+attach2+'</div><div class="col-sm-6 col-xl-3"> '+attach3+'</div><div class="col-sm-6 col-xl-3"> '+attach4+'</div></div>';
                                                }
               
                                                $('#commentContainer').append('<div class="d-flex mb-20"><div> <a class="img-link img-status" href="javascript:void(0)"> <img class="img-avatar img-avatar" style="width:40px;height:35px" src="http://myta.bacotlu.com/assets/images/'+result[i].comment_user_gambar+'" alt="Avatar"> <div class="img-status-indicator bg-success"></div> </a> </div> <div class="mx-10"> <div> <div class="bg-body-dark text-dark rounded px-15 py-10 mb-5"> <label for="">'+result[i].comment_user+'</label><br>'+result[i].comment_isi+'<br>'+attach+'</div></div> <div class="text-muted font-size-xs font-italic">'+result[i].comment_created+'</div> </div></div>');
                                                setTimeout(() => {
                                                    objDiv.scrollTop = objDiv.scrollHeight;
                                                }, 0);
                                                $.ajax({
                                                    url: "<?php echo base_url().'admin/comment/read_comment';?>",
                                                    type: "POST",
                                                    data: {
                                                        kode: work_id,
                                                    },
                                                    success: function(hsl) {
                                                        console.log('updated', hsl);
                                                    }
                                                }); 
                                            }
                                        }
                                    }
                                });
                            },3000);
                        }
                    },
                    fail: function(){
                        // $('#cover-spin').fadeOut();
                        alert('fail');
                    }
                });
            },0);
        </script>
    <?php endif ?>
    
</body>

</html>