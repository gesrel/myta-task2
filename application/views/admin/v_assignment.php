<!doctype html>
<html lang="en" class="no-focus">
<!--<![endif]-->

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Tim Task | Myta My Task</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

    <!-- END Icons -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>" />




</head>

<body>
    <!-- Page Container -->

    <div id="page-container"
        class="sidebar-o side-scroll side-trans-enabled page-header-fixed <?php if($dark === 'dark'): ?>page-header-inverse sidebar-inverse <?php endif; ?>">


        <?php echo $this->load->view('admin/v_sidemenu.php');?>


        <!-- Header -->
        <?php echo $this->load->view('admin/header.php');?>
        <!-- END Header -->

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <div class="row">
                    <div class="col-md-3">
                        <div class="block" data-spy="affix" data-offset-top="205">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Team Profile</h3>
                                <div class="block-options">
                                </div>
                            </div>
                            <div class="block-content">
                                <?php foreach($user->result() as $usr): 
                                    $linkImg = $usr->pengguna_photo;
                                    if($linkImg === ''){
                                        if($usr->pengguna_jenkel === 'L'){
                                            $linkImg = 'user_blank.png';
                                        }else{
                                            $linkImg = 'user_blank2.png';
                                        }
                                    }?>
                                <div class="text-center" style="margin-bottom:30px">
                                    <img class="" style="border-radius: 50%;"
                                        src="<?php echo base_url().'assets/images/'.$linkImg;?>"
                                        width="100px" alt="">
                                </div>
                                <?php endforeach; ?>
                                <div class="list-group" style="margin-bottom:30px">
                                    <a class="list-group-item list-group-item-action btn-user"
                                        data-nama="<?php echo $dev->pengguna_nama; ?>" href="javascript:void(0)">
                                        <i class="fa fa-fw fa-user mr-5"></i> <?php echo $usr->pengguna_nama; ?>
                                    </a>
                                    <a class="list-group-item list-group-item-action" href="javascript:void(0)">
                                        <i class="fa fa-fw fa-envelope mr-5"></i> <?php echo $usr->pengguna_email; ?>
                                    </a>
                                    <a class="list-group-item list-group-item-action" href="javascript:void(0)">
                                        <i class="fa fa-fw fa-phone mr-5"></i> <?php echo $usr->pengguna_nohp; ?>
                                    </a>
                                    <a class="list-group-item list-group-item-action" href="javascript:void(0)">
                                        <i class="fa fa-fw fa-user-plus mr-5"></i>
                                        <?php echo date("M d, 2019", strtotime($tglJoin)); ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title"><?php echo $projectName; ?> Ongoing Task List</h3>
                                <div class="block-options">

                                </div>
                                <div style="width:300px;">
                                    <?php if($this->uri->segment(6) === 'alltask'): 
                                            $uriSearch = base_url().'admin/assignment/search_task/'.$userId.'/'.$token;
                                        else :
                                            $uriSearch = base_url().'admin/assignment/search_task/'.$userId.'/'.$token.'/alltask';
                                        endif;
                                    ?>
                                    <!-- <form class="push" action="<?php echo $uriSearch; ?>" method="get">
                                        <div class="input-group input-group-sm">
                                            <input type="text" class="form-control" placeholder="Search Task..">
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-secondary btn-sm">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form> -->

                                </div>
                            </div>
                            <div class="block-content">

                                <?php if($onprogress->num_rows() > 0): ?>
                                <div id="accordion" class="gutters-tiny invisible" data-toggle="appear" role="tablist"
                                    aria-multiselectable="true">
                                    <?php foreach($onprogress->result() as $work): ?>
                                    <?php 
                                    $today = date("Y-m-d H:i");
                                    $deadline = date("Y-m-d H:i",strtotime($work->work_deadline));
                                    $pass = false;
                                    if($deadline < $today){
                                        $pass = true;
                                    }?>
                                    <div class="block block-bordered block-rounded mb-2">
                                        <div class="block-header" role="tab" id="accordion_h2">
                                            <a class="font-w600" data-toggle="collapse" data-parent="#accordion"
                                                href="#accordion_q2<?php echo $work->work_id; ?>" aria-expanded="true"
                                                aria-controls="accordion_q2"><?php echo $work->work_nama; ?>
                                                <?php if($work->work_status === 'A'): ?>
                                                <span class="badge badge-info"> Progress <i
                                                        class="fa fa-spinner fa-pulse fa-spin"></i></span>
                                                <?php elseif($work->work_status === 'B'): ?>
                                                <span class="badge badge-success"> Done <i
                                                        class="fa fa-check"></i></span>
                                                <?php elseif($work->work_status === 'C'): ?>
                                                <span class="badge badge-danger"> Canceled <i
                                                        class="fa fa-warning"></i></span>
                                                <?php elseif($work->work_status === 'D'): ?>
                                                <span class="badge badge-success"> Ready to Test <i
                                                        class="fa fa-check"></i></span>
                                                <?php elseif($work->work_status === 'E'): ?>
                                                <span class="badge badge-primary"> Wait for APK/IPA <i
                                                        class="fa fa-android"></i> <i class="fa fa-apple"></i></span>
                                                <?php elseif($work->work_status === 'F'): ?>
                                                <span class="badge badge-secondary"> Waiting Approval <i
                                                        class="fa fa-spinner fa-pulse fa-spin"></i></span>
                                                <?php else: ?>

                                                    <?php 
                                                    $today = date('Y-m-d H:i');
                                                    $deadLine = date('Y-m-d H:i', strtotime($work->work_deadline));
                                                    $dead = false;
                                                    $deadLineCommand = false;
                                                    if($deadLine === $today){
                                                        $dead = true; 
                                                        $deadLineCommand = false;
                                                    }elseif($today > $deadLine){
                                                        $dead = true; 
                                                        $deadLineCommand = true ; 
                                                    }else{
                                                        $dead = false; 
                                                        $deadLineCommand = false;
                                                    }
                
                                                    if($dead === true): ?>
                                                    <span class="fa fa-warning" style="color:red" data-toggle="popover"
                                                        title="Passing the Deadline" data-placement="right"
                                                        data-content="Specified deadline for this assignment <?php echo date("M d, Y H:i A", strtotime($work->work_deadline)) ?>">
                                                    </span>
                                                    <?php endif; ?>

                                                <?php endif;?>
                                               
                                            </a>
                                            <div class="pull-right">
                                                <small><span class="fa fa-clock-o"></span>
                                                    <?php echo date("M d, Y H:i A", strtotime($work->work_tgl));?></small>
                                            </div>
                                        </div>
                                        <div id="accordion_q2<?php echo $work->work_id; ?>" class="collapse"
                                            role="tabpanel" aria-labelledby="accordion_h2" data-parent="#accordion">
                                            <ul class="nav nav-tabs nav-tabs-block" style="background-color:#E8E8E8"
                                                data-toggle="tabs" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active"
                                                        href="#btabs-animated-fade-detail<?php echo $work->work_id; ?>"><span
                                                            class="si si-info"></span> Task
                                                        Detail</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link"
                                                        href="#btabs-animated-fade-timeline<?php echo $work->work_id; ?>"><span
                                                            class="si si-clock"></span> Status Line</a>
                                                </li>
                                                <!--<li class="nav-item">-->
                                                <!--    <a class="nav-link"-->
                                                <!--        href="#btabs-animated-fade-report<?php echo $work->work_id; ?>"><span-->
                                                <!--            class="si si-bar-chart"></span> Report</a>-->
                                                <!--</li>-->
                                                <!-- <li class="nav-item ml-auto" style="margin-top:1%">
                                                    <div class="btn-group btn-group-sm mr-20">
                                                        <a data-toggle="modal" data-target="#ModalCounter"
                                                            class="btn btn-secondary">
                                                            <i class="si si-action-redo"></i>
                                                        </a>
                                                        <a data-toggle="modal" data-target="#ModalCancel"
                                                            class="btn btn-secondary">
                                                            <i class="fa fa-remove"></i>
                                                        </a>
                                                    </div>
                                                </li> -->
                                            </ul>
                                            <div class="block-content tab-content overflow-hidden">
                                                <div class="tab-pane fade show active"
                                                    id="btabs-animated-fade-detail<?php echo $work->work_id; ?>"
                                                    role="tabpanel">
                                                    <div class="block-content" style="height:300px;overflow:auto">
                                                    <?php if($work->work_device === 'ANDROIOS'): ?>
                                                    <img src="<?php echo base_url().'theme/images/ios.png';?>" width="30px" alt=""> 
                                                    <img src="<?php echo base_url().'theme/images/android.png';?>" width="20px" alt=""> 
                                                    <span class="badge badge-primary">iOS & Android</span>
                                                    <?php elseif($work->work_device === 'IOS'): ?>
                                                    <img src="<?php echo base_url().'theme/images/ios.png';?>"
                                                        width="30px" alt=""> <span class="badge badge-info">iOS</span>
                                                    <?php elseif($work->work_device === 'ANDROID'): ?>
                                                    <img src="<?php echo base_url().'theme/images/android.png';?>"
                                                        width="20px" alt=""> <span class="badge badge-info">Android</span>
                                                    <?php elseif($work->work_device === 'CMS'): ?>
                                                    <img src="<?php echo base_url().'theme/images/cms.png';?>"
                                                        width="30px" alt=""><span class="badge badge-info">CMS</span>
                                                    <?php elseif($work->work_device === 'BACKEND'): ?>
                                                    <img src="<?php echo base_url().'theme/images/backend.png';?>"
                                                        width="30px" alt=""> <span class="badge badge-info">Backend</span>
                                                    <?php elseif($work->work_device === 'FRONTEND'): ?>
                                                    <img src="<?php echo base_url().'theme/images/frontend.png';?>"
                                                        width="30px" alt=""> <span class="badge badge-danger">Frontend</span>
                                                        <?php else: ?>
                                                        <img src="https://images.vexels.com/media/users/3/128132/isolated/preview/fa3b9aad78a9db81459bd03294a0f985-flat-laptop-icon-design-by-vexels.png"
                                                            width="30px" alt="">
                                                        <small><?php echo $work->work_device; ?></small>
                                                        <?php endif; ?>

                                                        <p><?php echo $work->work_deskripsi; ?></p>

                                                    </div>
                                                </div>
                                                <div class="tab-pane fade"
                                                    id="btabs-animated-fade-timeline<?php echo $work->work_id; ?>"
                                                    role="tabpanel">
                                                    <div class="block-content" style="height:300px;overflow:auto">
                                                        <ul class="list list-timeline list-timeline-modern pull-t">
                                                            <?php
                                                            $tglDL = date("Y-M-d", strtotime($work->work_tgl));
                                                            $today = date("Y-M-d");

                                                            if($tglDL === $today){
                                                                $viewTgl = 'Today, at <br>'.date("H:i A", strtotime($work->work_tgl));
                                                            }else{
                                                                $viewTgl = date("M d, Y H:i A", strtotime($work->work_tgl));
                                                            }
                                                        ?>
                                                            <?php $timeline = $this->db->query("SELECT * FROM timeline where timeline_work_id='$work->work_id' order by timeline_tgl DESC"); ?>
                                                            <?php foreach($timeline->result() as $tm): ?>
                                                            <?php $tglDL2 = date("Y-M-d", strtotime($tm->timeline_tgl));
                                                            $today2 = date("Y-M-d");
                                                            if($tglDL2 === $today2){
                                                                $viewTgl2 = 'Today, at <br>'.date("H:i A", strtotime($tm->timeline_tgl));
                                                            }else{
                                                                $viewTgl2 = date("M d, Y H:i A", strtotime($tm->timeline_tgl));
                                                            }?>
                                                            <li>
                                                                <div class="list-timeline-time"><?php echo $viewTgl2;?>
                                                                </div>
                                                                <?php if($tm->timeline_code === 'DONE'): ?>
                                                                <i
                                                                    class="list-timeline-icon fa fa-check bg-success"></i>
                                                                <?php elseif($tm->timeline_code === 'CANCELED'): ?>
                                                                <i
                                                                    class="list-timeline-icon fa fa-warning bg-danger"></i>
                                                                <?php elseif($tm->timeline_code === 'READYAPK'): ?>
                                                                <i
                                                                    class="list-timeline-icon fa fa-android bg-warning"></i>
                                                                <?php elseif($tm->timeline_code === 'READYTEST'): ?>
                                                                <i class="list-timeline-icon si si-info bg-success"></i>
                                                                <?php elseif($tm->timeline_code === 'REVERT'): ?>
                                                                <i
                                                                    class="list-timeline-icon si si-reload bg-primary"></i>
                                                                <?php endif; ?>
                                                                <div class="list-timeline-content">
                                                                    <p class="font-w600">
                                                                        <?php echo $tm->timeline_nama; ?>
                                                                    </p>
                                                                    <p><?php echo $tm->timeline_deskripsi; ?> -
                                                                        <b><?php echo $tm->timeline_user; ?></b>
                                                                    </p>
                                                                </div>
                                                            </li>
                                                            <?php endforeach; ?>
                                                            <li>
                                                                <div class="list-timeline-time">
                                                                    <?php echo $viewTgl; ?></div>
                                                                <i class="list-timeline-icon fa fa-plus bg-info"></i>
                                                                <div class="list-timeline-content">
                                                                    <p class="font-w600">+ Task Created</p>
                                                                    <?php $ba= $this->db->query("SELECT * FROM pengguna where pengguna_id='$work->work_user_id'")->row_array(); ?>
                                                                    <p><?php echo $work->work_nama;?> created by
                                                                        <b><?php echo $ba['pengguna_nama'];?></b></p>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade"
                                                    id="btabs-animated-fade-report<?php echo $work->work_id; ?>"
                                                    role="tabpanel">
                                                    <div style="height:300px" class="block-content block-content-full">
                                                        <div class="gutters-tiny invisible" data-toggle="appear">
                                                            
                                                            <div class="row">
                                                            <div class="col-6">
                                                                    <h2 style="color:#ABAAAA;font-weight:lighter;margin-bottom:-5px">First Response
                                                                    </h2>
                                                            <?php $assesment = $this->db->query("SELECT * FROM assesment where assesment_work_id='$work->work_id' AND assesment_kategori='RESPONSE'");
                                                            if($assesment->num_rows() > 0):
                                                            foreach($assesment->result() as $resp):?>
                                                                
                                                                    <div class="badge badge-info pull-right"><?php echo $resp->assesment_record;?> Minutes <i
                                                                        class="si si-info"></i>
                                                                        </div><br>
                                                                    <?php if($resp->assesment_nilai === 'Sangat Baik'): ?>
                                                                        <div class="progress push">
                                                                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                                                                <span class="progress-bar-label">100%</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="badge badge-success pull-right"><?php echo $resp->assesment_nilai;?> <i
                                                                            class="si si-emoticon-smile"></i></div>
                                                                    <?php elseif($resp->assesment_nilai === 'Baik'): ?>
                                                                        
                                                                        <div class="progress push">
                                                                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 70%;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                                                                <span class="progress-bar-label">70%</span>
                                                                            </div>
                                                                        </div>
                                                                        <center><div class="badge badge-success"><?php echo $resp->assesment_nilai;?> <i
                                                                            class="si si-like"></i></div></center>
                                                                    <?php elseif($$resp->assesment_nilai === 'Kurang'): ?>
                                                                    <div class="progress push">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: 40%;" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="progress-bar-label">40%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="badge badge-danger"><?php echo $resp->assesment_nilai;?> <i
                                                                        class="si si-dislike"></i></div>
                                                                    <?php elseif($resp->assesment_nilai === 'Sangat Kurang'): ?>
                                                                    <div class="progress push">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: 10%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="progress-bar-label">10%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="badge badge-danger"><?php echo $resp->assesment_nilai;?> <i
                                                                        class="si si-dislike"></i></div>   
                                                                    <?php else: ?>
                                                                    <div class="progress push">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="progress-bar-label">0%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="badge badge-primary">No Progress <i
                                                                        class="si si-info"></i></div>
                                                                    <?php endif; ?>
                                                        <?php endforeach; ?>
                                                            <?php else: ?>
                                                            <div class="progress push">
                                                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                                                    <span class="progress-bar-label">0%</span>
                                                                </div>
                                                            </div>
                                                            <div class="badge badge-primary">No Progress <i
                                                                class="si si-info"></i></div>
                                                            <?php endif; ?>
                                                        </div>

                                                        <div class="col-6">
                                                        <h2 style="color:#ABAAAA;font-weight:lighter;margin-bottom:-10px">Revert Quality <span
                                                            style="font-size:10px;font-weight:lighter">
                                                        </h2>
                                                        <?php $assesment2 = $this->db->query("SELECT * FROM assesment where assesment_work_id='$work->work_id' AND assesment_kategori='REVERT' order by assesment_record desc limit 1");
                                                            if($assesment2->num_rows() > 0):
                                                            foreach($assesment2->result() as $resp2):?>
                                                                    
                                                                    <div class="badge badge-info pull-right"><?php echo $resp2->assesment_record;?> Times Revert <i
                                                                        class="si si-info"></i>
                                                                        </div><br>
                                                                    <?php if($resp2->assesment_nilai === 'Sangat Baik'): ?>
                                                                        <div class="progress push">
                                                                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                                                                <span class="progress-bar-label">100%</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="badge badge-success pull-right"><?php echo $resp2->assesment_nilai;?> <i
                                                                            class="si si-emoticon-smile"></i></div>
                                                                    <?php elseif($resp2->assesment_nilai === 'Baik'): ?>
                                                                        <div class="progress push">
                                                                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 70%;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                                                                <span class="progress-bar-label">70%</span>
                                                                            </div>
                                                                        </div>
                                                                        <center><div class="badge badge-success"><?php echo $resp2->assesment_nilai;?> <i
                                                                            class="si si-like"></i></div></center>
                                                                    <?php elseif($resp2->assesment_nilai === 'Kurang'): ?>
                                                                    <div class="progress push">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: 40%;" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="progress-bar-label">40%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="badge badge-danger"><?php echo $resp2->assesment_nilai;?> <i
                                                                        class="si si-dislike"></i></div>
                                                                    <?php elseif($resp2->assesment_nilai === 'Sangat Kurang'): ?>
                                                                    <div class="progress push">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: 10%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="progress-bar-label">10%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="badge badge-danger"><?php echo $resp2->assesment_nilai;?> <i
                                                                        class="si si-dislike"></i></div>
                                                                    <?php else: ?>
                                                                    <div class="progress push">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="progress-bar-label">0%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="badge badge-primary">No Progress <i
                                                                        class="si si-info"></i></div>
                                                                    <?php endif; ?>
                                                                    
                                                                <?php endforeach; ?>
                                                                <?php else: ?>
                                                                <div class="progress push">
                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                                                            <span class="progress-bar-label">0%</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="badge badge-primary">No Progress <i
                                                                        class="si si-info"></i></div>
                                                                <?php endif; ?>
                                                                </div>

                                                                <div class="col-6">
                                                                    <h2 style="color:#ABAAAA;font-weight:lighter">Task Quality <span
                                                                        style="font-size:10px;font-weight:lighter">
                                                                    </h2>
                                                                        <?php $assesmentI = $this->db->query("SELECT * FROM assesment where assesment_work_id='$work->work_id' AND assesment_kategori='RESPONSE'"); 
                                                                        $assesmentII = $this->db->query("SELECT * FROM assesment where assesment_work_id='$work->work_id' AND assesment_kategori='REVERT' order by assesment_record desc limit 1"); ?>

                                                                        <?php 
                                                                        if($assesmentI->num_rows() > 0 && $assesment->num_rows() > 0):
                                                                        foreach($assesmentI->result() as $resp):
                                                                            foreach($assesmentII->result() as $rev):
                                                                                if($resp->assesment_nilai === 'Sangat Baik' && $rev->assesment_nilai === 'Sangat Baik'):?>
                                                                                    <div class="progress push">
                                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="progress-bar-label">100%</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="badge badge-success pull-right"> Super Good Task <i
                                                                                        class="si si-emoticon-smile"></i>
                                                                                    </div>
                                                                                <?php elseif($resp->assesment_nilai === 'Sangat Baik' && $rev->assesment_nilai === 'Baik'):?>
                                                                                    <div class="progress push">
                                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="progress-bar-label">90%</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="badge badge-success pull-right"> Very Good Task <i
                                                                                        class="si si-like"></i>
                                                                                    </div>
                                                                                <?php elseif($resp->assesment_nilai === 'Sangat Baik' && $rev->assesment_nilai === 'Kurang'):?>
                                                                                    <div class="progress push">
                                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 70%;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="progress-bar-label">70%</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <center><div class="badge badge-primary"> Good Task <i
                                                                                        class="si si-like"></i>
                                                                                    </div></center>

                                                                                <?php elseif($resp->assesment_nilai === 'Sangat Baik' && $rev->assesment_nilai === 'Sangat Kurang'):?>
                                                                                    <div class="progress push">
                                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="progress-bar-label">50%</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <center><div class="badge badge-warning"><?php echo $taskQuality; ?> <i
                                                                                        class="si si-info"></i>
                                                                                    </div><center>

                                                                                <?php elseif($resp->assesment_nilai === 'Baik' && $rev->assesment_nilai === 'Baik'):?>
                                                                                    <div class="progress push">
                                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="progress-bar-label">90%</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="badge badge-success pull-right">Super Good Task <i
                                                                                        class="si si-like"></i>
                                                                                    </div>
                                                                                <?php elseif($resp->assesment_nilai === 'Baik' && $rev->assesment_nilai === 'Kurang'):?>
                                                                                    <div class="progress push">
                                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="progress-bar-label">50%</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <center><div class="badge badge-warning">Not Bad Task <i
                                                                                        class="si si-info"></i>
                                                                                    </div><center>
                                                                            <?php elseif($resp->assesment_nilai === 'Baik' && $rev->assesment_nilai === 'Sangat Kurang'):?>
                                                                                    <div class="progress push">
                                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="progress-bar-label">50%</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <center><div class="badge badge-warning">Not Bad Task<i
                                                                                        class="si si-info"></i>
                                                                                    </div><center>
                                                                                <?php elseif($resp->assesment_nilai === 'Baik' && $rev->assesment_nilai === 'Sangat Kurang'):?>
                                                                                    <div class="progress push">
                                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="progress-bar-label">50%</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <center><div class="badge badge-warning">Not Bad Task <i
                                                                                        class="si si-info"></i>
                                                                                    </div><center>
                                                                                <?php elseif($resp->assesment_nilai === 'Kurang' && $rev->assesment_nilai === 'Kurang'):?>
                                                                                    <div class="progress push">
                                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="progress-bar-label">50%</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <center><div class="badge badge-warning">Not Bad Task <i
                                                                                        class="si si-info"></i>
                                                                                    </div><center>
                                                                            <?php elseif($resp->assesment_nilai === 'Kurang' && $rev->assesment_nilai === 'Sangat Kurang'):?>
                                                                                    <div class="progress push">
                                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: 30%;" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="progress-bar-label">30%</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="badge badge-danger ">Bad Task <i
                                                                                        class="si si-dislike"></i>
                                                                                    </div>
                                                                            <?php elseif($resp->assesment_nilai === 'Sangat Kurang' && $rev->assesment_nilai === 'Sangat Kurang'):?>
                                                                                    <div class="progress push">
                                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: 10%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="progress-bar-label">10%</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="badge badge-danger">Very Bad Task <i
                                                                                        class="si si-dislike"></i>
                                                                                    </div>
                                                                                <?php elseif($resp->assesment_nilai === 'Sangat Kurang' && $rev->assesment_nilai === 'Kurang'):?>
                                                                                    <div class="progress push">
                                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: 10%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="progress-bar-label">10%</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="badge badge-danger">Very Bad Task <i
                                                                                        class="si si-dislike"></i>
                                                                                    </div>
                                                                            <?php else: ?>
                                                                                    <div class="progress push">
                                                                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                                                                            <span class="progress-bar-label">0%</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="badge badge-info">No Progress<i
                                                                                        class="si si-info"></i>
                                                                                    </div>
                                                                            <?php endif;?>
                                                                        <?php endforeach;?>
                                                                        <?php endforeach;?>
                                                                            <?php else: ?>
                                                                            <div class="progress push">
                                                                                <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                                                                    <span class="progress-bar-label">0%</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="badge badge-info">No Progress<i
                                                                                class="si si-info"></i>
                                                                            </div>
                                                                            <?php endif;?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <?php endforeach; ?>


                                </div>
                                <!-- <div style="margin-bottom:10px">
                                        <?php if($this->uri->segment(6) === 'alltask'): ?>
                                        <a href="<?php echo base_url().'admin/assignment/task/'.$userId.'/'.$token?>">
                                            View Ongoing</a>
                                        <?php else: ?>
                                        <a class=""
                                            href="<?php echo base_url().'admin/assignment/task/'.$userId.'/'.$token.'/alltask';?>">
                                            View All Task</a>
                                        <?php endif; ?>
                                    </div> -->
                                <?php else: ?>
                                <div class="block-content block-content-full text-center">
                                    <div class="gutters-tiny invisible" data-toggle="appear" style="margin:100px">
                                        <h2 style="color:#ABAAAA;font-weight:lighter">No Assignment available. <span
                                                style="font-size:15px;font-weight:lighter">
                                        </h2>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
        <!-- Footer -->
        <?php echo $this->load->view('admin/v_footer');?>
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->


    <form action="<?php echo base_url().''?>" method="post" enctype="multipart/form-data">
        <div class="modal fade" id="ModalCounter" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Counter Task</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="form-group">
                                <label for="">Upload Prove (Screen Shots/ Other Confirmation)</label>
                                <input type="file" class="dropify" required>
                            </div>
                            <div class="form-group">
                                <label for="">Describe Countering Notes</label>
                                <textarea type="text" name="xdeskripsi" class="form-control"
                                    placeholder="Write countering notes..." required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-square">Send Counter</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Modal Hapus -->

    <form action="<?php echo base_url().'admin/kategori/simpan_kategori'?>" method="post" enctype="multipart/form-data">
        <div class="modal fade" id="ModalCancel" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Cancelation Task</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="form-group">
                                <label for="">Describe Cancelation Notes</label>
                                <textarea type="text" name="xdeskripsi" class="form-control"
                                    placeholder="Write cancelation notes..." required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger btn-square">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- Modal Hapus -->


    <!-- Codebase Core JS -->
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });

        //Show Modal Update Kategori
        $('.btn-edit').on('click', function() {
            var kategori_id = $(this).data('id');
            var kategori_nama = $(this).data('kategori');
            var kategori_deskripsi = $(this).data('deskripsi');
            $('#ModalUpdate').modal('show');
            $('[name="xkode"]').val(kategori_id);
            $('[name="xkategori2"]').val(kategori_nama);
            $('[name="xdeskripsi"]').val(kategori_deskripsi);
        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var kategori_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(kategori_id);
        });

    });
    </script>

    <script type="text/javascript">
    $('.dropify').dropify({ //overate input type file
        messages: {
            default: 'Image / Files',
            replace: 'Ganti',
            remove: 'Hapus',
            error: 'error'
        }
    });
    $(function() {
        CKEDITOR.replace('ckeditor', {
            height: '240px',
            toolbar: [{
                    name: 'clipboard',
                    groups: ['clipboard', 'undo'],
                    items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo',
                        'Redo'
                    ]
                },
                {
                    name: 'editing',
                    groups: ['find', 'selection', 'spellchecker'],
                    items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']
                },
                '/',
                {
                    name: 'basicstyles',
                    groups: ['basicstyles', 'cleanup'],
                    items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript',
                        '-', 'CopyFormatting', 'RemoveFormat'
                    ]
                },
                {
                    name: 'paragraph',
                    groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                    items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-',
                        'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter',
                        'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language'
                    ]
                },
                {
                    name: 'links',
                    items: ['Link', 'Unlink', 'Anchor']
                },
                {
                    name: 'insert',
                    items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar',
                        'PageBreak', 'Iframe'
                    ]
                },
                '/',
                {
                    name: 'styles',
                    items: ['Styles', 'Format', 'Font', 'FontSize']
                },
                {
                    name: 'colors',
                    items: ['TextColor', 'BGColor']
                },
            ]
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });



        $('.btn-detail').on('click', function() {
            var work_deskripsi = $(this).data('deskripsi');
            $('#ModalDetail').modal('show');
            $('[name="deskripsi"]').val(work_deskripsi);
        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var work_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(work_id);
        });

    });
    </script>

    <?php if($this->session->flashdata('msg')=='gagal-hapus'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Error',
        text: "There is still assignment attached in that project",
        showHideTransition: 'slide',
        icon: 'error',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#FF4859'
    });
    </script>

    <?php elseif($this->session->flashdata('msg')=='show-modal'):?>
    <script type="text/javascript">
    $('#ModalResetPassword').modal('show');
    </script>
    <?php else:?>

    <?php endif;?>

</body>

</html>