<!-- Header -->
<header id="page-header"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Header Content -->
    <div class="content-header">
        <!-- Left Section -->
        <div class="content-header-section">
            <!-- Toggle Sidebar -->
            <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
         
            <div class="btn-group" role="group">

            <?php if($this->uri->segment(4) !== null && $this->uri->segment(2) == 'dashboard'): ?>
                <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-project"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $namaProject;?><i class="fa fa-angle-down ml-5"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-left min-width-550"
                    aria-labelledby="page-header-project">
                    <h5 class="h6 text-center py-10 mb-5 border-b text-uppercase">Projects</h5>
                    <?php foreach($kategori->result() as $project): ?>
                        <?php if($this->uri->segment(4) === $project->kategori_slug): ?>
                        <?php else: ?>
                            <a class="dropdown-item btnSubmit" href="<?php echo base_url().'admin/dashboard/project/'.$project->kategori_slug;?>" data-toggle="layout" data-action="side_overlay_toggle">
                                <img style="border-radius:30px" src="<?php echo base_url().'assets/images/business/'.$project->kategori_gambar;?>" width="30px"> <?php echo $project->kategori_nama;?>
                            </a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php if($akses === '5' || $akses === '1'): ?>
                    <div class="dropdown-divider"></div>
                    <h5 class="h6 text-center py-10 mb-5 border-b text-uppercase">Research & Dev</h5>

                    <?php endif; ?>
                </div>
            <?php else: ?>
            <?php endif; ?>

            <?php if($this->uri->segment(4) !== null && $this->uri->segment(2) == 'tim'): ?>
                <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-project"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $namaProject;?><i class="fa fa-angle-down ml-5"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-left min-width-550"
                    aria-labelledby="page-header-project">
                    <h5 class="h6 text-center py-10 mb-5 border-b text-uppercase">Projects</h5>
                    <?php foreach($kategori->result() as $project): ?>
                        <?php if($this->uri->segment(4) === $project->kategori_slug): ?>
                        <?php else: ?>
                            <a class="dropdown-item btnSubmit" href="<?php echo base_url().'admin/tim/project/'.$project->kategori_slug;?>" data-toggle="layout" data-action="side_overlay_toggle">
                                <img style="border-radius:30px" src="<?php echo base_url().'assets/images/business/'.$project->kategori_gambar;?>" width="30px"> <?php echo $project->kategori_nama;?>
                            </a>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <div class="dropdown-divider"></div>
                    <h5 class="h6 text-center py-10 mb-5 border-b text-uppercase">Research & Dev</h5>
                </div>
            <?php else: ?>
            <?php endif; ?>
            </div>
            <!-- END Toggle Sidebar -->


        </div>
        <!-- END Left Section -->

        <!-- Right Section -->
        <div class="content-header-section">
            <!-- User Dropdown -->
          

                <div class="btn-group" role="group">

                <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown1"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php echo $this->session->userdata('nama');?><i class="fa fa-angle-down ml-5"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right min-width-150"
                    aria-labelledby="page-header-user-dropdown1">
                    <?php if($akses === '5'): ?>
                    <a class="dropdown-item" href="<?php echo base_url().'admin/tim';?>" data-toggle="layout" data-action="side_overlay_toggle">
                        <i class="si si-users"></i> Team
                    </a>
                    <?php endif; ?>
                    <a class="dropdown-item" href="<?php echo base_url().'admin/loguser';?>" data-toggle="layout" data-action="side_overlay_toggle">
                        <i class="si si-clock"></i> User Activity
                    </a>
                    <a class="dropdown-item" href="<?php echo base_url().'admin/setting';?>" data-toggle="layout" data-action="side_overlay_toggle">
                        <i class="si si-wrench mr-5"></i> Settings
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?php echo site_url('login/logout');?>">
                        <i class="si si-logout mr-5"></i> Sign Out
                    </a>
                </div>
            </div>
            <!-- END User Dropdown -->

        </div>
        <!-- END Right Section -->
    </div>
    <!-- END Header Content -->

    <!-- Header Loader -->
    <!-- Please check out the Activity page under Elements category to see examples of showing/hiding it -->
    <div id="page-header-loader" class="overlay-header bg-primary">
        <div class="content-header content-header-fullrow text-center">
            <div class="content-header-item">
                <i class="fa fa-sun-o fa-spin text-white"></i>
            </div>
        </div>
    </div>
    <!-- END Header Loader -->
</header>
<!-- END Header -->