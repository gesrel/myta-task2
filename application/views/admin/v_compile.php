<!doctype html>
<html lang="en" class="no-focus">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Compiled Apps | bacotLu assignment</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

    <!-- END Icons -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>" />




</head>

<body>
    <!-- Page Container -->

    <div id="page-container"
        class="sidebar-o side-scroll main-content-boxed side-trans-enabled page-header-fixed <?php if($dark === 'dark'): ?>page-header-inverse sidebar-inverse <?php endif; ?>">


        <?php echo $this->load->view('admin/v_sidemenu.php');?>


        <!-- Header -->
        <?php echo $this->load->view('admin/header.php');?>
        <!-- END Header -->

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Compiled App List</h3>
                                <div class="block-options">
                                    <button class="btn btn-primary" id="btn-add-new"><span class="fa fa-plus"></span>
                                        New Compile</button>
                                </div>
                            </div>
                            <div class="block-content block-content-full">
                                <table id="mytable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 120px;text-align: left;">No</th>
                                            <th>Project</th>
                                            <th>APK / IPA Compiled</th>
                                            <th>Compiler</th>
                                            <th>QR Code</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $no=0;
                                            foreach ($data->result() as $row) :
                                            $no++;
                                        ?>
                                        <tr>
                                            <td><?php echo $no;?></td>
                                            <td>
                                                 <?php
                                                      $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$row->compile_kategori_id'");
                                                    
                                                    foreach($kategori->result() as $kat): ?>
                                                    <img src="<?php echo base_url().'assets/images/business/'.$kat->kategori_gambar;?>"
                                                        width="55px" alt="" class="img-circle">
                                                    <?php echo $kat->kategori_nama; ?>
                                                    <?php endforeach; ?>
                                            </td>
                                            <td>
                                                <?php echo $row->compile_nama; ?>
                                                <?php if($row->compile_apkipa === 'APK'): ?>
                                                    <img src="<?php echo base_url().'theme/images/android.png';?>" width="20px" alt="">
                                                <?php elseif($row->compile_apkipa === 'IPA'): ?>
                                                    <img src="<?php echo base_url().'theme/images/ios.png';?>" width="30px" alt="">
                                                <?php endif; ?>
                                            </td>
                                            <td><?php echo $row->compile_user;?></td>
                                            <td><img src="<?php echo base_url().'assets/images/qr/'.$row->compile_nama.'.png';?>" width="150px" alt=""></td>
                                            <td>
                                                <a title="Download" class="btn btn-circle btn-primary" href="<?php echo base_url().'assets/compile/'.$row->compile_file;?>" download>
                                                    <span class="fa fa-download"></span>
                                                </a>
                                                <a title="Delete" href="#!" data-toggle="modal" data-target="#ModalDelete<?php echo $row->compile_id;?>" class="btn btn-circle btn-danger">
                                                    <span class="fa fa-trash"></span>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
        <!-- Footer -->
        <?php echo $this->load->view('admin/v_footer');?>
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->
    
    
    <!-- Modal New -->
    <form action="<?php echo base_url().'admin/compile/simpan_compile'?>" method="post" enctype="multipart/form-data">
        <div class="modal fade in" id="ModalAddNew" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Add New Compile APK/IPA</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                           <div class="form-group">
                                <select class="form-control" name="xbusiness" required>
                                    <option value="">- Choose Project -</option>
                                    <?php $kategori = $this->db->query("SELECT * FROM kategori");
                                        foreach($kategori->result() as $kat): ?>
                                    <option value="<?php echo $kat->kategori_id;?>"><?php echo $kat->kategori_nama; ?></option>
                                    <?php endforeach; ?>

                                </select>
                            </div>
                            <div class="form-group">
                                <input type="text" name="xnama" class="form-control" placeholder="Compile Name / Version"
                                    required>
                            </div>
                             <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="file" name="apk" class="dropify" size="100" data-height="140" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-square">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END Normal Modal -->
    
    
    <!-- Modal Hapus -->
    <?php foreach($data->result() as $row): ?>
    <form action="<?php echo base_url().'admin/compile/hapus_compile'?>" method="post">
        <div class="modal" id="ModalDelete<?php echo $row->compile_id; ?>" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Info</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <p>Are you sure want to delete this file?</p>
                            <input type="hidden" name="xkode" value="<?php echo $row->compile_id ?>" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-primary btn-square">Yes</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END Normal Modal -->
    <?php endforeach; ?>

    <!-- Codebase Core JS -->
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
     <script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });

        //Show Modal Update Kategori
        $('.btn-edit').on('click', function() {
            var kategori_id = $(this).data('id');
            var kategori_nama = $(this).data('kategori');
            var kategori_deskripsi = $(this).data('deskripsi');
            $('#ModalUpdate').modal('show');
            $('[name="xkode"]').val(kategori_id);
            $('[name="xkategori2"]').val(kategori_nama);
            $('[name="xdeskripsi"]').val(kategori_deskripsi);
        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var kategori_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(kategori_id);
        });

    });
    </script>
    
    <script type="text/javascript">
    $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Upload APK/IPA File',
                replace: 'Change',
                remove: 'Delete',
                error: 'error'
            }
        });
</script>
  
    <?php if($this->session->flashdata('msg')=='gagal-upload'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Error',
        text: "Gagal Upload <?php echo $this->session->flashdata('errorupload');?>",
        showHideTransition: 'slide',
        icon: 'error',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#FF4859'
    });
    </script>
      <?php elseif($this->session->flashdata('msg')=='success-insert'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Success',
        text: "Compiled file submitted.",
        showHideTransition: 'slide',
        icon: 'success',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#7EC857'
    });
    </script>
    <?php elseif($this->session->flashdata('msg')=='info'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Update',
        text: "Compiled file updated.",
        showHideTransition: 'slide',
        icon: 'success',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#7EC857'
    });
    </script>
    <?php elseif($this->session->flashdata('msg')=='success-hapus'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Success',
        text: "Compiled file deleted.",
        showHideTransition: 'slide',
        icon: 'success',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#7EC857'
    });
    </script>
    <?php elseif($this->session->flashdata('msg')=='show-modal'):?>
    <script type="text/javascript">
    $('#ModalResetPassword').modal('show');
    </script>
    <?php else:?>

    <?php endif;?>

</body>

</html>