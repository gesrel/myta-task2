<!doctype html>
<html lang="en" class="no-focus">
<!--<![endif]-->

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

<title><?php if($this->uri->segment(4) !== null): echo $namaProject;?><?php else: ?> MYTA | Dashboard<?php endif; ?></title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Codebase framework -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">

    <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/js/plugins/select2/select2.css'?>">
    <link rel="stylesheet" type="text/css"
        href="<?php echo base_url().'assets/js/plugins/sweetalert2/sweetalert2.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>" />
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/datetimepicker/jquery-ui-timepicker-addon.css';?>" />
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/magnific-popup/magnific-popup.css'?>" />

    <style>
    .cover-spin {
        position:fixed;
        width:100%;
        left:0;right:0;top:0;bottom:0;
        <?php if($dark === 'dark'): ?>
            background-color: rgba(0,0,0,0.8);
        <?php else:?>
            background-color: rgba(255,255,255,0.8);
        <?php endif;?>
        z-index:9999;
        display:none;
    }
    @-webkit-keyframes spin {
        from {-webkit-transform:rotate(0deg);}
        to {-webkit-transform:rotate(360deg);}
    }

    @keyframes spin {
        from {transform:rotate(0deg);}
        to {transform:rotate(360deg);}
    }
    .cover-spin::after {
        content:'';
        display:block;
        position:absolute;
        left:50%;top:40%;
        width:40px;height:40px;
        border-style:solid;
        <?php if($dark === 'dark'): ?>
            border-color:white;
        <?php else:?>
            border-color:black;
        <?php endif;?>
        border-top-color:transparent;
        border-width: 4px;
        border-radius:50%;
        -webkit-animation: spin .8s linear infinite;
        animation: spin .8s linear infinite;
    }
    .cover-spin p{
        margin: auto;
        width: 100%;
        top: 48%;
        left:49.5%;
        position: absolute;
        font-size: 16px;
        color: #ccc;
    }
    </style>

</head>

<body>
    <!-- Page Container -->

    <div id="cover-spin" class="cover-spin"></div>

    <div id="page-container"
class="sidebar-o side-scroll main-content-boxed side-trans-enabled page-header-fixed <?php if($this->uri->segment(4) === null):?>page-header-glass <?php endif; ?><?php if($dark === 'dark'): ?>page-header-inverse sidebar-inverse <?php endif; ?>">
        <?php echo $this->load->view('admin/v_sidemenu.php');?>

        <?php echo $this->load->view('admin/header.php');?>

        <!-- Main Container -->
        <?php if($dark === 'dark'){
                $slide = 'https://ifabula.com/images/slider1.jpg';
                $fontColor = 'text-white';
            }else{
                $slide = base_url().'theme/images/slide2.jpg';
                $fontColor = 'text-black';
            }?>
        <main id="main-container" class="gutters-tiny invisible" data-toggle="appear">
        <?php if($this->session->userdata('bannerLogin') === 'true' || $akses !== '1' && $this->uri->segment(4) === null): ?>
            <div id="banner" class="bg-image"
                style="margin-top:-100:100;background-color:#f1edec;background-size:100%;background-position:cover;background-repeat:no-repeat;background-image: url(<?php echo $slide;?>);filter:opacity(100%)">
                <div class="content content-top text-left overflow-hidden"><br>
                    <div class="pt-30 pb-20">
                        <h2 class="font-w700 <?php echo $fontColor;?> mb-5 invisible" data-toggle="appear"
                            data-class="animated fadeInDown">Hello, <?php echo $this->session->userdata('nama');?>
                        </h2>
                        <h3 class="h4 font-w300 <?php echo $fontColor;?>-op invisible" data-toggle="appear"
                            data-class="animated fadeInDown">Welcome to
                            <?php if($akses === '4'): ?>
                            Business Analyst
                            <?php elseif($akses === '2'): ?>
                            Project Manager
                            <?php elseif($akses === '5'): ?>
                            Tech Lead
                            <?php elseif($akses === '3'): ?>
                            Developer
                            <?php elseif($akses === '1'): ?>
                            Admin
                            <?php endif; ?> desk, do the best for today <span class="si si-emoticon-smile"></span></h3>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <div class="content">
                <div class="row">
                <?php if($this->uri->segment(4) === null): ?>
                    <?php if($kategori->num_rows() > 0): ?>
                    <?php foreach($kategori->result() as $project): ?>
                        <div class="col-md-6 col-xl-3">
                            <a href="<?php echo base_url().'admin/dashboard/project/'.$project->kategori_slug;?>" class="block block-link-pop text-center projectItem">
                                <div class="block-content block-content-full block-sticky-options pt-30 bg-lighter ribbon ribbon-top ribbon-bookmark ribbon-success">
                                    <div class="ribbon-box">
                                        <?php echo $project->kategori_status; ?>
                                    </div>
                                    <img width="40%" style="border-radius:30px" src="<?php echo base_url().'assets/images/business/'.$project->kategori_gambar;?>">
                                </div>
                                <div class="block-content block-content-full block-content-sm <?php if($dark === 'dark'):?> bg-primary-dark <?php else: ?> bg-corporate<?php endif;?>">
                                    <div class="font-w600 mb-5 text-white"><?php echo $project->kategori_nama;?></div>
                                    <div class="font-size-sm text-white"><span class="si si-control-play"></span> <?php echo date("l, d M Y", strtotime($project->kategori_tanggal));?> <hr> <span class="si si-user"></span> <?php echo $project->pengguna_nama; ?></div>
                                </div>
                                <div class="block-content bg-info text-white">
                                    <div class="row items-push">
                                        <div class="col-4">
                                            <?php $sprint = $this->db->query("SELECT * FROM sprint where sprint_kategori_id='$project->kategori_id'")->num_rows();?>
                                            <div class="mb-5"><i class="si si-notebook fa-2x"></i></div>
                                            <div class="font-size-sm text-white"><span data-toggle="countTo" data-to="<?php echo $sprint; ?>" data-speed="2000"><?php echo $sprint; ?> </span> Sprint</div>
                                        </div>
                                        <div class="col-4">
                                            <?php $task = $this->db->query("SELECT * FROM work where work_kategori_id='$project->kategori_id'")->num_rows();?>
                                            <div class="mb-5"><i class="si si-layers fa-2x"></i></div>
                                            <div class="font-size-sm text-white"><span data-toggle="countTo" data-to="<?php echo $task; ?>" data-speed="2000"> <?php echo $task; ?></span> Task</div>
                                        </div>
                                        <div class="col-4">
                                            <?php $tim = $this->db->query("SELECT * FROM tim where tim_kategori_id='$project->kategori_id'")->num_rows();?>
                                            <div class="mb-5"><i class="si si-users fa-2x"></i></div>
                                            <div class="font-size-sm text-white"><span data-toggle="countTo" data-to="<?php echo $tim; ?>" data-speed="2000"> <?php echo $tim; ?></span> Tim</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                    <?php else: ?>
                        <div class="col-md-12">
                            <div class="content content-full">
                                <div class="py-30 text-center">
                                    <div class="display-3 text-corporate">
                                        <img src="<?php echo base_url().'theme/images/favicon.png'?>" width="30px">
                                    </div>
                                    <h1 class="h2 font-w700 mt-30 mb-10">Oops.. There's no project available..</h1>
                                    <h2 class="h3 font-w400 text-muted mb-50">We are sorry but you can not use this app for now..</h2>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php if($allprojectName === ''): ?>
                    <?php if($akses === '2' || $akses === '1'): ?>
                        <div class="col-md-12 otherCon">
                    <?php else: ?>
                        <div class="col-md-8 otherCon">
                    <?php endif; ?>
                        <div class="block" style="border-radius:20px">
                            <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#btabs-animated-slideleft-home"><span
                                            class="si si-info"></span> Project Info</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#btabs-animated-slideleft-pm">
                                        <span class="si si-users"></span> Team 
                                        <?php if($user->num_rows() > 0): ?>
                                            <span class="badge badge-pill badge-primary"><?php $total = $user->num_rows() + 1 ; echo $total; ?></span></a>
                                        <?php endif; ?>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#btabs-animated-slideleft-apk"><span
                                            class="si si-screen-smartphone"></span> Deployment
                                        <?php if($latestAPK->num_rows() > 0): ?>
                                            <span class="badge badge-pill badge-danger"><?php echo $latestAPK->num_rows(); ?></span></a>
                                        <?php endif; ?>
                                    </a>
                                </li>
                                <?php if($akses === '1' || $akses === '2'): ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="#btabs-animated-slideleft-report"><span
                                            class="si si-docs"></span> Report</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#btabs-animated-slideleft-charts"><span
                                            class="si si-bar-chart"></span> Progress Chart</a>
                                </li>
                                <?php endif; ?>
                            </ul>
                            <div class="block-content tab-content overflow-hidden">
                                <div class="tab-pane fade fade-left show active" id="btabs-animated-slideleft-home"
                                    role="tabpanel">
                                    <div class="row" style="margin-bottom:20px">
                                        <div class="col-2">
                                            <img src="<?php echo base_url().'assets/images/business/'.$projectName['kategori_gambar'];?>"
                                                width="80%" style="border-radius:40px">
                                        </div>
                                        <div class="col-9">
                                            <h3 class="block-title">
                                                <?php echo $projectName['kategori_nama']; ?>
                                            </h3>
                                            <p>
                                                <small><?php echo $projectName['kategori_deskripsi'];?></small>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade fade-left" id="btabs-animated-slideleft-pm" role="tabpanel">
                                    <div style="margin-bottom:20px">
                                        <div class="row" style="margin-left:-30px">
                                            <?php foreach($timLeader->result() as $dv1): 
                                                $linkImg = $dv1->pengguna_photo;
                                                if($linkImg === '' || $linkImg === null){
                                                    if($dv1->pengguna_jenkel === 'L'){
                                                        $linkImg = 'user_blank.png';
                                                    }else{
                                                        $linkImg = 'user_blank2.png';
                                                    }
                                                }
                                                ?>
                                            <div class="col-2">
                                                    <center>
                                                    <img src="<?php echo base_url().'assets/images/'.$linkImg;?>" width="50px" height="50px" style="border-radius:50px" alt="" data-toggle="popover"
                                                        title="Tech Lead <?php echo $projectName['kategori_nama'];?>" data-placement="right"><br>
                                                    <?php if($agent !== true): ?>
                                                    <span style="font-size:9px">
                                                        <b><?php echo $dv1->pengguna_nama;?></b><br>
                                                        (Tech Lead)
                                                    </span>
                                                    <?php endif; ?>
                                                    </center>
                                                </div>
                                            <?php endforeach; ?>
                                                <?php foreach($user->result() as $dv):
                                                     $linkImg = $dv->pengguna_photo;
                                                     if($linkImg === '' || $linkImg === null){
                                                         if($dv->pengguna_jenkel === 'L'){
                                                             $linkImg = 'user_blank.png';
                                                         }else{
                                                             $linkImg = 'user_blank2.png';
                                                         }
                                                     }
                                                    ?>
                                                <div class="col-2">
                                                    <center><img src="<?php echo base_url().'assets/images/'.$linkImg;?>" width="50px" height="50px" style="border-radius:50px" data-toggle="popover"
                                                        title="<?php echo $dv->pengguna_nama; ?>" data-placement="right"
                                                        data-content="Invited at <?php echo date("d M Y", strtotime($dv->tim_tgl)); ?>"><br>
                                                    <span style="font-size:9px">
                                                    <?php if($agent !== true): ?>
                                                        <b><?php echo $dv->pengguna_nama;?></b></br>
                                                        (<?php if($dv->pengguna_level === '4'): ?>
                                                        Business Analyst
                                                        <?php elseif($dv->pengguna_level === '3'): ?>
                                                        Developer
                                                        <?php else: ?>
                                                        Project Manager
                                                        <?php endif; ?>)
                                                    </span>
                                                    <?php endif; ?>
                                                    </center>
                                                </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                </div>
                               
                                <div class="tab-pane fade fade-left" id="btabs-animated-slideleft-apk" role="tabpanel">
                                    <?php if($latestAPK->num_rows() > 0): ?>
                                    <div class="row">
                                        <?php foreach($latestAPK->result() as $apk): ?>
                                            <div class="col-4">
                                                <div class="form-group" style="background-color:#f0f0f0;padding:10px;border-radius:10px">
                                                    <?php if($apk->compile_apkipa === 'APK'): ?>
                                                        <img src="<?php echo base_url().'theme/images/android.png';?>" width="20px" alt="">
                                                        <?php echo $apk->compile_nama; ?><br>
                                                        <a href="#!" style="width:100%" data-toggle="modal"
                                                        data-target="#ModalQR<?php echo $apk->compile_id;?>"
                                                        class="badge badge-sm badge-primary" download>
                                                        <span class="fa fa-mobile"></span> Install
                                                        <?php else: ?>
                                                        <img src="<?php echo base_url().'theme/images/ios.png';?>" width="25px" alt="">
                                                        <?php echo $apk->compile_nama; ?><br>
                                                    
                                                        <a href="<?php echo base_url().'theme/compile/'.$apk->compile_file; ?>" style="width:100%"
                                                        class="badge badge-sm badge-primary" download>
                                                        <span class="fa fa-download"></span> Download
                                                    <?php endif; ?>
                                                   
                                                </a>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <?php else: ?>
                                    <p style="color:#ddd">No Builds available.</p>
                                    <?php endif ?>
                                    
                                </div>
                                <?php if($akses === '1' || $akses === '2'): ?>
                                <div class="tab-pane fade fade-left" id="btabs-animated-slideleft-report" role="tabpanel">
                                    <div class="row">
                                        <div class="col-6 col-md-4 col-xl-2">
                                            <a class="block block-transparent text-center bg-primary" href="javascript:void(0)">
                                                <div class="block-content">
                                                    <p class="mt-5">
                                                        <i class="si si-bar-chart fa-4x text-white-op"></i>
                                                    </p>
                                                    <p class="font-w600 text-white">Whole Project <span class="si si-cloud-download"></span>.xlsx</p>
                                                    
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-4 col-xl-2">
                                            <a class="block block-transparent text-center bg-secondary" href="javascript:void(0)">
                                                <div class="block-content">
                                                    <p class="mt-5">
                                                        <i class="si si-users fa-4x text-white-op"></i>
                                                    </p>
                                                    <p class="font-w600 text-white">Team Assesment <span class="si si-cloud-download"></span>.xlsx</p>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-4 col-xl-2">
                                            <a class="block block-transparent text-center bg-info" href="javascript:void(0)">
                                                <div class="block-content">
                                                    <p class="mt-5">
                                                        <i class="si si-speech fa-4x text-white-op"></i>
                                                    </p>
                                                    <p class="font-w600 text-white">Task Request</p>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-4 col-xl-2">
                                            <a class="block block-transparent text-center bg-warning" href="javascript:void(0)">
                                                <div class="block-content">
                                                    <p class="mt-5">
                                                        <i class="si si-clock fa-4x text-white-op"></i>
                                                    </p>
                                                    <p class="font-w600 text-white">Task Passing Deadline</p>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-4 col-xl-2">
                                            <a class="block block-transparent text-center bg-danger" href="javascript:void(0)">
                                                <div class="block-content">
                                                    <p class="mt-5">
                                                        <i class="si si-ban fa-4x text-white-op"></i>
                                                    </p>
                                                    <p class="font-w600 text-white"><?php echo $totalCanceled; ?> Task Canceled</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade " id="btabs-animated-slideleft-charts" role="tabpanel">
                                <div class="content content-full">
                                    <div class="py-30 text-center">
                                        <div class="display-3 text-corporate">
                                            <i class="si si-wrench"></i>
                                        </div>
                                            <h1 class="h2 font-w700 mt-30 mb-10">Oops.. Under Construction.</h1>
                                            <h2 class="h3 font-w400 text-muted mb-50">We are sorry but this feature still under construction..</h2>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                    <?php if($akses === '4' || $akses === '5' || $akses === '3'): ?>
                        <div class="col-md-4 otherCon">
                            <div class="block" style="border-radius:20px">
                                <div class="block-header">
                                    <h3 class="block-title">
                                    Task <small> Request</small>
                                    </h3>
                                    <span id="spinner" class="spinner" style="display:none"></span>
                                    <div class="block-options">
                                    </div>
                                </div>
                                <div class="block-content block-content-full">
                                <?php if($requested->num_rows() > 0): ?>
                                    <table class="table table-striped">
                                        <thead>
                                            <th>
                                                <?php echo $pic; ?>
                                            </th>
                                            <th>Task</th>
                                            <th style="text-align:right">Action</th>
                                        </thead>
                                        <tbody>
                                       <?php 
                                         foreach($requested->result() as $req):?>
                                            <tr>
                                                <td>
                                                    <?php 
                                                        $linkImg = $req->pengguna_photo;
                                                        if($linkImg === '' || $linkImg === null){
                                                            if($req->pengguna_jenkel === 'L'){
                                                                $linkImg = 'user_blank.png';
                                                            }else{
                                                                $linkImg = 'user_blank2.png';
                                                            }
                                                        }?>
                                                        <img src="<?php echo base_url().'assets/images/'.$linkImg;?>" width="40" height="40" style="border-radius:50px">
                                                        <?php echo $req->pengguna_nama; ?>
                                                </td>
                                                <td>
                                                    <a href="#!" class="btnDetail" data-id="<?php echo $req->work_id;?>">
                                                        <?php echo $req->work_nama; ?>
                                                        </a><br>
                                                        <span class="badge badge-warning"><i class="fa fa-clock-o"></i> <?php echo date('d M Y', strtotime($req->work_tgl));?></span>

                                                </td>
                                                </td>

                                                <td class="pull-right">
                                                    <div class="row">
                                                        <?php if($akses === '4' || $akses === '5'): ?>
                                                        <div class="col-2">
                                                            <button style="cursor: pointer" title="Approve"
                                                                    href="javascript:void(0);"
                                                                    data-id="<?php echo $req->work_id; ?>"
                                                                    data-slug="<?php echo $this->uri->segment(4);?>"
                                                                    class="btn btn-sm btn-primary btn-circle btnAcc">
                                                                    <span class="si si-check"></span>
                                                            </button> 
                                                        </div>
                                                        <div class="col-2">
                                                            <button style="cursor: pointer" title="Reject"
                                                                    href="javascript:void(0);"
                                                                    data-id="<?php echo $req->work_id; ?>"
                                                                    data-slug="<?php echo $this->uri->segment(4);?>"
                                                                    class="btn btn-sm btn-danger btn-circle btnReject">
                                                                    <span class="fa fa-warning"></span>
                                                            </button> 
                                                        </div>
                                                    <?php else: ?>
                                                    <div class="col-2">
                                                        <button style="cursor: pointer" title="Cancel"
                                                            href="javascript:void(0);"
                                                            data-id="<?php echo $req->work_id; ?>"
                                                            data-slug="<?php echo $this->uri->segment(4);?>"
                                                            data-nama="<?php echo $req->work_nama; ?>"
                                                            class="btn btn-sm btn-danger btn-circle btnCancel">
                                                            <span class="si si-close"></span>
                                                        </button>  
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php else: ?>
                                        <p style="color:#ddd;text-align:center">No Request available.</p>
                                    <?php endif ?>

                                </div>
                            </div>
                            </div>
                            <?php endif; ?>
                            
                    
                    <?php else: ?>
                    <?php endif; ?>

                </div>
                <!-- Row #2 -->
              
                <div class="row">
                    <div class="row" id="ModalDetail" style="width:100%;height:100%">
                    <div class="js-chat-container row no-gutters content content-full" style="margin-top:-30px">
                        <div class="js-chat-options d-none d-md-block col-md-6 col-lg-4 bg-white border-right">
                            <div class="js-chat-logged-user m-15">
                                <div class="d-flex align-items-center">
                                    <a class="img-link img-status" href="javascript:void(0)">
                                        <img class="img-avatar img-avatar" style="width:50px;height:50px" id="kategori_gambar1" alt="Avatar">
                                        <div class="img-status-indicator bg-success"></div>
                                    </a>
                                    <div class="ml-10">
                                        <a class="font-w600" href="javascript:void(0)" id="nama_task"></a>
                                        <div class="font-size-sm text-muted" id="namaSprint"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="block block-transparent mb-0">
                                <ul class="js-chat-tabs nav nav-tabs nav-tabs-alt nav-justified px-15" data-toggle="tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#chat-tabs-chats">
                                            <i class="si si-user text-muted font-size-lg"></i>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#chat-tabs-timeline">
                                            <i class="si si-clock text-muted font-size-lg"></i>
                                        </a>
                                    </li>
                                </ul>
                                <div class="js-chat-tabs-content block-content tab-content p-0">
                                    <div style="overflow:auto;width:100%;height:500px" class="tab-pane active p-15" id="chat-tabs-chats" role="tabpanel" data-simplebar>
                                        <div class="push">
                                            <div class="d-flex align-items-center">
                                                <a class="img-link img-status" href="javascript:void(0)">
                                                    <img class="img-avatar img-avatar" style="width:50px;height:50px    " id="work_assignor_gambar" alt="Avatar">
                                                    <div class="img-status-indicator bg-success"></div>
                                                </a>
                                                <div class="ml-10">
                                                    <a class="font-w600" href="javascript:void(0)" id="work_assignor"></a>
                                                    <div class="font-size-sm text-muted">PIC / DIC</div>
                                                </div>
                                            </div><br>
                                            <div class="d-flex align-items-center justify-content-between mb-5">
                                                <span class="font-w600 font-size-xs text-muted text-uppercase">Description</span>
                                            
                                            </div>
                                            <div id="deskripsi_task"></div>
                                        </div>
                                    </div>
                                    <div style="overflow:auto;height:350px;width:600px" class="tab-pane p-15" id="chat-tabs-timeline" role="tabpanel" data-simplebar>
                                    </div>
                                </div>
                            </div>
                            <div class="d-md-none py-5 bg-body-dark"></div>
                        </div>
                        <div class="col-md-6 col-lg-8 bg-white d-flex flex-column">
                            <div class="js-chat-active-user p-15 d-flex align-items-center justify-content-between bg-white">
                                <div class="d-flex align-items-center">
                                </div>
                                <div class="ml-10">
                                    <a style="cursor:pointer" id="btnCancelModalDetail" data-toggle="dropdown">
                                        <i class="si si-close"></i>
                                    </a>
                                    <button type="button" class="d-md-none btn btn-sm btn-circle btn-alt-success ml-5" data-toggle="class-toggle" data-target=".js-chat-options" data-class="d-none">
                                        <i class="fa fa-bars"></i>
                                    </button>
                                </div>
                            </div>
                            <div id="scrollDiv" class="js-chat-window p-15 bg-light flex-grow-1 text-wrap-break-word overflow-y-auto" style="overflow:auto;height:500px">
                                <div class="d-flex mb-20">
                                    <div>
                                        <a class="img-link img-status" href="javascript:void(0)">
                                            <img class="img-avatar img-avatar" style="width:40px;height:30px" src="<?php echo base_url().'theme/images/favicon.png';?>" alt="Avatar">
                                            <div class="img-status-indicator bg-success"></div>
                                        </a>
                                    </div>
                                    <div class="mx-10">
                                        <div>
                                            <p class="bg-body-dark text-dark rounded px-15 py-10 mb-5">
                                            <label for="">Myta</label><br>
                                                Hello there! if you had couple question about this task, ask your team mates
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div id="commentContainer" class="items-push js-gallery img-fluid-100"></div>
                                <div class="d-flex flex-row-reverse mb-20 " >
                                    <div class="onlineContainer">
                                        <a class="img-link img-status" href="javascript:void(0)">
                                            <img id="onlineUserGambar" class="img-avatar img-avatar32" alt="Avatar">
                                        </a>
                                    </div>
                                    <div class="mx-10 onlineContainer">
                                        <div>
                                            <p id="onlineUserNama" class="bg-primary-lighter text-primary-darker rounded px-15 py-10 mb-5 d-inline-block"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="js-chat-message p-10 mt-auto">
                                <form id="formComment" action="<?php echo base_url().'admin/comment/tambah_comment';?>" method="post" enctype="multipart/form-data">
                                    <div style="margin-bottom:10px;margin-top:10px;width:100%" class="conEmot">
                                        <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="smile.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/smile.gif'?>"></button>
                                        <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="laugh.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/laugh.gif'?>"></button>
                                        <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="cry.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/cry.gif'?>"></button>
                                        <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="tears.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/tears.gif'?>"></button>
                                        <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="shock.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/shock.gif'?>"></button>
                                        <!--<button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="tang.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/tang.gif'?>"></button>-->
                                        <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="think.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/think.gif'?>"></button>
                                        <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="cool.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/cool.gif'?>"></button>
                                        <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="love.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/love.gif'?>"></button>
                                        <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="sweat.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/sweat.gif'?>"></button>
                                        <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="ok.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/ok.gif'?>"></button>
                                        <button style="cursor:pointer;margin-bottom:10px;background-color:#fff" class="btn btnSubmit" name="emoticon" value="clap.gif"><img style="width:30px" alt="emo" src="<?php echo base_url().'assets/images/attachment/clap.gif'?>"></button>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 attachment">
                                            <input type="file" data-height="70" name="attach1" class="form-control dropify" data-max-file-size-preview="1M">
                                        </div>
                                        <div class="col-md-3 attachment">
                                            <input name="attach2" data-height="70"  type="file" class="form-control dropify">
                                        </div>
                                        <div class="col-md-3 attachment">
                                            <input name="attach3" data-height="70" type="file" class="form-control dropify" >
                                        </div>
                                        <div class="col-md-3 attachment">
                                            <input name="attach4" data-height="70" type="file" class="form-control dropify">
                                        </div>
                                        <div style="margin-left:20px" class="attachment">
                                            <small>2MB Max Size / Attachment</small>
                                        </div>
                                        <input type="hidden" id="idForNewComment">
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <a href="javascript:void(0)" onclick="$('.conEmot').fadeIn()" class="btn btn-alt-secondary btn-circle mr-5">
                                            <i class="fa fa-fw fa-smile-o font-size-lg"></i>
                                        </a>
                                        <a href="javascript:void(0)" id="btnAttach" onclick="$('.attachment').fadeIn()" class="btn btn-alt-secondary btn-circle mr-5">
                                            <i class="fa fa-fw fa-paperclip font-size-lg"></i>
                                        </a>
                                        <input type="hidden" name="taskId" id="taskId" required>
                                        <input type="hidden" name="screen" value="dashboard" required>
                                        <textarea style="height:40px" type="text" class="form-control flex-grow mr-5" placeholder="Type a message.." name="isi" id="xcomment"></textarea>
                                        <button type="submit" class="btn btn-circle btn-alt-primary">
                                            <i class="fa fa-share-square"></i>
                                        </button>    
                                    </div><br>
                                </form>
                            </div>
                        </div>
                    </div>
                        
                    </div>
                    <?php if($this->uri->segment(4) !== null): ?>
                    <?php if($akses === '4' || $akses === '5'): ?>
                    <div class="col-md-8 otherCon">
                        <?php else: ?>
                        <?php if($akses == '1' || $akses =='2'): ?>
                        <div class="col-md-12 otherCon">
                        <?php else: ?>
                        <div class="col-md-8 otherCon">
                            <?php endif; ?>
                            <?php endif; ?>
                            <div class="block" style="border-radius:20px">
                            <div class="block-header">
                                    <h3 class="block-title">
                                        In Progress <small> Task </small>
                                    </h3>
                                    <div class="block-options">
                                    <!-- <a style="cursor: pointer"class="btn btn-outline-secondary btn-sm" href="<?php echo base_url().'admin/calendar';?>">
                                        <i class="si si-calendar"></i> Calendar
                                    </a> -->
                                    <?php if($akses === '4' || $akses === '5' || $akses === '3'): ?>
                                    <a style="cursor: pointer"class="btn btn-outline-secondary btn-sm" href="<?php echo base_url().'admin/scrumboard';?>">
                                        <i class="si si-layers"></i> Scrum Board
                                    </a>
                                    <?php endif; ?>
                                        <?php if($akses === '4' || $akses === '5'): 
                                            
                                            if($this->uri->segment(4) !== null):?>
                                        
                                        <button style="cursor: pointer" type="button" class="btn btn-outline-secondary btn-sm btn-add-new" data-slug="<?php echo $this->uri->segment(4); ?>">
                                            <i class="si si-plus"></i> Create Task
                                        </button>
                                        <?php endif; ?>
                                        <?php elseif($akses === '3'): ?>
                                            <?php if($allprojectName === ''): ?>
                                                
                                                <button style="cursor: pointer" type="button" class="btn btn-outline-secondary btn-sm btn-add-new" data-toggle="modal"
                                                data-slug="<?php echo $this->uri->segment(4); ?>">
                                                    <i class="si si-plus"></i> Request Task
                                                </button>
                                            <?php endif; ?>
                                        <?php else: ?>
                                        <?php endif; ?>
                                        <!-- <a class="btn btn-primary btn-square btn-sm" href="<?php echo base_url().'admin/scrumboard';?>">
                                            View all task via Scrumboard
                                        </a> -->

                                    </div>
                                </div>
                                <div class="block">
                                    <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                                        <?php if($this->uri->segment(4) !== null): ?>
                                            <?php foreach($listSprint->result() as $sprint): 
                                                if($exactSprint === $sprint->sprint_id):?>
                                                    <li class="nav-item">
                                                        <form id="formSprint" action="<?php echo base_url().'admin/dashboard/project/'.$this->uri->segment(4);?>" method="get">
                                                            <input type="hidden" value="<?php echo $sprint->sprint_id;?>" name="xsprint">
                                                            <button style="background-color:#fff;color:#000;cursor:pointer" type="submit" class="nav-link active btnSubmit" href="#btabs-animated-slideleft-1"><?php echo $sprint->sprint_nama; ?></button>
                                                        </form>
                                                    </li>
                                                <?php else: ?>
                                                    <form id="formSprint" action="<?php echo base_url().'admin/dashboard/project/'.$this->uri->segment(4);?>" method="get">
                                                        <input type="hidden" value="<?php echo $sprint->sprint_id;?>" name="xsprint">
                                                        <button type="submit" style="background-color:#F3FBFF;color:#000;cursor:pointer" class="nav-link btnSubmit" href="#btabs-animated-slideleft-2"><?php echo $sprint->sprint_nama; ?></button>
                                                    </form>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                            
                                        <?php endif; ?>
                                    </ul>
                                    <div id="ongoingCon" class="block-content tab-content overflow-hidden">
                                        <div class="tab-pane fade fade-left show active" id="btabs-animated-slideleft-1">
                                            <?php if($ongoing->num_rows() === 0): ?>
                                            <div class="block-content block-content-full text-center">
                                                <div class="gutters-tiny invisible" data-toggle="appear" style="margin:100px">
                                                    <h2 style="color:#ABAAAA;font-weight:lighter">Sorry, there's no task available
                                                        . <br>
                                                        <?php if($akses === '3'): ?>
                                                            <span style="font-size:15px;font-weight:lighter">click at the request task above to start working</span>
                                                        <?php elseif($akses === '4' || $akses === '5'): ?>
                                                            <span style="font-size:15px;font-weight:lighter">click at the create task above to start monitoring</span>
                                                        <?php endif; ?>
                                                    </h2>
                                                </div>
                                            </div>
                                            <?php else: ?>
                                                <table class="table table-striped datatable">
                                                    <thead>
                                                        <th style="width:180px">Find Date / <?php echo $pic; ?></th>
                                                        <th>Task</th>
                                                        <th>Deadline</th>
                                                        <th style="width:100px">Status</th>
                                                        <?php if($akses === '2'): ?>
                                                        <?php else: ?>
                                                            <th>Action</th>
                                                        <?php endif; ?>
                                                    </thead>
                                                    <tbody>
                                                    <?php     
                                                    foreach($ongoing->result() as $ong):?>
                                                        <?php
                                                            $today = date('Y-m-d H:i');
                                                            $deadLine = date('Y-m-d H:i', strtotime($ong->work_deadline));
                                                            $dead = false;
                                                            $deadLineCommand = false;
                                                        if($deadLine === $today): 
                                                            $dead = true; 
                                                            $deadLineCommand = false;
                                                        elseif($today > $deadLine): 
                                                            $dead = true; 
                                                            $deadLineCommand = true ; 
                                                        else : 
                                                            $dead = false; 
                                                            $deadLineCommand = false;
                                                        endif;?>
                                                        <tr>
                                                            <td>
                                                                <small><?php echo date("d M Y H:i A", strtotime($ong->work_tgl));?></small><br>
                                                                <?php
                                                                    $linkImg = $ong->pengguna_photo;
                                                                    if($linkImg === '' || $linkImg === null){
                                                                        if($ong->pengguna_jenkel === 'L'){
                                                                            $linkImg = 'user_blank.png';
                                                                        }else{
                                                                            $linkImg = 'user_blank2.png';
                                                                        }
                                                                    }
                                                                    if($akses === '3'){
                                                                      $assignor = $this->db->query("SELECT * FROM pengguna where pengguna_id='$ong->work_assignor_id'")->row_array();
                                                                    }
                                                                ?>
                                                                <?php if($ong->work_assignor_id != ''): ?>
                                                                <img src="<?php echo base_url().'assets/images/'.$linkImg;?>"
                                                                    width="35px" style="border-radius:50px" class="img-circle">
                                                                <?php echo $ong->pengguna_nama; ?>
                                                                <?php else: ?>
                                                                    <span class="badge badge-secondary">No Assign</span>
                                                                <?php endif; ?>
                                                                <?php if($akses === '4' || $akses === '5'): ?>
                                                                    <?php if($ong->work_status === 'D' || $ong->work_status === 'E' || $ong->work_status === 'O'): ?>
                                                                    <?php else: ?>
                                                                        <?php if($dead == true): ?>
                                                                        <div class="pull-right">
                                                                        <a class="badge badge-success" target="_blank"
                                                                            href="https://wa.me/62<?php echo substr($ong->pengguna_nohp,1);?>?text=Reminder dari Myta Task : *(<?php echo $ong->work_nama;?>)* <?php echo strip_tags($ong->work_deskripsi);?> ada update atau problem? yuk diceck lagi Tasknya. Thankyou by <?php echo $namaAkses;?> <?php echo base_url(); ?>">
                                                                            <span class="fa fa-whatsapp"></span></a>
                                                                        <a href="mailto:<?php echo $ong->pengguna_email; ?>?subject=Reminder%20Task%20(<?php echo $ong->work_nama;?>)&amp;body=Reminder Task :<?php echo $ong->work_nama;?> | <?php echo strip_tags($ong->work_deskripsi);?>. ada update atau problem? udah lewat deadline nih, yuk diceck lagi Tasknya. Thankyou by <?php echo $namaAkses;?> <?php echo base_url(); ?>"
                                                                            class="badge badge-primary"><span
                                                                                class="fa fa-envelope"></span></a>
                                                                        </div>
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                <?php endif ?>
                                                            </td>
                                                            <td>
                                                                <a href="#!" class="btnDetail"
                                                                    data-id="<?php echo $ong->work_id;?>"><?php echo $ong->work_nama; ?></a></br>
                                                                <?php if($ong->work_device === 'ANDROIOS'): ?>
                                                                <img src="<?php echo base_url().'theme/images/ios.png';?>" width="30px" alt=""> 
                                                                <img src="<?php echo base_url().'theme/images/android.png';?>" width="20px" alt=""> 
                                                                <span class="badge badge-info">iOS & Android</span>
                                                                <?php elseif($ong->work_device === 'IOS'): ?>
                                                                <img src="<?php echo base_url().'theme/images/ios.png';?>"
                                                                    width="30px" alt=""> <span class="badge badge-info">iOS</span>
                                                                <?php elseif($ong->work_device === 'ANDROID'): ?>
                                                                <img src="<?php echo base_url().'theme/images/android.png';?>"
                                                                    width="20px" alt=""> <span class="badge badge-info">Android</span>
                                                                <?php elseif($ong->work_device === 'CMS'): ?>
                                                                <img src="<?php echo base_url().'theme/images/cms.png';?>"
                                                                    width="30px" alt=""><span class="badge badge-info">CMS</span>
                                                                <?php elseif($ong->work_device === 'BACKEND'): ?>
                                                                <img src="<?php echo base_url().'theme/images/backend.png';?>"
                                                                    width="30px" alt=""> <span class="badge badge-info">Backend</span>
                                                                <?php elseif($ong->work_device === 'UI & UX'): ?>
                                                                <img src="<?php echo base_url().'theme/images/frontend.png';?>"
                                                                    width="30px" alt=""> <span class="badge badge-danger">UI & UX</span>
                                                                <?php else: ?>
                                                                <img src="<?php echo base_url().'theme/images/favicon.png';?>"
                                                                    width="30px" alt="">
                                                                    <span class="badge badge-primary"><?php echo $ong->work_device;?></span>
                                                                <?php endif; ?>
                                                            </td>

                                                            <td>
                                                                <b> 
                                                                    <?php $deadline = strtotime($ong->work_deadline);
                                                                    if($deadline > 0): ?>
                                                                        <?php echo date("D, d M y H:i A", strtotime($ong->work_deadline)); ?>
                                                                    <?php else: ?>
                                                                    - Not Set -

                                                                    <?php endif ?><br>
                                                                    <?php if($ong->work_priority === '10'): ?>
                                                                        <img src="<?php echo base_url().'theme/images/5star.png';?>"
                                                                        width="100px" alt="">
                                                                    <?php elseif($ong->work_priority === '5'): ?>
                                                                        <img src="<?php echo base_url().'theme/images/3star.png';?>"
                                                                            width="65px" alt="">
                                                                    <?php else:?>
                                                                        <img src="<?php echo base_url().'theme/images/2star.png';?>"
                                                                            width="45px" alt="">
                                                                    <?php endif; ?>
                                                                </b>
                                                                <?php if($dead === true): ?>
                                                                    <?php if($ong->work_status === 'D' || $ong->work_status === 'E'): ?>                                                     
                                                                    <?php endif; ?>
                                                                <?php endif; ?><br>

                                                                <?php if($deadLineCommand === true): ?>
                                                                    <?php if($ong->work_status === 'D' || $ong->work_status === 'E'): ?>
                                                                    <?php else: ?>
                                                                        <span class="badge badge-danger">Passing the Deadline!</span>
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                            </td>
                                                            <?php if($akses === '4' || $akses === '5'): ?>
                                                                <td>
                                                                    <?php if($ong->work_status === 'A'): ?>
                                                                    <span class="badge badge-info"> In Progress <i
                                                                            class="fa fa-spinner fa-pulse fa-spin"></i></span>
                                                                    <?php elseif($ong->work_status === 'B'): ?>
                                                                    <span class="badge badge-success"> Done <i
                                                                            class="fa fa-check"></i></span>
                                                                    <?php elseif($ong->work_status === 'C'): ?>
                                                                    <span class="badge badge-danger"> Canceled <i
                                                                            class="fa fa-warning"></i></span>
                                                                    <?php elseif($ong->work_status === 'D'): ?>
                                                                    <span class="badge badge-success"> Ready to Test <i
                                                                            class="fa fa-check"></i></span>
                                                                    <?php elseif($ong->work_status === 'E'): ?>
                                                                    <span class="badge badge-primary"> Deployment<i
                                                                            class="fa fa-android"></i> <i
                                                                            class="fa fa-apple"></i></span>
                                                                    <?php elseif($ong->work_status === 'O'): ?>
                                                                    <span class="badge badge-secondary"> Open <i
                                                                            class="si si-user"></i></span>
                                                                    <?php else: ?>
                                                                    <?php endif; ?>
                                                                </td>
                                                                <td>
                                                                    <div class="row pull-right">
                                                                        <?php if($ong->work_status === 'A'): ?>
                                                                        <?php endif; ?>
                                                                        <?php if($ong->work_status !== 'A' && $ong->work_status !== 'O'): ?>
                                                                        <div class="col-3">
                                                                                <button style="cursor: pointer" title="In Progress"
                                                                                    href="javascript:void(0);"
                                                                                    data-id="<?php echo $ong->work_id; ?>"
                                                                                    data-slug="<?php echo $this->uri->segment(4);?>"
                                                                                    class="btn btn-sm btn-info btn-circle btnOpen">
                                                                                    <span class="si si-reload"></span>
                                                                                </button>  
                                                                        </div>
                                                                    <?php endif; ?>
                                                                        <?php if($ong->work_status === 'A' || $ong->work_status === 'O'): ?>
                                                                        <div class="col-3">
                                                                            <a title="Change DIC" href="#!"
                                                                                class="btn btn-sm btn-primary btn-circle btn-assignor"
                                                                                data-id="<?php echo $ong->work_id;?>"
                                                                                data-deadline="<?php echo $ong->work_deadline;?>"><span
                                                                                    class="si si-user"></span></a>
                                                                        </div>
                                                                        <div class="col-2">
                                                                            <a style="cursor: pointer" title="Cancel"
                                                                                href="javascript:void(0);"
                                                                                data-id="<?php echo $req->work_id; ?>"
                                                                                data-slug="<?php echo $this->uri->segment(4);?>"
                                                                                data-nama="<?php echo $req->work_nama; ?>"
                                                                                class="btn btn-sm btn-danger btn-circle btnCancel">
                                                                                <span class="si si-close"></span>
                                                                            </a>  
                                                                        </div>
                                                                        </div>
                                                                        <?php else: ?>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </td>
                                                            <?php else: ?>
                                                                <td>
                                                                    <?php if($ong->work_status === 'A'): ?>
                                                                    <span class="badge badge-info"> In Progress <i
                                                                            class="fa fa-spinner fa-pulse fa-spin"></i></span>
                                                                    <?php elseif($ong->work_status === 'B'): ?>
                                                                    <span class="badge badge-success"> Done <i
                                                                            class="fa fa-check"></i></span>
                                                                    <?php elseif($ong->work_status === 'C'): ?>
                                                                    <span class="badge badge-danger"> Canceled <i
                                                                            class="fa fa-warning"></i></span>
                                                                    <?php elseif($ong->work_status === 'D'): ?>
                                                                    <span class="badge badge-success"> Ready to Test <i
                                                                            class="fa fa-check"></i></span>
                                                                    <?php elseif($ong->work_status === 'E'): ?>
                                                                    <span class="badge badge-primary"> Deployment <i
                                                                            class="fa fa-android"></i> <i
                                                                            class="fa fa-apple"></i></span>
                                                                    <?php elseif($ong->work_status === 'F'): ?>
                                                                    <span class="badge badge-secondary"> Approval <i
                                                                            class="fa fa-spinner fa-pulse fa-spin"></i></span>
                                                                    <?php elseif($ong->work_status === 'O'): ?>
                                                                    <span class="badge badge-secondary"> Open <i
                                                                            class="si si-user"></i></span>
                                                                    <?php else: ?>
                                                                    <?php endif; ?>
                                                                </td>
                                                            <?php endif; ?>
                                                            <?php if($akses === '4' || $akses === '5' || $akses === '2'): ?>
                                                            <?php else: ?>
                                                                <?php if($ong->work_user_id === $this->session->userdata('idadmin') || $ong->work_assignor_id === $this->session->userdata('idadmin')):?>
                                                                <td>
                                                                    <div class="row pull-right">
                                                                        <?php if($ong->work_status === 'A'): ?>
                                                                        <div class="col-md-2">
                                                                            <button style="cursor: pointer" title="Ready to Test"
                                                                                href="javascript:void(0);"
                                                                                data-id="<?php echo $ong->work_id; ?>"
                                                                                data-slug="<?php echo $this->uri->segment(4);?>"
                                                                                class="btn btn-sm btn-success btn-circle btnReady">
                                                                                <span class="si si-check"></span>
                                                                            </button>  
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <button style="cursor: pointer" title="Deployment"
                                                                                href="javascript:void(0);"
                                                                                data-id="<?php echo $ong->work_id; ?>"
                                                                                data-slug="<?php echo $this->uri->segment(4);?>"
                                                                                class="btn btn-sm btn-primary btn-circle btnAPK">
                                                                                <span class="fa fa-android"></span> 
                                                                                <span class="fa fa-apple"></span>
                                                                            </button> 
                                                                        </div>
                                                                        <?php elseif($ong->work_status === 'D' || $ong->work_status === 'E'): ?>
                                                                        <div class="col-md-2">
                                                                            <button style="cursor: pointer" title="In Progress"
                                                                                href="javascript:void(0);"
                                                                                data-id="<?php echo $ong->work_id; ?>"
                                                                                data-slug="<?php echo $this->uri->segment(4);?>"
                                                                                class="btn btn-sm btn-info btn-circle btnOpen">
                                                                                <span class="si si-reload"></span>
                                                                            </button>  
                                                                        </div>
                                                                        <?php elseif($ong->work_status === 'F'): ?>
                                                                        <div class="col-md-2">
                                                                            <a style="cursor: pointer" title="Cancel"
                                                                                href="javascript:void(0);"
                                                                                data-id="<?php echo $ong->work_id; ?>"
                                                                                data-slug="<?php echo $this->uri->segment(4);?>"
                                                                                data-nama="<?php echo $ong->work_nama; ?>"
                                                                                class="btn btn-sm btn-danger btn-circle btnCancel">
                                                                                <span class="si si-close"></span>
                                                                            </a>  
                                                                        </div>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </td>
                                                                <?php else: ?>
                                                                <td>
                                                                    <?php if($akses === '3'): ?>
                                                                    <span class="badge badge-secondary"><?php echo $assignor['pengguna_nama']; ?></span>
                                                                    <?php else: ?>
                                                                    -
                                                                    <?php endif; ?>
                                                                </td>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if($akses === '4' || $akses === '5' || $akses === '3'): ?>
                        <div class="col-md-4 otherCon">
                            <div class="block" style="border-radius:20px">
                                <div class="block-header">
                                    <h3 class="block-title">
                                        Finished <small> Task</small>
                                    </h3>
                                    <span id="spinner" class="spinner" style="display:none"></span>
                                    <div class="block-options">
                                    </div>
                                </div>
                                <div class="block-content block-content-full">
                                <?php if($done->num_rows() > 0): ?>
                                    <table class="table table-striped">
                                        <thead>
                                            <th>Project</th>
                                            <th>Task</th>
                                            <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <?php foreach($done->result() as $dn):?>
                                            <tr>
                                                <td>
                                                    <small><?php echo $dn->kategori_nama; ?></small>
                                                </td>
                                                <td>
                                                    <a href="#!" class="btnDetail" data-id="<?php echo $dn->work_id;?>"><?php echo $dn->work_nama; ?></a>
                                                </td>
                                                <td>
                                                    <div class="row justify-content-center">
                                                        <div class="col-md-2">
                                                        <button style="cursor: pointer" title="In Progress"
                                                                    href="javascript:void(0);"
                                                                    data-id="<?php echo $dn->work_id; ?>"
                                                                    data-slug="<?php echo $this->uri->segment(4);?>"
                                                                    class="btn btn-sm btn-info btn-circle btnOpen">
                                                                    <span class="si si-reload"></span>
                                                                </button> 
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                                    <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php else: ?>
                                    <p style="color:#ddd;text-align:center">No Task available.</p>

                                    <?php endif; ?>
                                </div>
                            </div>
                            <!-- END Row #2 -->
                        </div>
                        <?php else: ?>
                        <?php endif; ?>
                        <?php endif; ?>
                    </div>
        </main>
        <!-- END Main Container -->
        <!-- Footer -->
        <?php echo $this->load->view('admin/v_footer'); ?>
        <!-- END Footer -->
    </div>



    <?php if($akses !== '1'): ?>
    <?php foreach($latestAPK->result() as $apk): ?>
    <div class="modal fade in" id="ModalQR<?php echo $apk->compile_id;?>" tabindex="-1" role="dialog"
        aria-labelledby="modal-normal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Download & Install App</h3>
                        <div class="block-options">
                            <button style="cursor: pointer" type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <center>
                            <h5 style="margin-bottom:-5px"><?php echo $apk->compile_nama; ?></h5>
                            <img src="<?php echo base_url().'assets/images/qr/'.$apk->compile_nama.'.png' ?>"
                                width="50%">
                            <img style="margin-bottom:40px"
                                src="https://static.wixstatic.com/media/1b9317_a6583396afd44d2ca206e177acd449a5~mv2.gif"
                                width="45%">
                            <small style="margin-top:-20px">Scan QR above to Install the Application or download
                                manually <a href="<?php echo base_url().'assets/compile/'.$apk->compile_file; ?>"
                                    rel="noopener noreferrer">here</a></small>
                        </center>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="cursor: pointer" type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
    <?php endif; ?>

    <!-- Modal Hapus -->
    <?php if($akses === '4' || $akses === '5'): ?>
    <form class="form" action="<?php echo base_url().'admin/work/update_assignor'?>" method="post" enctype="multipart/form-data">
        <div class="modal fade in" id="ModalAssignor" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Change DIC / Change Deadline</h3>
                            <div class="block-options">
                                <button style="cursor: pointer" type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">

                            <div class="form-group">
                                <input name="kode" type="hidden" value="<?php echo $wrks->work_id; ?>">
                                <!-- <select name="xassignor" class="form-control" id="" required>
                                    <option value="">- Choose Assignor -</option>
                                    <?php 
                                    $developer = $this->db->query("SELECT * FROM pengguna where pengguna_level IN ('3','4')");
                                    foreach($developer->result() as $dev): ?>
                                    <option value="<?php echo $dev->pengguna_id;?>"><?php echo $dev->pengguna_nama; ?>
                                        |
                                        <?php echo $dev->pengguna_email; ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select> -->
                                <select name="xassignor" class="form-control" id="" required>
                                    <option value="">- Choose DIC -</option>
                                    <?php 
                                    $slug = $this->uri->segment(4);
                                    $kategori = $this->db->query("SELECT * FROM kategori where kategori_slug ='$slug'")->row_array();
                                    $katId = $kategori['kategori_id'];
                                    $tim = $this->db->query("SELECT * FROM tim where tim_kategori_id ='$katId' AND tim_acc='VERIFIED'");
                                    foreach($tim->result() as $tm): 
                                        $developer = $this->db->query("SELECT * FROM pengguna where pengguna_id='$tm->tim_user_id' AND pengguna_level='3'");
                                        foreach($developer->result() as $dev):?>
                                            <option value="<?php echo $dev->pengguna_id;?>">
                                                <?php echo $dev->pengguna_nama; ?>
                                                |
                                                <?php echo $dev->pengguna_email; ?>
                                            </option>
                                    <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                    <label for="">Deadline</label>
                                        <input type="text" name="xdeadline" class="form-control" readonly value="<?php echo $wrks->work_deadline;?>" required>
                                    </div>
                                    <div class="col-md-6">
                                    <label for=""> New Deadline</label>
                                    <input type="datetime-local" name="xnewdeadline" class="form-control"
                                            placeholder="Deadline Date" data-week-start="1" data-autoclose="true"
                                            data-today-highlight="true" data-date-format="mm/dd/yyyy"
                                            placeholder="mm/dd/yy" required>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button style="cursor: pointer" type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button style="cursor: pointer" type="submit" class="btn btn-primary btn-square">Change</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <?php else: ?>
    <?php endif; ?>
    <?php if($slug !== null): ?>
    <?php if($akses === '4' || $akses === '5'): ?>
    <form class="form" action="<?php echo base_url().'admin/work/simpan_work'?>" method="post" enctype="multipart/form-data">
        <div class="modal fade in" id="ModalAddNew" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Create Task</h3>
                            <div class="block-options">
                                <button style="cursor: pointer" type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <select class="form-control" name="xbusiness2" style="width: 100%;"
                                            data-placeholder="Choose one.." disabled="disabled" required>
                                            <option value="">- Choose Project -</option>
                                            <?php $kategori = $this->db->query("SELECT * FROM kategori");
                                                $kat = $kategori->row_array();
                                                $katIds = $kat['kategori_id'];
                                                foreach($kategori->result() as $kat): ?>
                                            <?php if($this->uri->segment(4) === $kat->kategori_slug):?>
                                            <option value="<?php echo $kat->kategori_id;?>" selected>
                                                <?php echo $kat->kategori_nama; ?></option>
                                            <?php else: ?>
                                            <option value="<?php echo $kat->kategori_id;?>">
                                                <?php echo $kat->kategori_nama; ?>
                                            </option>
                                            <?php endif; ?>
                                            <?php endforeach; ?>
                                        </select>
                                        <?php $kategori = $this->db->query("SELECT * FROM kategori");
                                        $kat = $kategori->row_array();
                                        $katIds = $kat['kategori_id'];
                                        foreach($kategori->result() as $kat): ?>
                                        <?php if($this->uri->segment(4) === $kat->kategori_slug):?>
                                            <input type="hidden" value="<?php echo $kat->kategori_id;?>" name="xbusiness">
                                        <?php else: ?>
                                        <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="col-md-3">
                                        <select name="xsprint" id="" class="form-control" required>
                                            <option value="">- Choose Sprint -</option>
                                            <?php foreach($listSprint->result() as $sprint): ?>
                                            <option value="<?php echo $sprint->sprint_id;?>"><?php echo $sprint->sprint_nama;?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="xassignor" class="form-control" id="">
                                            <option value="">- Choose PIC -</option>
                                            <?php 
                                                foreach($assignor->result() as $dev):?>
                                                        <option value="<?php echo $dev->pengguna_id;?>">
                                                            <?php echo $dev->pengguna_nama; ?>
                                                            |
                                                            <?php echo $dev->pengguna_email; ?>
                                                        </option>
                                                <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                          
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Task Subject</label>
                                        <input type="text" name="xwork" class="form-control"
                                            placeholder="Write subject..." required>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Set Deadline</label>
                                        <input type="datetime-local" name="xdeadline" class="form-control"
                                            placeholder="Deadline Date" data-week-start="1" data-autoclose="true"
                                            data-today-highlight="true" data-date-format="mm/dd/yyyy"
                                            placeholder="mm/dd/yy">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Job Load</label><br>
                                        <?php foreach($stage->result() as $st):?>
                                        <label class="css-control css-control-sm css-control-secondary css-radio">
                                            <input type="radio" value="<?php echo $st->devbug_nama; ?>"
                                                class="css-control-input" name="xdevbug" checked>
                                            <span class="css-control-indicator"></span> <?php echo $st->devbug_nama; ?>
                                        </label>
                                        <?php endforeach; ?>
                                        <label for="">Priority Level</label><br>
                                        <label class="css-control css-control-sm css-control-danger css-radio">
                                            <input type="radio" value="10" class="css-control-input" name="xpriority" checked>
                                            <span class="css-control-indicator"></span> High
                                        </label>
                                        <label class="css-control css-control-sm css-control-warning css-radio">
                                            <input type="radio" value="5" class="css-control-input" name="xpriority">
                                            <span class="css-control-indicator"></span> Medium
                                        </label>
                                        <label class="css-control css-control-sm css-control-success css-radio">
                                            <input type="radio" value="1" class="css-control-input" name="xpriority">
                                            <span class="css-control-indicator"></span> Low
                                        </label>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <label for="">Device</label><br>
                                        <label class="css-control css-control-sm css-control-success css-radio">
                                            <input type="radio" class="css-control-input checkDevice" value="ANDROID" name="xdevice"
                                                checked>
                                            <span class="css-control-indicator"></span> Android
                                        </label>
                                        <label class="css-control css-control-sm css-control-info css-radio">
                                            <input type="radio" class="css-control-input checkDevice" value="IOS" name="xdevice">
                                            <span class="css-control-indicator"></span> iOS
                                        </label>
                                        <label class="css-control css-control-sm css-control-primary css-radio">
                                            <input type="radio" class="css-control-input checkDevice" value="ANDROIOS"
                                                name="xdevice">
                                            <span class="css-control-indicator"></span> Android & iOS
                                        </label>
                                        <label class="css-control css-control-sm css-control-warning css-radio">
                                            <input type="radio" class="css-control-input checkDevice" value="CMS" name="xdevice">
                                            <span class="css-control-indicator"></span> CMS
                                        </label>
                                        <label class="css-control css-control-sm css-control-danger css-radio">
                                            <input type="radio" class="css-control-input checkDevice" value="UI & UX"
                                                name="xdevice">
                                            <span class="css-control-indicator"></span> UI & UX
                                        </label>
                                        <label class="css-control css-control-sm css-control-secondary css-radio">
                                            <input type="radio" class="css-control-input checkDevice" value="BACKEND"
                                                name="xdevice">
                                            <span class="css-control-indicator"></span> Backend
                                        </label>
                                        <label class="css-control css-control-sm css-control-secondary css-radio">
                                            <input id="otherCheck" type="radio" class="css-control-input" value="OTHER" name="xdevice">
                                            <span class="css-control-indicator"></span> Others
                                        </label>
                                        <input id="otherDevice" type="text" name="xdevice" class="form-control pull-right" placeholder="Write down other device">
                                    </div>

                                </div>

                            </div>
                            <div class="form-group">
                                <textarea id="ckeditor" type="text" name="xdeskripsi" class="form-control"
                                    placeholder="Deskripsi" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button style="cursor: pointer" type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button style="cursor: pointer" type="submit" class="btn btn-primary btn-square">Create</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php else: ?>
    <form class="form" action="<?php echo base_url().'admin/work/simpan_work_dev'?>" method="post" enctype="multipart/form-data">
        <div class="modal fade in" id="ModalAddNewDev" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Request Task</h3>
                            <div class="block-options">
                                <button style="cursor: pointer" type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        <select class="form-control" name="xbusiness2" style="width: 100%;"
                                            data-placeholder="Choose one.." disabled="disabled" required>
                                            <option value="">- Choose Project -</option>
                                            <?php $kategori = $this->db->query("SELECT * FROM kategori");
                                            $kat = $kategori->row_array();
                                            $katIds = $kat['kategori_id'];
                                        foreach($kategori->result() as $kat): ?>
                                            <?php if($this->uri->segment(4) === $kat->kategori_slug):?>
                                            <option value="<?php echo $kat->kategori_id;?>" selected>
                                                <?php echo $kat->kategori_nama; ?></option>
                                            <?php else: ?>
                                            <option value="<?php echo $kat->kategori_id;?>">
                                                <?php echo $kat->kategori_nama; ?>
                                            </option>
                                            <?php endif; ?>
                                            <?php endforeach; ?>
                                        </select>
                                         <?php $kategori = $this->db->query("SELECT * FROM kategori");
                                        $kat = $kategori->row_array();
                                        $katIds = $kat['kategori_id'];
                                        foreach($kategori->result() as $kat): ?>
                                        <?php if($this->uri->segment(4) === $kat->kategori_slug):?>
                                            <input type="hidden" value="<?php echo $kat->kategori_id;?>" name="xbusiness">
                                        <?php else: ?>
                                        <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="col-md-3">
                                        <select name="xsprint" id="" class="form-control" required>
                                            <option value="">- Choose Sprint -</option>
                                            <?php foreach($listSprint->result() as $sprint): ?>
                                            <option value="<?php echo $sprint->sprint_id;?>"><?php echo $sprint->sprint_nama;?></option>
                                            <?php endforeach; ?>
                                          
                                        </select>
                                    </div>

                                    <div class="col-md-6">

                                    <select name="xassignor" class="form-control" id="" required>
                                            <option value="">- Choose PIC -</option>
                                            <?php $slug = $this->uri->segment(4);
                                            $kategori = $this->db->query("SELECT * FROM kategori where kategori_slug ='$slug'")->row_array();
                                            $katId = $kategori['kategori_id'];
                                            $idTl = $kategori['kategori_tl_id'];
                                            $tim = $this->db->query("SELECT * FROM tim where tim_kategori_id ='$katId' AND tim_acc='VERIFIED'");
                                            foreach($tim->result() as $tm): 
                                                $developer = $this->db->query("SELECT * FROM pengguna where pengguna_id='$tm->tim_user_id' AND pengguna_level IN ('4')");
                                                foreach($developer->result() as $dev):?>
                                                    <option value="<?php echo $dev->pengguna_id;?>">
                                                        <?php echo $dev->pengguna_nama; ?>
                                                        | Business Analyst
                                                    </option>
                                                <?php endforeach; ?>
                                                    
                                            <?php endforeach; ?>
                                            <?php if($akses === '3'): ?>
                                                    <?php $tl = $this->db->query("SELECT * FROM pengguna where pengguna_id='$idTl'")->row_array();?>
                                                    <option value="<?php echo $tl['pengguna_id'];?>">
                                                        <?php echo $tl['pengguna_nama']; ?>
                                                        | Tech Lead
                                                    </option>
                                                    <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="xdeadline"> Subject</label>
                                        <input type="text" name="xwork" class="form-control"
                                            placeholder="Write Subject..." required>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="xdeadline"> Deadline</label>
                                        <input type="datetime-local" name="xdeadline" class="form-control"
                                            placeholder="Deadline Date" data-week-start="1" data-autoclose="true"
                                            data-today-highlight="true" data-date-format="mm/dd/yyyy"
                                            placeholder="mm/dd/yy" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                    <label for="">Job Load</label><br>
                                        <?php foreach($stage->result() as $st):?>
                                        <label class="css-control css-control-sm css-control-secondary css-radio">
                                            <input type="radio" value="<?php echo $st->devbug_nama; ?>"
                                                class="css-control-input" name="xdevbug" checked>
                                            <span class="css-control-indicator"></span> <?php echo $st->devbug_nama; ?>
                                        </label>
                                        <?php endforeach; ?>
                                        <label for="">Priority Level</label><br>
                                        <label class="css-control css-control-sm css-control-danger css-radio">
                                            <input type="radio" value="10" class="css-control-input" name="xpriority" checked>
                                            <span class="css-control-indicator"></span> High
                                        </label>
                                        <label class="css-control css-control-sm css-control-warning css-radio">
                                            <input type="radio" value="5" class="css-control-input" name="xpriority">
                                            <span class="css-control-indicator"></span> Medium
                                        </label>
                                        <label class="css-control css-control-sm css-control-success css-radio">
                                            <input type="radio" value="1" class="css-control-input" name="xpriority">
                                            <span class="css-control-indicator"></span> Low
                                        </label>
                                    </div>

                                    <div class="col-md-6">
                                        <label class="css-control css-control-sm css-control-success css-radio">
                                            <input type="radio" class="css-control-input checkDevice" value="ANDROID" name="xdevice"
                                                checked>
                                            <span class="css-control-indicator"></span> Android
                                        </label>
                                        <label class="css-control css-control-sm css-control-info css-radio">
                                            <input type="radio" class="css-control-input checkDevice" value="IOS" name="xdevice">
                                            <span class="css-control-indicator"></span> iOS
                                        </label>
                                        <label class="css-control css-control-sm css-control-primary css-radio">
                                            <input type="radio" class="css-control-input checkDevice" value="ANDROIOS"
                                                name="xdevice">
                                            <span class="css-control-indicator"></span> Android & iOS
                                        </label>
                                        <label class="css-control css-control-sm css-control-warning css-radio">
                                            <input type="radio" class="css-control-input checkDevice" value="CMS" name="xdevice">
                                            <span class="css-control-indicator"></span> CMS
                                        </label>
                                        <label class="css-control css-control-sm css-control-danger css-radio">
                                            <input type="radio" class="css-control-input checkDevice" value="UI & UX"
                                                name="xdevice">
                                            <span class="css-control-indicator"></span> UI & UX
                                        </label>
                                        <label class="css-control css-control-sm css-control-secondary css-radio">
                                            <input type="radio" class="css-control-input checkDevice" value="BACKEND"
                                                name="xdevice">
                                            <span class="css-control-indicator"></span> Backend
                                        </label>
                                        <label class="css-control css-control-sm css-control-secondary css-radio">
                                            <input id="otherCheck" type="radio" class="css-control-input" value="OTHER" name="xdevice">
                                            <span class="css-control-indicator"></span> Others
                                        </label>
                                        <input id="otherDevice" type="text" name="xdevice" class="form-control pull-right" placeholder="Write down other device" >
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <textarea id="ckeditor" type="text" name="xdeskripsi" class="form-control"
                                    placeholder="Deskripsi" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button style="cursor: pointer" type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button style="cursor: pointer" type="submit" class="btn btn-primary btn-square">Request</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <?php endif; ?>
    <!-- END Normal Modal -->
    <?php endif; ?>
     </div>

      <!-- Modal Hapus -->
        <div class="modal fade in" id="ModalCancel" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-popout" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Cancel Task</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <p id="namaTaskCancel"></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">No</button>
                        <button type="submit" id="confirmCancel"data-slug="<?php echo $this->uri->segment(4); ?>" class="btn btn-danger btn-square">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    <!-- END Normal Modal -->
    <audio>
        <source src="<?php echo base_url().'assets/notif_sound.mp3' ?>" type="audio/ogg">
    </audio>

    <div id="toastMessage"></div>
    <!-- Codebase Core JS -->
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/pages/be_pages_dashboard.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/select2/select2.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/sweetalert2/sweetalert2.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/easy-pie-chart/jquery.easypiechart.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/magnific-popup/magnific-popup.min.js'?>"></script>

    <script>
        jQuery(function() {
            Codebase.helpers(['magnific-popup','easy-pie-chart', 'flatpickr', 'colorpicker', 'maxlength', 'select2',
                'masked-inputs',
                'rangeslider', 'tags-inputs'
            ]);
        });
        <?php if($akses === '1'): ?>
        setTimeout(() => {
            $('#banner').slideUp();
        }, 5000);
    <?php endif; ?>
        $( "#formProject" ).submit(function( event ) {
            $('#cover-spin').fadeIn();
        });
        $('#otherDevice').hide();
        $('.conEmot').hide();

        $('#otherCheck').click(function(){
            if($(this).prop("checked") == true){
                $('#otherDevice').show();
            }   
        });
        
        $('.checkDevice').click(function(){
            if($(this).prop("checked") == true){
                $('#otherDevice').hide();
                $('#otherDevice').val('');
            }
        });

        $('.projectItem').click(function(){
            $('#cover-spin').show();
        });
        
        var objDiv = document.getElementById("scrollDiv");
        $('#deskripsi_task2').hide();
        $('#ModalDetail').hide();
        $('#btnUpdateDesc').on('click', function(){
            $('#deskripsi_task2').show();
            $('#btnUpdateDesc').hide();
        });
        
        
        $('#btnCancelModalDetail').on('click', function(){
            $('#ModalDetail').hide();
            $('.otherCon').slideDown();
        });


        setInterval(function(){
            var $audio = $('audio');
            var audio = $audio[0];
            $.ajax({
                url: "<?php echo base_url().'admin/comment/hit_newComment_dashboard';?>",
                success: function(hsl) {
                    if(hsl === 'noNotif'){
                    }else{
                        let result = JSON.parse(hsl);
                        if(result){
                            audio.play();
                            for(let k =0; k<result.length;k++){
                                var content = '<b>'+result[k].task_nama+'</b><br>'+result[k].comment_user+' : '+result[k].comment_isi;
                                if(result[k].comment_isi === ''){
                                    content = '<b>'+result[k].task_nama+'</b><br>'+result[k].comment_user+' : Attachment';
                                }
                                if(result[k].comment_gif === true){
                                    content = '<b>'+result[k].task_nama+'</b><br>'+result[k].comment_user+' : '+result[k].comment_attachment1;
                                }
                                $.toast({
                                    heading: result[k].kategori_nama,
                                    text: content,
                                    showHideTransition: 'fade',
                                    icon: 'info',
                                    loader:false,
                                    hideAfter: 3000,
                                    position: 'bottom-right',
                                    // bgColor: '#00C9E6',
                                    afterHidden: function () {
                                        $.ajax({
                                            url: "<?php echo base_url().'admin/comment/read_comment';?>",
                                            type: "POST",
                                            data: {
                                                kode: result[k].task_id,
                                            },
                                            success: function(hsl) {
                                                console.log('updated', hsl);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                }
            });
        },4000);

        $('#deskripsiTitle').on('click',function(){
            var title = $('#deskripsiTitle').text();
            if(title === 'Show Description'){
                $('#deskripsiTitle').text('Hide Description');
            }else if(title === 'Hide Description'){
                $('#deskripsiTitle').text('Show Description');
            }
        });

        $('.dropify').dropify({ //overate input type file
            messages: {
                default: '<small>.gif | .jpg | .png | .pdf </small>',
                replace: 'Change',
                remove: 'Delete',
                error: 'error'
            }
        });
        $('.btnSubmit').on('click', function() {
            $('#cover-spin').show();
        });

        $('.form').submit(function() {
            $('#cover-spin').show();
        });

        $('#btn-banner').on('click', function() {
            $('#banner').fadeOut();
            $('#btn-banner').fadeOut();
        });

        $('#xcomment').keypress(function(e){
            if(e.keyCode == 13 && e.shiftKey) {
            }else if(e.keyCode == 13){
                var taskId = $('#taskId').val();
                $.ajax({
                    url: "<?php echo base_url().'admin/comment/unset_userOnline';?>",
                    type: "POST",
                    data: {
                        kode:taskId
                    },
                    beforeSend: function() {
                        $('#cover-spin').show();
                    },
                    success: function(hsl) {
                        $('#formComment').submit();
                        $('#xcomment').val('');
                    }
                });
            }
        });
        $('#xcomment').blur(function(){
            var taskId = $('#taskId').val();
            $.ajax({
                url: "<?php echo base_url().'admin/comment/unset_userOnline';?>",
                type: "POST",
                data: {
                    kode: taskId,
                },
                beforeSend: function() {
                },
                success: function(hsl) {
                },
                fail: function(){
                }
            });
           
        });
        $('#xcomment').keyup(function(e){
            var taskId = $('#taskId').val();
            $.ajax({
                url: "<?php echo base_url().'admin/comment/set_userOnline';?>",
                type: "POST",
                data: {
                    kode: taskId,
                },
                beforeSend: function() {
                },
                success: function(hsl) {
                    console.log('updated onilne');
                },
                fail: function(){
                }
            });
        });

        $('.btn-add-new').on('click', function() {
            $.ajax({
                url: "<?php echo base_url().'admin/work/validate_project_time';?>",
                type: "POST",
                data: {
                    xslug: $(this).data('slug'),
                },
                beforeSend: function() {
                    $('#cover-spin').show();
                },
                success: function(hsl) {
                    if(hsl === 'addNewTrue'){
                        $('#cover-spin').fadeOut();
                        <?php if($akses === '4' || $akses === '5'): ?>
                        $('#ModalAddNew').modal('show');
                        <?php elseif($akses === '3'): ?>
                        $('#ModalAddNewDev').modal('show');
                        <?php endif; ?>
                    }else if(hsl === 'addNewFalse'){
                        $('#cover-spin').fadeOut();
                        $.toast({
                            heading: 'Ops, Something went wrong!',
                            text: "You cannot add task at this time!",
                            showHideTransition: 'slide',
                            icon: 'error',
                            hideAfter: 5000,
                            position: 'bottom-right',
                            bgColor: '#FF4859'
                        });
                    }else{
                        $('#cover-spin').fadeOut();
                        $.toast({
                            heading: 'Opps, Something went wrong!',
                            text: "Please try again!",
                            showHideTransition: 'slide',
                            icon: 'error',
                            hideAfter: 5000,
                            position: 'bottom-right',
                            bgColor: '#FF4859'
                        });
                    }
                },
                fail: function(){
                    $('#cover-spin').fadeOut();
                    alert('fail');
                }
            });
        });
        

        $('.btnDetail').on('click', function() {
            $('.otherCon').hide();
            $('#commentContainer').html('');
            $('#namaKategori').text('');
            $('#kategori_gambar1').attr('src','');
            var work_id = $(this).data('id');
            $.ajax({
                url: "<?php echo base_url().'admin/dashboard/get_detail_work';?>",
                type: "POST",
                data: {
                    kode: work_id
                },
                beforeSend: function() {
                    $('#cover-spin').show();
                },
                success: function(hsl) {
                    console.log("hasil : ", hsl);
                    if(hsl === false){
                        alert(hsl);
                    }else{
                        $('#cover-spin').fadeOut();
                        let result = JSON.parse(hsl);
                        console.log("hasil : ", result);
                        var deadline = result.work_deadline;
                        $('#namaKategori').text(result.kategori_nama);
                        $('#namaSprint').text(result.work_sprint);
                        $('#work_assignor').text(result.work_assignor);
                        $('#work_assignor_gambar').attr('src','<?php echo base_url();?>assets/images/'+result.work_assignor_gambar);
                        $('#nama_task').text(result.work_nama);
                        $('#kategori_gambar1').attr('src','<?php echo base_url();?>assets/images/business/'+result.kategori_gambar);
                        $('#deskripsi_task').html(result.work_deskripsi);
                        $('#deskripsi_task2').text(result.work_deskripsi);
                        $('#taskId').attr("value",work_id);
                        $('#device').attr('src','<?php echo base_url();?>theme/images/'+result.work_device);
                        $('#device_desc').text(result.work_device_deskripsi);
                        $('#dic').text(result.work_assignor);
                        $('#deadline_task').text(deadline);
                        $('#idForNewComment').attr("value", work_id);
                        var id_task = result.work_id;
                        for(let i=0;i<result.listComment.length;i++){
                            var attach = '';
                            var idUser = <?php echo $this->session->userdata('idadmin')?>;
                            var userId = result.listComment[i].comment_user_id;
                            var attach1 = result.listComment[i].comment_attach1;
                            var attach2 = result.listComment[i].comment_attach2;
                            var attach3 = result.listComment[i].comment_attach3;
                            var attach4 = result.listComment[i].comment_attach4;
                            
                            if(userId == idUser){
                                if(attach1 === '' && attach2 === '' && attach3 === '' && attach4 === ''){
                                    attach = '';
                                }else{
                                    attach = '<div class="row items-push js-gallery img-fluid-100"><div class="col-sm-3"> '+attach4+'</div><div class="col-3"> '+attach3+'</div><div class="col-3"> '+attach2+'</div><div class="col-3"> '+attach1+'</div></div>';
                                }
                                $('#commentContainer').append('<div class="d-flex flex-row-reverse mb-20"><div> <a class="img-status" href="javascript:void(0)"><img class="img-avatar img-avatar" style="width:40px;height:35px" src="http://myta.bacotlu.com/assets/images/'+result.listComment[i].comment_user_gambar+'" alt="Avatar"> <div class="img-status-indicator bg-success"></div> </a> </div> <div class="mx-10 text-right"> <div> <div class="bg-primary-lighter text-dark rounded px-15 py-10 mb-5"> <label for="">'+result.listComment[i].comment_user+'</label> <br>'+result.listComment[i].comment_isi+'<br>'+attach+'</div></div> <div class="text-right text-muted font-size-xs font-italic">'+result.listComment[i].comment_created+'</div> </div></div>')
                            }else{
                                if(attach1 === '' && attach2 === '' && attach3 === '' && attach4 === ''){
                                    attach = '';
                                }else{
                                    attach = '<div class="row items-push js-gallery img-fluid-100"><div class="col-sm-3"> '+attach1+'</div><div class="col-3"> '+attach2+'</div><div class="col-3"> '+attach3+'</div><div class="col-3"> '+attach4+'</div></div>';
                                }
                                $('#commentContainer').append('<div class="d-flex mb-20"><div> <a class="img-status" href="javascript:void(0)"> <img class="img-avatar img-avatar" style="width:40px;height:35px" src="http://myta.bacotlu.com/assets/images/'+result.listComment[i].comment_user_gambar+'" alt="Avatar"> <div class="img-status-indicator bg-success"></div> </a> </div> <div class="mx-10"> <div> <div class="bg-body-dark text-dark rounded px-15 py-10 mb-5"> <label for="">'+result.listComment[i].comment_user+'</label><br>'+result.listComment[i].comment_isi+'<br>'+attach+'</div></div> <div class="text-muted font-size-xs font-italic">'+result.listComment[i].comment_created+'</div> </div></div>');
                            }
                        }
                        setTimeout(() => {
                            objDiv.scrollTop = objDiv.scrollHeight;
                        }, 0);
                        $('#ModalDetail').show();
                        $('.attachment').hide()
                        $('.onlineContainer').hide();
                        $.ajax({
                                url: "<?php echo base_url().'admin/comment/unset_userOnline';?>",
                                type: "POST",
                                data: {
                                    kode: work_id
                                },
                                beforeSend: function() {
                                },
                                success: function(hsl) {
                                }
                            });
                            setInterval(function(){
                                $.ajax({
                                    url: "<?php echo base_url().'admin/comment/check_online';?>",
                                    type: "POST",
                                    data: {
                                        kode: work_id
                                    },
                                    beforeSend: function() {
                                    },
                                    success: function(hsl) {
                                        if(hsl === 'noOnline'){
                                            $('.onlineContainer').hide();
                                        }else{
                                            setTimeout(() => {
                                                objDiv.scrollTop = objDiv.scrollHeight;
                                            }, 0);
                                            $('.onlineContainer').show();
                                            let result = JSON.parse(hsl);
                                            console.log('hasil :', result);
                                            for(let p=0;p<result.length;p++){
                                                $('#onlineUserGambar').attr("src", result[p].online_user_gambar);
                                                $('#onlineUserNama').text(result[p].online_user+' is typing...');
                                            }
                                        }
                                    }
                                });
                            },1000);


                        setInterval(function(){
                            $.ajax({
                                url: "<?php echo base_url().'admin/comment/hit_newComment';?>",
                                type: "POST",
                                data: {
                                    kode: $('#idForNewComment').val()
                                },
                                beforeSend: function() {
                                },
                                success: function(hsl) {
                                    $('#cover-spin').hide();
                                    var $audio = $('audio');
                                    var audio = $audio[0];
                                    let result = JSON.parse(hsl);
                                    if(result.length > 0){
                                        for(let i=0;i<result.length;i++){
                                            var attach = '';
                                            var attach1 = result[i].comment_attach1;
                                            var attach2 = result[i].comment_attach2;
                                            var attach3 = result[i].comment_attach3;
                                            var attach4 = result[i].comment_attach4;
                                            if(attach1 === "" && attach2 === "" && attach3 === "" && attach4 === ""){
                                                attach = "";
                                            }else{
                                                attach = '<div class="row items-push js-gallery img-fluid-100"><div class="col-3"> '+attach1+'</div><div class="col-3"> '+attach2+'</div><div class="col-3"> '+attach3+'</div><div class="col-3"> '+attach4+'</div></div>';
                                            }
                                            $('#commentContainer').append('<div class="d-flex mb-20"><div> <a class="img-link img-status" href="javascript:void(0)"> <img class="img-avatar img-avatar" style="width:40px;height:35px" src="http://myta.bacotlu.com/assets/images/'+result[i].comment_user_gambar+'" alt="Avatar"> <div class="img-status-indicator bg-success"></div> </a> </div> <div class="mx-10"> <div> <div class="bg-body-dark text-dark rounded px-15 py-10 mb-5"> <label for="">'+result[i].comment_user+'</label><br>'+result[i].comment_isi+'<br>'+attach+'</div></div> <div class="text-muted font-size-xs font-italic">'+result[i].comment_created+'</div> </div></div>');
                                            setTimeout(() => {
                                                objDiv.scrollTop = objDiv.scrollHeight;
                                            }, 0);
                                            $.ajax({
                                                url: "<?php echo base_url().'admin/comment/read_comment';?>",
                                                type: "POST",
                                                data: {
                                                    kode: work_id,
                                                },
                                                success: function(hsl) {
                                                    console.log('updated', hsl);
                                                }
                                            }); 
                                        }
                                    }
                                }
                            });
                        },3000);
                    }
                },
                fail: function(){
                    $('#cover-spin').fadeOut();
                    alert('fail');
                }
            });
        });
        <?php if($akses === '4' || $akses === '5'): ?>
        $('.btn-assignor').on('click', function() {
            var id = $(this).data('id');
            var deadline = $(this).data('deadline');
            $('#ModalAssignor').modal('show');
            $('[name="kode"]').val(id);
            $('[name="xdeadline"]').val(deadline);
        });
        $('.btnDone').on('click', function() {
            $.ajax({
                url: "<?php echo base_url().'admin/work/done_work_dashboard';?>",
                type: "POST",
                data: {
                    xkode: $(this).data('id'),
                    slug: $(this).data('slug')
                },
                beforeSend: function() {
                    $('#cover-spin').show();
                },
                success: function(hsl) {
                    window.location.reload();
                },
                fail: function(){
                    $('#cover-spin').fadeOut();
                    alert('fail');
                }
            });
        });

        $('.btnCancel').on('click', function() {
            var kode = $(this).data('id');
            var nama = $(this).data('nama');
            $('#confirmCancel').attr("data-id",kode);
            $('#namaTaskCancel').text("Are you sure want to cancel "+nama+" ?");

            $('#ModalCancel').modal('show');
        });
 
        $('#confirmCancel').on('click', function() {
            $.ajax({
                url: "<?php echo base_url().'admin/work/cancel_work_dashboard';?>",
                type: "POST",
                data: {
                    xkode: $(this).data('id'),
                    slug: $(this).data('slug')
                },
                beforeSend: function() {
                    $('#cover-spin').show();
                },
                success: function(hsl) {
                    window.location.reload();
                },
                fail: function(){
                    $('#cover-spin').fadeOut();
                    alert('fail');
                }
            });
        });


        $('.btnReject').on('click', function() {
            $.ajax({
                url: "<?php echo base_url().'admin/work/reject_work_dashboard';?>",
                type: "POST",
                data: {
                    xkode: $(this).data('id'),
                    slug: $(this).data('slug')
                },
                beforeSend: function() {
                    $('#cover-spin').show();
                },
                success: function(hsl) {
                    window.location.reload();
                },
                fail: function(){
                    $('#cover-spin').fadeOut();
                    alert('fail');
                }
            });
        });

        $('.btnAcc').on('click', function() {
            $.ajax({
                url: "<?php echo base_url().'admin/work/approve_work_dashboard';?>",
                type: "POST",
                data: {
                    xkode: $(this).data('id'),
                    slug: $(this).data('slug')
                },
                beforeSend: function() {
                    $('#cover-spin').show();
                },
                success: function(hsl) {
                    window.location.reload();
                },
                fail: function(){
                    $('#cover-spin').fadeOut();
                    alert('fail');
                }
            });
        });
        <?php endif; ?>

        <?php if($akses === '3'): ?>
        $('.btnReady').on('click', function() {
            $.ajax({
                url: "<?php echo base_url().'admin/work/ready_test';?>",
                type: "POST",
                data: {
                    xkode: $(this).data('id'),
                    slug: $(this).data('slug')
                },
                beforeSend: function() {
                    $('#cover-spin').show();
                },
                success: function(hsl) {
                    window.location.reload();
                },
                fail: function(){
                    $('#cover-spin').fadeOut();
                    alert('fail');
                }
            });
        });
        $('.btnAPK').on('click', function() {
            $.ajax({
                url: "<?php echo base_url().'admin/work/ready_test_APK';?>",
                type: "POST",
                data: {
                    xkode: $(this).data('id'),
                    slug: $(this).data('slug')
                },
                beforeSend: function() {
                    $('#cover-spin').show();
                },
                success: function(hsl) {
                    window.location.reload();
                },
                fail: function(){
                    $('#cover-spin').fadeOut();
                    alert('fail');
                }
            });
        });
        <?php endif; ?>

        $('.btnOpen').on('click', function() {
            $.ajax({
                url: "<?php echo base_url().'admin/work/progress_work_dashboard';?>",
                type: "POST",
                data: {
                    xkode: $(this).data('id'),
                    slug: $(this).data('slug')
                },
                beforeSend: function() {
                    $('#cover-spin').show();
                },
                success: function(hsl) {
                    window.location.reload();
                },
                fail: function(){
                    $('#cover-spin').fadeOut();
                    alert('fail');
                }
            });
        });
    $('.datatable').DataTable();

    $(".form").submit(function( event ) {
        $('#cover-spin').show();
    });
    
    $(function() {
        CKEDITOR.replace('deskripsi_task2', {
            height: '240px',
            toolbar: [{
                    name: 'clipboard',
                    groups: ['clipboard', 'undo'],
                    items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo',
                        'Redo'
                    ]
                },
                {
                    name: 'editing',
                    groups: ['find', 'selection', 'spellchecker'],
                    items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']
                },
                '/',
                {
                    name: 'basicstyles',
                    groups: ['basicstyles', 'cleanup'],
                    items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript',
                        '-', 'CopyFormatting', 'RemoveFormat'
                    ]
                }
            ]
        });
    });
    

    $(function() {
        CKEDITOR.replace('ckeditor', {
            height: '240px',
            toolbar: [{
                    name: 'clipboard',
                    groups: ['clipboard', 'undo'],
                    items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo',
                        'Redo'
                    ]
                },
                {
                    name: 'editing',
                    groups: ['find', 'selection', 'spellchecker'],
                    items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']
                },
                '/',
                {
                    name: 'basicstyles',
                    groups: ['basicstyles', 'cleanup'],
                    items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript',
                        '-', 'CopyFormatting', 'RemoveFormat'
                    ]
                },
                {
                    name: 'paragraph',
                    groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                    items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-',
                        'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter',
                        'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language'
                    ]
                },
                {
                    name: 'links',
                    items: ['Link', 'Unlink', 'Anchor']
                },
                {
                    name: 'insert',
                    items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar',
                        'PageBreak', 'Iframe'
                    ]
                },
                '/',
                {
                    name: 'styles',
                    items: ['Styles', 'Format', 'Font', 'FontSize']
                },
                {
                    name: 'colors',
                    items: ['TextColor', 'BGColor']
                },
                {
                    name: 'tools',
                    items: ['Maximize', 'ShowBlocks']

                },
            ]
        });
    });
    </script>
    <script src="<?php echo base_url().'assets/js/plugins/jquery-ui/jquery.ui.touch-punch.min.js'; ?>"></script>
    <?php if($this->session->flashdata('msg-akses')=='akses'):?>
        <script type="text/javascript">
        $.toast({
            heading: 'Oops! Something went wrong!',
            text: "You dont have permission to access the menu!",
            showHideTransition: 'slide',
            icon: 'error',
            hideAfter: 5000,
            position: 'bottom-right',
            bgColor: '#FF4859'
        });
        </script>
    <?php endif; ?>
    <?php if($this->session->flashdata('msg')=='show-modal'):?>
    <script type="text/javascript">
        $('#modalWelcome').modal('show');
    </script>
    <?php elseif($this->session->flashdata('msg')=='project-kosong'):?>
        <script type="text/javascript">
        $.toast({
            heading: 'Something went wrong!',
            text: "There is no task in that Project!",
            showHideTransition: 'slide',
            icon: 'error',
            hideAfter: 5000,
            position: 'bottom-right',
            bgColor: '#FF4859'
        });
        </script>
    <?php elseif($this->session->flashdata('msg')=='unverified'):?>
        <script type="text/javascript">
        $.toast({
            heading: 'Something went wrong!',
            text: "Invitation is Expired!",
            showHideTransition: 'slide',
            icon: 'error',
            hideAfter: 5000,
            position: 'bottom-right',
            bgColor: '#FF4859'
        });
        </script>
    <?php elseif($this->session->flashdata('msgPrior')=='priority'):?>
        <script type="text/javascript">
        $.toast({
            heading:" <?php echo $this->session->flashdata('project'); ?>",
            text: "is your main your priority",
            showHideTransition: 'slide',
            icon: 'success',
            hideAfter: 5000,
            position: 'bottom-right',
            bgColor: '#7EC857'
        });
        </script>
    <?php elseif($this->session->flashdata('msg')=='verified'):?>
        <script type="text/javascript">
        $.toast({
            heading: 'Congratulations!',
            text: "You part of this project now!",
            showHideTransition: 'slide',
            icon: 'success',
            hideAfter: 5000,
            position: 'bottom-right',
            bgColor: '#7EC857'
        });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-sentWork'):?>
        <script type="text/javascript">
        $.toast({
            heading: 'Task Request Sent!',
            text: "Waiting for BA/PM Approval",
            showHideTransition: 'slide',
            icon: 'success',
            hideAfter: 5000,
            position: 'bottom-right',
            bgColor: '#7EC857'
        });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-addWork'):?>
        <script type="text/javascript">
        $.toast({
            heading: 'Submitted',
            text: "New Task submitted",
            showHideTransition: 'slide',
            icon: 'success',
            hideAfter: 5000,
            position: 'bottom-right',
            bgColor: '#7EC857'
        });
        </script>
    <?php elseif($this->session->flashdata('msg')=='success-editAssign'):?>
        <script type="text/javascript">
        $.toast({
            heading: 'Updated',
            text: "Successfully update Assign",
            showHideTransition: 'slide',
            icon: 'info',
            hideAfter: 5000,
            position: 'bottom-right',
            bgColor: '#7EC857'
        });
        </script>
    <?php endif; ?>
    <?php if($this->session->userdata('succesComment')=='success_add_comment'): ?>
        <script type="text/javascript">
            $('.otherCon').hide();
            $('.onlineContainer').hide();
            var work_id = "<?php echo $this->session->userdata('idtask'); ?>";
            var objDiv = document.getElementById("scrollDiv");
            setTimeout(() => {
                $('#namaKategori').text('');
                $('#kategori_gambar1').attr('src','');
                $.ajax({
                    url: "<?php echo base_url().'admin/dashboard/get_detail_work';?>",
                    type: "POST",
                    data: {
                        kode: work_id
                    },
                    beforeSend: function() {
                        $('#cover-spin').show();
                    },
                    success: function(hsl) {
                         $('#ModalDetail').show();

                        console.log("hasil : ", hsl);
                        if(hsl === false){
                            alert(hsl);
                        }else{
                            $('#cover-spin').fadeOut();
                            let result = JSON.parse(hsl);
                            var deadline = result.work_deadline;
                            $('#namaKategori').text(result.kategori_nama);
                            $('#namaSprint').text(result.work_sprint);
                            $('#nama_task').text(result.work_nama);
                            $('#work_assignor').text(result.work_assignor);
                            $('#work_assignor_gambar').attr('src','<?php echo base_url();?>assets/images/'+result.work_assignor_gambar);
                            $('#kategori_gambar1').attr('src','<?php echo base_url();?>assets/images/business/'+result.kategori_gambar);
                            $('#deskripsi_task').html(result.work_deskripsi);
                            $('#taskId').attr("value",work_id);
                            $('#device').attr('src','<?php echo base_url();?>theme/images/'+result.work_device);
                            $('#device_desc').text(result.work_device_deskripsi);
                            $('#dic').text(result.work_assignor);
                            $('#deadline_task').text(deadline);
                            $('#idForNewComment').attr("value", work_id);
                            

                            var id_task = result.work_id;
                            for(let i=0;i<result.listComment.length;i++){
                                var attach = '';
                                var idUser = <?php echo $this->session->userdata('idadmin')?>;
                                var userId = result.listComment[i].comment_user_id;
                                var attach1 = result.listComment[i].comment_attach1;
                                var attach2 = result.listComment[i].comment_attach2;
                                var attach3 = result.listComment[i].comment_attach3;
                                var attach4 = result.listComment[i].comment_attach4;

                                if(userId == idUser){
                                    if(attach1 === '' && attach2 === '' && attach3 === '' && attach4 === ''){
                                        attach = '';
                                    }else{
                                        attach = '<div class="row items-push js-gallery img-fluid-100"><div class="col-3">'+attach4+'</div><div class="col-3"> '+attach3+'</div><div class="col-3"> '+attach2+'</div><div class="col-3"> '+attach1+'</div></div>';
                                    }
                                    $('#commentContainer').append('<div class="d-flex flex-row-reverse mb-20"> <div> <a class="img-link img-status" href="javascript:void(0)"> <img class="img-avatar img-avatar" style="width:40px;height:35px" src="http://myta.bacotlu.com/assets/images/'+result.listComment[i].comment_user_gambar+'" alt="Avatar"> <div class="img-status-indicator bg-success"></div> </a> </div> <div class="mx-10 text-right"> <div> <div class="bg-primary-lighter text-dark rounded px-15 py-10 mb-5"> <label for="">'+result.listComment[i].comment_user+'</label> <br>'+result.listComment[i].comment_isi+'<br>'+attach+'</div></div> <div class="text-right text-muted font-size-xs font-italic">'+result.listComment[i].comment_created+'</div> </div></div>')
                                }else{
                                    if(attach1 === '' && attach2 === '' && attach3 === '' && attach4 === ''){
                                        attach = '';
                                    }else{
                                        attach = '<div class="row items-push js-gallery img-fluid-100"><div class="col-3"> '+attach1+'</div><div class="col-3"> '+attach2+'</div><div class="col-3"> '+attach3+'</div><div class="col-3"> '+attach4+'</div></div>';
                                    }
                                    $('#commentContainer').append('<div class="d-flex mb-20"><div> <a class="img-link img-status" href="javascript:void(0)"> <img class="img-avatar img-avatar" style="width:40px;height:35px" src="http://myta.bacotlu.com/assets/images/'+result.listComment[i].comment_user_gambar+'" alt="Avatar"> <div class="img-status-indicator bg-success"></div> </a> </div> <div class="mx-10"> <div> <div class="bg-body-dark text-dark rounded px-15 py-10 mb-5"> <label for="">'+result.listComment[i].comment_user+'</label><br>'+result.listComment[i].comment_isi+'<br>'+attach+'</div></div> <div class="text-muted font-size-xs font-italic">'+result.listComment[i].comment_created+'</div> </div></div>');
                                }
                            }
        
                            // $('#ModalDetail').show();
                            $('.attachment').hide()

                           var taskId = $('#taskId').val();
                            setTimeout(() => {
                                objDiv.scrollTop = objDiv.scrollHeight;
                            }, 0);
                            $.ajax({
                                url: "<?php echo base_url().'admin/comment/unset_userOnline';?>",
                                type: "POST",
                                data: {
                                    kode:id_task
                                },
                                beforeSend: function() {
                                },
                                success: function(hsl) {
                                }
                            });
                            setInterval(function(){
                                $.ajax({
                                    url: "<?php echo base_url().'admin/comment/check_online';?>",
                                    type: "POST",
                                    data: {
                                        kode: id_task
                                    },
                                    beforeSend: function() {
                                    },
                                    success: function(hsl) {
                                        if(hsl === 'noOnline'){
                                            $('.onlineContainer').hide();
                                        }else{
                                            setTimeout(() => {
                                                objDiv.scrollTop = objDiv.scrollHeight;
                                            }, 0);
                                            $('.onlineContainer').show();
                                            let result = JSON.parse(hsl);
                                            console.log('hasil :', result);
                                            for(let p=0;p<result.length;p++){
                                                $('#onlineUserGambar').attr("src", result[p].online_user_gambar);
                                                $('#onlineUserNama').text(result[p].online_user+' is typing...');
                                            }
                                        }
                                    }
                                });
                            },1500);
                            console.log("response : ",result);
                                setInterval(function(){
                                $.ajax({
                                    url: "<?php echo base_url().'admin/comment/hit_newComment';?>",
                                    type: "POST",
                                    data: {
                                        kode: work_id
                                    },
                                    beforeSend: function() {
                                    },
                                    success: function(hsl) {
                                        $('#cover-spin').hide();
                                        var $audio = $('audio');
                                        var audio = $audio[0];
                                        let result = JSON.parse(hsl);
                                        if(result.length > 0){
                                            for(let i=0;i<result.length;i++){
                                                var attach = '';
                                                var attach1 = result[i].comment_attach1;
                                                var attach2 = result[i].comment_attach2;
                                                var attach3 = result[i].comment_attach3;
                                                var attach4 = result[i].comment_attach4;

                                                if(attach1 === "" && attach2 === "" && attach3 === "" && attach4 === ""){
                                                    attach = "";
                                                }else{
                                                    attach = '<div class="row items-push js-gallery img-fluid-100"><div class="col-3"> '+attach1+'</div><div class="col-3"> '+attach2+'</div><div class="col-3"> '+attach3+'</div><div class="col-3"> '+attach4+'</div></div>';
                                                }
               
                                                $('#commentContainer').append('<div class="d-flex mb-20"><div> <a class="img-link img-status" href="javascript:void(0)"> <img class="img-avatar img-avatar" style="width:40px;height:35px" src="http://myta.bacotlu.com/assets/images/'+result[i].comment_user_gambar+'" alt="Avatar"> <div class="img-status-indicator bg-success"></div> </a> </div> <div class="mx-10"> <div> <div class="bg-body-dark text-dark rounded px-15 py-10 mb-5"> <label for="">'+result[i].comment_user+'</label><br>'+result[i].comment_isi+'<br>'+attach+'</div></div> <div class="text-muted font-size-xs font-italic">'+result[i].comment_created+'</div> </div></div>');
                                                setTimeout(() => {
                                                    objDiv.scrollTop = objDiv.scrollHeight;
                                                }, 0);
                                                $.ajax({
                                                    url: "<?php echo base_url().'admin/comment/read_comment';?>",
                                                    type: "POST",
                                                    data: {
                                                        kode: work_id,
                                                    },
                                                    success: function(hsl) {
                                                        console.log('updated', hsl);
                                                    }
                                                }); 
                                            }
                                        }
                                    }
                                });
                            },3000);
                        }
                    },
                    fail: function(){
                        // $('#cover-spin').fadeOut();
                        alert('fail');
                    }
                });
            },0);
        </script>
    <?php endif;?>

</body>
</html>