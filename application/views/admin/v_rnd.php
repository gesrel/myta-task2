<!doctype html>
<html lang="en" class="no-focus">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Research Development | MYTA</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

    <!-- END Icons -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>" />




</head>

<body>
    <!-- Page Container -->

    <div id="page-container" class="sidebar-o side-scroll main-content-boxed side-trans-enabled page-header-fixed <?php if($dark === 'dark'): ?>page-header-inverse sidebar-inverse <?php endif; ?>">
        <?php echo $this->load->view('admin/v_sidemenu.php');?>
        <!-- Header -->
        <?php echo $this->load->view('admin/header.php');?>
        <!-- END Header -->

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content content-full">
                <div class="py-30 text-center">
                    <div class="display-3 text-corporate">
                        <i class="si si-wrench"></i>
                    </div>
                        <h1 class="h2 font-w700 mt-30 mb-10">Oops.. Under Construction.</h1>
                        <h2 class="h3 font-w400 text-muted mb-50">We are sorry but this feature still under construction..</h2>
                    </div>
                </div>
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
        <!-- Footer -->
        <?php echo $this->load->view('admin/v_footer');?>
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <!-- Modal Hapus -->
    <form action="<?php echo base_url().'admin/kategori/simpan_kategori'?>" method="post" enctype="multipart/form-data">
        <div class="modal fade in" id="ModalAddNew" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Create New Project</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="file" name="filefoto" class="dropify" data-height="140" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="xkategori" class="form-control" placeholder="Project Name"
                                    required>
                            </div>
                            <div class="form-group">
                                <select name="xtl" class="js-select2 form-control" id="example-select2" required>
                                    <option value="">Choose Tech Lead</option>
                                    <?php foreach($tl->result() as $tlead): ?>
                                        <option value="<?php echo $tlead->pengguna_id;?>"><?php echo $tlead->pengguna_nama; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-6">
                                        <label for="">New Task Start</label>
                                        <input type="time" name="xstart" class="form-control" required>
                                    </div>
                                    <div class="col-6">
                                        <label for="">New Task End</label>
                                        <input type="time" name="xend" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea type="text" name="xdeskripsi" class="form-control" placeholder="Deskripsi"
                                    required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-square">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END Normal Modal -->


    <!-- Codebase Core JS -->
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
     <script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>

    <script type="text/javascript">
    
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });

        //Show Modal Update Kategori
        $('.btn-edit').on('click', function() {
            var kategori_id = $(this).data('id');
            var kategori_nama = $(this).data('kategori');
            var kategori_deskripsi = $(this).data('deskripsi');
            var kategori_tl_id = $(this).data('tl');
            var kategori_start = $(this).data('start');
            var kategori_end = $(this).data('end');

            $('#ModalUpdate').modal('show');
            $('[name="xkode"]').val(kategori_id);
            $('[name="xkategori2"]').val(kategori_nama);
            $('[name="xtl"]').val(kategori_tl_id);
            $('[name="xdeskripsi"]').val(kategori_deskripsi);
            $('[name="xstart"]').val(kategori_start);
            $('[name="xend"]').val(kategori_end);
        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var kategori_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(kategori_id);
        });

    });
    </script>
    
    <script type="text/javascript">
    $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
    $(function() {
        CKEDITOR.replace('ckeditor', {
            height: '240px',
            toolbar: [{
                    name: 'clipboard',
                    groups: ['clipboard', 'undo'],
                    items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo',
                        'Redo'
                    ]
                },
                {
                    name: 'editing',
                    groups: ['find', 'selection', 'spellchecker'],
                    items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']
                },
                '/',
                {
                    name: 'basicstyles',
                    groups: ['basicstyles', 'cleanup'],
                    items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript',
                        '-', 'CopyFormatting', 'RemoveFormat'
                    ]
                },
                {
                    name: 'paragraph',
                    groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                    items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-',
                        'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter',
                        'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language'
                    ]
                },
                {
                    name: 'links',
                    items: ['Link', 'Unlink', 'Anchor']
                },
                {
                    name: 'insert',
                    items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar',
                        'PageBreak', 'Iframe'
                    ]
                },
                '/',
                {
                    name: 'styles',
                    items: ['Styles', 'Format', 'Font', 'FontSize']
                },
                {
                    name: 'colors',
                    items: ['TextColor', 'BGColor']
                },
            ]
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });



        $('.btn-detail').on('click', function() {
            var work_deskripsi = $(this).data('deskripsi');
            $('#ModalDetail').modal('show');
            $('[name="deskripsi"]').val(work_deskripsi);
        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var work_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(work_id);
        });

    });
    </script>
  
    <?php if($this->session->flashdata('msg')=='gagal-hapus'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Error',
        text: "There is still assignment attached in that project",
        showHideTransition: 'slide',
        icon: 'error',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#FF4859'
    });
    </script>
  
    <?php elseif($this->session->flashdata('msg')=='show-modal'):?>
    <script type="text/javascript">
    $('#ModalResetPassword').modal('show');
    </script>
    <?php else:?>

    <?php endif;?>

</body>

</html>