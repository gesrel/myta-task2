<!doctype html>
<html lang="en" class="no-focus">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Calendar</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

    <!-- END Icons -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">
    <link rel="stylesheet" id="css-main" href="https://demo.pixelcave.com/codebase/assets/css/codebase.min-3.3.css';?>">

    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/fullcalendar/fullcalendar.min.css'?>">
</head>

<body>
    <!-- Page Container -->

    <div id="page-container"
        class="sidebar-o side-scroll main-content-boxed side-trans-enabled page-header-fixed ">

        <?php echo $this->load->view('admin/v_sidemenu.php');?>
        <!-- Header -->
        <?php echo $this->load->view('admin/header.php');?>
        <!-- END Header -->

        <!-- Main Container -->
        <main id="main-container">
            <div class="content">
                <div class="block">
                    <div class="block-content">
                        <div class="row items-push">
                            <div class="col-xl-9">
                                <div class="js-calendar"></div>
                            </div>
                            <div class="col-xl-3 d-none d-xl-block">
                                <form class="js-form-add-event mb-30" action="be_comp_calendar.php" method="post">
                                    <div class="input-group">
                                        <input type="text" class="js-add-event form-control" placeholder="Add Event..">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-secondary">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <ul class="js-events list list-events">
                                    <li class="bg-info-light">Project Mars</li>
                                    <li class="bg-success-light">Cinema</li>
                                    <li class="bg-danger-light">Project X</li>
                                    <li class="bg-warning-light">Skype Meeting</li>
                                    <li class="bg-info-light">Codename PX</li>
                                    <li class="bg-success-light">Weekend Adventure</li>
                                    <li class="bg-warning-light">Meeting</li>
                                    <li class="bg-success-light">Walk the dog</li>
                                    <li class="bg-info-light">AI schedule</li>
                                </ul>
                                <div class="text-center">
                                    <em class="font-size-xs text-muted"><i class="fa fa-arrows"></i> Drag and drop events on the calendar</em>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- END Main Container -->
        <!-- Footer -->
        <?php echo $this->load->view('admin/v_footer');?>
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <!-- Modal Hapus -->
    <form action="<?php echo base_url().'admin/assignor/hapus_assignor'?>" method="post">
        <div class="modal" id="Modalhapus" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Info</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <p>Anda yakin mau menghapus assignor ini?</p>
                            <input type="hidden" name="kode" id="kode" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-primary btn-square">Ya</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END Normal Modal -->


    <!-- Codebase Core JS -->
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
    <script src="assets/js/codebase.core.min-3.3.js"></script>
<script src="<?php echo base_url().'assets/js/plugins/jquery-ui/jquery-ui.min.js';?>"></script>
<script src="<?php echo base_url().'assets/js/plugins/moment/moment.min.js';?>"></script>
<script src="<?php echo base_url().'assets/js/plugins/fullcalendar/fullcalendar.min.js';?>"></script>
<script src="<?php echo base_url().'assets/js/pages/be_comp_calendar.min.js';?>"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });

        //Show Modal Update assignor
        $('.btn-edit').on('click', function() {
            var assignor_id = $(this).data('id');
            var assignor_nama = $(this).data('nama');
            var assignor_email = $(this).data('email');
            $('#ModalUpdate').modal('show');
            $('[name="xkode"]').val(assignor_id);
            $('[name="xnama"]').val(assignor_nama);
            $('[name="xemail"]').val(assignor_email);
        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var assignor_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(assignor_id);
        });

    });
    </script>

</body>

</html>