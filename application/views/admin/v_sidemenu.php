 <!-- Sidebar -->

 <nav id="sidebar">
     <!-- Sidebar Scroll Container -->
     <div id="sidebar-scroll">
         <!-- Sidebar Content -->
         <div class="sidebar-content">
             <!-- Side Header -->
             <div class="content-header content-header-fullrow px-15">
                 <!-- Mini Mode -->
                 <div class="content-header-section sidebar-mini-visible-b">
                     <!-- Logo -->
                     <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                         <a class="link-effect font-w700" href="<?php echo base_url().'admin/dashboard'?>">
                             <img src="<?php echo base_url().'theme/images/favicon.png';?>" width="30px">
                         </a>
                     </span>
                     <!-- END Logo -->
                 </div>
                 <!-- END Mini Mode -->

                 <!-- Normal Mode -->
                 <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                     <!-- Close Sidebar, Visible only on mobile screens -->
                     <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                     <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r"
                         data-toggle="layout" data-action="sidebar_close">
                         <i class="fa fa-times text-danger"></i>
                     </button>
                     <!-- END Close Sidebar -->

                     <!-- Logo -->
                     <div class="content-header-item">
                         <a class="link-effect font-w700" href="<?php echo base_url().'admin/dashboard'?>">
                             <img style="margin-top:-5px" src="<?php echo base_url().'theme/images/myta.png';?>"
                                 width="140px">
                                 <!-- <h2 style="color:#fff">myta</h2> -->
                         </a>
                     </div>
                     <!-- END Logo -->
                 </div>
                 <!-- END Normal Mode -->
             </div>
             <!-- END Side Header -->
             <?php 
                error_reporting(0);
                $idadmin=$this->session->userdata('idadmin');
                $query=$this->db->query("SELECT * FROM pengguna WHERE pengguna_id='$idadmin'");
                $data=$query->row_array();
            ?>
             <!-- Side User -->
             <div class="content-side content-side-full content-side-user px-10 align-parent">
                 <!-- Visible only in mini mode -->
                 <div class="sidebar-mini-visible-b align-v animated fadeIn">
                     <?php if($data['pengguna_photo'] === ''):?>
                     <a class="img-link" href="<?php echo base_url().'assets/images/user_blank.png'?>">
                         <img class="img-avatar img-avatar32"
                             src="<?php echo base_url().'assets/images/user_blank.png'?>" alt="">
                     </a>
                     <?php else:?>
                     <a class="img-link" href="#">
                         <img class="img-avatar img-avatar32"
                             src="<?php echo base_url().'assets/images/'.$data['pengguna_photo'];?>" alt="">
                     </a>
                     <?php endif;?>
                 </div>
                 <!-- END Visible only in mini mode -->

                 <!-- Visible only in normal mode -->
                 <div class="sidebar-mini-hidden-b text-center">
                     <?php if($data['pengguna_photo'] === '' || $data['pengguna_photo'] === null):?>
                        <?php if($data['pengguna_jenkel'] === 'L'): ?>
                            <a class="img-link" href="<?php echo base_url().'assets/images/user_blank.png'?>">
                                <img class="img-avatar" src="<?php echo base_url().'assets/images/user_blank.png'?>" alt="">
                            </a>
                        <?php else: ?>
                            <a class="img-link" href="<?php echo base_url().'assets/images/user_blank2.png'?>">
                                <img class="img-avatar" src="<?php echo base_url().'assets/images/user_blank2.png'?>" alt="">
                            </a>
                        <?php endif; ?>
                     <?php else:?>
                     <a class="img-link" href="#">
                         <img class="img-avatar" src="<?php echo base_url().'assets/images/'.$data['pengguna_photo'];?>"
                             alt="">
                     </a>
                     <?php endif;?>
                     <ul class="list-inline mt-10">
                         <li class="list-inline-item">
                             <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase"
                                 href="#"><?php echo $this->session->userdata('nama');?></a><br>
                             <?php if($this->session->userdata('akses') === '4'): ?>
                             <p class="text-dual-primary-dark font-size-xs font-w600 text-uppercase"> Business Analyst
                             </p>
                             <?php elseif($this->session->userdata('akses') === '2'): ?>
                             <p class="text-dual-primary-dark font-size-xs font-w600 text-uppercase"> Project Manager
                             </p>
                             <?php elseif($this->session->userdata('akses') === '3'): ?>
                             <p class="text-dual-primary-dark font-size-xs font-w600 text-uppercase"> Developer</p>
                             <?php elseif($this->session->userdata('akses') === '5'): ?>
                             <p class="text-dual-primary-dark font-size-xs font-w600 text-uppercase"> Tech Lead</p>
                             <?php elseif($this->session->userdata('akses') === '1'): ?>
                             <p class="text-dual-primary-dark font-size-xs font-w600 text-uppercase"> Management</p>
                             <?php else: ?>
                             <?php endif; ?>
                         </li>
                     </ul>

                 </div>


                 <!-- END Visible only in normal mode -->
             </div>
             <!-- END Side User -->

             <!-- Side Navigation -->
             <div class="content-side content-side-full">
                 <ul class="nav-main">
                     <?php if($this->session->userdata('akses') === '1'): ?>
                     <li>
                         <a class="<?php if($this->uri->segment(2) === 'dashboard'): ?>active <?php else:endif; ?>"
                             href="<?php echo base_url().'admin/dashboard'?>"><i class="si si-screen-desktop"></i><span
                                 class="sidebar-mini-hide">Dashboard</span></a>
                     </li>
                     <li>
                         <a class="<?php if($this->uri->segment(2) === 'rnd'): ?>active <?php endif; ?>" href="<?php echo base_url().'admin/rnd'?>"><i class="fa fa-code""></i><span
                                 class="sidebar-mini-hide">Research & Dev</span></a>
                     </li>
                     <li>
                         <a class="<?php if($this->uri->segment(2) === 'pengguna'): ?>active <?php else:endif; ?>"
                             href="<?php echo base_url().'admin/pengguna'?>"><i class="si si-user"></i><span
                                 class="sidebar-mini-hide">Users</span></a>
                     </li>

                     <?php elseif($this->session->userdata('akses') === '4'): ?>
                     <li>
                         <a class="<?php if($this->uri->segment(2) === 'dashboard'): ?>active <?php else:endif; ?>"
                             href="<?php echo base_url().'admin/dashboard'?>"><i class="si si-screen-desktop"></i><span
                                 class="sidebar-mini-hide">Dashboard</span></a>
                     </li>
                     <li>
                         <a href="<?php echo base_url().'admin/dashboard'?>"><i class="fa fa-code""></i><span
                                 class="sidebar-mini-hide">Research & Dev</span></a>
                     </li>

                     <?php elseif($this->session->userdata('akses') === '2' ): ?>
                     <li>
                         <a class="<?php if($this->uri->segment(2) === 'dashboard'): ?>active <?php else:endif; ?>"
                             href="<?php echo base_url().'admin/dashboard'?>"><i class="si si-screen-desktop"></i><span
                                 class="sidebar-mini-hide">Dashboard</span></a>
                     </li>
                     <li>
                         <a class="<?php if($this->uri->segment(2) === 'kategori'): ?>active <?php else:endif; ?>"
                             href="<?php echo base_url().'admin/kategori'?>"><i class="si si-briefcase"></i><span
                                 class="sidebar-mini-hide">Project</span></a>
                     </li>
                     <li>
                         <a class="<?php if($this->uri->segment(2) === 'rnd'): ?>active <?php endif; ?>" href="<?php echo base_url().'admin/rnd'?>"><i class="fa fa-code""></i><span
                                 class="sidebar-mini-hide">Research & Dev</span></a>
                     </li>
                     <?php elseif($this->session->userdata('akses') === '5' ): ?>
                     <li>
                         <a class="<?php if($this->uri->segment(2) === 'dashboard'): ?>active <?php else:endif; ?>"
                             href="<?php echo base_url().'admin/dashboard'?>"><i class="si si-screen-desktop"></i><span
                                 class="sidebar-mini-hide">Dashboard</span></a>
                     </li>  
                     <li>
                         <a class="<?php if($this->uri->segment(2) === 'rnd'): ?>active <?php endif; ?>" href="<?php echo base_url().'admin/rnd'?>"><i class="fa fa-code""></i><span
                                 class="sidebar-mini-hide">Research & Dev</span></a>
                     </li>
                     <li>
                         <a class="<?php if($this->uri->segment(2) === 'tim'): ?>active <?php else:endif; ?>"
                             href="<?php echo base_url().'admin/tim'?>"><i class="si si-users"></i><span
                                 class="sidebar-mini-hide">Team</span></a>
                     </li>



                     <?php else: ?>
                     <li>
                         <a class="<?php if($this->uri->segment(2) === 'dashboard'): ?>active <?php else:endif; ?>"
                             href="<?php echo base_url().'admin/dashboard'?>"><i class="si si-screen-desktop"></i><span
                                 class="sidebar-mini-hide">Dashboard</span></a>
                     </li>
                     <li>
                         <a class="<?php if($this->uri->segment(2) === 'rnd'): ?>active <?php endif; ?>" href="<?php echo base_url().'admin/rnd'?>"><i class="fa fa-code""></i><span
                                 class="sidebar-mini-hide">Research & Dev</span></a>
                     </li>
                     <li>
                         <a class="<?php if($this->uri->segment(2) === 'compile'): ?>active <?php else:endif; ?>"
                             href="<?php echo base_url().'admin/compile'?>"><i class="fa fa-android"></i><span
                                 class="sidebar-mini-hide">App Package</span></a>
                     </li>

                     <?php endif;?>

                 </ul>
             </div>
             <!-- END Side Navigation -->

         </div>
         <!-- Sidebar Content -->
     </div>
     <!-- END Sidebar Scroll Container -->
 </nav>
 <!-- END Sidebar -->