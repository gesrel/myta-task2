<!doctype html>
<html lang="en" class="no-focus">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Reponse Time Work | Myta My Task</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

    <!-- END Icons -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>" />




</head>

<body>
    <!-- Page Container -->

    <div id="page-container"
        class="sidebar-o side-scroll main-content-boxed side-trans-enabled page-header-fixed ">


        <?php echo $this->load->view('admin/v_sidemenu.php');?>


        <!-- Header -->
        <?php echo $this->load->view('admin/header.php');?>
        <!-- END Header -->

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <div class="row">
                    <div class="col-md-8">
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Responsing Task</h3>
                                <div class="block-options">
                                </div>
                            </div>
                            <div class="block-content block-content-full">
                                <table id="" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 120px;text-align: left;">No</th>
                                            <th>Name</th>
                                            <th>Duration</th>
                                            <th style="text-align:center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $no=0;
                                            foreach ($response->result() as $row) :
                                            $no++;
                                        ?>
                                        <tr>
                                            <td><?php echo $no;?></td>
                                            <td><?php echo $row->response_nama;?></td>
                                            <td><?php echo $row->response_durasi; ?> Minutes</td>
                                            <td style="width: 90px;text-align: center;">
                                                <a href="javascript:void(0);"
                                                    class="btn btn-sm btn-secondary btn-circle btn-editResponse"
                                                    data-id="<?php echo $row->response_id;?>"
                                                    data-nama="<?php echo $row->response_nama;?>"
                                                    data-durasi="<?php echo $row->response_durasi;?>"><span
                                                        class="fa fa-pencil"></span></a>
                                                
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Developer Revert Task</h3>
                                <div class="block-options">
                                </div>
                            </div>
                            <div class="block-content block-content-full">
                                <table id="" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 120px;text-align: left;">No</th>
                                            <th>Name</th>
                                            <th>Amount Revert</th>
                                            <th style="text-align:center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $no=0;
                                            foreach ($revertedDev->result() as $row) :
                                            $no++;
                                        ?>
                                        <tr>
                                            <td><?php echo $no;?></td>
                                            <td><?php echo $row->reverted_nama;?></td>
                                            <td><?php echo $row->reverted_jumlah; ?> times</td>
                                            <td style="width: 90px;text-align: center;">
                                                <a href="javascript:void(0);"
                                                    class="btn btn-sm btn-secondary btn-circle btn-editRevert"
                                                    data-id="<?php echo $row->reverted_id;?>"
                                                    data-nama="<?php echo $row->reverted_nama;?>"
                                                    data-jumlah="<?php echo $row->reverted_jumlah;?>"><span
                                                        class="fa fa-pencil"></span></a>
                                                
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">BA Revert Task</h3>
                                <div class="block-options">
                                </div>
                            </div>
                            <div class="block-content block-content-full">
                                <table id="" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 120px;text-align: left;">No</th>
                                            <th>Name</th>
                                            <th>Amount Revert</th>
                                            <th style="text-align:center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $no=0;
                                            foreach ($revertedBA->result() as $row) :
                                            $no++;
                                        ?>
                                        <tr>
                                            <td><?php echo $no;?></td>
                                            <td><?php echo $row->reverted_nama;?></td>
                                            <td><?php echo $row->reverted_jumlah; ?> times</td>
                                            <td style="width: 90px;text-align: center;">
                                                <a href="javascript:void(0);"
                                                    class="btn btn-sm btn-secondary btn-circle btn-editRevert"
                                                    data-id="<?php echo $row->reverted_id;?>"
                                                    data-nama="<?php echo $row->reverted_nama;?>"
                                                    data-jumlah="<?php echo $row->reverted_jumlah;?>"><span
                                                        class="fa fa-pencil"></span></a>
                                                
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                                            </div>
                    <div class="col-md-4">
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Project Detail</h3>
                                <div class="block-options">
                                </div>
                            </div>
                            <div class="block-content block-content-full">
                                    <table class="table table-striped">
                                        <tr>
                                            <td>
                                                <center>
                                                    <img style="border-radius:50px" src="<?php echo base_url().'assets/images/business/'.$project['kategori_gambar'];?>" width="100px" alt=""><br></br>
                                                    <h4><?php echo $project['kategori_nama'];?></h4>
                                                    <span class="badge badge-primary"> 
                                                        <?php echo date("H i : A", strtotime($project['kategori_active_start'])); ?>
                                                    </span>
                                                    <span class="badge badge-warning"> 
                                                        <?php echo date("H i : A", strtotime($project['kategori_active_end'])); ?>
                                                    </span>
                                                </center>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?php echo $project['kategori_deskripsi'];?></td>
                                        </tr>
                                        <tr>
                                            <td> 
                                                <?php
                                                    $linkImg = $project['pengguna_photo'];
                                                    if($linkImg === ''){
                                                        if($project['pengguna_jenkel'] === 'L'){
                                                            $linkImg = 'user_blank.png';
                                                        }else{
                                                            $linkImg = 'user_blank2.png';
                                                        }
                                                    }
                                                ?>
                                                <img src="<?php echo base_url().'assets/images/'.$linkImg;?>" width="60px" alt="">
                                                <?php echo $project['pengguna_nama'];?>
                                            </td>
                                        </tr>
                                    </table>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
        <!-- Footer -->
        <?php echo $this->load->view('admin/v_footer');?>
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <!-- Modal Hapus -->
    <form action="<?php echo base_url().'admin/assesment/update_response'?>" method="post" enctype="multipart/form-data">
        <div class="modal" id="ModalEditResponse" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title" name="responseName"></h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            
                            <div class="form-group">
                                <label for="">Response in Minutes</label>
                                <input type="hidden" value="<?php echo $this->uri->segment(4);?>" name="xslug" required>
                                <input type="text" name="xdurasi" class="form-control" placeholder="Durasi"
                                    required>
                            </div>
                            <input type="hidden" name="xkode" id="kode" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-square">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END Normal Modal -->

    <!-- Modal Hapus -->
    <form action="<?php echo base_url().'admin/assesment/update_reverted'?>" method="post" enctype="multipart/form-data">
        <div class="modal" id="ModalRevert" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title" name="revertedName"></h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            
                            <div class="form-group">
                                <input type="text" name="xjumlah" class="form-control" placeholder="Jumlah"
                                    required>
                            </div>
                            <input type="hidden" value="<?php echo $this->uri->segment(4);?>" name="xslug" required>
                            <input type="hidden" name="xkode" id="kode" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-square">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END Normal Modal -->


    <!-- Codebase Core JS -->
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
     <script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });

        //Show Modal Update Kategori
        $('.btn-editResponse').on('click', function() {
            var id = $(this).data('id');
            var nama = $(this).data('nama');
            var durasi = $(this).data('durasi');
            $('#ModalEditResponse').modal('show');
            $('[name="responseName"').text(nama)
            $('[name="xkode"]').val(id);
            $('[name="xdurasi"]').val(durasi);
        });


        //Show Modal Update Kategori
        $('.btn-editRevert').on('click', function() {
            var id = $(this).data('id');
            var nama = $(this).data('nama');
            var jumlah = $(this).data('jumlah');
            $('#ModalRevert').modal('show');
            $('[name="revertedName"').text(nama)
            $('[name="xkode"]').val(id);
            $('[name="xjumlah"]').val(jumlah);
        });

 

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var kategori_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(kategori_id);
        });

    });
    </script>
    
    <script type="text/javascript">
    $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
    $(function() {
        CKEDITOR.replace('ckeditor', {
            height: '240px',
            toolbar: [{
                    name: 'clipboard',
                    groups: ['clipboard', 'undo'],
                    items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo',
                        'Redo'
                    ]
                },
                {
                    name: 'editing',
                    groups: ['find', 'selection', 'spellchecker'],
                    items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']
                },
                '/',
                {
                    name: 'basicstyles',
                    groups: ['basicstyles', 'cleanup'],
                    items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript',
                        '-', 'CopyFormatting', 'RemoveFormat'
                    ]
                },
                {
                    name: 'paragraph',
                    groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                    items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-',
                        'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter',
                        'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language'
                    ]
                },
                {
                    name: 'links',
                    items: ['Link', 'Unlink', 'Anchor']
                },
                {
                    name: 'insert',
                    items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar',
                        'PageBreak', 'Iframe'
                    ]
                },
                '/',
                {
                    name: 'styles',
                    items: ['Styles', 'Format', 'Font', 'FontSize']
                },
                {
                    name: 'colors',
                    items: ['TextColor', 'BGColor']
                },
            ]
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });



        $('.btn-detail').on('click', function() {
            var work_deskripsi = $(this).data('deskripsi');
            $('#ModalDetail').modal('show');
            $('[name="deskripsi"]').val(work_deskripsi);
        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var work_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(work_id);
        });

    });
    </script>
  
    <?php if($this->session->flashdata('msg')=='gagal-hapus'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Error',
        text: "There is still assignment attached in that project",
        showHideTransition: 'slide',
        icon: 'error',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#FF4859'
    });
    </script>
  
    <?php elseif($this->session->flashdata('msg')=='show-modal'):?>
    <script type="text/javascript">
    $('#ModalResetPassword').modal('show');
    </script>
    <?php else:?>

    <?php endif;?>

</body>

</html>