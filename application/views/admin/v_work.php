<!doctype html>
<html lang="en" class="no-focus">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Work List</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

    <!-- END Icons -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">




</head>

<body>
    <!-- Page Container -->

    <div id="page-container"
        class="sidebar-o side-scroll main-content-boxed side-trans-enabled page-header-fixed <?php if($dark === 'dark'): ?>page-header-inverse sidebar-inverse <?php endif; ?>">


        <?php echo $this->load->view('admin/v_sidemenu.php');?>


        <!-- Header -->
        <?php echo $this->load->view('admin/header.php');?>
        <!-- END Header -->

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Work List</h3>
                                <div class="block-options">
                                </div>
                            </div>
                            <div style="overflow: auto;" class="block-content block-content-full">
                                <table id="mytable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 60px;text-align: left;">No</th>
                                            <th style="width:190px">Project</th>
                                            <th>Assign to</th>
                                            <th>Work</th>
                                            <th>Deadline</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $no=0;
                                            foreach ($data->result() as $row) :
                                            $no++;
                                        ?>
                                        <tr>
                                            <td><?php echo $no;?></td>
                                            <td>
                                                <?php $kategori1 = $this->db->query("SELECT * FROM kategori where kategori_id='$row->work_kategori_id'"); ?>
                                                <?php foreach($kategori1->result() as $kt):?>
                                                <img src="<?php echo base_url().'assets/images/business/'.$kt->kategori_gambar;?>"
                                                    width="30px" alt="">
                                                <?php echo $kt->kategori_nama;?>
                                                <?php endforeach; ?>
                                            </td>
                                            <td>

                                                <?php 
                                                if($this->session->userdata('akses') === '4' || $this->session->userdata('akses') === '2'){
                                                    $assignor1 = $this->db->query("SELECT * FROM pengguna where pengguna_id='$row->work_assignor_id'");
                                                }else{
                                                    $assignor1 = $this->db->query("SELECT * FROM pengguna where pengguna_id='$row->work_user_id'");
                                                }?>
                                                <?php foreach($assignor1->result() as $assig):?>
                                                <?php echo $assig->pengguna_nama;?>
                                                (<?php echo $assig->pengguna_email;?>)
                                                <?php endforeach; ?>
                                            </td>
                                            <td><a href="#!" data-toggle="modal"
                                                    data-target="#ModalDetail<?php echo $row->work_id; ?>"><?php echo $row->work_nama;?></a><br>
                                                <?php if($row->work_device === 'ANDROIOS'): ?>
                                                <img src="<?php echo base_url().'theme/images/ios.png';?>" width="30px"
                                                    alt="">
                                                <img src="<?php echo base_url().'theme/images/android.png';?>"
                                                    width="20px" alt="">
                                                <?php elseif($row->work_device === 'IOS'): ?>
                                                <img src="<?php echo base_url().'theme/images/ios.png';?>" width="30px"
                                                    alt="">
                                                <?php elseif($row->work_device === 'ANDROID'): ?>
                                                <img src="<?php echo base_url().'theme/images/android.png';?>"
                                                    width="20px" alt="">
                                                <?php else: ?>
                                                <img src="https://images.vexels.com/media/users/3/128132/isolated/preview/fa3b9aad78a9db81459bd03294a0f985-flat-laptop-icon-design-by-vexels.png"
                                                    width="30px" alt="">
                                                <?php endif; ?>
                                            </td>
                                            <th><?php echo date("d M Y H:i A", strtotime($row->work_deadline));?></th>
                                            <td>
                                                <?php if($row->work_status === 'A'): ?>
                                                <span class="badge badge-info"> Progress <i
                                                        class="fa fa-spinner fa-spin"></i></span>
                                                <?php elseif($row->work_status === 'C'): ?>
                                                <span class="badge badge-danger"> Canceled <i
                                                        class="fa fa-warning"></i></span>
                                                <?php elseif($row->work_status === 'B'):?>
                                                <span class="badge badge-success"> Done <i
                                                        class="fa fa-check"></i></span>
                                                <?php elseif($row->work_status === 'D'):?>
                                                <span class="badge badge-success"> Ready to Test <i
                                                        class="fa fa-check"></i></span>
                                                <?php elseif($row->work_status === 'E'):?>
                                                <span class="badge badge-primary"> Wait For APK/IPA <i
                                                        class="fa fa-android"></i> <i class="fa fa-apple"></i></span>
                                                <?php elseif($row->work_status === 'E'):?>
                                                <span class="badge badge-secondary"> Wait For Approval <i
                                                        class="fa fa-spinner fa-pulse fa-spin"></i>/span>
                                                    <?php endif; ?>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
        <!-- Footer -->
        <?php echo $this->load->view('admin/v_footer');?>
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->


    <!-- Modal Detail -->
    <?php
    foreach($workDetail->result() as $wrkD): ?>
    <div class="modal fade in" id="ModalDetail<?php echo $wrkD->work_id;?>" tabindex="-1" role="dialog"
        aria-labelledby="modal-normal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Detail Assignment</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-3">
                                <?php 
                                $busD = $this->db->query("SELECT * FROM kategori where kategori_id='$wrkD->work_kategori_id'");
                                foreach($busD->result() as $busDe): ?>
                                <img src="<?php echo base_url().'assets/images/business/'.$busDe->kategori_gambar;?>"
                                    width="80%" alt=""><br>
                                <small><?php echo $busDe->kategori_slug; ?></small>

                                <?php endforeach; ?>
                            </div>
                            <div class="col-9">
                                <h3><?php echo $wrkD->work_nama; ?></h3>
                                <?php 
                                $assignD2 = $this->db->query("SELECT * FROM pengguna where pengguna_id='$wrkD->work_assignor_id'");
                                foreach($assignD2->result() as $assigD2): ?>
                                Assign : <?php echo $assigD2->pengguna_nama; ?>
                                <?php endforeach; ?>
                                <br>
                                Deadline :
                                <b><?php echo date('l, d M Y H:i', strtotime($wrkD->work_deadline));?></b></br>
                                Device :<?php if($wrkD->work_device === 'ANDROIOS'): ?>
                                <img src="<?php echo base_url().'theme/images/ios.png';?>" width="40px" alt="">
                                <img src="<?php echo base_url().'theme/images/android.png';?>" width="30px" alt="">
                                <?php elseif($wrkD->work_device === 'IOS'): ?>
                                <img src="<?php echo base_url().'theme/images/ios.png';?>" width="40px" alt="">
                                <?php elseif($wrkD->work_device === 'ANDROID'): ?>
                                <img src="<?php echo base_url().'theme/images/android.png';?>" width="30px" alt="">
                                <?php else: ?>
                                <?php endif; ?>
                            </div>
                        </div><br>
                        <div style="width:100%"><?php echo $wrkD->work_deskripsi;?></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <form action="<?php echo base_url().'admin/work/export_word';?>" method="post">
                        <input type="hidden" name="xkode" value="<?php echo $wrkD->work_id;?>" required>
                        <button type="submit" class="btn btn-primary btn-sm btn-square"><span
                                class="fa fa-download"></span> Download Word</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>


    <!-- Codebase Core JS -->
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });



        $('.btn-detail').on('click', function() {
            var work_deskripsi = $(this).data('deskripsi');
            $('#ModalDetail').modal('show');
            $('[name="deskripsi"]').val(work_deskripsi);
        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var work_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(work_id);
        });

    });
    </script>
    <script type="text/javascript">
    < ? php foreach($data - > result() as $wrk): ? >
        $(function() {
            CKEDITOR.replace('ckeditor<?php echo $wrk->work_id; ?>', {
                height: '240px',
                extraPlugins: 'syntaxhighlight',
                toolbar: [
                    ['Source'],
                    ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink', '-',
                        'Image', '-', 'Blockquote', '-', 'Styles', '-', 'Format', '-', 'FontSize'
                    ]

                ]
            });
        }); <
    ?
    php endforeach; ? >
    </script>

</body>

</html>