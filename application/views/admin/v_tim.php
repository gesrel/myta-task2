<!doctype html>
<html lang="en" class="no-focus">
<!--<![endif]-->

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>MYTA</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

    <!-- END Icons -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
    <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/js/plugins/select2/select2.css'?>">
</head>


<body>
    <!-- Page Container -->

    <div id="page-container"
        class="sidebar-o side-scroll side-trans-enabled page-header-fixed <?php if($dark === 'dark'): ?>page-header-inverse sidebar-inverse <?php endif; ?>">
        <?php echo $this->load->view('admin/v_sidemenu.php');?>
        <!-- Header -->
        <?php echo $this->load->view('admin/header.php');?>
        <!-- END Header -->

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block">
                            <div class="block-header block-header-default">
                                <div>
                                    <h2 style="font-weight:lighter;margin-bottom:10px">Project Team</h2>
                                    <span>Submit your team to start monitoring.</span>
                                </div>
                                <div class="block-options">
                                    <?php if($data->num_rows() > 0): ?>
                                    <a href="#!" class="btn btn-outline-secondary btn-sm" id="btn-add-new"><span
                                            class="fa fa-plus-circle"></span>
                                        Invite New Team</a>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="block-content block-content-full" style="background-color:#ECECEC">
                                <div class="row">
                                    <?php if($data->num_rows() > 0): ?>
                                    <?php foreach($data->result() as $user):
                                         $linkImg = $user->pengguna_photo;
                                         if($linkImg === ''){
                                             if($user->pengguna_jenkel === 'L'){
                                                 $linkImg = 'user_blank.png';
                                             }else{
                                                 $linkImg = 'user_blank2.png';
                                             }
                                         }?>
                                    <div class="col-md-6 col-xl-3 gutters-tiny invisible" data-toggle="appear"
                                        data-toggle="appear" data-offset="-200" data-timeout="400">
                                        <div class="block block-themed">
                                        <div class="block-content block-content-full text-center <?php if($dark === 'dark'):?> bg-primary-dark <?php else: ?> bg-secondary<?php endif;?>">
                                                
                                                <img class="img-avatar img-avatar-thumb"
                                                    src="<?php echo base_url().'assets/images/'.$linkImg?>"
                                                    alt="">
                                                <div class="font-w600 text-white mt-10">
                                                    <?php echo $user->pengguna_nama;?>
                                                    <span style="color:#D6CB1E;" data-toggle="popover"
                                                        title="User is Still Active" data-placement="right"
                                                        data-content="This symbol it means that this user is still has the working progress assigment and wont let you remove this member from the team."
                                                        class="fa fa-circle"></span>
                                                </div>
                                                <div class="font-size-sm text-white-op">
                                                    <?php if($user->pengguna_level=='2'):?>
                                                    Project Manager
                                                    <?php elseif($user->pengguna_level=='3'):?>
                                                    Developer
                                                    <?php elseif($user->pengguna_level=='4'):?>
                                                    Business Analyst
                                                    <?php elseif($user->pengguna_level=='5'):?>
                                                    Tech Lead
                                                    <?php endif;?>
                                                </div>
                                            </div>
                                            <div class="block-content block-content-full">
                                                <div class="list-group">
                                                    <?php if($user->tim_acc === 'UNVERIFIED'): ?>
                                                    <span class="list-group-item list-group-item-action"
                                                        href="javascript:void(0)" data-toggle="popover"
                                                        title="Waiting for Acceptance" data-placement="top"
                                                        data-content="Teaming up request has been sent to <?php echo $dev->pengguna_email; ?>.">
                                                        <i style="color:yellow" class="fa fa-fw fa-circle mr-5"></i>
                                                        <span style="font-weight:lighter">Waiting for Acceptance</span>
                                                    </span>
                                                    <span class="list-group-item list-group-item-action"
                                                        href="javascript:void(0)">
                                                        <i class="fa fa-fw fa-envelope mr-5"></i> Team Up Invitation
                                                        Sent<br>
                                                        <small><?php echo date('M d, Y H:i A', strtotime($user->tim_tgl)); ?></small>
                                                        <?php if($user->tim_resend === '0'): ?>
                                                        <small>
                                                            <form name="formResend"
                                                                action="<?php echo base_url().'admin/tim/email_invitation2';?>"
                                                                method="post">
                                                                <input type="hidden" value="<?php echo $user->tim_id;?>"
                                                                    name="kode" required>
                                                                <input type="hidden" value="<?php echo $slugProject;?>"
                                                                    name="slug" required>
                                                                <input type="hidden"
                                                                    value="<?php echo $user->tim_user_id;?>" name="user"
                                                                    required>
                                                                <input type="hidden"
                                                                    value="<?php echo $user->tim_tl_id;?>" name="tl"
                                                                    required>
                                                                <input type="hidden"
                                                                    value="<?php echo $user->tim_kategori_id;?>"
                                                                    name="kategori" required>
                                                                <input type="hidden"
                                                                    value="<?php echo $user->tim_token;?>" name="token"
                                                                    required>
                                                                <input type="submit" value="Resend Invitation"
                                                                    style="color:#A7A6FF;border:0px;padding:0px;cursor:pointer;">
                                                            </form>
                                                        </small>
                                                        <?php endif; ?>
                                                    </span>
                                                    <?php else: ?>
                                                    <?php if($user->pengguna_level === '3'): ?>
                                                    <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
                                                        href="<?php echo base_url().'admin/assignment/task/'.$user->pengguna_id.'/'.$user->tim_token.'/'.$slugProject;?>">
                                                        <span><i class="fa fa-fw fa-line-chart mr-5"></i>
                                                            Task List</span>
                                                        <span>
                                                        <?php 
                                                            $work = $this->db->query("SELECT * FROM work where work_kategori_id='$user->tim_kategori_id' AND work_assignor_id='$user->tim_user_id' AND work_deadline < CURDATE()")->num_rows();
                                                            $dead = false;
                                                            if($work > 0){
                                                                $dead = true; 
                                                            }else{
                                                                $dead = false; 
                                                            }

                                                            if($dead === true): ?>
                                                            <span class="badge badge-primary">
                                                                <?php echo $work; ?>
                                                            </span>
                                                            <?php endif; ?>
                                                        </span>
                                                    </a>
                                                        <?php endif; ?>
                                                    <!--<a class="list-group-item list-group-item-action"-->
                                                    <!--    href="javascript:void(0)">-->
                                                    <!--    <i class="fa fa-fw fa-tasks mr-5"></i> Activity-->
                                                    <!--</a>-->

                                                    <!--<a class="list-group-item list-group-item-action"-->
                                                    <!--    href="javascript:void(0)">-->
                                                    <!--    <i class="fa fa-fw fa-clock-o mr-5"></i> Project History-->
                                                    <!--</a>-->
                                                    <a href="#!" class="list-group-item list-group-item-action btn-hapus" data-id="<?php echo $user->tim_id;?>"
                                                        data-slug="<?php echo $slugProject;?>" data-userid="<?php echo $user->pengguna_id; ?>">
                                                        <i class="fa fa-fw fa-trash mr-5"></i> Remove
                                                    </a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                    <?php else: ?>
                                    <div class="block-content block-content-full text-center">
                                        <div class="gutters-tiny invisible" data-toggle="appear" style="margin:100px">
                                            <h2 style="color:#ABAAAA;font-weight:lighter">No Team in this project. <span
                                                    style="font-size:15px;font-weight:lighter"><a href="#!"
                                                        data-toggle="modal" data-target="#ModalAddNew">Submit your team
                                                        member</a></span></h2>
                                        </div>
                                    </div>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
        <!-- Footer -->
        <?php echo $this->load->view('admin/v_footer');?>
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <!-- Modal Hapus -->
    <form action="<?php echo base_url().'admin/tim/simpan_tim'?>" method="post" enctype="multipart/form-data">
        <div class="modal fade in" id="ModalAddNew" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-popout" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Invite New Team</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="form-group">
                                <select style="width:100%" class="form-control" id="example2-select2"
                                    placeholder="Choose many.." required disabled>
                                    <?php foreach($kategori->result() as $kat): ?>
                                    <?php if($kat->kategori_id === $idProject): ?>
                                    <option value="<?php echo $kat->kategori_id; ?>" selected>
                                        <?php echo $kat->kategori_nama; ?>
                                        <?php else: ?>
                                    <option value="<?php echo $kat->kategori_id; ?>"> <?php echo $kat->kategori_nama; ?>
                                        <?php endif ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="xdeveloper">Developer or BA</label>
                                <input type="hidden" value="<?php echo $idProject; ?>" name="xkategori" required>
                                <select name="xdeveloper[]" class="js-select2 form-control"
                                    id="example-select2-multiple" style="width:100%" data-placeholder="Choose.."
                                    multiple required>
                                    <?php foreach($developer->result() as $dev): ?>
                                    <option value="<?php echo $dev->pengguna_id; ?>"> <?php echo $dev->pengguna_nama;?> | 
                                        <?php if($dev->pengguna_level === '4'): ?>
                                            Business Analyst
                                        <?php elseif($dev->pengguna_level === '3'): ?>
                                            Developer
                                        <?php endif; ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Additional Notes..." name="xnotes"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-square">Sent Invitation</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- END Normal Modal -->

    <!-- Modal Hapus -->
    <div class="modal fade" id="ModalUser" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
        aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-slideup" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Developer Profile</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-md-4">
                                <img alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Normal Modal -->

    <!-- Modal Hapus -->
    <form action="<?php echo base_url().'admin/tim/hapus_tim'?>" method="post">
        <div class="modal fade" id="Modalhapus" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-slideup" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Info</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <p>Are you sure want to remove this developer from your team?</p>
                            <input type="hidden" name="kode" required>
                            <input type="hidden" name="slug2" required>
                            <input type="hidden" name="xuserid" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-danger btn-square">Remove</butedsston>
                    </div>
                </div>
            </div>
        </div>
    </form> 4
    <!-- END Normal Modal -->
    <!-- Codebase Core JS -->
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/select2/select2.js'?>"></script>
    <script>
    jQuery(function() {
        Codebase.helpers(['easy-pie-chart', 'flatpickr', 'datepicker', 'colorpicker', 'maxlength', 'select2',
            'masked-inputs',
            'rangeslider', 'tags-inputs'
        ]);
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });

        //Show Modal Update tim
        $('.btn-edit').on('click', function() {
            var tim_id = $(this).data('id');
            var tim_nama = $(this).data('tim');
            var tim_deskripsi = $(this).data('deskripsi');
            $('#ModalUpdate').modal('show');
            $('[name="xkode"]').val(tim_id);
            $('[name="xtim2"]').val(tim_nama);
            $('[name="xdeskripsi"]').val(tim_deskripsi);
        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var tim_id = $(this).data('id');
            var slug = $(this).data('slug');
            var nama = $(this).data('nama');
            var userid = $(this).data('userid');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(tim_id);
            $('[name="slug2"]').val(slug);
            $('[name="xuserid"]').val(userid);
        });

        //Show user modal 
        $('.btn-user').on('click', function() {
            var user_nama = $(this).data('nama');
            var gambar = $(this).data('gambar');
            // var url = 'http://localhost/internal_uat/assets/images/'$gambar;
            $('#ModalUser').modal('show');
            $('#nama_user').html(user_nama);
            // $('#gambar').attr('src', url+gambar);
        });

    });
    </script>

    <script type="text/javascript">
    $('.dropify').dropify({ //overate input type file
        messages: {
            default: 'Photo',
            replace: 'Ganti',
            remove: 'Hapus',
            error: 'error'
        }
    });
    $(function() {
        CKEDITOR.replace('ckeditor', {
            height: '240px',
            toolbar: [{
                    name: 'clipboard',
                    groups: ['clipboard', 'undo'],
                    items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo',
                        'Redo'
                    ]
                },
                {
                    name: 'editing',
                    groups: ['find', 'selection', 'spellchecker'],
                    items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']
                },
                '/',
                {
                    name: 'basicstyles',
                    groups: ['basicstyles', 'cleanup'],
                    items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript',
                        '-', 'CopyFormatting', 'RemoveFormat'
                    ]
                },
                {
                    name: 'paragraph',
                    groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                    items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-',
                        'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter',
                        'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language'
                    ]
                },
                {
                    name: 'links',
                    items: ['Link', 'Unlink', 'Anchor']
                },
                {
                    name: 'insert',
                    items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar',
                        'PageBreak', 'Iframe'
                    ]
                },
                '/',
                {
                    name: 'styles',
                    items: ['Styles', 'Format', 'Font', 'FontSize']
                },
                {
                    name: 'colors',
                    items: ['TextColor', 'BGColor']
                },
            ]
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('.dropify').dropify({ //overate input type file
            messages: {
                default: 'Photo',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        //Show Modal Add New
        $('#btn-add-new').on('click', function() {
            $('#ModalAddNew').modal('show');
        });



        $('.btn-detail').on('click', function() {
            var work_deskripsi = $(this).data('deskripsi');
            $('#ModalDetail').modal('show');
            $('[name="deskripsi"]').val(work_deskripsi);
        });

        //Show Konfirmasi modal hapus record
        $('.btn-hapus').on('click', function() {
            var work_id = $(this).data('id');
            $('#Modalhapus').modal('show');
            $('[name="kode"]').val(work_id);
        });

    });
    </script>

    <?php if($this->session->flashdata('msg')=='gagal-hapus'):?>
    <script type="text/javascript">
    $.toast({
        heading: 'Error',
        text: "There is still assignment attached in that project",
        showHideTransition: 'slide',
        icon: 'error',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#FF4859'
    });
    </script>

    <?php elseif($this->session->flashdata('msg')=='forbidden'):?>
    <script type="text/javascript">
    var obj = <?php echo $this->session->flashdata('listForbiddens');?> ;
    var myJSON = JSON.stringify(obj);
    $.toast({
        heading: "Already added to this Project!",
        text: myJSON,
        showHideTransition: 'slide',
        icon: 'error',
        hideAfter: 5000,
        position: 'bottom-right',
        bgColor: '#FF4859'
    });
    </script>

    <?php elseif($this->session->flashdata('msg')=='show-modal'):?>
    <script type="text/javascript">
    $('#ModalResetPassword').modal('show');
    </script>
    <?php else:?>

    <?php endif;?>

</body>

</html>