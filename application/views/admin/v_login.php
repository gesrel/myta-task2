<!doctype html>

<html lang="en" class="no-focus">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>MYTA - Task Management</title>
        <meta name="title" content="MYTA - Task Management">
        <meta name="description" content="Task Management System 
        Ifabula Digital Kreasi
        Get inspired, Arranged and Creative">
        <meta property="og:image" itemprop="image" content="<?php echo base_url().'theme/images/favicon.png';?>">
        <meta property="og:type" content="website" />
        <meta property="og:image:type" content="image/png" />
        <meta property="og:image:width" content="400" />
        <meta property="og:image:height" content="400" />
        <link rel="shortcut icon" href="<?php echo base_url().'theme/images/favicon.png'?>">

        <!-- Stylesheets -->
        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>" />
    </head>
<body>
<div id="page-container" class="main-content-boxed">
<main id="main-container">
<div class="bg-image" style="background-image: url('https://ifabula.com/images/slider1.jpg');">
    <div class="row mx-0 bg-black-op">
        <div class="hero-static col-md-6 col-xl-8 d-none d-md-flex align-items-md-end">
            <div class="p-30 invisible" data-toggle="appear">
                <p class="font-size-h3 font-w600 text-white">
                    Get Inspired, Arranged and Creative.
                </p>
                <p class="font-italic text-white-op">
                    Copyright &copy; <span class="js-year-copy"></span>
                </p>
            </div>
        </div>
        <div class="hero-static col-md-6 col-xl-4 d-flex align-items-center bg-white invisible" data-toggle="appear" data-class="animated fadeInRight">
            <div class="content content-full">
                <div class="px-30 py-10">
                    
                    <div style="margin-left:-10%">
                        <a class="link-effect font-w700" href="">
                            <img style="margin-top:30px" width="150px" src="<?php echo base_url().'theme/images/myta.png';?>">
                        </a>
                        <h1 class="h4 font-w700 mt-30 mb-10">Welcome to Your Dashboard</h1>
                        <h2 class="h5 font-w400 text-muted mb-0">Please sign in</h2>
                    </div>
                </div>
                <?php echo $this->session->flashdata('msg');?> 
                <form class="js-validation-signin" action="<?php echo base_url().'login/auth'?>" method="post" >
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material floating">
                                <input type="email" class="form-control" id="signup-email" name="email" required>
                                <label for="login-email">Email</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material floating">
                                <input type="password" class="form-control" id="login-password" name="password" required>
                                <label for="login-password">Password</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-sm btn-hero btn-alt-primary">
                            <i class="si si-login mr-10"></i> Sign In
                        </button>
                        <div class="mt-30">
                            <a class="link-effect text-muted mr-10 mb-5 d-inline-block btnRegis" href="#!">
                                <i class="fa fa-plus mr-5"></i> Create Account
                            </a>
                            <a class="link-effect text-muted mr-10 mb-5 d-inline-block btnRegis" href="#!">
                                <i class="fa fa-warning mr-5"></i> Forgot Password
                            </a>
                        </div>
                        <div class="mt-30">
                            <a class="btn btn-info bg-white btnRegis" style="color:#000" href="#!">
                                <img src="https://cdn4.iconfinder.com/data/icons/new-google-logo-2015/400/new-google-favicon-512.png" width="20px"> Google
                            </a>
                            <a class="btn btn-primary btnRegis" href="#!">
                                <img src="https://www.freepnglogos.com/uploads/facebook-icons/facebook-icon-transparent-background-3.png" width="20px"> Facebook
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    </main>
    </div>
 <!-- Codebase Core JS -->
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>

    <!-- Page JS Plugins -->
    <script src="<?php echo base_url().'assets/js/plugins/jquery-validation/jquery.validate.min.js'?>"></script>

    <!-- Page JS Code -->
    <script src="<?php echo base_url().'assets/js/pages/op_auth_signin.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
    <script>
        $( ".btnRegis" ).on('click',function() {
            $.toast({
                heading: 'Info',
                text: "This feature still under develop",
                showHideTransition: 'slide',
                icon: 'info',
                stack : 1, 
                loader:false,
                hideAfter: 5000,
                position: 'bottom-right',
            });
        });
    </script>
    </body>
</html>
