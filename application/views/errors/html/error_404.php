
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>MYTA | 404</title>
     
        <link rel="shortcut icon" href="http://localhost/myta_update/assets/media/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="http://localhost/myta_update/assets/media/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="http://localhost/myta_update/assets/media/favicons/apple-touch-icon-180x180.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700&display=swap">
    <link rel="stylesheet" id="css-main" href="http://localhost/myta_update/assets/css/codebase.min-3.2.css">
	<link rel="stylesheet" id="css-main" href="http://localhost/myta_update/assets/css/codebase.min.css">

		<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-16158021-6', 'auto');ga('send', 'pageview');</script>
</head>
<body>
<div id="page-container" class="main-content-boxed">
                <main id="main-container">
<div class="hero bg-white">
    <div class="hero-inner">
        <div class="content content-full">
            <div class="py-30 text-center">
                <div class="display-3 text-danger">
                    <i class="fa fa-warning"></i> 404
                </div>
                <h1 class="h2 font-w700 mt-30 mb-10">Oops.. You just found an error page..</h1>
                <h2 class="h3 font-w400 text-muted mb-50">We are sorry but the page you are looking for was not found..</h2>
                <a class="btn btn-hero btn-rounded btn-alt-secondary" href="http://localhost/myta_update">
                    <i class="fa fa-arrow-left mr-10"></i> Back
                </a>
            </div>
        </div>
    </div>
</div>
    </main>
    </div>
<script src="http://localhost/myta_update/assets/js/codebase.core.min-3.2.js"></script>
<script src="http://localhost/myta_update/assets/js/codebase.app.min-3.2.js"></script>
    </body>
</html>