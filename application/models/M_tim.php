<?php
class M_tim extends CI_Model{

	function get_my_tim($kode){
		$hsl=$this->db->query("SELECT * FROM tim where tim_tl_id = '$kode'");
		return $hsl;
	}
	function get_my_tim_kategori($kode, $idproject){
		$hsl=$this->db->query("SELECT * FROM pengguna
		inner join tim on pengguna.pengguna_id=tim.tim_user_id
		inner join kategori on tim.tim_kategori_id=kategori.kategori_id
		where kategori.kategori_id='$idproject' AND
		kategori.kategori_tl_id='$kode' AND
		 pengguna.pengguna_level IN ('3','4')");
		return $hsl;
	}
	function simpan_tim($kategori,$user,$keterangan,$token){
		$id=rand(1111,123456789);
		$userId = $this->session->userdata('idadmin');
		$hsl=$this->db->query("insert into tim(tim_id,tim_kategori_id,tim_tl_id, tim_user_id,tim_keterangan,tim_token)
		values('$id','$kategori','$userId','$user','$keterangan','$token')");
		return $hsl;
	}
	function update_tim($kode,$tim,$deskripsi,$gambar,$slug){
	    $userNama = $this->session->userdata('nama');
	    $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Mengubah data project $tim')");
		$hsl=$this->db->query("update tim set tim_nama='$tim', tim_deskripsi='$deskripsi', tim_gambar='$gambar',tim_slug='$slug$kode' where tim_id='$kode'");
		return $hsl;
	}
	function hapus_tim($kode){
	    $userNama = $this->session->userdata('nama');
	    $tim = $this->db->query("SELECT * FROM tim where tim_id='$kode'")->row_array();
	    $katNama = $tim['tim_nama'];
	    $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Menghapus project data project $katNama')");
		$hsl=$this->db->query("delete from tim where tim_id='$kode'");
		return $hsl;
	}

	function get_tim_byid($tim_id){
		$hsl=$this->db->query("select * from tim where tim_id='$tim_id'");
		return $hsl;
	}

}