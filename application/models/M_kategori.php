<?php
class M_kategori extends CI_Model{

	function get_all_kategori(){
		$userId = $this->session->userdata('idadmin');
		$hsl=$this->db->query("SELECT * FROM kategori 
		inner join pengguna ON kategori.kategori_tl_id=pengguna.pengguna_id
		where kategori_user_id='$userId' AND pengguna_level='5'");
		return $hsl;
	}
	function simpan_kategori($id, $kategori,$tl,$deskripsi,$gambar,$slug, $start, $end){
		$userId = $this->session->userdata('idadmin');
		$userNama = $this->session->userdata('nama');
		$this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Menambah project baru $kategori')");
		$hsl=$this->db->query("insert into kategori(kategori_id,kategori_user_id, kategori_nama,kategori_tl_id,kategori_deskripsi,kategori_gambar,kategori_slug, kategori_active_start, kategori_active_end) values('$id','$userId','$kategori','$tl','$deskripsi','$gambar','$slug$id','$start','$end')");
		return $hsl;
	}
	function update_kategori($kode,$kategori,$tl,$deskripsi,$gambar,$slug, $start, $end){
	    $userNama = $this->session->userdata('nama');
	    $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Mengubah data project $kategori')");
		$hsl=$this->db->query("update kategori set kategori_nama='$kategori', kategori_tl_id='$tl', kategori_deskripsi='$deskripsi', kategori_gambar='$gambar',kategori_slug='$slug$kode', kategori_active_start='$start', kategori_active_end='$end' where kategori_id='$kode'");
		return $hsl;
	}
	function update_kategori_nogambar($kode,$kategori,$tl,$deskripsi,$slug, $start, $end){
	    $userNama = $this->session->userdata('nama');
	    $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Mengubah data project $kategori')");
		$hsl=$this->db->query("update kategori set kategori_nama='$kategori',kategori_tl_id='$tl', kategori_deskripsi='$deskripsi', kategori_slug='$slug$kode', kategori_active_start='$start', kategori_active_end='$end' where kategori_id='$kode'");
		return $hsl;
	}
	function hapus_kategori($kode){
	    $userNama = $this->session->userdata('nama');
	    $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$kode'")->row_array();
	    $katNama = $kategori['kategori_nama'];
	    $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Menghapus project data project $katNama')");
		$hsl=$this->db->query("delete from kategori where kategori_id='$kode'");
		return $hsl;
	}

	function get_kategori_byid($kategori_id){
		$hsl=$this->db->query("select * from kategori where kategori_id='$kategori_id'");
		return $hsl;
	}
	function get_kategori_by_slugpm($userId, $slug){
		$hsl=$this->db->query("select * from kategori where kategori_user_id='$userId' AND kategori_slug='$slug'");
		return $hsl;
	}
	function get_kategori_byslug($slug){
		$hsl=$this->db->query("select * from kategori where kategori_slug='$slug'");
		return $hsl;
	}
}