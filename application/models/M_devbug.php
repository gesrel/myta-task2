<?php
class M_devbug extends CI_Model{

	function get_all_devbug(){
		$hsl=$this->db->query("SELECT * FROM devbug");
		return $hsl;
	}
	function simpan_devbug($devbug){
		$userId = $this->session->userdata('idadmin');
		$userNama = $this->session->userdata('nama');
		$id=rand(1111,123456789);
		$this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Membuat stage baru $devbug')");
		$hsl=$this->db->query("insert into devbug(devbug_id,devbug_nama) values('$id','$devbug')");
		return $hsl;
	}
	function update_devbug($kode,$devbug){
	    $userNama = $this->session->userdata('nama');
	    $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Mengubah data stage $devbug')");
		$hsl=$this->db->query("update devbug set devbug_nama='$devbug' where devbug_id='$kode'");
		return $hsl;
	}
	function hapus_devbug($kode){
	    $userNama = $this->session->userdata('nama');
	    $devbug = $this->db->query("SELECT * FROM devbug where devbug_id='$kode'")->row_array();
	    $devbugNama = $devbug['devbug_nama'];
	    $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Menghapus project data project $devbugNama')");
		$hsl=$this->db->query("delete from devbug where devbug_id='$kode'");
		return $hsl;
	}

}