<?php
class M_work extends CI_Model{

	function get_all_work(){
		$hsl=$this->db->query("SELECT * FROM work order by work_status ASC");
		return $hsl;
	}
	function get_all_work_user($kode){
		$hsl=$this->db->query("SELECT * FROM work where work_assignor_id='$kode' order by work_status ASC");
		return $hsl;
	}
	function simpan_work($business, $assignor,$work,$stage,$priority,$sprint,$device,$deadline,$deskripsi){
		$userId = $this->session->userdata('idadmin');
		$id=rand(111111,123456789);
		if($assignor == '' || $deadline == '' || $assignor == NULL || $deadline == NULL){
			$hsl=$this->db->query("insert into work(work_id,work_user_id,work_nama,work_devbug_id,work_priority,work_sprint_id,work_device,work_kategori_id, work_assignor_id, work_deadline, work_deskripsi, work_status) values('$id','$userId','$work','$stage','$priority','$sprint','$device','$business','$assignor','$deadline','$deskripsi','O')");
		}else{
			$hsl=$this->db->query("insert into work(work_id,work_user_id,work_nama,work_devbug_id,work_priority,work_sprint_id,work_device,work_kategori_id, work_assignor_id, work_deadline, work_deskripsi) values('$id','$userId','$work','$stage','$priority','$sprint','$device','$business','$assignor','$deadline','$deskripsi')");
		}
		return $hsl;
	}
	function simpan_work_dev($business, $assignor,$work,$stage,$sprint,$device,$deadline,$deskripsi){
		$userId = $this->session->userdata('idadmin');
        $id=rand(111111,123456789);
		$hsl=$this->db->query("insert into work(work_id,work_user_id,work_nama,work_devbug_id,work_sprint_id,work_device,work_kategori_id, work_assignor_id, work_deadline, work_deskripsi, work_status) values('$id','$assignor','$work','$stage','$sprint','$device','$business','$userId','$deadline','$deskripsi','F')");
		return $hsl;
	}
	function simpan_checkpoint($business,$work,$device,$deskripsi){
		$userId = $this->session->userdata('idadmin');
        $id=rand(111111,123456789);
		$hsl=$this->db->query("insert into work(work_id,work_user_id,work_nama,work_device,work_kategori_id, work_deskripsi, work_status) values('$id','$userId','$work','$device','$business','$deskripsi','0')");
		return $hsl;
	}
	function update_work($kode,$business, $assignor,$work,$deadline,$deskripsi){
		$hsl=$this->db->query("update work set work_nama='$work', work_kategori_id='$business', work_assignor_id='$assignor', work_deadline='$deadline', work_deskripsi='$deskripsi' where work_id='$kode'");
		return $hsl;
    }

    
    function done_work($kode){
		$hsl=$this->db->query("update work set work_status='B' where work_id='$kode'");
		return $hsl;
    }
    function cancel_work($kode){
		$hsl=$this->db->query("update work set work_status='C' where work_id='$kode'");
		return $hsl;
    }
    function progress_work($kode){
        $hsl=$this->db->query("update work set work_status='A' where work_id='$kode'");
		return $hsl;
    }
    
    
	function hapus_work($kode){
		$hsl=$this->db->query("delete from work where work_id='$kode'");
		return $hsl;
	}

	function get_work_byid($work_id){
		$hsl=$this->db->query("select * from work where work_id='$work_id'");
		return $hsl;
	}




	//BA TL
	function get_ongoing_ba_tl_dashboard($userId){
		$hsl = $this->db->query("SELECT * FROM work 
		LEFT JOIN pengguna ON work.work_assignor_id=pengguna.pengguna_id
		where work_status IN ('A','D','E','O') AND work_user_id='$userId' 
		order by work_priority DESC"); 
		return $hsl;
	}
	function get_requested_ba_tl_dashboard($userId){
		$hsl = $this->db->query("SELECT * FROM work 
		INNER JOIN pengguna ON work.work_assignor_id=pengguna.pengguna_id
		where work_status='F' AND 
		work_user_id='$userId'
		order by work_priority DESC");
		return $hsl;
	}
	function get_done_ba_tl_dashboard($userId){
		$hsl = $this->db->query("SELECT * FROM work 
		INNER JOIN kategori ON work.work_kategori_id=kategori.kategori_id
		where work_status='B' AND 
		work_user_id='$userId' 
		order by work_priority DESC limit 10");
		return $hsl;
	}
	function get_ongoing_ba_tl_project($userId, $idProject,$idSprint){
		$hsl = $this->db->query("SELECT * FROM work 
		LEFT JOIN pengguna ON work.work_assignor_id=pengguna.pengguna_id
		where work_kategori_id = '$idProject' AND
		work_status IN ('A','D','E','O') AND
		work_sprint_id='$idSprint' order by work_priority DESC");
		return $hsl;
	}
	function get_requested_ba_tl_project($userId, $idProject){
		$hsl = $this->db->query("SELECT * FROM work 
		INNER JOIN pengguna ON work.work_assignor_id=pengguna.pengguna_id	
		where work_status='F' AND 
		work_kategori_id = '$idProject' AND 
		work_user_id='$userId' 
		order by work_priority DESC");
		return $hsl;
	}
	function get_done_ba_tl_project($userId, $idProject){
		$hsl = $this->db->query("SELECT * FROM work 
		INNER JOIN kategori ON work.work_kategori_id=kategori.kategori_id
		where work_status='B' AND 
		work_kategori_id='$idProject' AND 
		work_user_id='$userId' 
		order by work_priority DESC limit 10"); 
		return $hsl;
	}


	function get_assignor_tl($userId, $idProject){
		$hsl = $this->db->query("SELECT * FROM tim
		INNER JOIN pengguna ON tim.tim_user_id=pengguna.pengguna_id						
		where tim_kategori_id='$idProject' AND
		tim_acc='VERIFIED' AND 
		pengguna.pengguna_level='3' AND
		tim_tl_id='$userId'");
		return $hsl;
	}

	function get_assignor_ba($idTL, $idProject){
		$hsl = $this->db->query("SELECT * FROM tim
		INNER JOIN pengguna ON tim.tim_user_id=pengguna.pengguna_id	
		where tim_kategori_id='$idProject' AND
		tim_acc='VERIFIED' AND
		tim_tl_id='$idTL' AND pengguna.pengguna_level='3'");
		return $hsl;
	}

	function get_assignor_tl_dashboard($userId){
		$hsl = $this->db->query("SELECT * FROM tim
		INNER JOIN pengguna ON tim.tim_user_id=pengguna.pengguna_id						
		where tim_acc='VERIFIED' AND
		tim_tl_id='$userId'");
		return $hsl;
	}

	function get_assignor_ba_dashboard($idTL){
		$hsl = $this->db->query("SELECT * FROM tim
		INNER JOIN pengguna ON tim.tim_user_id=pengguna.pengguna_id	
		where tim_acc='VERIFIED' AND
		tim_tl_id='$idTL' AND pengguna.pengguna_level='3'");
		return $hsl;
	}


	//DEV
	function get_ongoing_dev_dashboard($userId){
		$hsl = $this->db->query("SELECT * FROM work 
		INNER JOIN pengguna ON work.work_user_id=pengguna.pengguna_id
		where work_status IN ('A', 'D','E') AND 
		work_assignor_id='$userId' 
		order by work_priority DESC"); 
		return $hsl;
	}
	function get_requested_dev_dashboard($userId){
		$hsl = $this->db->query("SELECT * FROM work 
		INNER JOIN pengguna ON work.work_user_id=pengguna.pengguna_id
		where work_status='F' AND 
		work_assignor_id='$userId' 
		order by work_priority DESC");
		return $hsl;
	}
	function get_done_dev_dashboard($userId){
		$hsl = $this->db->query("SELECT * FROM work 
		INNER JOIN kategori ON work.work_kategori_id=kategori.kategori_id
		where work_status='B' AND 
		work_assignor_id='$userId' 
		order by work_priority DESC limit 10"); 
		return $hsl;
	}
	function get_ongoing_dev_project($userId, $idProject, $idSprint){
		$hsl = $this->db->query("SELECT * FROM work 
		INNER JOIN pengguna ON work.work_user_id=pengguna.pengguna_id
		where work_kategori_id = '$idProject'AND 
		work_status IN ('A','D','E') AND 
		work_sprint_id='$idSprint'
		order by work_priority DESC");
		return $hsl;
	}
	function get_requested_dev_project($userId, $idProject){
		$hsl = $this->db->query("SELECT * FROM work 
		INNER JOIN pengguna ON work.work_user_id=pengguna.pengguna_id
		where work_status='F' AND 
		work_kategori_id = '$idProject' AND 
		work_assignor_id='$userId' 
		order by work_priority DESC");
		return $hsl;
	}
	function get_done_dev_project($userId, $idProject){
		$hsl = $this->db->query("SELECT * FROM work 
		INNER JOIN kategori ON work.work_kategori_id=kategori.kategori_id
		where work_status='B' AND 
		work_kategori_id='$idProject' AND 
		work_assignor_id='$userId' 
		order by work_priority DESC limit 10"); 
		return $hsl;
	}


	// PM
	function get_ongoing_pm_dashboard($userId){
		$hsl = $this->db->query("SELECT * FROM work 
		INNER JOIN pengguna ON work.work_assignor_id=pengguna.pengguna_id
		INNER JOIN kategori on work.work_kategori_id=kategori.kategori_id
		where work_status IN ('A','D','E','F') AND
		kategori_user_id = '$userId'
		order by work_priority DESC"); 
		return $hsl;
	}
	function get_ongoing_pm_project($idProject,$idSprint){
		$hsl = $this->db->query("SELECT * FROM work 
		INNER JOIN pengguna ON work.work_assignor_id=pengguna.pengguna_id
		where work_status IN ('A','D','E','F') AND
		work_kategori_id='$idProject' AND
		work_sprint_id='$idSprint'
		order by work_priority DESC"); 
		return $hsl;
	}

}