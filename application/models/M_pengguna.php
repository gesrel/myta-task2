<?php
class M_pengguna extends CI_Model{

	function get_all_pengguna(){
		$hsl=$this->db->query("SELECT pengguna.*,IF(pengguna_jenkel='L','Laki-Laki','Perempuan') AS jenkel FROM pengguna");
		return $hsl;	
	}

	function simpan_pengguna($nama,$jenkel,$username,$password,$email,$nohp,$gambar,$level,$prior){
	    $userNama = $this->session->userdata('nama');
	    if($level === '1'){
	        $akses = 'Admin';
	    }elseif($level === '2'){
	        $akses = 'Project Manager';
	        
	    }elseif($level === '3'){
	        $akses = 'Developer';
	    }elseif($level === '4'){
	        $akses = 'Business Analyst';
	    }
	    $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Menambah data user $akses baru $nama($email)')");
		$hsl=$this->db->query("INSERT INTO pengguna (pengguna_nama,pengguna_jenkel,pengguna_username,pengguna_password,pengguna_email,pengguna_nohp,pengguna_level,pengguna_prior,pengguna_photo) VALUES ('$nama','$jenkel','$username',md5('$password'),'$email','$nohp','$level','$prior','$gambar')");
		return $hsl;
	}

	function simpan_pengguna_tanpa_gambar($nama,$jenkel,$username,$password,$email,$nohp,$level,$prior){
	    $userNama = $this->session->userdata('nama');
	    if($level === '1'){
	        $akses = 'Admin';
	    }elseif($level === '2'){
	        $akses = 'Project Manager';
	        
	    }elseif($level === '3'){
	        $akses = 'Developer';
	    }elseif($level === '4'){
	        $akses = 'Business Analyst';
	    }
	    $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Menambah data user $akses baru $nama($email)')");
		$hsl=$this->db->query("INSERT INTO pengguna (pengguna_nama,pengguna_jenkel,pengguna_username,pengguna_password,pengguna_email,pengguna_nohp,pengguna_level,pengguna_prior) VALUES ('$nama','$jenkel','$username',md5('$password'),'$email','$nohp','$level','$prior')");
		return $hsl;
	}

	//UPDATE PENGGUNA //
	function update_pengguna_tanpa_pass($kode,$nama,$jenkel,$username,$password,$email,$nohp,$gambar,$level,$prior){
	    $userNama = $this->session->userdata('nama');
	    $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Mengubah data user $nama')");
		$hsl=$this->db->query("UPDATE pengguna set pengguna_nama='$nama',pengguna_jenkel='$jenkel',pengguna_username='$username',pengguna_email='$email',pengguna_nohp='$nohp',pengguna_photo='$gambar',pengguna_level='$level', pengguna_prior='$prior' where pengguna_id='$kode'");
		return $hsl;
	}
	function update_pengguna($kode,$nama,$jenkel,$username,$password,$email,$nohp,$gambar,$level,$prior){
	    $userNama = $this->session->userdata('nama');
	    $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Mengubah data user $nama')");
		$hsl=$this->db->query("UPDATE pengguna set pengguna_nama='$nama',pengguna_jenkel='$jenkel',pengguna_username='$username',pengguna_password=md5('$password'),pengguna_email='$email',pengguna_nohp='$nohp',pengguna_photo='$gambar',pengguna_level='$level', pengguna_prior='$prior' where pengguna_id='$kode'");
		return $hsl;
	}

	function update_pengguna_tanpa_pass_dan_gambar($kode,$nama,$jenkel,$username,$password,$email,$nohp,$level,$prior){
	    $userNama = $this->session->userdata('nama');
	    $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Mengubah data user $nama')");
		$hsl=$this->db->query("UPDATE pengguna set pengguna_nama='$nama',pengguna_jenkel='$jenkel',pengguna_username='$username',pengguna_email='$email',pengguna_nohp='$nohp',pengguna_level='$level', pengguna_prior='$prior' where pengguna_id='$kode'");
		return $hsl;
	}
	function update_pengguna_tanpa_gambar($kode,$nama,$jenkel,$username,$password,$email,$nohp,$level,$prior){
	    $userNama = $this->session->userdata('nama');
	    $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Mengubah data user $nama')");
		$hsl=$this->db->query("UPDATE pengguna set pengguna_nama='$nama',pengguna_jenkel='$jenkel',pengguna_username='$username',pengguna_password=md5('$password'),pengguna_email='$email',pengguna_nohp='$nohp',pengguna_level='$level', pengguna_prior='$prior' where pengguna_id='$kode'");
		return $hsl;
	}
	//END UPDATE PENGGUNA//

	function hapus_pengguna($kode){
	    $userNama = $this->session->userdata('nama');
	    $pengguna = $this->db->query("SELECT * FROM pengguna where pengguna_id='$kode'")->row_array();
	    $pengNama = $pengguna['pengguna_nama'];
	    $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Menghapus data user $pengNama')");
		$hsl=$this->db->query("DELETE FROM pengguna where pengguna_id='$kode'");
		return $hsl;
	}
	function getusername($id){
		$hsl=$this->db->query("SELECT * FROM pengguna where pengguna_id='$id'");
		return $hsl;
	}
	function reset_password($id,$pass){
	    $pengguna = $this->db->query("SELECT * FROM pengguna where pengguna_id='$id'")->row_array();
	    $pengNama = $pengguna['pengguna_nama'];
	    $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$userNama','Mereset password user $pengNama')");
		$hsl=$this->db->query("UPDATE pengguna set pengguna_password=md5('$pass') where pengguna_id='$id'");
		return $hsl;
	}

	function get_pengguna_login($kode){
		$hsl=$this->db->query("SELECT * FROM pengguna where pengguna_id='$kode'");
		return $hsl;
	}


}