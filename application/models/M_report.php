<?php
class M_report extends CI_Model{

	function month_issue(){
        $query = $this->db->query("SELECT DATE_FORMAT(work_tgl,'%d') AS tgl,COUNT(work_tgl) AS jumlah FROM work WHERE MONTH(work_tgl)=MONTH(CURDATE()) GROUP BY DATE(work_tgl)");
         
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
    
    function year_issue(){
        $query = $this->db->query("SELECT DATE_FORMAT(work_tgl,'%M') AS bulan,COUNT(work_tgl) AS jumlah FROM work WHERE YEAR(work_tgl)=YEAR(CURDATE()) GROUP BY MONTH(work_tgl)");
         
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
    
    function month_issue_pro($project){
        $query = $this->db->query("SELECT DATE_FORMAT(work_tgl,'%d') AS tgl,COUNT(work_tgl) AS jumlah FROM work WHERE work_kategori_id='$project' AND MONTH(work_tgl)=MONTH(CURDATE()) GROUP BY DATE(work_tgl)");
         
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
        
    }
    
    function year_issue_pro($project){
        $query = $this->db->query("SELECT DATE_FORMAT(work_tgl,'%M') AS bulan,COUNT(work_tgl) AS jumlah FROM work WHERE work_kategori_id='$project' AND YEAR(work_tgl)=YEAR(CURDATE()) GROUP BY MONTH(work_tgl)");
         
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
    
    

}