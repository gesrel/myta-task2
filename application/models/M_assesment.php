<?php
class M_assesment extends CI_Model{


    function simpan_assesment_response_dev($idWork, $idUser, $nilai, $menit){
        $randID = rand(111111,123456789);
        $kategori = 'RESPONSE';
        $hsl = $this->db->query("INSERT INTO assesment (assesment_id, assesment_work_id, assesment_user_id, assesment_nilai, assesment_kategori, assesment_record) values
        ('$randID', '$idWork', '$idUser', '$nilai', '$kategori','$menit')");
        return $hsl;
    }

    function simpan_assesment_response_ba($idWork, $idUser, $nilai, $menit){
        $randID = rand(111111,123456789);
        $kategori = 'RESPONSE';
        $hsl = $this->db->query("INSERT INTO assesment (assesment_id, assesment_work_id, assesment_user_id, assesment_nilai, assesment_kategori,assesment_record) values
        ('$randID', '$idWork', '$idUser', '$nilai', '$kategori','$menit')");
        return $hsl;
    }

    function simpan_assesment_revert_dev($idWork, $idUser, $nilai, $jumlah){
        $randID = rand(111111,123456789);
        $kategori = 'REVERT';
        $hsl = $this->db->query("INSERT INTO assesment (assesment_id, assesment_work_id, assesment_user_id, assesment_nilai, assesment_kategori,assesment_record) values
        ('$randID', '$idWork', '$idUser', '$nilai', '$kategori','$jumlah')");
        return $hsl;
    }

    function simpan_assesment_revert_ba($idWork, $idUser, $nilai, $jumlah){
        $randID = rand(111111,123456789);
        $kategori = 'REVERT';
        $hsl = $this->db->query("INSERT INTO assesment (assesment_id, assesment_work_id, assesment_user_id, assesment_nilai, assesment_kategori, assesment_record) values
        ('$randID', '$idWork', '$idUser', '$nilai', '$kategori','$jumlah')");
        return $hsl;
    }

	function simpan_default($idProject){
        $randID = rand(111111,123456789);
        $this->db->query("INSERT INTO response 
            (response_id, response_kategori_id, response_nama, response_durasi,response_kategori_nilai) values 
            ('$randID','$idProject','Sangat Baik','60','');
        ");

        $randID2 = rand(111111,123456789);
        $this->db->query("INSERT INTO response 
            (response_id, response_kategori_id, response_nama, response_durasi,response_kategori_nilai) values 
            ('$randID2','$idProject','Baik','120','');
        ");

        $randID3 = rand(111111,123456789);
        $this->db->query("INSERT INTO response 
            (response_id, response_kategori_id, response_nama, response_durasi,response_kategori_nilai) values 
            ('$randID3','$idProject','Kurang','180','');
        ");

        $randID4 = rand(111111,123456789);
        $this->db->query("INSERT INTO response 
            (response_id, response_kategori_id, response_nama, response_durasi,response_kategori_nilai) values 
            ('$randID4','$idProject','Sangat Kurang','240','');
        ");
    }
    
    function simpan_reverted_dev($idProject){
        $randID = rand(111111,123456789);
        $this->db->query("INSERT INTO reverted 
            (reverted_id, reverted_kategori_id, reverted_nama, reverted_jumlah,reverted_devba) values 
            ('$randID','$idProject','Sangat Baik','0','DEV');
        ");

        $randID2 = rand(111111,123456789);
        $this->db->query("INSERT INTO reverted 
            (reverted_id, reverted_kategori_id, reverted_nama, reverted_jumlah,reverted_devba) values 
            ('$randID2','$idProject','Baik','3','DEV');
        ");

        $randID3 = rand(111111,123456789);
        $this->db->query("INSERT INTO reverted 
            (reverted_id, reverted_kategori_id, reverted_nama, reverted_jumlah,reverted_devba) values 
            ('$randID3','$idProject','Kurang','6','DEV');
        ");

        $randID4 = rand(111111,123456789);
        $this->db->query("INSERT INTO reverted 
            (reverted_id, reverted_kategori_id, reverted_nama, reverted_jumlah,reverted_devba) values 
            ('$randID4','$idProject','Sangat Kurang','10','DEV');
        ");
    }

    function simpan_reverted_ba($idProject){
        $randID = rand(111111,123456789);
        $this->db->query("INSERT INTO reverted 
            (reverted_id, reverted_kategori_id, reverted_nama, reverted_jumlah,reverted_devba) values 
            ('$randID','$idProject','Sangat Baik','0','BA');
        ");

        $randID2 = rand(111111,123456789);
        $this->db->query("INSERT INTO reverted 
            (reverted_id, reverted_kategori_id, reverted_nama, reverted_jumlah,reverted_devba) values 
            ('$randID2','$idProject','Baik','3','BA');
        ");

        $randID3 = rand(111111,123456789);
        $this->db->query("INSERT INTO reverted 
            (reverted_id, reverted_kategori_id, reverted_nama, reverted_jumlah,reverted_devba) values 
            ('$randID3','$idProject','Kurang','6','BA');
        ");

        $randID4 = rand(111111,123456789);
        $this->db->query("INSERT INTO reverted 
            (reverted_id, reverted_kategori_id, reverted_nama, reverted_jumlah,reverted_devba) values 
            ('$randID4','$idProject','Sangat Kurang','10','BA');
        ");
    }

}