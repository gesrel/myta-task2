<?php
class compile extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
        $this->load->library('upload');
        $this->load->library('email');
	}


	function index(){
		if($this->session->userdata('akses')=='3'){
            $userId = $this->session->userdata('idadmin');
				$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
				$x['dark'] = $userDB['pengguna_moto'];
			$kode = $this->session->userdata('idadmin');
			$x['data']=$this->db->query("SELECT * FROM compile");
			$this->load->view('admin/v_compile',$x);
		}else{
			redirect('permission');
		}
	}

	function simpan_compile(){
		if($this->session->userdata('akses')=='3'){
		    $business=$this->input->post('xbusiness');
    		$nama=$this->input->post('xnama');
    		$namaUser = $this->session->userdata('nama');
		    $compileJum = $this->db->query("SELECT * FROM compile where compile_kategori_id='$business'")->num_rows();
		    if($compileJum >= 3){
			    $error = $this->upload->display_errors();
            	echo $this->session->set_flashdata('errorupload','uploaded file has reached the storage limit');
            	echo $this->session->set_flashdata('msg','gagal-upload');
		        redirect('admin/compile');
			}else{
			    ini_set('upload_max_filesize', '200M');
                ini_set('post_max_size', '200M');                               
                ini_set('max_input_time', 3000);                                
                ini_set('max_execution_time', 3000);
        	    $config['upload_path']   = './assets/compile/'; //path folder
        		$config['encrypt_name']  = FALSE; //nama yang terupload nantinya
                $config['allowed_types'] = '*';
                $config['max_size'] = '1000000';
                $config['max_width']  = '1024000';
                $config['max_height']  = '768000';
                $this->upload->initialize($config);
            
                if(!$this->upload->do_upload('apk')){
                    $error = $this->upload->display_errors();
                	echo $this->session->set_flashdata('errorupload',$error);
                	echo $this->session->set_flashdata('msg','gagal-upload');
                	
    		        redirect('admin/compile');
                }else{
                    $this->upload->do_upload('apk');
                    $gbr = $this->upload->data();
                    $config['image_library']='gd2';
        			$config['source_image']='./assets/compile/'.$gbr['file_name'];
        	
        			$config['quality']= '100%';
        			$config['new_image']= './assets/compile/'.$gbr['file_name'];
    
                    $gambar = $gbr['file_name'];
                    $type = strstr($gambar,".");
                    
                    if($type === '.apk'){
                        $apkipa = 'APK';
                    }elseif($type === '.ipa'){
                        $apkipa = 'IPA';
                    }else{
                       echo $this->session->set_flashdata('errorupload', 'File type you attempt to upload not match');
                	    echo $this->session->set_flashdata('msg','gagal-upload');
    		            redirect('admin/compile');
                    }
                    $this->load->library('ciqrcode'); //pemanggilan library QR CODE
                    $config['cacheable']    = true; //boolean, the default is true
                    $config['cachedir']     = './assets/'; //string, the default is application/cache/
                    $config['errorlog']     = './assets/'; //string, the default is application/logs/
                    $config['imagedir']     = './assets/images/qr/'; //direktori penyimpanan qr code
                    $config['quality']      = true; //boolean, the default is true
                    $config['size']         = '1024'; //interger, the default is 1024
                    $config['black']        = array(224,255,255); // array, default is array(255,255,255)
                    $config['white']        = array(70,130,180); // array, default is array(0,0,0)
                    $this->ciqrcode->initialize($config);
                    
                    $image_name=$nama.'.png'; //buat name dari qr code sesuai dengan nim
                    $dataLink = base_url().'assets/compile/'.$gambar;
                    $params['data'] = $dataLink; //data yang akan di jadikan QR CODE
                    $params['level'] = 'H'; //H=High
                    $params['size'] = 10;
                    $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
                    $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
                    
        			$this->db->query("INSERT INTO compile (compile_kategori_id, compile_nama, compile_file,compile_apkipa, compile_user) VALUES ('$business','$nama','$gambar','$apkipa','$namaUser')");
                    $work = $this->db->query("SELECT * FROM work where work_kategori_id='$business'");
                    $proj = $this->db->query("SELECT * FROM kategori where kategori_id='$business'")->row_array();
                    $namaProject = $proj['kategori_nama'];
                    
                    $listWorkBA = array();
                    foreach($work->result() as $wrk){
                      array_push($listWorkBA,$wrk->work_user_id);
                    }
                    $listBa = implode("','",array_unique($listWorkBA));
                    $dbBA = $this->db->query("SELECT * FROM pengguna where pengguna_level='4' AND pengguna_id IN ('$listBa')");
             
                    $namaUserLog = $this->session->userdata('nama');
                    // $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$namaUserLog','Mengunggah $apkipa $nama -> project $namaProject')");
                    
                    foreach($dbBA->result() as $ba):
                     $config = Array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'ssl://mail.bacotlu.com',
                        'smtp_port' => 465,
                        'smtp_user' => 'notification@bacotlu.com',
                        'smtp_pass' => 'bacotlu123',
                        'mailtype'  => 'html',
                        'charset' => 'utf-8',);
                        $this->email->initialize($config);
                        $this->email->from('notification@bacotlu.com','bacotLu Assignment System');
                        $this->email->to($ba->pengguna_email);
                        $this->email->subject("$namaUserLog upload new $apkipa in $namaProject");
                        $this->email->message("
                        <html>
                        <body
                            style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
                            <table
                                style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
                                            <center>
                                                <img style='margin:20px' src='http://www.gadgetadda.com/wp-content/uploads/2014/08/android-to-ios-contacts.jpg' width='200px'>
                                            </center>
                                            <center>
                                                <h2>$nama $apkipa <br> Uploaded!</h2>
                                                <p style='font-size:14px;margin:0 0 6px 0;'>
                                                    Lets check your dashboard to test the app
                                                </p><br>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='height:35px;'></td>
                                    </tr>
                                </tbody>
                                <tfooter>
                                    <tr>
                                        <td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
                                            <strong style='display:block;margin:0 0 10px 0;'>Regards,</strong> $namaUserLog <br> Jakarta Barat, DKI
                                            Jakarta, IND<br><br>
                                            This email is automatic sent by system, please dont reply this message, Thankyou
                                        </td>
                                    </tr>
                                </tfooter>
                            </table>
                        </body>
                        </html>");
                    // $this->email->send();
                    endforeach;
        			echo $this->session->set_flashdata('msg','success-insert');
        			redirect('admin/compile');
                }
			}
		}else{
		    echo $this->session->set_flashdata('msg','akses');
			redirect('admin/dashboard');
		}		
	}

	function hapus_compile(){
		if($this->session->userdata('akses')=='3'){
			$kode=strip_tags($this->input->post('xkode'));
			   $compile = $this->db->query("SELECT * FROM compile where compile_id='$kode'")->row_array();
			   $namaCompile = $compile['compile_nama'];
			   $apkIPA = $compile['compile_apkipa'];
			   $file = $compile['compile_file'];
			   $namaUserLog = $this->session->userdata('nama');
                $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$namaUserLog','Menghapus $apkIPA $namaCompile')");
                $this->db->query("DELETE FROM compile where compile_id='$kode'");
                
                $path='./assets/compile/'.$file;
		        unlink($path);
    			echo $this->session->set_flashdata('msg','success-hapus');
    			redirect('admin/compile'); 
		}else{
			echo $this->session->set_flashdata('msg','akses');
			redirect('admin/dashboard');
		}
	}

}