<?php
class Assignment extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
		$this->load->library('upload');
	}


	function index(){
		
    }
    
    function task(){
        if($this->session->userdata('akses')=='5'){
            $userId = $this->session->userdata('idadmin');
				$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
				$x['dark'] = $userDB['pengguna_moto'];
            $userId = $this->uri->segment(4);
            $token = $this->uri->segment(5);
            $project = $this->uri->segment(6);
            $allTask = $this->uri->segment(7);
            $tim = $this->db->query("SELECT * FROM tim where tim_user_id='$userId' AND tim_token='$token'");
            if($tim->num_rows() > 0){
                $hasilTim = $this->db->query("SELECT * FROM tim where tim_user_id='$userId' AND tim_token='$token'")->row_array();
                $x['user'] = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'");
                $hasilProject = $this->db->query("SELECT * FROM kategori where kategori_slug='$project'")->row_array();
                $kategoriId = $hasilProject['kategori_id'];
                $x['tglJoin'] = $hasilTim['tim_tgl'];
                $x['userId'] = $userId;
                $x['token'] = $token;
                $project= $this->db->query("SELECT * FROM kategori where kategori_id='$kategoriId'")->row_array();
                $x['projectName'] = $project['kategori_nama'];
                $user = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
                $akses = $user['pengguna_level'];
              
                    if($akses === '4'){
                        $x['onprogress']=$this->db->query("SELECT * FROM work where work_user_id='$userId' AND work_kategori_id='$kategoriId' order by work_deadline ASC");
                       
                    }else{
                        $x['onprogress']=$this->db->query("SELECT * FROM work 
                        where work_assignor_id='$userId' AND
                         work_kategori_id='$kategoriId'
                         order by work_deadline ASC");

                        $xonprogress=$this->db->query("SELECT * FROM work 
                        where work_assignor_id='$userId' AND
                        work_kategori_id='$kategoriId'
                        order by work_deadline ASC");
                        
                        $reverted = $this->db->query("SELECT * FROM assesment 
                        inner join work on assesment.assesment_work_id=work.work_id
                        where assesment_user_id='$userId' AND 
                        assesment_kategori='REVERT' AND
                        assesment_work_id=work.work_id
                        order by assesment_record DESC limit 1")->row_array();

                        $responseTime = $this->db->query("SELECT * FROM assesment  
                        inner join work on assesment.assesment_work_id=work.work_id
                        where assesment_user_id='$userId' AND 
                        assesment_kategori='RESPONSE' AND 
                        assesment_work_id=work.work_id")->row_array();
                            
                        $x['responseTime']= $responseTime;
                        $x['reverted'] = $reverted;

                    }
                $this->load->view('admin/v_assignment',$x);
            }else{
                redirect('admin/dashboard');
            }
		}else{
			redirect('permission');
		}
    }

    function search_task(){
        if($this->session->userdata('akses')=='5'){
            $userId = $this->session->userdata('idadmin');
				$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
				$x['dark'] = $userDB['pengguna_moto'];
            $userId = $this->uri->segment(4);
            $token = $this->uri->segment(5);
            $tim = $this->db->query("SELECT * FROM tim where tim_user_id='$userId' AND tim_token='$token'");
            $allTask = $this->uri->segment(6);
           
            if($tim->num_rows() > 0){
                $hasilTim = $this->db->query("SELECT * FROM tim where tim_user_id='$userId' AND tim_token='$token'")->row_array();
                $x['user'] = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'");

                $kategoriId = $hasilTim['tim_kategori_id'];
                $x['tglJoin'] = $hasilTim['tim_tgl'];
                $x['userId'] = $userId;
                $x['token'] = $token;
                $project= $this->db->query("SELECT * FROM kategori where kategori_id='$kategoriId'")->row_array();
                $x['projectName'] = $project['kategori_nama'];

                if($allTask === 'alltask'){
                    $keyword = $this->input->get('keyword');
                    $x['onprogress']=$this->db->query("SELECT * FROM work where work_assignor_id='$userId' AND work_nama LIKE '%$keyword%'");
                }else{
                    $keyword = $this->input->get('keyword');
                    $x['onprogress']=$this->db->query("SELECT * FROM work where work_assignor_id='$userId' AND work_nama LIKE '%$keyword%' AND  work_status='A' order by work_status DESC");
                }
                $this->load->view('admin/v_assignment',$x);
            }else{
                redirect('admin/dashboard');
            }
		}else{
			redirect('permission');
		}
    }

}