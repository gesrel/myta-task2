<?php
class Setting extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
		$this->load->library('upload');
	}


	function index(){
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='3' || $this->session->userdata('akses')=='5' ){
		    $idUser = $this->session->userdata('idadmin');
			$x['dataUser'] = $this->db->query("SELECT * FROM pengguna where pengguna_id='$idUser'");
			$x['akses'] = $this->session->userdata('akses');			
		    $data = $this->db->query("SELECT * FROM pengguna where pengguna_id='$idUser'")->row_array();
			$x['prior'] = $data['pengguna_prior'];
			$x['dark'] = $data['pengguna_moto'];
			$this->load->view('admin/v_setting',$x);
		}else{
			redirect('permission');
		}
	}
	
	function update_user(){ 
	    if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='3'|| $this->session->userdata('akses')=='5' ){
		   $kode = $this->input->post('xkode');
		   $oldpass = $this->input->post('xoldpass');
		   $pass1 = $this->input->post('xpass1');
	       $pass2 = $this->input->post('xpass2');
	       
	        $config['upload_path'] = './assets/images/'; //path folder
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

            $this->upload->initialize($config);
            $checkUser = $this->db->query("SELECT * FROM pengguna where pengguna_id='$kode' AND pengguna_password=md5('$oldpass')")->num_rows();
            if($checkUser > 0){
                if ($this->upload->do_upload('filefoto')){
                    $gbr = $this->upload->data();
                    //Compress Image
                    $config['image_library']='gd2';
                    $config['source_image']='./assets/images/'.$gbr['file_name'];
                    $config['create_thumb']= FALSE;
                    $config['maintain_ratio']= FALSE;
                    $config['quality']= '100%';
                    $config['new_image']= './assets/images/'.$gbr['file_name'];
                    $this->load->library('image_lib', $config);
                    $this->image_lib->resize();
    
                    $gambar=$gbr['file_name'];
                    if($pass1 !== $pass2){
                        echo $this->session->set_flashdata('msg','password');
	                    redirect('admin/setting');
                    }else{
                        if($pass1 === ''){
	                        $this->db->query("UPDATE pengguna set pengguna_photo='$gambar' where pengguna_id='$kode'");
                            echo $this->session->set_flashdata('msg','sukses');
	                        redirect('admin/setting');
                        }else{
                            $this->db->query("UPDATE pengguna set pengguna_photo='$gambar', pengguna_password=md5('$pass1') where pengguna_id='$kode'");
                            echo $this->session->set_flashdata('msg','sukses');
	                        redirect('admin/setting');
                        }
                    }
                }else{
                    if($pass1 === '' && $pass2 === ''){
						$this->db->query("UPDATE pengguna set pengguna_photo='' where pengguna_id='$kode'");
	                    redirect('admin/setting');
                    }else{
                        if($pass1 === $pass2){
                            $this->db->query("UPDATE pengguna set pengguna_password=md5('$pass1') where pengguna_id='$kode'");
                            echo $this->session->set_flashdata('msg','sukses');
    	                    redirect('admin/setting');
                        }else{
                            echo $this->session->set_flashdata('msg','password');
    	                    redirect('admin/setting');
                        }  
                    }
                    
                }
            }else{
                echo $this->session->set_flashdata('msg','wrong-pass');
    	        redirect('admin/setting');
            }
	        
		}else{
			redirect('permission');
		}
	}
	
	function update_prior(){
	     if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='3' || $this->session->userdata('akses')=='5' ){
	        $kode = $this->input->post("xkode");
	        $project = $this->input->post("xproject");
	        $this->db->query("UPDATE pengguna set pengguna_prior='$project' where pengguna_id='$kode'");
	        echo $this->session->set_flashdata('msg','prior');
	        redirect('admin/setting');
	     }else{
	         redirect('permission');
	     }
	}

	function update_dark(){
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='3' || $this->session->userdata('akses')=='5' ){
		   $userId = $this->session->userdata('idadmin');
		   $dark = $this->input->post("xdark");
		   $this->db->query("UPDATE pengguna set pengguna_moto='$dark' where pengguna_id='$userId'");
		   echo $this->session->set_flashdata('bannerLogin','true');
 
		   redirect('admin/dashboard');
		}else{
			redirect('permission');
		}
   }

}