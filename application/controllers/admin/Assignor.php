<?php
class Assignor extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != true) {
            $url = base_url('login');
            redirect($url);
        };
        $this->load->model('M_assignor', 'm_assignor');
        $this->load->library('upload');
    }

    public function index()
    {
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='3' || $this->session->userdata('akses')=='4'){
            $kode = $this->session->userdata('idadmin');
            $x['data'] = $this->m_assignor->get_all_assignor_user($kode);
            $this->load->view('admin/v_assignor', $x);
        } else {
            redirect('administrator');
        }
    }

    public function simpan_assignor()
    {
        $config['upload_path'] = './assets/images/assignor/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = true; //nama yang terupload nantinya

        $this->upload->initialize($config);

		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='3' || $this->session->userdata('akses')=='4'){
            $this->upload->do_upload('filefoto');
            $gbr = $this->upload->data();
            //Compress Image
            $config['image_library'] = 'gd2';
            $config['source_image'] = './assets/images/assignor/'.$gbr['file_name'];
            $config['create_thumb'] = false;
            $config['maintain_ratio'] = false;
            $config['quality'] = '60%';
            $config['width'] = 300;
            $config['height'] = 300;
            $config['new_image'] = './assets/images/assignor/'.$gbr['file_name'];
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            $gambar = $gbr['file_name'];

            $nama = strip_tags($this->input->post('xnama'));
            $email = strip_tags($this->input->post('xemail'));

            $this->m_assignor->simpan_assignor($nama, $email, $gambar);
            echo $this->session->set_flashdata('msg', 'success');
            redirect('admin/assignor');
        } else {
            redirect('administrator');
        }
    }

    public function update_assignor()
    {
        $config['upload_path'] = './assets/images/assignor/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = true; //nama yang terupload nantinya
		$this->upload->initialize($config);

		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='3' || $this->session->userdata('akses')=='4'){
            $kode = strip_tags($this->input->post('xkode'));
			$nama=strip_tags($this->input->post('xnama'));
			$email=strip_tags($this->input->post('xemail'));

            if ($this->upload->do_upload('filefoto2')) {
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets/images/assignor/'.$gbr['file_name'];
                $config['create_thumb'] = false;
                $config['maintain_ratio'] = false;
                $config['quality'] = '60%';
                $config['width'] = 300;
                $config['height'] = 300;
                $config['new_image'] = './assets/images/assignor/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                $gambar = $gbr['file_name'];

                $this->m_assignor->update_assignor($kode, $nama, $email, $gambar);
                echo $this->session->set_flashdata('msg', 'info');
                redirect('admin/assignor');
            } else {
                $this->m_assignor->update_assignor_nogambar($kode, $nama, $email);
                echo $this->session->set_flashdata('msg', 'info');
                redirect('admin/assignor');
            }
        } else {
            redirect('administrator');
        }

    }
    public function hapus_assignor()
    {
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='3' || $this->session->userdata('akses')=='4'){
            $kode = strip_tags($this->input->post('kode'));
            $this->m_assignor->hapus_assignor($kode);
            echo $this->session->set_flashdata('msg', 'success-hapus');
            redirect('admin/assignor');
        } else {
            redirect('administrator');
        }
    }

}
