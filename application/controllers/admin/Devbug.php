<?php
class Devbug extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
		$this->load->model('M_devbug','m_devbug');
		$this->load->library('upload');
	}


	function index(){
		if($this->session->userdata('akses')=='1'){
			$userId = $this->session->userdata('idadmin');
				$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
				$x['dark'] = $userDB['pengguna_moto'];
			$kode = $this->session->userdata('idadmin');
			$x['data']=$this->m_devbug->get_all_devbug();
			$this->load->view('admin/v_devbug',$x);
		}else{
			redirect('permission');
		}
	}

	function simpan_devbug(){
		if($this->session->userdata('akses')=='1'){
			$devbug=strip_tags($this->input->post('xdevbug'));
			$this->m_devbug->simpan_devbug($devbug);
			echo $this->session->set_flashdata('msg','success');
			redirect('admin/devbug');
		}else{
			redirect('permission');
		}		
	}

	function update_devbug(){
		if($this->session->userdata('akses')=='1'){
            $kode=strip_tags($this->input->post('xkode'));
			$devbug=strip_tags($this->input->post('xdevbug2'));
            $this->m_devbug->update_devbug($kode,$devbug);
            echo $this->session->set_flashdata('msg','info');
            redirect('admin/devbug');
		}else{
			redirect('permission');
		}
	}
	function hapus_devbug(){
		if($this->session->userdata('akses')=='1'){
			$kode=strip_tags($this->input->post('kode'));
			$work = $this->db->query("SELECT * FROM work where work_status IN ('A','B','D','E') AND work_devbug_id='$kode'");
			if($work->num_rows() > 0){
			    echo $this->session->set_flashdata('msg','gagal-hapus');
			    redirect('admin/devbug');
			}else{
			   $this->m_devbug->hapus_devbug($kode);
    			echo $this->session->set_flashdata('msg','success-hapus');
    			redirect('admin/devbug'); 
			}
			
		}else{
			redirect('permission');
		}
	}

}