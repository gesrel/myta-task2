<?php
class Notification extends CI_Controller{
	function __construct(){
		parent::__construct();

        $this->load->library('email');
	}
	function workDeadline(){
	    $work = $this->db->query("SELECT * FROM work where work_status = 'A' AND work_deadline > CURDATE()");
    	    if($work->num_rows() > 0){
    	        foreach($work->result() as $worksDead){
    	            $idKategori = $worksDead->work_kategori_id;
        	        $project = $this->db->query("SELECT * FROM kategori where kategori_id='$idKategori'")->row_array();
        	        $iconProject = $project['kategori_gambar'];
        	        
        	        $idBA = $worksDead->work_user_id;
        	        $ba = $this->db->query("SELECT * FROM pengguna where pengguna_id='$idBA'")->row_array();
        	            $emailBA = $ba['pengguna_email'];
        	            $namaBA = $ba['pengguna_nama'];
        	        
        	        $idDev = $worksDead->work_assignor_id;
        	        $dev = $this->db->query("SELECT * FROM pengguna where pengguna_id='$idDev'")->row_array();
        	            $emailDev = $dev['pengguna_email'];
        	            $namaDev = $dev['pengguna_nama'];
        	            
        	        $pm1 = $this->db->query("SELECT * FROM pengguna where pengguna_level='2'");
        	        $pmList = array();
        	        foreach($pm1->result() as $pm):
        	            array_push($pmList,$pm->pengguna_email);
        	        endforeach;
        	            
        	        $deadlineEmail = date("d M Y H:i A",strtotime($worksDead->work_deadline));
        	        
        	        $workNama = $worksDead->work_nama;
        	        $workDeskripsi = strip_tags($worksDead->work_deskripsi);
        	        
        	        $devbaList = array($emailBA, $emailDev);
        	        $listTotal = array_merge($devbaList,$pmList);
        	        
        	        $allList = implode(',',$listTotal);
        	        
        	        $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://mail.bacotlu.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'notification@bacotlu.com',
                    'smtp_pass' => 'bacotlu123',
                    'mailtype'  => 'html',
                    'charset' => 'utf-8',);
                    $this->email->initialize($config);
                    $this->email->from('notification@bacotlu.com','bacotLu Notification');
                    $this->email->to($allList);
                    $this->email->subject("$namaDev Passing the Deadline");
                    $this->email->message("
                    <html>
                    <body
                        style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
                        <table
                            style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
                                        <center>
                                            <img style='margin:20px' src='https://uat.bacotlu.com/assets/images/business/$iconProject' width='120px'>
                                        </center>
                                        
                                        <center>
                                            <h2>$workNama</h2>
                                            <p style='font-size:14px;margin:0 0 6px 0;'>
                                                $workDeskripsi
                                            </p><br>
                                            <center>
                                                <img style='margin:20px' src='https://uat.bacotlu.com/theme/images/warning.gif' width='50px'>
                                            </center>
                                            <p style='font-size:14px;margin:0 0 6px 0;'>
                                                Deadline : <b>$deadlineEmail</b>, <br>Please be concern to finish the Task!
                                            </p><br>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='height:35px;'></td>
                                </tr>
                            </tbody>
                            <tfooter>
                                <tr>
                                    <td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
                                        <strong style='display:block;margin:0 0 10px 0;'>Assignor,</strong> $namaBA | Developer, $namaDev <br> Jakarta Barat, DKI
                                        Jakarta, IND<br><br>
                                        This email is automatic sent by system, please dont reply this message, Thankyou
                                    </td>
                                </tr>
                            </tfooter>
                        </table>
                    </body>
                    </html>");
                 $this->email->send();
    	        }
    	        echo '<center><div>Deadline Notif Sent '. $allList.'</div></center>';
    	        
    	    }else{
    	        echo '<center><div>Deadline Nihil</div></center>';
    	    }
	        
	    }
}