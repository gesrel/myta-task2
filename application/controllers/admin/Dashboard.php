<?php
class Dashboard extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
		$this->load->helper('short_number');
		
        $this->load->model('M_kategori','m_kategori');
		$this->load->model('M_assignor','m_assignor');
		$this->load->model('M_work', 'm_work');
		$this->load->library('user_agent');
	}

	function index(){
		if($this->session->userdata('akses')=='1'){
			$this->index_core1();
		}else{
			if($this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='3' || $this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='5' ){
				$akses = $this->session->userdata('akses');
				$x['akses'] = $akses;
				$x['userId']=$this->session->userdata('idadmin');
				$userId = $this->session->userdata('idadmin');
				$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
				$x['dark'] = $userDB['pengguna_moto'];
				$x['slug'] = $this->uri->segment(4);
				$priorData = $this->db->query("SELECT * FROM pengguna where pengguna_prior !='' AND pengguna_id='$userId'");
	
				if($akses === '5'){
					$x['kategori'] = $this->db->query("SELECT * FROM kategori 
					INNER JOIN pengguna on pengguna.pengguna_id=kategori.kategori_tl_id
					where kategori_tl_id='$userId'");
				}else if($akses === '2'){
					$x['kategori'] = $this->db->query("SELECT * FROM kategori
					INNER JOIN pengguna on pengguna.pengguna_id=kategori.kategori_tl_id
					where kategori_user_id='$userId'");
				}else{
					$x['kategori'] = $this->db->query("SELECT * FROM tim 
					INNER JOIN kategori ON tim.tim_kategori_id=kategori.kategori_id
					INNER JOIN pengguna on pengguna.pengguna_id=kategori.kategori_tl_id
					where tim_user_id='$userId' AND tim_acc='VERIFIED'");
				}
	
				if($priorData->num_rows() > 0){
					$dataPrior = $priorData->row_array();
					$idProject = $dataPrior['pengguna_prior'];
					$projectData = $this->db->query("SELECT * FROM kategori where kategori_id='$idProject'")->row_array();
					$projectSlug = $projectData['kategori_slug'];
					redirect('admin/dashboard/project/'.$projectSlug);
				}else{
					$x['prior'] = false;
					$x['namaAkses'] = $this->session->userdata('nama');
					$x['assignor2']=$this->db->query("SELECT * FROM pengguna");
					$x['stage']=$this->db->query("SELECT * FROM devbug");
					$x['kategoriId'] = '';
					if($akses =='5'){
						$x['workDetail'] = $this->db->query("SELECT * FROM work where work_user_id='$userId' AND work_status IN ('A','D','B','E','F')");
						$x['ongoing'] = $this->m_work->get_ongoing_ba_tl_dashboard($userId);
						$x['requested'] = $this->m_work->get_requested_ba_tl_dashboard($userId);
						$x['done'] = $this->m_work->get_done_ba_tl_dashboard($userId); 
						$x['pic'] = 'DIC';
					}else if($akses == '4'){
						$userId = $this->session->userdata('idadmin');
						$x['workDetail'] = $this->db->query("SELECT * FROM work where work_user_id='$userId' AND work_status IN ('A','D','B','E','F')");
						$x['ongoing'] = $this->m_work->get_ongoing_ba_tl_dashboard($userId);
						$x['requested'] = $this->m_work->get_requested_ba_tl_dashboard($userId);
						$x['done'] = $this->m_work->get_done_ba_tl_dashboard($userId); 
						$x['pic'] = 'DIC';
					}else if($akses == '2'){
						$x['workDetail'] = $this->db->query("SELECT * FROM work where work_status IN ('A','D','B','E','F')");
						$x['ongoing'] = $this->m_work->get_ongoing_pm_dashboard($userId);
						$x['pic'] = 'DIC';
					}else{
						$x['workDetail'] = $this->db->query("SELECT * FROM work where work_assignor_id='$userId' AND work_status IN ('A','D','B','E','F')");
						$x['ongoing'] = $this->m_work->get_ongoing_dev_dashboard($userId);
						$x['requested'] = $this->m_work->get_requested_dev_dashboard($userId);
						$x['done'] = $this->m_work->get_done_dev_dashboard($userId);
						$x['pic'] = 'PIC';
					}
					// $x['listSprint'] = $this->db->query("SELECT * FROM sprint where sprint_kategori_id='$idProject'");
					$x['latestAPK'] = $this->db->query("SELECT * FROM compile where compile_apkipa IN('APK','IPA') order by compile_id");
					$x['allprojectName'] = 'All Project';
					
					$this->load->view('admin/v_dashboard',$x);
				}
			}else{
				redirect('permission');
			}
		}
		
	}

	function comment(){
		$this->load->view('admin/v_comment');
	}
	
	function goToProject(){
	    $project=$this->input->post('xproject');
	    $data = $this->db->query("SELECT * FROM kategori where kategori_id='$project'");
	   
	   if($data->num_rows() > 0){
	       $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$project'");
	       $proj = $kategori->row_array();
	       $projectCode = $proj['kategori_slug'];
	       	       redirect('admin/dashboard/project/'.$projectCode);

	       
	   }else{
	       redirect('admin/dashboard');
	   }
	    
	}
	
	function project(){
	    $akses = $this->session->userdata('akses');
		$x['akses'] = $this->session->userdata('akses');
		$x['namaAkses'] = $this->session->userdata('nama');
		$x['kategori']=$this->m_kategori->get_all_kategori();
		$x['assignor2']=$this->db->query("SELECT * FROM pengguna");
		$x['userId']=$this->session->userdata('idadmin');
		$x['agent']=$this->agent->is_mobile();
		
		$x['stage']=$this->db->query("SELECT * FROM devbug");
		
		$userId=$this->session->userdata('idadmin');
		
		$appCode = $this->uri->segment(4);
		$x['slug'] = $this->uri->segment(4);
		$userId = $this->session->userdata('idadmin');
			$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
			$x['dark'] = $userDB['pengguna_moto'];
		$priorData = $this->db->query("SELECT * FROM pengguna where pengguna_prior !='' AND pengguna_id='$userId'");
		if($priorData->num_rows() > 0){
			$x['prior'] = true;
		}else{
			$x['prior'] = false;
		}

		if($akses === '5'){
			$kategori = $this->db->query("SELECT * FROM kategori where kategori_tl_id='$userId' AND kategori_slug='$appCode'");
			$x['kategori'] = $this->db->query("SELECT * FROM kategori where kategori_tl_id='$userId' ");

		}else if($akses === '2'){
			$kategori = $this->db->query("SELECT * FROM kategori where kategori_user_id='$userId' AND kategori_slug='$appCode'");
			$x['kategori'] = $this->db->query("SELECT * FROM kategori where kategori_user_id='$userId'");
		}else if($akses === '1'){
			$kategori = $this->db->query("SELECT * FROM kategori where kategori_slug='$appCode'");
			$x['kategori'] = $this->db->query("SELECT * FROM kategori");
		}else{
			$kategori = $this->db->query("SELECT * FROM kategori where kategori_slug='$appCode'");
			$x['kategori'] = $this->db->query("SELECT * FROM tim 
			INNER JOIN kategori ON tim.tim_kategori_id=kategori.kategori_id
			where tim_user_id='$userId' AND tim_acc='VERIFIED'");
		}
		
		
	    if($kategori->num_rows() > 0){
				$list = $kategori->row_array();
				$idProject = $list['kategori_id'];
				$namaProject = $list['kategori_nama'];
				$x['namaProject'] = $list['kategori_nama'];
				$idTL = $list['kategori_tl_id'];
				$x['totalCanceled'] = $this->db->query("SELECT * FROM work where work_kategori_id='$idProject' AND work_status='C'")->num_rows();

				$tim = $this->db->query("SELECT * FROM tim where tim_user_id='$userId' AND tim_kategori_id='$idProject' AND tim_acc='VERIFIED'");
				$x['listSprint'] = $this->db->query("SELECT * FROM sprint where sprint_kategori_id='$idProject' order by sprint_start");
				
				$selectedSprint = $this->input->get('xsprint');
				if($selectedSprint === null){
					$getSprint = $this->db->query("SELECT * FROM sprint where sprint_kategori_id='$idProject' AND sprint_start <= CURDATE() AND sprint_end >= CURDATE()")->row_array();
					$x['exactSprint'] = $getSprint['sprint_id'];
					$idSprint = $getSprint['sprint_id'];
				}else{
					$getSprint = $this->db->query("SELECT * FROM sprint where sprint_kategori_id='$idProject' AND sprint_id='$selectedSprint'")->row_array();
					$x['exactSprint'] = $getSprint['sprint_id'];
					$idSprint = $getSprint['sprint_id'];
				}
				

				if($tim->num_rows() > 0 || $akses === '5' || $akses === '2' || $akses === '1'){
					$x['projectName'] = $this->db->query("SELECT * FROM kategori where kategori_id='$idProject'")->row_array();
					
					if($akses =='4' || $akses =='5' ){
						$x['workDetail'] = $this->db->query("SELECT * FROM work where work_user_id='$userId' AND work_kategori_id='$idProject' AND work_status IN ('A','D','B','E','F')");
						$x['ongoing'] = $this->m_work->get_ongoing_ba_tl_project($userId, $idProject, $idSprint);
						$x['requested'] = $this->m_work->get_requested_ba_tl_project($userId, $idProject);
						$x['done'] = $this->m_work->get_done_ba_tl_project($userId, $idProject);
						$x['pic'] = 'DIC';
						
						if($akses === '5'){
							$x['assignor'] = $this->m_work->get_assignor_tl($userId, $idProject);
						}else{
							$x['assignor'] = $this->m_work->get_assignor_ba($idTL, $idProject);
						}

					}else if($akses == '2' || $akses == '1'){
						$x['workDetail'] = $this->db->query("SELECT * FROM work where work_kategori_id='$idProject' AND work_status IN ('A','D','B','E','F')");
						$x['ongoing'] = $this->m_work->get_ongoing_pm_project($idProject, $idSprint);
						$x['pic'] = 'DIC';
					}else if($akses == '3'){
						$x['workDetail'] = $this->db->query("SELECT * FROM work where work_kategori_id='$idProject' AND work_assignor_id='$userId' AND work_status IN ('A','D','B','E','F')");
						$x['ongoing'] = $this->m_work->get_ongoing_dev_project($userId, $idProject,$idSprint);
						$x['requested'] = $this->m_work->get_requested_dev_project($userId, $idProject);
						$x['done'] = $this->m_work->get_done_dev_project($userId, $idProject);
						$x['pic'] = 'PIC';
					}
					$x['latestAPK'] = $this->db->query("SELECT * FROM compile where compile_kategori_id='$idProject' AND compile_apkipa IN('APK','IPA') order by compile_id DESC");
					// $x['latestIPA'] = $this->db->query("SELECT * FROM compile where compile_kategori_id='$idProject' AND compile_apkipa='IPA' order by compile_id DESC limit 1");
					$works = $this->db->query("SELECT * FROM work where work_kategori_id = '$idProject'")->row_array();
					$x['workDev'] = $works['work_assignor_id'];
					$x['timLeader'] = $this->db->query("SELECT * FROM pengguna where pengguna_id='$idTL'");

					$x['kategoriId'] = $idProject;
					$x['user'] = $this->db->query("SELECT * FROM tim 
					INNER JOIN pengguna ON tim.tim_user_id=pengguna.pengguna_id
					where tim_kategori_id='$idProject' AND tim_tl_id='$idTL' AND tim_acc='VERIFIED'");

					$x['allprojectName'] = '';

					$this->load->view('admin/v_dashboard',$x);
				}else{
					redirect('permission');
				}
	    }else{
			redirect('permission');
	    }
	}

	function get_detail_work(){
		$kode=$this->input->post('kode');
		
		$akses=$this->session->userdata('akses');
		$assign = 'work_assignor_id';
		if($akses === '3'){
		    $assign = 'work_user_id';
		}
		$work = $this->db->query("SELECT * FROM work
		INNER JOIN kategori on work.work_kategori_id=kategori.kategori_id
		LEFT JOIN pengguna on work.$assign=pengguna.pengguna_id
		INNER JOIN sprint on sprint.sprint_id=work.work_sprint_id
		where work_id='$kode'");

		$hasil = $work->row_array();
		$deskripsi = $hasil['work_deskripsi'];
		$device = $hasil['work_device'];
		$userId = $this->session->userdata('idadmin');
        $this->session->set_userdata('succesComment', '');
        $this->session->set_userdata('idtask', '');

		if($device === 'ANDROID'){
			$device_img = 'android.png';
		}else if($device === 'IOS'){
			$device_img = 'ios.png';
		}else if($device === 'ANDROIOS'){
			$device_img = 'androios.png';
		}else if($device === 'CMS'){
			$device_img = 'cms.png';
		}else if($device === 'UI & UX'){
			$device_img = 'frontend.png';
		}else if($device === 'BACKEND'){
			$device_img = 'backend.png';
		}else{
			$device_img = 'favicon.png';
		}

		if($deskripsi === ''){
			$deskripsi = 'There is no Description for this task';
		}

		$deadline = date('l, d M Y h:i A', strtotime($hasil['work_deadline']));
		if($work->num_rows() > 0){
			$response = array(
				'kategori_nama'=> $hasil['kategori_nama'],
				'kategori_gambar'=> $hasil['kategori_gambar'],
				'work_nama'=> $hasil['work_nama'],
				'work_deskripsi' => $deskripsi,
				'work_device' => $device_img,
				'work_device_deskripsi' => $hasil['work_device'],
				'work_assignor' => $hasil['pengguna_nama'],
				'work_assignor_gambar' => $hasil['pengguna_photo'],
				'work_deadline' => $deadline,
				'work_id' => $hasil['work_id'],
				'work_sprint' => $hasil['sprint_nama']
			);

			$hasilComment = array();

			$comment = $this->db->query("SELECT * FROM comment 
			inner join pengguna on pengguna.pengguna_id=comment_user_id
			inner join attachment on attachment.attachment_comment_id=comment_id
			where comment_task_id='$kode' order by comment_created");

			$this->db->query("UPDATE comment set comment_read='READ' where comment_user_id!='$userId' AND comment_task_id='$kode'");

			foreach($comment->result() as $com){
				$gambar = $com->pengguna_photo;	
				$gender = $com->pengguna_jenkel;
				if($gambar === '' || $gambar === null){
					if($gender === 'L'){
						$gambar = 'user_blank.png';
					}else{
						$gambar = 'user_blank2	.png';
					}
				}
				$time = date("d M Y h:i A", strtotime($com->comment_created));
				$attach1 = $com->attachment_file1;
				$attach2 = $com->attachment_file2;
				$attach3 = $com->attachment_file3;
				$attach4 = $com->attachment_file4;

				if(strpos($attach1, '.PDF') || strpos($attach1, '.pdf')){
					$attach1 = "pdf.png";
				}
				if(strpos($attach2, '.PDF') || strpos($attach2, '.pdf')){
					$attach2 = "pdf.png";
				}
				if(strpos($attach3, '.PDF') || strpos($attach3, '.pdf')){
					$attach3 = "pdf.png";
				}
				if(strpos($attach4, '.PDF') || strpos($attach4, '.pdf')){
					$attach4 = "pdf.png";
				}

				if($attach1 == ""){
					$attach1 = '';
				}else{
				    if($attach1 === 'pdf.png'){
				        $attach1 = '<a target="_blank" href="'.base_url().'assets/images/attachment/'.$com->attachment_file1.'"><img class="img-fluid" src="'.base_url().'assets/images/attachment/'.$attach1.'" alt=""></a>';
				    }else{
				        $attach1 = '<a class="img-link img-link-zoom-in img-lightbox" href="'.base_url().'assets/images/attachment/'.$com->attachment_file1.'"><img class="img-fluid" src="'.base_url().'assets/images/attachment/'.$attach1.'" alt=""></a>';
				    }
				}
				
				if($attach2 == ""){
					$attach2 = '';
				}else{
				    if($attach2 === 'pdf.png'){
					    $attach2 = '<a target="_blank"  href="'.base_url().'assets/images/attachment/'.$com->attachment_file2.'"><img class="img-fluid" src="'.base_url().'assets/images/attachment/'.$attach2.'" alt=""></a>';
				    }else{
					    $attach2 = '<a class="img-link img-link-zoom-in img-lightbox" target="_blank"  href="'.base_url().'assets/images/attachment/'.$com->attachment_file2.'"><img class="img-fluid" src="'.base_url().'assets/images/attachment/'.$attach2.'" alt=""></a>';
				    }
				}

				if($attach3 == ""){
					$attach3 = '';
				}else{
				    if($attach3 === 'pdf.png'){
					    $attach3 = '<a target="_blank"  href="'.base_url().'assets/images/attachment/'.$com->attachment_file3.'"><img class="img-fluid" src="'.base_url().'assets/images/attachment/'.$attach3.'" alt=""></a>';
				    }else{
					    $attach3 = '<a class="img-link img-link-zoom-in img-lightbox" href="'.base_url().'assets/images/attachment/'.$com->attachment_file3.'"><img class="img-fluid" src="'.base_url().'assets/images/attachment/'.$attach3.'" alt=""></a>';
				    }
				}

				if($attach4 == ""){
					$attach4 = '';
				}else{
				    if($attach4 === 'pdf.png'){
					    $attach4 = '<a target="_blank"  href="'.base_url().'assets/images/attachment/'.$com->attachment_file4.'"><img class="img-fluid" src="'.base_url().'assets/images/attachment/'.$attach4.'" alt=""></a>';
				    }else{
					    $attach4 = '<a class="img-link img-link-zoom-in img-lightbox" href="'.base_url().'assets/images/attachment/'.$com->attachment_file4.'"><img class="img-fluid" src="'.base_url().'assets/images/attachment/'.$attach4.'" alt=""></a>';
				    }
				}

				$isi = $com->comment_isi;

				
				$hasilComment1 = array(
					'comment_created' => $time,
					'comment_user' => $com->pengguna_nama,
					'comment_user_id' => $com->pengguna_id,
					'comment_user_gambar' => $gambar,
					'comment_isi' => $com->comment_isi,
					'comment_attach1' => $attach1 ,
					'comment_attach2' => $attach2 ,
					'comment_attach3' => $attach3 ,
					'comment_attach4' => $attach4 ,
				);
				array_push($hasilComment, $hasilComment1);
			}
			$response['listComment'] = $hasilComment; 
			
			echo json_encode($response);
		}else{
			echo false;
		}
	}

	function get_pm_list(){
		$getPM = $this->db->query("SELECT * FROM pengguna where pengguna_level='2'");
		$hasilPM = array();
		$hasil = array();
		foreach($getPM->result() as $pm){
			$gambarPM = $pm->pengguna_photo;
			if($gambarPM === '' || $gambarPM === null){
				if($pm->pengguna_jenkel === 'L'){
					$gambarPM = 'user_blank.png';
				}else{
					$gambarPM = 'user_blank2.png';
				}
			}
			$hasilPM = array(
				'pm_id' => $pm->pengguna_id,
				'pm_nama' => $pm->pengguna_nama,
				'pm_gambar' => $gambarPM,
				'pm_email' => $pm->pengguna_email
			);
			array_push($hasil, $hasilPM);
		}
		echo json_encode($hasil);
	}

	function pm_project(){
		redirect('admin/dashboard/index_core2');
	}

	function index_core1(){
		if($this->session->userdata('akses') === '1'){
			$x['listProject'] = $this->db->query("SELECT * FROM kategori
			inner join pengguna on kategori.kategori_tl_id=pengguna.pengguna_id");
			$userId = $this->session->userdata('idadmin');
			$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
			$x['dark'] = $userDB['pengguna_moto'];
			$this->load->view('admin/v_dashboard_core',$x);
		}else{
			redirect('permission');
		}
	}

	function index_core2(){
		if($this->session->userdata('akses') === '1'){
			$pm = $this->input->get('pm');
			$getPM = $this->db->query("SELECT * FROM pengguna where pengguna_email='$pm' and pengguna_level='2'")->row_array();
			$idPM = $getPM['pengguna_id'];
			$x['idSelectedPM'] = $getPM['pengguna_id'];
			$x['listProject'] = $this->db->query("SELECT * FROM kategori 
			inner join pengguna on kategori.kategori_tl_id=pengguna.pengguna_id
			where kategori_user_id='$idPM'");
			$userId = $this->session->userdata('idadmin');
			$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
			$x['dark'] = $userDB['pengguna_moto'];
			$this->load->view('admin/v_dashboard_core',$x);
		}else{
			redirect('permission');
		}
		
	}
}