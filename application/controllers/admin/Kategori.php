<?php
class Kategori extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
		$this->load->model('M_kategori','m_kategori');
		$this->load->model('M_assesment','m_assesment');

		$this->load->library('upload');
	}


	function index(){
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2'  ){
			$kode = $this->session->userdata('idadmin');
			$userId = $this->session->userdata('idadmin');
				$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
				$x['dark'] = $userDB['pengguna_moto'];
			$x['data']=$this->m_kategori->get_all_kategori();
			$x['tl']= $this->db->query("SELECT * FROM pengguna where pengguna_level='5'");
			$this->load->view('admin/v_kategori',$x);
		}else{
			redirect('permission');
		}
	}

	function simpan_kategori(){
		$config['upload_path'] = './assets/images/business/'; //path folder
		$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya

		$this->upload->initialize($config);

		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2'  ){
			$this->upload->do_upload('filefoto');
			$gbr = $this->upload->data();
			//Compress Image
			$config['image_library']='gd2';
			$config['source_image']='./assets/images/business/'.$gbr['file_name'];
			$config['create_thumb']= FALSE;
			$config['maintain_ratio']= FALSE;
			$config['quality']= '60%';
			$config['new_image']= './assets/images/business/'.$gbr['file_name'];
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();

			$gambar=$gbr['file_name'];

			$kategori=strip_tags($this->input->post('xkategori'));
			$tl=strip_tags($this->input->post('xtl'));
			$deskripsi=strip_tags($this->input->post('xdeskripsi'));
			$string=preg_replace('/[^a-zA-Z0-9 \&%|{.}=,?!*()"-_+$@;<>\']/', '', $kategori);
			$trim=trim($string);
			$add_slash=strtolower(str_replace(" ", "-", $trim));
			$slug=$add_slash;
			$start = $this->input->post('xstart');
			$end = $this->input->post('xend');
			$id=rand(1111,123456789);
			$this->m_assesment->simpan_default($id);
			$this->m_assesment->simpan_reverted_dev($id);
			$this->m_assesment->simpan_reverted_ba($id);
			$this->m_kategori->simpan_kategori($id,$kategori,$tl,$deskripsi,$gambar,$slug, $start, $end);
			echo $this->session->set_flashdata('msg','success');
			redirect('admin/kategori');
		}else{
			redirect('permission');
		}		
	}

	function update_kategori(){
		$config['upload_path'] = './assets/images/business/'; //path folder
		$config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
		$config['encrypt_name'] = TRUE; //nama yang terupload nantinya

		$this->upload->initialize($config);

		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2'  ){
			$kode=strip_tags($this->input->post('xkode'));
			$kategori=strip_tags($this->input->post('xkategori2'));
			$tl=strip_tags($this->input->post('xtl'));
			$deskripsi=strip_tags($this->input->post('xdeskripsi'));
			$string=preg_replace('/[^a-zA-Z0-9 \&%|{.}=,?!*()"-_+$@;<>\']/', '', $kategori);
			$trim=trim($string);
			$add_slash=strtolower(str_replace(" ", "-", $trim));
			$slug=$add_slash;
			$start = $this->input->post('xstartnew');
			$end = $this->input->post('xendnew');

			if($start === ''){
				$start = $this->input->post('xstart');
			}
			if($end === ''){
				$end = $this->input->post('xend');
			}
			if($this->upload->do_upload('filefoto')){
				$gbr = $this->upload->data();
				//Compress Image
				$config['image_library']='gd2';
				$config['source_image']='./assets/images/business/'.$gbr['file_name'];
				$config['create_thumb']= FALSE;
				$config['maintain_ratio']= FALSE;
				$config['quality']= '60%';
				$config['new_image']= './assets/images/business/'.$gbr['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
	
				$gambar=$gbr['file_name'];
				$this->m_kategori->update_kategori($kode,$kategori,$tl,$deskripsi,$gambar,$slug, $start, $end);
			
				echo $this->session->set_flashdata('msg','info');
				redirect('admin/kategori');
			}else{
				$this->m_kategori->update_kategori_nogambar($kode,$kategori,$tl,$deskripsi,$slug,$start, $end);
				echo $this->session->set_flashdata('msg','info');
				redirect('admin/kategori');
			}

			
		}else{
			redirect('permission');
		}
	}
	function hapus_kategori(){
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2'){
			$kode=strip_tags($this->input->post('kode'));
			$work = $this->db->query("SELECT * FROM work where work_status IN ('A','B','D','E') AND work_kategori_id='$kode'");
			if($work->num_rows() > 0){
			    echo $this->session->set_flashdata('msg','gagal-hapus');
			    redirect('admin/kategori');
			}else{
			   $this->m_kategori->hapus_kategori($kode);
    			echo $this->session->set_flashdata('msg','success-hapus');
    			redirect('admin/kategori'); 
			}
			
		}else{
			redirect('permission');
		}
	}

}