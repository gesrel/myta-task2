<?php
class Loguser extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
		$this->load->library('upload');
	}


	function index(){
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='5' ){
			$kode = $this->session->userdata('idadmin');
			$userId = $this->session->userdata('idadmin');
				$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
				$x['dark'] = $userDB['pengguna_moto'];
			$x['data']=$this->db->query("SELECT * FROM log order by log_id DESC");
			$this->load->view('admin/v_log',$x);
		}elseif($this->session->userdata('akses') === '3' || $this->session->userdata('akses') === '4'){
		    $kode = $this->session->userdata('idadmin');
		    $userId = $this->session->userdata('idadmin');
				$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
				$x['dark'] = $userDB['pengguna_moto'];
		    $nama = $this->session->userdata('nama');
			$x['data']=$this->db->query("SELECT * FROM log where log_user='$nama' order by log_id DESC");
			$this->load->view('admin/v_log',$x);
		}else{
			redirect('permission');
		}
	}

}