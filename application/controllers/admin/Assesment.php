<?php
class Assesment extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
	}


	function index(){
        
	}

    
    function data(){
        $akses = $this->session->userdata('akses');
		$userId = $this->session->userdata('idadmin');
		$userId = $this->session->userdata('idadmin');
				$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
				$x['dark'] = $userDB['pengguna_moto'];
		if($akses =='2'){
            $slug = $this->uri->segment(4);
            $project = $this->db->query("SELECT * FROM kategori
			inner join pengguna on kategori.kategori_tl_id=pengguna.pengguna_id
			where kategori_slug='$slug' AND kategori_user_id='$userId'");
            if($project->num_rows() > 0){
                $pro = $project->row_array();
                $idProject = $pro['kategori_id'];
				$x['response'] = $this->db->query("SELECT * FROM response where response_kategori_id='$idProject' order by response_durasi");
				$x['revertedDev'] = $this->db->query("SELECT * FROM reverted where reverted_kategori_id='$idProject' AND reverted_devba='DEV' order by reverted_jumlah");
				$x['revertedBA'] = $this->db->query("SELECT * FROM reverted where reverted_kategori_id='$idProject' AND reverted_devba='BA' order by reverted_jumlah");
                $x['project'] = $project->row_array();
                $this->load->view('admin/v_assesment', $x);
            }else{
                redirect('permission');
            }
		}else{
			redirect('permission');
		}
    }

	function update_response(){
		$akses = $this->session->userdata('akses');
		if($akses=='2'){
			$kode = $this->input->post('xkode');
			$durasi = $this->input->post('xdurasi');
			$slug = $this->input->post('xslug');
			$this->db->query("UPDATE response set response_durasi='$durasi' where response_id='$kode'");
			redirect('admin/assesment/data/'.$slug);
		}else{
			redirect('permission');
		}
	}

	function update_reverted(){
		$akses = $this->session->userdata('akses');
		if($akses=='2'){
			$kode = $this->input->post('xkode');
			$jumlah = $this->input->post('xjumlah');
			$slug = $this->input->post('xslug');
			$this->db->query("UPDATE reverted set reverted_jumlah='$jumlah' where reverted_id='$kode'");
			redirect('admin/assesment/data/'.$slug);
		}else{
			redirect('permission');
		}
	}

	function hapus_responsetime(){
        $akses = $this->session->userdata('akses');
		if($akses=='2'){
			
		}else{
			redirect('permission');
		}
	}

}