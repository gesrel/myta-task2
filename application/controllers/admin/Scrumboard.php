<?php
class Scrumboard extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
        $this->load->model('M_assesment','m_assesment');

	}


	function index(){
        $akses = $this->session->userdata('akses');
		if($akses =='3' || $akses =='5' || $akses =='4' ){
            $kode = $this->session->userdata('idadmin');

            $userId = $this->session->userdata('idadmin');
            $userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
            $x['dark'] = $userDB['pengguna_moto'];
            $x['akses'] = $akses;

            if($akses === '4' || $akses === '5'){
                $x['task'] = $this->db->query("SELECT * FROM work where work_user_id='$kode'");
                $idAssign = 'work_user_id';
                $idAssignScrum = 'work_assignor_id';
                $x['width'] = "width:300px";
                $x['pic'] = 'DIC :';
                $x['heightContainer'] = 'height:700px';
            }else{
                $x['task'] = $this->db->query("SELECT * FROM work where work_assignor_id='$kode'");
                $idAssign = 'work_assignor_id';
                $idAssignScrum = 'work_user_id';
                $x['width'] = "width:300";
                $x['heightContainer'] = 'height:550px';
                $x['pic'] = 'PIC :';
            }
            
            $x['ongoing'] = $this->db->query("SELECT * FROM work 
            INNER JOIN pengguna ON work.$idAssignScrum=pengguna.pengguna_id
            INNER JOIN kategori ON work.work_kategori_id=kategori.kategori_id 
            where work_status IN ('A','F') AND $idAssign='$kode' order by work_status"); 

            $x['readytotest'] = $this->db->query("SELECT * FROM work 
            INNER JOIN pengguna ON work.$idAssignScrum=pengguna.pengguna_id 
            INNER JOIN kategori ON work.work_kategori_id=kategori.kategori_id 
            where work_status ='D' AND $idAssign='$kode'"); 

            $x['readytoapk'] = $this->db->query("SELECT * FROM work 
            INNER JOIN pengguna ON work.$idAssignScrum=pengguna.pengguna_id 
            INNER JOIN kategori ON work.work_kategori_id=kategori.kategori_id 
            where work_status ='E' AND $idAssign='$kode'"); 

            $x['done'] = $this->db->query("SELECT * FROM work 
            INNER JOIN pengguna ON work.$idAssignScrum=pengguna.pengguna_id
            INNER JOIN kategori ON work.work_kategori_id=kategori.kategori_id 
            where work_status ='B' AND work_user_id='$kode'"); 

            $x['cancel'] = $this->db->query("SELECT * FROM work 
            INNER JOIN pengguna ON work.$idAssignScrum=pengguna.pengguna_id
            INNER JOIN kategori ON work.work_kategori_id=kategori.kategori_id 
            where work_status ='C' AND work_user_id='$kode'"); 

        
			$this->load->view('admin/v_scrumboard',$x);
		}else{
			redirect('permission');
		}
    }

    function update_task_ongoing(){
        $kode = $this->input->post('kode');
        $statusCode = 'A';
        $checkWork = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
        $namaUser = $this->session->userdata("nama");
        $timelineId = rand(123456789,99999999);
        if($checkWork['work_status'] === $statusCode){
            //NOTHING HAPPEN!;
            echo "nothing";
        }else{
            //AKAN DIISI DENGAN FUNCTION (NotifikasiEmail, Timeline, Log!);
        $business = $checkWork['work_kategori_id'];
        $userId = $this->session->userdata("idadmin");
         $jumlah = 1;
        $this->db->query("UPDATE work set work_revert=work_revert+$jumlah where work_id='$kode'");

        $getSangatBaik = $this->db->query("SELECT * FROM reverted where reverted_nama='Sangat Baik' AND reverted_kategori_id='$business'")->row_array();
        $getBaik = $this->db->query("SELECT * FROM reverted where reverted_nama='Baik' AND reverted_kategori_id='$business'")->row_array();
        $getKurang = $this->db->query("SELECT * FROM reverted where reverted_nama='Kurang' AND reverted_kategori_id='$business'")->row_array();
        $getSangatKurang = $this->db->query("SELECT * FROM reverted where reverted_nama='Sangat Kurang' AND reverted_kategori_id='$business'")->row_array();
        
        $updatedWork = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
        $workRevert = $updatedWork['work_revert'];

        if($workRevert <= $getSangatBaik['reverted_jumlah']){
            $nilai = 'Sangat Baik';
        }else if($workRevert > $getSangatBaik['reverted_jumlah'] && $workRevert <= $getBaik['reverted_jumlah']){
            $nilai = 'Baik';
        }else if($workRevert > $getBaik['reverted_jumlah'] && $workRevert <= $getKurang['reverted_jumlah']){
            $nilai = 'Kurang';
        }else if($workRevert > $getKurang['reverted_jumlah'] ){
            $nilai = 'Sangat Kurang';
        }

        $this->m_assesment->simpan_assesment_revert_dev($kode, $userId, $nilai, $workRevert);
        
        $this->db->query("INSERT into timeline (timeline_id,timeline_work_id, timeline_user, timeline_code, timeline_nama, timeline_deskripsi)
        VALUES('$timelineId','$kode','$namaUser','REVERT','Revert to Progress','Reverted to progress again')");
        $this->db->query("UPDATE work set work_status='$statusCode' where work_id='$kode'");
        echo "ok";
        }
    }
    
    function update_task_ready(){
        $kode = $this->input->post('kode');
        $statusCode = 'D';
        $checkWork = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
        $namaUser = $this->session->userdata("nama");
        $timelineId = rand(123456789,99999999);
        if($checkWork['work_status'] === $statusCode){
            //NOTHING HAPPEN!;
            echo "nothing";
        }else{
            $workTgl = $checkWork['work_tgl'];
            $business = $checkWork['work_kategori_id'];
            $userId = $this->session->userdata("idadmin");

            $checkAssesmentResponse = $this->db->query("SELECT * FROM assesment where assesment_user_id='$userId' AND assesment_work_id='$kode'");
            if($checkAssesmentResponse->num_rows() === 0){
                $workTime = strtotime($workTgl);
                date_default_timezone_set("Asia/Jakarta");
                $time = new DateTime($workTgl);
                $diff = $time->diff(new DateTime());
                $menit = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i;

                $getSangatBaik = $this->db->query("SELECT * FROM response where response_nama='Sangat Baik' AND response_kategori_id='$business'")->row_array();
                $getBaik = $this->db->query("SELECT * FROM response where response_nama='Baik' AND response_kategori_id='$business'")->row_array();
                $getKurang = $this->db->query("SELECT * FROM response where response_nama='Kurang' AND response_kategori_id='$business'")->row_array();
                $getSangatKurang = $this->db->query("SELECT * FROM response where response_nama='Sangat Kurang' AND response_kategori_id='$business'")->row_array();
                
                if($menit <= $getSangatBaik['response_durasi']){
                    $nilai = 'Sangat Baik';
                }else if($menit > $getSangatBaik['response_durasi'] && $menit < $getBaik['response_durasi']){
                    $nilai = 'Baik';
                }else if($menit > $getBaik['response_durasi'] && $menit < $getKurang['response_durasi']){
                    $nilai = 'Kurang';
                }else if($menit > $getSangatKurang['response_durasi']){
                    $nilai = 'Sangat Kurang';
                }
                $this->m_assesment->simpan_assesment_response_dev($kode, $userId, $nilai, $menit);
            }
            //AKAN DIISI DENGAN FUNCTION (NotifikasiEmail, Timeline, Log!);
            $this->db->query("INSERT into timeline (timeline_id,timeline_work_id, timeline_user, timeline_code, timeline_nama, timeline_deskripsi)
            VALUES('$timelineId','$kode','$namaUser','READYTEST','Ready to test','Task is ready to test')");
            $this->db->query("UPDATE work set work_status='$statusCode' where work_id='$kode'");
            echo "ok";

        }
    }

    function update_task_apk(){
        $kode = $this->input->post('kode');
        $statusCode = 'E';
        $checkWork = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
        $namaUser = $this->session->userdata("nama");
        $timelineId = rand(123456789,99999999);
        if($checkWork['work_status'] === $statusCode){
            //NOTHING HAPPEN!;
            echo "nothing";
        }else{
            $workTgl = $checkWork['work_tgl'];
            $business = $checkWork['work_kategori_id'];
            $userId = $this->session->userdata("idadmin");

            $checkAssesmentResponse = $this->db->query("SELECT * FROM assesment where assesment_user_id='$userId' AND assesment_work_id='$kode'");
            if($checkAssesmentResponse->num_rows() === 0){
                $workTime = strtotime($workTgl);
                date_default_timezone_set("Asia/Jakarta");
                $time = new DateTime($workTgl);
                $diff = $time->diff(new DateTime());
                $menit = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i;

                $getSangatBaik = $this->db->query("SELECT * FROM response where response_nama='Sangat Baik' AND response_kategori_id='$business'")->row_array();
                $getBaik = $this->db->query("SELECT * FROM response where response_nama='Baik' AND response_kategori_id='$business'")->row_array();
                $getKurang = $this->db->query("SELECT * FROM response where response_nama='Kurang' AND response_kategori_id='$business'")->row_array();
                $getSangatKurang = $this->db->query("SELECT * FROM response where response_nama='Sangat Kurang' AND response_kategori_id='$business'")->row_array();
                
                if($menit <= $getSangatBaik['response_durasi']){
                    $nilai = 'Sangat Baik';
                }else if($menit > $getSangatBaik['response_durasi'] && $menit < $getBaik['response_durasi']){
                    $nilai = 'Baik';
                }else if($menit > $getBaik['response_durasi'] && $menit < $getKurang['response_durasi']){
                    $nilai = 'Kurang';
                }else if($menit > $getSangatKurang['response_durasi']){
                    $nilai = 'Sangat Kurang';
                }
                $this->m_assesment->simpan_assesment_response_dev($kode, $userId, $nilai, $menit);
            }
            //AKAN DIISI DENGAN FUNCTION (NotifikasiEmail, Timeline, Log!);
            $this->db->query("INSERT into timeline (timeline_id,timeline_work_id, timeline_user, timeline_code, timeline_nama, timeline_deskripsi)
            VALUES('$timelineId','$kode','$namaUser','READYAPK','Update to wait for new APK/IPA/Builds','Task is ready to test in the new APK/IPA/Builds')");
            $this->db->query("UPDATE work set work_status='$statusCode' where work_id='$kode'");
            echo "ok";

        }
    }

    function update_task_done(){
        $akses = $this->session->userdata('akses');
        if($akses === '4' || $akses === '5'){
        $kode = $this->input->post('kode');
        $statusCode = 'B';
        $checkWork = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
        $namaUser = $this->session->userdata("nama");
        $timelineId = rand(123456789,99999999);
            if($checkWork['work_status'] === $statusCode){
                //NOTHING HAPPEN!;
                echo "nothing";
            }else{
                //AKAN DIISI DENGAN FUNCTION (NotifikasiEmail, Timeline, Log!);
                $this->db->query("INSERT into timeline (timeline_id,timeline_work_id, timeline_user, timeline_code, timeline_nama, timeline_deskripsi)
                VALUES('$timelineId','$kode','$namaUser','DONE','Update to done','Task is finished')");
                $this->db->query("UPDATE work set work_status='$statusCode' where work_id='$kode'");
                echo "ok";
            }
        }else{
            redirect('permission');
        }
    }


    function update_task_cancel(){
        $akses = $this->session->userdata('akses');
        if($akses === '4' || $akses === '5'){
            $kode = $this->input->post('kode');
            $statusCode = 'C';
            $checkWork = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
            $namaUser = $this->session->userdata("nama");
            $timelineId = rand(123456789,99999999);
            if($checkWork['work_status'] === $statusCode){
                //NOTHING HAPPEN!;
                echo "nothing";
            }else{
                //AKAN DIISI DENGAN FUNCTION (NotifikasiEmail, Timeline, Log!);
                $this->db->query("INSERT into timeline (timeline_id,timeline_work_id, timeline_user, timeline_code, timeline_nama, timeline_deskripsi)
                VALUES('$timelineId','$kode','$namaUser','CANCELED','Canceling the task','Task is canceled')");
                $this->db->query("UPDATE work set work_status='$statusCode' where work_id='$kode'");
                echo "ok";
            }
        }else{
            redirect('permission');
        }
        
    }

}