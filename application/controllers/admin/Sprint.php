<?php
class Sprint extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != true) {
            $url = base_url('login');
            redirect($url);
        };
        $this->load->model('M_sprint', 'm_sprint');
        $this->load->model('M_kategori', 'm_kategori');

    }

    public function index(){
		
    }

    function data(){
        if($this->session->userdata('akses')=='2'){
            $userId = $this->session->userdata('idadmin');
			
            $slug = $this->uri->segment(4);
            $getKategori = $this->m_kategori->get_kategori_by_slugpm($userId,$slug);
            if($getKategori->num_rows() > 0){
                    $project = $getKategori->row_array();
                    $idProject = $project['kategori_id'];
                    $userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
			$x['dark'] = $userDB['pengguna_moto'];
                    $x['data'] = $this->m_sprint->get_all_sprint_user($idProject);
                    $x['project_nama'] = $project['kategori_nama'];
                    $x['project_id]'] = $project['kategori_id'];
                    $x['project_slug]'] = $project['kategori_slug'];
                    $this->load->view('admin/v_sprint', $x);
        
            }else{
                redirect('permission');
            }
            
        } else {
            redirect('permission');
        }
    }

    public function simpan_sprint(){

		if($this->session->userdata('akses')=='2'){
            $userId = $this->session->userdata('idadmin');
            $slug = strip_tags($this->input->post('xslug'));
            $getKategori1 = $this->m_kategori->get_kategori_by_slugpm($userId,$slug);
            
            if($getKategori1->num_rows() > 0){
                $id = rand(123456789,99999999);
                $getKategori = $getKategori1->row_array();
                $idProject = $getKategori['kategori_id'];
                $slug1 = $getKategori['kategori_slug'];
    
                $nama = strip_tags($this->input->post('xnama'));
                $start = strip_tags($this->input->post('xstart'));
                $end = strip_tags($this->input->post('xend'));
                

                $checkOtherSprintDate = $this->m_sprint->check_sprint_date_start($idProject, $start);
                if($checkOtherSprintDate->num_rows() > 0){
                    echo $this->session->set_flashdata('msg', 'invalid-date');
                    redirect('admin/sprint/data/'.$slug);
                }else{
                    $checkOtherSprint = $this->m_sprint->get_all_sprint_user($idProject);
                    $takenSprint = false;
                    foreach($checkOtherSprint->result() as $sprint){
                        if($start >= $sprint->sprint_start && $end <= $sprint->sprint_end){
                            $takenSprint = true;
                        }
                    }
                    if($takenSprint === true){
                        echo $this->session->set_flashdata('msg', 'invalid-date');
                        redirect('admin/sprint/data/'.$slug);
                    }else{
                        if($start >= $end){
                            echo $this->session->set_flashdata('msg', 'date');
                            redirect('admin/sprint/data/'.$slug);
                        }else{
                            $this->m_sprint->simpan_sprint($id,$idProject,$nama,$start,$end);
                            echo $this->session->set_flashdata('msg', 'success');
                            redirect('admin/sprint/data/'.$slug1);
                        }
                    }
                }
            }else{
                redirect('permission');
            }
            
        } else {
            redirect('permission');
        }
    }

    public function update_sprint(){
		if($this->session->userdata('akses')=='2'){
            $userId = $this->session->userdata('idadmin');
            $slug = strip_tags($this->input->post('xslug'));
            $getKategori1 = $this->m_kategori->get_kategori_by_slugpm($userId,$slug);
            
            if($getKategori1->num_rows() > 0){
                $project = $getKategori1->row_array();
                $idProject = $project['kategori_id'];
                $kode = strip_tags($this->input->post('xkode'));
                $nama = strip_tags($this->input->post('xnama'));
                $start = strip_tags($this->input->post('xstartnew'));
                $end = strip_tags($this->input->post('xendnew'));
                if($start === ''){
                    $start = strip_tags($this->input->post('xstart'));
                }
                if($end === ''){
                    $end = strip_tags($this->input->post('xend'));
                }
                $checkOtherSprintDate = $this->m_sprint->check_sprint_date_start($idProject, $start);
                if($checkOtherSprintDate->num_rows() > 0){
                    echo $this->session->set_flashdata('msg', 'invalid-date');
                    redirect('admin/sprint/data/'.$slug);
                }else{
                    $checkOtherSprint = $this->m_sprint->get_all_sprint_user($idProject);
                    $takenSprint = false;
                    foreach($checkOtherSprint->result() as $sprint){
                        if($start >= $sprint->sprint_start && $end <= $sprint->sprint_end){
                            $takenSprint = true;
                        }
                    }                    
                    
                    if($takenSprint === true){
                        echo $this->session->set_flashdata('msg', 'invalid-date');
                        redirect('admin/sprint/data/'.$slug);
                    }else{                
                        if($start >= $end){
                            echo $this->session->set_flashdata('msg', 'date');
                            redirect('admin/sprint/data/'.$slug);
                        }else{
                            $this->m_sprint->update_sprint($kode, $nama, $start,$end);
                            echo $this->session->set_flashdata('msg', 'info');
                            redirect('admin/sprint/data/'.$slug);
                        }
                    }
                }
                
            }else{
                redirect('permission');
            }
        } else {
            redirect('permission');
        }

    }
    public function hapus_sprint(){
		if($this->session->userdata('akses')=='2'){
            $kode = strip_tags($this->input->post('kode'));
            $this->m_sprint->hapus_sprint($kode);
            echo $this->session->set_flashdata('msg', 'success-hapus');
            redirect('admin/sprint');
        } else {
            redirect('permission');
        }
    }

}
