<?php
class Rnd extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
		$this->load->library('upload');
	}


	function index(){
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='3' ){
            $idUser = $this->session->userdata('idadmin');
			$x['dataUser'] = $this->db->query("SELECT * FROM pengguna where pengguna_id='$idUser'");
			$x['akses'] = $this->session->userdata('akses');			
		    $data = $this->db->query("SELECT * FROM pengguna where pengguna_id='$idUser'")->row_array();
			$x['prior'] = $data['pengguna_prior'];
			$x['dark'] = $data['pengguna_moto'];
            $this->load->view('admin/v_rnd',$x);
		}else{
			redirect('permission');
		}
	}

}