<?php
class Tim extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
		$this->load->model('M_tim','m_tim');
		$this->load->library('upload');
		$this->load->library('email');
	}
	function index(){
		if($this->session->userdata('akses')=='5' || $this->session->userdata('akses')=='3'){
			$kode = $this->session->userdata('idadmin');
			$userId = $this->session->userdata('idadmin');
				$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
				$x['dark'] = $userDB['pengguna_moto'];
			$prior = $this->db->query("SELECT * FROM pengguna where pengguna_id='$kode'")->row_array();
			if($prior['pengguna_prior'] !== ''){
				$idKat = $prior['pengguna_prior'];
				$kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$idKat'")->row_array();
			}else{
				$kategori = $this->db->query("SELECT * FROM kategori where kategori_tl_id='$kode' order by kategori_tanggal DESC limit 1")->row_array();
			}
			redirect('admin/tim/project/'.$kategori['kategori_slug']);
		}else{
			redirect('permission');
		}
	}


	function goToProject(){
		$project = $this->input->post('xproject');
		$getPro = $this->db->query("SELECT * FROM kategori where kategori_id='$project'");
		if($getPro->num_rows() > 0){
			$getSlug = $getPro->row_array();
			$slug = $getSlug['kategori_slug'];
			redirect('admin/tim/project/'.$slug);
		}else{
			redirect('admin/tim');
		}
	}


	function project() {
		$kode = $this->session->userdata('idadmin');
		$userId = $this->session->userdata('idadmin');
				$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
				$x['dark'] = $userDB['pengguna_moto'];
		$slugProject = $this->uri->segment(4);
		$kategori1 = $this->db->query("SELECT * FROM kategori where kategori_slug='$slugProject' AND kategori_tl_id='$kode'");
		if($kategori1->num_rows() > 0){
			$kategori = $this->db->query("SELECT * FROM kategori where kategori_slug='$slugProject' AND kategori_tl_id='$kode'")->row_array();
			$idproject = $kategori['kategori_id'];
			$x['project'] = $kategori;
			$x['idProject'] = $kategori['kategori_id'];
			$x['namaProject'] = $kategori['kategori_nama'];

			$x['slugProject'] = $kategori['kategori_slug'];
			$x['userId'] = $this->session->userdata('idadmin');
			$x['akses'] = $this->session->userdata('akses');

			$x['data'] = $this->m_tim->get_my_tim_kategori($kode, $idproject);
			$data = $this->m_tim->get_my_tim_kategori($kode, $idproject);
			 
			if($data->num_rows() > 0 ){
				$listDev = array();
				foreach($data->result() as $tm){
					array_push($listDev, $tm->tim_user_id);
				}
				$list = substr(json_encode($listDev), 1, -1);
				$x['developer']=$this->db->query("SELECT * FROM 
				pengguna where pengguna_id NOT IN ($list) 
				AND pengguna_level IN ('4','3')");
			}else{
				$x['developer']=$this->db->query("SELECT * FROM pengguna where pengguna_level IN ('4','3')");
			}	
			$x['kategori'] = $this->db->query("SELECT * FROM kategori where kategori_tl_id='$userId'");
			$this->load->view('admin/v_tim',$x);
		}else{
			redirect('admin/dashboard');
		}
	}
	function simpan_tim(){
		if($this->session->userdata('akses')=='5'){
			$kategori=strip_tags($this->input->post('xkategori'));
			$keterangan=strip_tags($this->input->post('xketerangan'));
			$notes=$this->input->post('xnotes');
			$namaTL = $this->session->userdata('nama');
			$forbidden = false;
			$listForbidden = array();
			$getSlug = $this->db->query("SELECT * FROM kategori where kategori_id='$kategori'")->row_array();
			$slug = $getSlug['kategori_slug'];
			foreach($this->input->post("xdeveloper[]") as $user){
				$tim = $this->db->query("SELECT * FROM tim where tim_user_id='$user' AND tim_kategori_id='$kategori'");
				if($tim->num_rows() > 0){
					$forbidden = true;
					foreach($tim->result() as $tm){
						$devT = $this->db->query("SELECT * FROM pengguna where pengguna_id='$tm->tim_user_id'")->row_array();
						array_push($listForbidden, $devT['pengguna_nama']);
					}
				}else{
					$forbidden = false;
					$str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
					$token = substr(str_shuffle($str), 0, 200);
					$this->m_tim->simpan_tim($kategori,$user,$keterangan,$token);
					$this->email_invitation($namaTL, $user, $kategori, $token,$notes);
				}
			}
			$showForbidden = json_encode($listForbidden);
			if($forbidden === true){
				echo $this->session->set_flashdata('msg','forbidden');
				echo $this->session->set_flashdata('listForbiddens',$showForbidden);
			}else{
				echo $this->session->set_flashdata('msg','sukses');
			}
			
			redirect('admin/tim/project/'.$slug);
		}else{
			redirect('permission');
		}		
	}

	function update_tim(){
		if($this->session->userdata('akses')=='5'){
			$kode=strip_tags($this->input->post('xkode'));
			$tim=strip_tags($this->input->post('xtim2'));
			$deskripsi=strip_tags($this->input->post('xdeskripsi'));

            $this->m_tim->update_tim($kode,$tim,$deskripsi,$gambar,$slug);
            echo $this->session->set_flashdata('msg','info');
            redirect('admin/tim');			
		}else{
			redirect('permission');
		}
	}
	function hapus_tim(){
		if($this->session->userdata('akses')=='5'){
			$kode=strip_tags($this->input->post('kode'));
			$userId=strip_tags($this->input->post('xuserid'));
			$slug=strip_tags($this->input->post('slug2'));
			$this->db->query("UPDATE pengguna set pengguna_prior='' where pengguna_id='$userId'");
			$this->m_tim->hapus_tim($kode);
			echo $this->session->set_flashdata('msg','success-hapus');
			redirect('admin/tim/project/'.$slug); 	
		}else{
			redirect('permission');
		}
	}

	function resend_tim(){
		if($this->session->userdata('akses')=='5'){
			$kode=strip_tags($this->input->post('kode'));
			$slug=strip_tags($this->input->post('slug'));
		}else{
			redirect('permission');
		}
	}


	function email_invitation($namaTL, $userId1, $kategori,$token,$notes){
		$pro = $this->db->query("SELECT * FROM kategori where kategori_id='$kategori'")->row_array();
		$iconProject = $pro['kategori_gambar'];
		$namaProject = $pro['kategori_nama'];
		$keteranganProject = $pro['kategori_deskripsi'];
		$slugProject = $pro['kategori_slug'];

		$user = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId1'")->row_array();
		$userEmail = $user['pengguna_email'];
		$userNama = $user['pengguna_nama'];
		$userId = $user['pengguna_id'];
		$accUrl = base_url().'admin/Emailacc/email_response/'.$userId.'/'.$token.'/'.$slugProject;
		$config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.bacotlu.com',
            'smtp_port' => 465,
            'smtp_user' => 'notification@bacotlu.com',
            'smtp_pass' => 'bacotlu123',
            'mailtype'  => 'html',
            'charset' => 'utf-8',);
            $this->email->initialize($config);
            $this->email->from('notification@bacotlu.com','Myta Team Invitation');
            $this->email->to($userEmail);
            $this->email->subject("$namaProject team invitation from $namaTL");
            $this->email->message("
            <html>
            <body
                style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
                <table
                    style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
                    </thead>
                    <tbody>
                        <tr>
							<td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
								<h3>You're Invited to join $namaTL's team</h3><br>
								<p style='font-size:14px;margin:0 0 8px 0;'>
									Dear $userNama <br>
                                    $notes
								</p><br>
									<img style='margin:20px' src='https://myta.bacotlu.com/assets/images/business/$iconProject' width='70px'>
									<h2>$namaProject</h2>
								
                                <p style='font-size:14px;margin:0 0 6px 0;'>
                                        $keteranganProject
								 </p><br>
									<a href='$accUrl' target='_blank'>Accept Invitation</a>
                            </td>
                        </tr>
                        <tr>
                            <td style='height:35px;'></td>
                        </tr>
                    </tbody>
                    <tfooter>
                        <tr>
                            <td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
                                <strong style='display:block;margin:0 0 10px 0;'>Assignor,</strong> $namaTL <br> Jakarta Barat, DKI
                                Jakarta, IND<br><br>
                                This email is automatic sent by system, please dont reply this message, Thankyou
                            </td>
                        </tr>
                    </tfooter>
                </table>
            </body>
            </html>");
		$this->email->send();
	}


	function email_invitation2(){
		if($this->session->userdata('akses')=='5'){
			$kode=strip_tags($this->input->post('kode'));
			$user = strip_tags($this->input->post('user'));
			$tl = strip_tags($this->input->post('tl'));
			$kategori = strip_tags($this->input->post('kategori'));
			$token = $this->input->post('token');

			$pro = $this->db->query("SELECT * FROM kategori where kategori_id='$kategori'")->row_array();
			$iconProject = $pro['kategori_gambar'];
			$namaProject = $pro['kategori_nama'];
			$keteranganProject = $pro['kategori_deskripsi'];
			$slugProject = $pro['kategori_slug'];

			$user = $this->db->query("SELECT * FROM pengguna where pengguna_id='$user'")->row_array();
			$userEmail = $user['pengguna_email'];
			$userId = $user['pengguna_id'];

			$checkTL = $this->db->query("SELECT * FROM pengguna where pengguna_id='$tl'")->row_array();
			$namaTL = $checkTL['pengguna_nama'];

			$accUrl = base_url().'admin/Emailacc/email_response/'.$userId.'/'.$token.'/'.$slugProject;
			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://mail.bacotlu.com',
				'smtp_port' => 465,
				'smtp_user' => 'notification@bacotlu.com',
				'smtp_pass' => 'bacotlu123',
				'mailtype'  => 'html',
				'charset' => 'utf-8',);
				$this->email->initialize($config);
				$this->email->from('notification@bacotlu.com','Myta Team Invitation');
				$this->email->to($userEmail);
				$this->email->subject("$namaProject team invitation from $namaTL");
				$this->email->message("
				<html>
				<body
					style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
					<table
						style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
						</thead>
						<tbody>
							<tr>
								<td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
									<h3>You're Invited to join $namaTL's team</h3>
									
										<img style='margin:20px' src='https://myta.bacotlu.com/assets/images/business/$iconProject' width='70px'>
										<h2>$namaProject</h2>
									<p style='font-size:14px;margin:0 0 6px 0;'>
											$keteranganProject
									</p><br>
									<center>
										<a href='$accUrl' target='_blank'>Accept Invitation</a>
									</center>
								</td>
							</tr>
							<tr>
								<td style='height:35px;'></td>
							</tr>
						</tbody>
						<tfooter>
							<tr>
								<td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
									<strong style='display:block;margin:0 0 10px 0;'>Assignor,</strong> $namaTL <br> Jakarta Barat, DKI
									Jakarta, IND<br><br>
									This email is automatic sent by system, please dont reply this message, Thankyou
								</td>
							</tr>
						</tfooter>
					</table>
				</body>
				</html>");
			$this->email->send();
			$curdate = date("Y-m-d H:i A");
			$this->db->query("UPDATE tim set tim_tgl=CURDATE(), tim_resend='1' where tim_id='$kode'");
			redirect('admin/tim/project/'.$slugProject); 
		}else{
			redirect('permission');
		}
	}

}