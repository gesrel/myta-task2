<?php
class Kategori extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('M_kategori','m_kategori');
		$this->load->library('upload');
	}

    public function index($id = 0)
    {
        if(!empty($id)){
            $data = $this->db->get_where("work", ['work_id' => $id])->row_array();
        }else{
            $data = $this->db->get("work")->result();
        }
        
        $this->response($data, REST_Controller::HTTP_OK);
    }


}