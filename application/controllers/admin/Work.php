<?php
class work extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
        $this->load->model('M_work','m_work');
        $this->load->model('M_kategori','m_kategori');
        $this->load->model('M_assignor','m_assignor');
        $this->load->model('M_assesment','m_assesment');
        $this->load->library('email');
         $this->load->library('excel');


		$this->load->library('upload');
	}


	function index(){
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='3' || $this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='5'){
            $kode = $this->session->userdata('idadmin');
            $prior = $this->session->userdata('prior');
            $userId = $this->session->userdata('idadmin');
				$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
				$x['dark'] = $userDB['pengguna_moto'];
            if($this->session->userdata('akses') === '4' || $this->session->userdata('akses')=='5' ){
                $x['data']=$this->db->query("SELECT * FROM work where work_status != '0' AND work_user_id ='$kode' order by work_status ASC");

            }elseif($this->session->userdata('akses') === '2'){
                $x['data']=$this->db->query("SELECT * FROM work where work_status != '0' order by work_status ASC");
            }else{
                $x['data']=$this->db->query("SELECT * FROM work where work_assignor_id ='$kode' AND work_status != '0' order by work_status ASC");
            }
            $x['kategori']=$this->m_kategori->get_all_kategori();
            $x['assignor2']=$this->db->query("SELECT * FROM pengguna");
            $x['userId']=$this->session->userdata('idadmin');
            if($this->session->userdata('akses') === '4' || $this->session->userdata('akses') === '2'){
                $x['workDetail'] = $this->db->query("SELECT * FROM work ");
            }else{
                $x['workDetail'] = $this->db->query("SELECT * FROM work where work_assignor_id='$kode'");
            }
            $this->load->view('admin/v_work',$x);
            
		}else{
			redirect('permission');
		}
    }
    
    function validate_project_time(){
        $slug=strip_tags($this->input->post('xslug'));
        date_default_timezone_set("Asia/Jakarta");
        $curtime = new DateTime();
        $project = $this->db->query("SELECT * FROM kategori where kategori_slug='$slug'")->row_array();
        $pro = $this->db->query("SELECT * FROM kategori where kategori_slug='$slug'");
        $start = $project['kategori_active_start'];
        $end = $project['kategori_active_end'];
        
        if($pro->num_rows() > 0){
            if($curtime > $start){
                echo 'addNewTrue';
            }else{
                echo 'addNewFalse';
            }
        }else{
            echo 'addNewError';
        }
        
    }

	function simpan_work(){
		if($this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='5'){
            $business=strip_tags($this->input->post('xbusiness'));
            $assignor=strip_tags($this->input->post('xassignor'));
            $work=strip_tags($this->input->post('xwork'));
            $stage=$this->input->post('xdevbug');
            $priority=$this->input->post('xpriority');
            $sprint=$this->input->post('xsprint');
            $device=$this->input->post('xdevice');
            $deadline=strip_tags($this->input->post('xdeadline'));
            $deadlineEmail = date("d M Y H:i A", strtotime($deadline));
			$deskripsi=$this->input->post('xdeskripsi');
            $segment = $this->uri->segment(2);
            $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$business'")->row_array();
           
            $assign = $this->db->query("SELECT * FROM pengguna where pengguna_id='$assignor'")->row_array();
            $namaAssign = $assign['pengguna_nama'];
            $emailAssign = $assign['pengguna_email'];
            $namaProject = $kategori['kategori_nama'];
            $iconProject = $kategori['kategori_gambar'];
            $namaUserLog = $this->session->userdata('nama');
            $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$namaUserLog','Membuat task baru untuk $namaAssign ($work) $deskripsi project $namaProject')");
            
			$this->m_work->simpan_work($business, $assignor,$work,$stage,$priority,$sprint,$device,$deadline, $deskripsi);
            
            $slug = $kategori['kategori_slug'];

            $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.bacotlu.com',
            'smtp_port' => 465,
            'smtp_user' => 'notification@bacotlu.com',
            'smtp_pass' => 'bacotlu123',
            'mailtype'  => 'html',
            'charset' => 'utf-8',);
            $this->email->initialize($config);
            $this->email->from('notification@bacotlu.com','Myta Notification');
            $this->email->to($emailAssign);
            $this->email->subject("New Task from $namaUserLog ($work)");
            $this->email->message("
            <html>
            <body
                style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
                <table
                    style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
                                <center><img style='margin:20px' src='https://myta.bacotlu.com/assets/images/business/$iconProject' width='70px'>
                                </center>
                                <center>
                                    <h2>$work</h2>
                                    <p style='font-size:14px;margin:0 0 6px 0;'>
                                        $deskripsi
                                    </p><br>
                                    <p style='font-size:14px;margin:0 0 6px 0;'>
                                        Deadline : <b>$deadlineEmail</b>
                                    </p><br>
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td style='height:35px;'></td>
                        </tr>
                    </tbody>
                    <tfooter>
                        <tr>
                            <td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
                                <strong style='display:block;margin:0 0 10px 0;'>Assignor,</strong> $namaUserLog <br> Jakarta Barat, DKI
                                Jakarta, IND<br><br>
                                This email is automatic sent by system, please dont reply this message, Thankyou
                            </td>
                        </tr>
                    </tfooter>
                </table>
            </body>
            </html>");
            $this->email->send();
            echo $this->session->set_flashdata('date', $deadline);
            redirect('admin/dashboard/project/'.$slug);
          
		}else{
			redirect('permission');
		}		
	}
	
	
	function simpan_work_dev(){
		if($this->session->userdata('akses')=='3'){
            $business=strip_tags($this->input->post('xbusiness'));
            $assignor=strip_tags($this->input->post('xassignor'));
            $work=strip_tags($this->input->post('xwork'));
            $stage=strip_tags($this->input->post('xdevbug'));
            $sprint=$this->input->post('xsprint');
            $device=$this->input->post('xdevice');
            $deadline=$this->input->post('xdeadline');
            $deadlineEmail = date("d M Y H:i A", strtotime($deadline));
			$deskripsi=$this->input->post('xdeskripsi');

            $segment = $this->uri->segment(2);
            
            $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$business'")->row_array();

            date_default_timezone_set("Asia/Jakarta");
            $curtime = new DateTime();

            
            $assign = $this->db->query("SELECT * FROM pengguna where pengguna_id='$assignor'")->row_array();
            $namaAssign = $assign['pengguna_nama'];
            $emailAssign = $assign['pengguna_email'];
            $namaProject = $kategori['kategori_nama'];
            $iconProject = $kategori['kategori_gambar'];
            $namaUserLog = $this->session->userdata('nama');
            $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$namaUserLog','Me-request task kepada $namaAssign ($work) $deskripsi project $namaProject')");
            
			$this->m_work->simpan_work_dev($business, $assignor,$work,$stage,$sprint,$device,$deadline, $deskripsi);
            
            $slug = $kategori['kategori_slug'];
            $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.bacotlu.com',
            'smtp_port' => 465,
            'smtp_user' => 'notification@bacotlu.com',
            'smtp_pass' => 'bacotlu123',
            'mailtype'  => 'html',
            'charset' => 'utf-8',);
            $this->email->initialize($config);
            $this->email->from('notification@bacotlu.com','Myta Notification');
            $this->email->to($emailAssign);
            $this->email->subject("New Task Request $namaUserLog ($work)");
            $this->email->message("
            <html>
            <body
                style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
                <table
                    style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
                                <center><img style='margin:20px' src='https://myta.bacotlu.com/assets/images/business/$iconProject' width='70px'>
                                </center>
                                <center>
                                    <h2>$work</h2>
                                    
                                    <p style='font-size:14px;margin:0 0 6px 0;'>
                                        Deadline : <b>$deadlineEmail</b>
                                    </p><br>
                                </center>
                                <p style='font-size:14px;margin:0 0 6px 0;'>
                                        $deskripsi
                                    </p><br>
                            </td>
                        </tr>
                        <tr>
                            <td style='height:35px;'></td>
                        </tr>
                    </tbody>
                    <tfooter>
                        <tr>
                            <td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
                                <strong style='display:block;margin:0 0 10px 0;'>Assignor,</strong> $namaAssign <br> Jakarta Barat, DKI
                                Jakarta, IND<br><br>
                                This email is automatic sent by system, please dont reply this message, Thankyou
                            </td>
                        </tr>
                    </tfooter>
                </table>
            </body>
            </html>");
            $this->email->send();
            echo $this->session->set_flashdata('msg','success-sentWork');
            redirect('admin/dashboard/project/'.$slug);
          
		}else{
			redirect('permission');
		}		
	}

    function update_assignor(){
		if($this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='5'){
            $kode=strip_tags($this->input->post('kode'));
            $assignor=strip_tags($this->input->post('xassignor'));
            $deadline=strip_tags($this->input->post('xdeadline'));
            $newdeadline=strip_tags($this->input->post('xnewdeadline'));
            if($newdeadline !== ''){
                $deadline = $newdeadline;
            }else{
                $deadline=strip_tags($this->input->post('xdeadline'));
            }
            $deadlineEmail = date("d M Y H:i A", strtotime($deadline));
            $segment = $this->uri->segment(2);
    
            $work = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
            $business = $work['work_kategori_id'];
            $assign1 = $work['work_assignor_id'];
            $workNama = $work['work_nama'];
            
            $workDeskripsi = $work['work_deskripsi'];
            $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$business'")->row_array();
            $assign = $this->db->query("SELECT * FROM pengguna where pengguna_id='$assign1'")->row_array();
            $namaAssign = $assign['pengguna_nama'];
            
            $assign2 = $this->db->query("SELECT * FROM pengguna where pengguna_id='$assignor'")->row_array();
            $namaAssign2 = $assign2['pengguna_nama'];
            $emailAssign2 = $assign2['pengguna_email'];
            
            $namaProject = $kategori['kategori_nama'];
            $iconProject = $kategori['kategori_gambar'];
            $namaUserLog = $this->session->userdata('nama');
            $userId = $this->session->userdata('idadmin');
            $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$namaUserLog','Memindahkan assignment $workNama, dari $namaAssign ke $namaAssign2')");
            
            $this->db->query("UPDATE work set work_user_id='$userId', work_assignor_id='$assignor',work_deadline='$deadline', work_status='A' where work_id='$kode'");
            
            $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.bacotlu.com',
            'smtp_port' => 465,
            'smtp_user' => 'notification@bacotlu.com',
            'smtp_pass' => 'bacotlu123',
            'mailtype'  => 'html',
            'charset' => 'utf-8',);
            $this->email->initialize($config);
            $this->email->from('notification@bacotlu.com','Myta My Task');
            $this->email->to($emailAssign2);
            $this->email->subject("New Task from $namaUserLog ($workNama)");
            $this->email->message("
            <html>
            <body
                style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
                <table
                    style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
                                <center><img style='margin:20px' src='https://myta.bacotlu.com/assets/images/business/$iconProject' width='70px'>
                                </center>
                                <center>
                                    <h2>$workNama</h2>
                                    
                                    <p style='font-size:14px;margin:0 0 6px 0;'>
                                        Deadline : <b>$deadlineEmail</b>
                                    </p><br>
                                </center>
                                <p style='font-size:14px;margin:0 0 6px 0;'>
                                        $workDeskripsi
                                    </p><br>
                            </td>
                        </tr>
                        <tr>
                            <td style='height:35px;'></td>
                        </tr>
                    </tbody>
                    <tfooter>
                        <tr>
                            <td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
                                <strong style='display:block;margin:0 0 10px 0;'>Assignor,</strong> $namaUserLog <br> Jakarta Barat, DKI
                                Jakarta, IND<br><br>
                                This email is automatic sent by system, please dont reply this message, Thankyou
                            </td>
                        </tr>
                    </tfooter>
                </table>
            </body>
            </html>");
            $this->email->send();
            
                echo $this->session->set_flashdata('msg','success-editAssign');          
                redirect('admin/dashboard');
            

		}else{
			redirect('permission');
		}
    }

    function done_work_dashboard(){
		if($this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='5'){
            $kode=strip_tags($this->input->post('xkode'));
            $slug = $this->input->post('slug');
            $nama = $this->session->userdata('nama');
            $telp = $this->session->userdata('telp');
            $email = $this->session->userdata('email');
            $this->m_work->done_work($kode);
            
            $work = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
            $business = $work['work_kategori_id'];
            $assign1 = $work['work_assignor_id'];
            $workNama = $work['work_nama'];
            $workDeskripsi = $work['work_deskripsi'];
            $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$business'")->row_array();
            $assign = $this->db->query("SELECT * FROM pengguna where pengguna_id='$assign1'")->row_array();
            $namaAssign = $assign['pengguna_nama'];
            $emailAssign = $assign['pengguna_email'];
            $namaProject = $kategori['kategori_nama'];
            $namaUserLog = $this->session->userdata('nama');
            $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$namaUserLog','Menyelesaikan task $namaAssign $workNama $workDeskripsi project $namaProject')");
            
             $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://mail.bacotlu.com',
                'smtp_port' => 465,
                'smtp_user' => 'notification@bacotlu.com',
                'smtp_pass' => 'bacotlu123',
                'mailtype'  => 'html',
                'charset' => 'utf-8',);
                $this->email->initialize($config);
                $this->email->from('notification@bacotlu.com','Myta My Task');
                $this->email->to($emailAssign);
                $this->email->subject("Task $workNama has been completed by $namaUserLog");
                $this->email->message("
                <html>
                <body
                    style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
                    <table
                        style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
                                    <center><img style='margin:20px' src='https://sssc.vic.edu.au/wp-content/uploads/2018/08/checkmark.gif' width='70px'>
                                    </center>
                                    <center>
                                        <h2>$workNama Completed!</h2>
                                        <p style='font-size:14px;margin:0 0 6px 0;'>
                                            $workDeskripsi
                                        </p><br>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <td style='height:35px;'></td>
                            </tr>
                        </tbody>
                        <tfooter>
                            <tr>
                                <td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
                                    <strong style='display:block;margin:0 0 10px 0;'>Regards,</strong> $namaUserLog <br> Jakarta Barat, DKI
                                    Jakarta, IND<br><br>

                                    This email is automatic sent by system, please dont reply this message, Thankyou
                                </td>
                            </tr>
                        </tfooter>
                    </table>
                </body>
                </html>");
        $this->email->send();
        $timelineId = rand(123456789,99999999);
        $statusCode = 'B';
        $this->db->query("INSERT into timeline (timeline_id,timeline_work_id, timeline_user, timeline_code, timeline_nama, timeline_deskripsi)
        VALUES('$timelineId','$kode','$namaUserLog','DONE','Update to done','Task is finished')");
        $this->db->query("UPDATE work set work_status='$statusCode' where work_id='$kode'");

         echo $this->session->set_flashdata('msg','success-doneWork');
            if($slug !== ''){
			    redirect('admin/dashboard/project/'.$slug);
			}else{
			    redirect('admin/dashboard');
			}
        }else{
            redirect('permission');
        }
    }
    function cancel_work_dashboard(){
		if($this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='3' || $this->session->userdata('akses')=='5'){
            $kode=strip_tags($this->input->post('xkode'));
            $slug = $this->input->post('slug');
            $this->m_work->cancel_work($kode);
            
            $work = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
            $business = $work['work_kategori_id'];
            $assign1 = $work['work_assignor_id'];
            $workNama = $work['work_nama'];
            $workDeskripsi = $work['work_deskripsi'];
            $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$business'")->row_array();
            $assign = $this->db->query("SELECT * FROM pengguna where pengguna_id='$assign1'")->row_array();
            $namaAssign = $assign['pengguna_nama'];
            $emailAssign = $assign['pengguna_email'];
            $namaProject = $kategori['kategori_nama'];
            $namaUserLog = $this->session->userdata('nama');
            $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$namaUserLog','Membatalkan task $namaAssign $workNama $workDeskripsi project $namaProject')");
            
             $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://mail.bacotlu.com',
                'smtp_port' => 465,
                'smtp_user' => 'notification@bacotlu.com',
                'smtp_pass' => 'bacotlu123',
                'mailtype'  => 'html',
                'charset' => 'utf-8',);
                $this->email->initialize($config);
                $this->email->from('notification@bacotlu.com','Myta My Task');
                $this->email->to($emailAssign);
                $this->email->subject("Task $workNama has been canceled by $namaUserLog");
                $this->email->message("
                <html>
                <body
                    style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
                    <table
                        style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
                                    <center><img style='margin:20px' src='https://cdn2.iconfinder.com/data/icons/free-basic-icon-set-2/300/17-512.png' width='70px'>
                                    </center>
                                    <center>
                                        <h2>$workNama Canceled!</h2>
                                        <p style='font-size:14px;margin:0 0 6px 0;'>
                                            $workDeskripsi
                                        </p><br>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <td style='height:35px;'></td>
                            </tr>
                        </tbody>
                        <tfooter>
                            <tr>
                                <td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
                                    <strong style='display:block;margin:0 0 10px 0;'>Regards,</strong> $namaUserLog <br> Jakarta Barat, DKI
                                    Jakarta, IND<br><br>

                                    This email is automatic sent by system, please dont reply this message, Thankyou
                                </td>
                            </tr>
                        </tfooter>
                    </table>
                </body>
                </html>");
        $this->email->send();
        $timelineId = rand(123456789,99999999);
        $statusCode = 'C';
        $this->db->query("INSERT into timeline (timeline_id,timeline_work_id, timeline_user, timeline_code, timeline_nama, timeline_deskripsi)
        VALUES('$timelineId','$kode','$namaUser','CANCELED','Canceling the task','Task is canceled')");
        $this->db->query("UPDATE work set work_status='$statusCode' where work_id='$kode'");
         echo $this->session->set_flashdata('msg','success-cancelWork');
            if($slug !== ''){
			    redirect('admin/dashboard/project/'.$slug);
			}else{
			    redirect('admin/dashboard');
			}
        }else{
            redirect('permission');
        }
    }

    function progress_work_dashboard(){
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='3' || $this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='5'){
            $kode=strip_tags($this->input->post('xkode'));
            $userId = $this->session->userdata('idadmin');
            $slug = $this->input->post('slug');
            $this->m_work->progress_work($kode);
            
            $work = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
            $business = $work['work_kategori_id'];
            $assign1 = $work['work_assignor_id'];
            $workNama = $work['work_nama'];
            $workDeskripsi = $work['work_deskripsi'];
            $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$business'")->row_array();
            $assign = $this->db->query("SELECT * FROM pengguna where pengguna_id='$assign1'")->row_array();
            $namaAssign = $assign['pengguna_nama'];
            $emailAssign = $assign['pengguna_email'];
            $namaProject = $kategori['kategori_nama'];
            $namaUserLog = $this->session->userdata('nama');
            $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$namaUserLog','Me-revert kembali task $namaAssign $workNama $workDeskripsi project $namaProject')");
            
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://mail.bacotlu.com',
                'smtp_port' => 465,
                'smtp_user' => 'notification@bacotlu.com',
                'smtp_pass' => 'bacotlu123',
                'mailtype'  => 'html',
                'charset' => 'utf-8',);
                $this->email->initialize($config);
                $this->email->from('notification@bacotlu.com','Myta My Task');
                $this->email->to($emailAssign);
                $this->email->subject("Task $workNama has been reverted by $namaUserLog");
                $this->email->message("
                <html>
                <body
                    style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
                    <table
                        style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
                                    <center><img style='margin:20px' src='https://loading.io/spinners/flat-reload/lg.flat-ajax-syncing-loading-icon.gif' width='70px'>
                                    </center>
                                    <center>
                                        <h2>$workNama Reverted</h2>
                                        <p style='font-size:14px;margin:0 0 6px 0;'>
                                            $workDeskripsi
                                        </p><br>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <td style='height:35px;'></td>
                            </tr>
                        </tbody>
                        <tfooter>
                            <tr>
                                <td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
                                    <strong style='display:block;margin:0 0 10px 0;'>Regards,</strong> $namaUserLog <br> Jakarta Barat, DKI
                                    Jakarta, IND<br><br>

                                    This email is automatic sent by system, please dont reply this message, Thankyou
                                </td>
                            </tr>
                        </tfooter>
                    </table>
                </body>
                </html>");
        $this->email->send();

        $jumlah = 1;
        $this->db->query("UPDATE work set work_revert=work_revert+$jumlah where work_id='$kode'");

        $getSangatBaik = $this->db->query("SELECT * FROM reverted where reverted_nama='Sangat Baik' AND reverted_kategori_id='$business'")->row_array();
        $getBaik = $this->db->query("SELECT * FROM reverted where reverted_nama='Baik' AND reverted_kategori_id='$business'")->row_array();
        $getKurang = $this->db->query("SELECT * FROM reverted where reverted_nama='Kurang' AND reverted_kategori_id='$business'")->row_array();
        $getSangatKurang = $this->db->query("SELECT * FROM reverted where reverted_nama='Sangat Kurang' AND reverted_kategori_id='$business'")->row_array();
        
        $updatedWork = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
        $workRevert = $updatedWork['work_revert'];

        if($workRevert <= $getSangatBaik['reverted_jumlah']){
            $nilai = 'Sangat Baik';
        }else if($workRevert > $getSangatBaik['reverted_jumlah'] && $workRevert <= $getBaik['reverted_jumlah']){
            $nilai = 'Baik';
        }else if($workRevert > $getBaik['reverted_jumlah'] && $workRevert <= $getKurang['reverted_jumlah']){
            $nilai = 'Kurang';
        }else if($workRevert > $getKurang['reverted_jumlah'] ){
            $nilai = 'Sangat Kurang';
        }

        $this->m_assesment->simpan_assesment_revert_dev($kode, $userId, $nilai, $workRevert);
        $timelineId = rand(123456789,99999999);
        $statusCode = 'A';
        $this->db->query("INSERT into timeline (timeline_id,timeline_work_id, timeline_user, timeline_code, timeline_nama, timeline_deskripsi)
        VALUES('$timelineId','$kode','$namaUserLog','REVERT','Revert to Progress','Reverted to progress again')");
        $this->db->query("UPDATE work set work_status='$statusCode' where work_id='$kode'");
        
        echo $this->session->set_flashdata('msg','success-revertWork');
            
            if($slug !== ''){
			    redirect('admin/dashboard/project/'.$slug);
			}else{
			    redirect('admin/dashboard');
			}
        }else{
            redirect('permission');
        }
    }
    
    
    function approve_work_dashboard(){
		if($this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='5'){
            $kode=strip_tags($this->input->post('xkode'));
            $slug = $this->input->post('slug');
            $this->m_work->progress_work($kode);
            
             $work = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
            $business = $work['work_kategori_id'];
            $assign1 = $work['work_assignor_id'];
            $workNama = $work['work_nama'];
            $workDeskripsi = $work['work_deskripsi'];
            $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$business'")->row_array();
            $assign = $this->db->query("SELECT * FROM pengguna where pengguna_id='$assign1'")->row_array();
            $namaAssign = $assign['pengguna_nama'];
            $emailAssign = $assign['pengguna_email'];
            $namaProject = $kategori['kategori_nama'];
            $iconProject = $kategori['kategori_gambar'];
            $namaUserLog = $this->session->userdata('nama');
            $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$namaUserLog','Menyetujui task $namaAssign $workNama $workDeskripsi project $namaProject')");
            
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://mail.bacotlu.com',
                'smtp_port' => 465,
                'smtp_user' => 'notification@bacotlu.com',
                'smtp_pass' => 'bacotlu123',
                'mailtype'  => 'html',
                'charset' => 'utf-8',);
                $this->email->initialize($config);
                $this->email->from('notification@bacotlu.com','Myta My Task');
                $this->email->to($emailAssign);
                $this->email->subject("Task $workNama has been approved by $namaUserLog");
                $this->email->message("
                <html>
                <body
                    style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
                    <table
                        style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
                                    <center><img style='margin:20px' src='https://myta.bacotlu.com/assets/images/business/$iconProject' width='70px'>
                                    </center>
                                    <center>
                                        <h2>$workNama Approved</h2>
                                        <p style='font-size:14px;margin:0 0 6px 0;'>
                                            $workDeskripsi
                                        </p><br>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <td style='height:35px;'></td>
                            </tr>
                        </tbody>
                        <tfooter>
                            <tr>
                                <td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
                                    <strong style='display:block;margin:0 0 10px 0;'>Regards,</strong> $namaUserLog <br> Jakarta Barat, DKI
                                    Jakarta, IND<br><br>

                                    This email is automatic sent by system, please dont reply this message, Thankyou
                                </td>
                            </tr>
                        </tfooter>
                    </table>
                </body>
                </html>");
        $this->email->send();
             echo $this->session->set_flashdata('msg','success-approveWork');
             $timelineId = rand(123456789,99999999);
                $statusCode = 'A';
                $this->db->query("INSERT into timeline (timeline_id,timeline_work_id, timeline_user, timeline_code, timeline_nama, timeline_deskripsi)
                VALUES('$timelineId','$kode','$namaUserLog','APPROVED','Approved to Progress','Approving a requested task')");
                $this->db->query("UPDATE work set work_status='$statusCode' where work_id='$kode'");
            if($slug !== ''){
			    redirect('admin/dashboard/project/'.$slug);
			}else{
			    redirect('admin/dashboard');
			}
        }else{
            redirect('permission');
        }
    }
    
    
    function reject_work_dashboard(){
		if($this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='5'){
            $kode=strip_tags($this->input->post('xkode'));
            $slug = $this->input->post('slug');
            $this->m_work->cancel_work($kode);
            
             $work = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
            $business = $work['work_kategori_id'];
            $assign1 = $work['work_assignor_id'];
            $workNama = $work['work_nama'];
            $workDeskripsi = $work['work_deskripsi'];
            $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$business'")->row_array();
            $assign = $this->db->query("SELECT * FROM pengguna where pengguna_id='$assign1'")->row_array();
            $namaAssign = $assign['pengguna_nama'];
            $emailAssign = $assign['pengguna_email'];
            $namaProject = $kategori['kategori_nama'];
            $namaUserLog = $this->session->userdata('nama');
            $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$namaUserLog','Tidak menyetujui task $namaAssign $workNama $workDeskripsi project $namaProject')");
            
             $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://mail.bacotlu.com',
                'smtp_port' => 465,
                'smtp_user' => 'notification@bacotlu.com',
                'smtp_pass' => 'bacotlu123',
                'mailtype'  => 'html',
                'charset' => 'utf-8',);
                $this->email->initialize($config);
                $this->email->from('notification@bacotlu.com','Myta My Task');
                $this->email->to($emailAssign);
                $this->email->subject("Task $workNama has been rejected by $namaUserLog");
                $this->email->message("
                <html>
                <body
                    style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
                    <table
                        style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
                                    <center><img style='margin:20px' src='https://cdn2.iconfinder.com/data/icons/free-basic-icon-set-2/300/17-512.png' width='70px'>
                                    </center>
                                    <center>
                                        <h2>$workNama Rejected!</h2>
                                        <p style='font-size:14px;margin:0 0 6px 0;'>
                                            $workDeskripsi
                                        </p><br>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <td style='height:35px;'></td>
                            </tr>
                        </tbody>
                        <tfooter>
                            <tr>
                                <td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
                                    <strong style='display:block;margin:0 0 10px 0;'>Regards,</strong> $namaUserLog <br> Jakarta Barat, DKI
                                    Jakarta, IND<br><br>

                                    This email is automatic sent by system, please dont reply this message, Thankyou
                                </td>
                            </tr>
                        </tfooter>
                    </table>
                </body>
                </html>");
        $this->email->send();
        $timelineId = rand(123456789,99999999);
        $statusCode = 'C';
        $this->db->query("INSERT into timeline (timeline_id,timeline_work_id, timeline_user, timeline_code, timeline_nama, timeline_deskripsi)
        VALUES('$timelineId','$kode','$namaUserLog','REJECTED','Rejected to Cancel','Task rejected to cancel again')");
        $this->db->query("UPDATE work set work_status='$statusCode' where work_id='$kode'");
         echo $this->session->set_flashdata('msg','success-rejectWork');
            if($slug !== ''){
			    redirect('admin/dashboard/project/'.$slug);
			}else{
			    redirect('admin/dashboard');
			}
        }else{
            redirect('permission');
        }
    }

	function hapus_work(){
		if($this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='5'){
			$kode=strip_tags($this->input->post('kode'));
			$this->m_work->hapus_work($kode);
			
			$work = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
            $business = $work['work_kategori_id'];
            $assign1 = $work['work_assignor_id'];
            $workNama= $work['work_nama'];
            $workDeskripsi = $work['work_deskripsi'];
            $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$business'")->row_array();
            $assign = $this->db->query("SELECT * FROM pengguna where pengguna_id='$assign1'")->row_array();
            $namaAssign = $assign['pengguna_nama'];
            $namaProject = $kategori['kategori_nama'];
            $namaUserLog = $this->session->userdata('nama');
            $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$namaUserLog','Menghapus task $namaAssign $workNama $workDeskripsi project $namaProject')");
			
			echo $this->session->set_flashdata('msg','success-hapus');
			redirect('admin/work');
		}else{
			redirect('permission');
		}
	}
	
	
	function ready_test(){
	    if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='3' || $this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='5'){
            $kode=strip_tags($this->input->post('xkode'));
            $userId = $this->session->userdata('idadmin');
			$slug = $this->input->post('slug');
			$this->db->query("update work set work_status='D' where work_id='$kode'");
			$work = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
            $business = $work['work_kategori_id'];
            $assign1 = $work['work_assignor_id'];
            $assignBA = $work['work_user_id'];
            $workNama = $work['work_nama'];
            $workTgl = $work['work_tgl'];
            $workDeskripsi = $work['work_deskripsi'];
            $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$business'")->row_array();
            $assign = $this->db->query("SELECT * FROM pengguna where pengguna_id='$assign1'")->row_array();
            $assign2 = $this->db->query("SELECT * FROM pengguna where pengguna_id='$assignBA'")->row_array();
            $namaAssign = $assign['pengguna_nama'];
            $emailAssign = $assign2['pengguna_email'];
            $namaProject = $kategori['kategori_nama'];
            $namaUserLog = $this->session->userdata('nama');
            
            if($userId !== $work['work_assignor_id']){
                redirect('admin/dashboard');
            }else{
             $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$namaUserLog','Update Ready to Test untuk task $namaAssign $workNama $workDeskripsi project $namaProject')");
    			$config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://mail.bacotlu.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'notification@bacotlu.com',
                    'smtp_pass' => 'bacotlu123',
                    'mailtype'  => 'html',
                    'charset' => 'utf-8',);
                    $this->email->initialize($config);
                    $this->email->from('notification@bacotlu.com','Myta My Task');
                    $this->email->to($emailAssign);
                    $this->email->subject("Task $workNama Ready to Test by $namaUserLog");
                    $this->email->message("
                    <html>
                    <body
                        style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
                        <table
                            style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
                                        <center><img style='margin:20px' src='https://sssc.vic.edu.au/wp-content/uploads/2018/08/checkmark.gif' width='70px'>
                                        </center>
                                        <center>
                                            <h2>$workNama Ready to Test</h2>
                                            <p style='font-size:14px;margin:0 0 6px 0;'>
                                                $workDeskripsi
                                            </p><br>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='height:35px;'></td>
                                </tr>
                            </tbody>
                            <tfooter>
                                <tr>
                                    <td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
                                        <strong style='display:block;margin:0 0 10px 0;'>Regards,</strong> $namaUserLog <br> Jakarta Barat, DKI
                                        Jakarta, IND<br><br>
    
                                        This email is automatic sent by system, please dont reply this message, Thankyou
                                    </td>
                                </tr>
                            </tfooter>
                        </table>
                    </body>
                    </html>");
                $this->email->send();
                $checkAssesmentResponse = $this->db->query("SELECT * FROM assesment where assesment_work_id='$kode'");
                if($checkAssesmentResponse->num_rows() === 0){
                    $workTime = strtotime($workTgl);
                    date_default_timezone_set("Asia/Jakarta");
                    $time = new DateTime($workTgl);
                    $diff = $time->diff(new DateTime());
                    $menit = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i;
                    $getSangatBaik = $this->db->query("SELECT * FROM response where response_nama='Sangat Baik' AND response_kategori_id='$business'")->row_array();
                    $getBaik = $this->db->query("SELECT * FROM response where response_nama='Baik' AND response_kategori_id='$business'")->row_array();
                    $getKurang = $this->db->query("SELECT * FROM response where response_nama='Kurang' AND response_kategori_id='$business'")->row_array();
                    $getSangatKurang = $this->db->query("SELECT * FROM response where response_nama='Sangat Kurang' AND response_kategori_id='$business'")->row_array();
                    
                    if($menit <= $getSangatBaik['response_durasi']){
                        $nilai = 'Sangat Baik';
                    }else if($menit > $getSangatBaik['response_durasi'] && $menit < $getBaik['response_durasi']){
                        $nilai = 'Baik';
                    }else if($menit > $getBaik['response_durasi'] && $menit < $getKurang['response_durasi']){
                        $nilai = 'Kurang';
                    }else if($menit > $getSangatKurang['response_durasi']){
                        $nilai = 'Sangat Kurang';
                    }
                    $this->m_assesment->simpan_assesment_response_dev($kode, $userId, $nilai, $menit);
                    $timelineId = rand(123456789,99999999);
                    $statusCode = 'D';
                    $this->db->query("INSERT into timeline (timeline_id,timeline_work_id, timeline_user, timeline_code, timeline_nama, timeline_deskripsi)
                    VALUES('$timelineId','$kode','$namaUserLog','READYTEST','Ready to test','Task is ready to test')");
                    $this->db->query("UPDATE work set work_status='$statusCode' where work_id='$kode'");
                }
    			echo $this->session->set_flashdata('msg','success-readyWork');
    			if($slug !== ''){
    			    redirect('admin/dashboard/project/'.$slug);
    			}else{
    			    redirect('admin/dashboard');
    			}   
            }
		}else{
			redirect('permission');
		}
	}
	
	function ready_test_APK(){
	    if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='5' || $this->session->userdata('akses')=='3' || $this->session->userdata('akses')=='4'){
			$kode=strip_tags($this->input->post('xkode'));
            $slug = $this->input->post('slug');
            $userId = $this->session->userdata('idadmin');
			$this->db->query("update work set work_status='E' where work_id='$kode'");
			echo $this->session->set_flashdata('msg','');
			
			$work = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
            $business = $work['work_kategori_id'];
            $assign1 = $work['work_assignor_id'];
            $assignBA = $work['work_user_id'];
            $workNama = $work['work_nama'];
            $workDeskripsi = $work['work_deskripsi'];
            $workTgl = $work['work_tgl'];
            $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$business'")->row_array();
            $assign = $this->db->query("SELECT * FROM pengguna where pengguna_id='$assign1'")->row_array();
            $assign2 = $this->db->query("SELECT * FROM pengguna where pengguna_id='$assignBA'")->row_array();
            $namaAssign = $assign['pengguna_nama'];
            $emailAssign2 = $assign2['pengguna_email'];
            $namaProject = $kategori['kategori_nama'];
            $iconProject = $kategori['kategori_gambar'];
            $namaUserLog = $this->session->userdata('nama');
            
            if($userId !== $work['work_assignor_id']){
                redirect('admin/dashboard');
            }else{
                $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$namaUserLog','Update Wait for new Deployment untuk task $namaAssign $workNama $workDeskripsi project $namaProject')");
    			$config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://mail.bacotlu.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'notification@bacotlu.com',
                    'smtp_pass' => 'bacotlu123',
                    'mailtype'  => 'html',
                    'charset' => 'utf-8',);
                    $this->email->initialize($config);
                    $this->email->from('notification@bacotlu.com','Myta My Task');
                    $this->email->to($emailAssign2);
                    $this->email->subject("Task $workNama Wait for new Deployment by $namaUserLog");
                    $this->email->message("
                    <html>
                    <body
                        style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
                        <table
                            style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
                                         <center>
                                                <img style='margin:20px' src='https://myta.bacotlu.com/assets/images/business/$iconProject' width='120px'>
                                        </center>
                                        <center><img style='margin:20px' src='http://www.gadgetadda.com/wp-content/uploads/2014/08/android-to-ios-contacts.jpg' width='100px'>
                                        </center>
                                        <center>
                                            <h2>$workNama Wait for new Deployment </h2>
                                            <p style='font-size:14px;margin:0 0 6px 0;'>
                                                $workDeskripsi
                                            </p><br>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='height:35px;'></td>
                                </tr>
                            </tbody>
                            <tfooter>
                                <tr>
                                    <td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
                                        <strong style='display:block;margin:0 0 10px 0;'>Regards,</strong> $namaUserLog <br> Jakarta Barat, DKI
                                        Jakarta, IND<br><br>
    
                                        This email is automatic sent by system, please dont reply this message, Thankyou
                                    </td>
                                </tr>
                            </tfooter>
                        </table>
                    </body>
                    </html>");
                $this->email->send();
                $checkAssesmentResponse = $this->db->query("SELECT * FROM assesment where assesment_work_id='$kode'");
                if($checkAssesmentResponse->num_rows() === 0){
                    $workTime = strtotime($workTgl);
                    date_default_timezone_set("Asia/Jakarta");
                    $time = new DateTime($workTgl);
                    $diff = $time->diff(new DateTime());
                    $menit = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i;
    
                    $getSangatBaik = $this->db->query("SELECT * FROM response where response_nama='Sangat Baik' AND response_kategori_id='$business'")->row_array();
                    $getBaik = $this->db->query("SELECT * FROM response where response_nama='Baik' AND response_kategori_id='$business'")->row_array();
                    $getKurang = $this->db->query("SELECT * FROM response where response_nama='Kurang' AND response_kategori_id='$business'")->row_array();
                    $getSangatKurang = $this->db->query("SELECT * FROM response where response_nama='Sangat Kurang' AND response_kategori_id='$business'")->row_array();
                    
                    if($menit <= $getSangatBaik['response_durasi']){
                        $nilai = 'Sangat Baik';
                    }else if($menit > $getSangatBaik['response_durasi'] && $menit < $getBaik['response_durasi']){
                        $nilai = 'Baik';
                    }else if($menit > $getBaik['response_durasi'] && $menit < $getKurang['response_durasi']){
                        $nilai = 'Kurang';
                    }else if($menit > $getSangatKurang['response_durasi']){
                        $nilai = 'Sangat Kurang';
                    }
                    $this->m_assesment->simpan_assesment_response_dev($kode, $userId, $nilai, $menit);
                }
                $timelineId = rand(123456789,99999999);
                $statusCode = 'F';
                $this->db->query("INSERT into timeline (timeline_id,timeline_work_id, timeline_user, timeline_code, timeline_nama, timeline_deskripsi)
                VALUES('$timelineId','$kode','$namaUser','READYAPK','Update to wait for new APK/IPA/Builds','Task is ready to test in the new APK/IPA/Builds')");
                $this->db->query("UPDATE work set work_status='$statusCode' where work_id='$kode'");
                echo $this->session->set_flashdata('msg','success-apkWork');
    			if($slug !== ''){
    			    redirect('admin/dashboard/project/'.$slug);
    			}else{
    			    redirect('admin/dashboard');
    			}
            }
		}else{
			redirect('permission');
		}
	}
	
	function checkpoint(){
		if($this->session->userdata('akses')=='4' || $this->session->userdata('akses') === '2' || $this->session->userdata('akses')=='5'){
            $kode = $this->session->userdata('idadmin');
			$priorData = $this->db->query("SELECT * FROM pengguna where pengguna_prior !='' AND pengguna_id='$kode'");
			$x['idProject'] = '';
			$proj = $priorData->row_array();
			$idProject = $proj['pengguna_prior'];
			$kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$idProject'")->row_array();
			$slugProject = $kategori['kategori_slug'];
			$x['kategoriId'] = $kategori['kategori_id'];
			if($priorData->num_rows() > 0){
			    redirect('admin/work/checkpoint_project/'.$slugProject);
			}else{
			    $x['idProject'] = null;
			    if($this->session->userdata('akses') === '2'){
    			    $x['data']=$this->db->query("SELECT * FROM work where work_status='0'");
    			}else{
    			    $x['data']=$this->db->query("SELECT * FROM work where work_user_id='$kode' AND work_status='0'");
    			}
			}
			
            $x['kategori']=$this->m_kategori->get_all_kategori();
            $x['assignor2']=$this->db->query("SELECT * FROM pengguna where pengguna_level='3'");
            $x['userId']=$this->session->userdata('idadmin');
            $x['akses']=$this->session->userdata('akses');
			$this->load->view('admin/v_checkpoint',$x);
		}else{
			redirect('permission');
		}
	}
	
	function goToProject(){
	    $project=$this->input->post('xproject');
	    $data = $this->db->query("SELECT * FROM work where work_kategori_id='$project'");
	   
	   // if($data->num_rows() > 0){
	       $kategori = $this->db->query("SELECT * FROM kategori where kategori_id='$project'");
	       $proj = $kategori->row_array();
	       $projectCode = $proj['kategori_slug'];
	       
	       redirect('admin/work/checkpoint_project/'.$projectCode);
	   // }else{
    //         echo $this->session->set_flashdata('msg','project-kosong');
	   //     redirect('admin/dashboard');
	   // }
	    
	}
	
    	    
	
	
	function notification_workDeadline(){
	    $work = $this->db->query("SELECT * FROM work where work_status = 'A' AND work_deadline > CURDATE()");
    	    if($work->num_rows() > 0){
    	        foreach($work->result() as $worksDead){
    	            $idKategori = $worksDead->work_kategori_id;
        	        $project = $this->db->query("SELECT * FROM kategori where kategori_id='$idKategori'")->row_array();
        	        $iconProject = $project['kategori_gambar'];
        	        
        	        $idBA = $worksDead->work_user_id;
        	        $ba = $this->db->query("SELECT * FROM pengguna where pengguna_id='$idBA'")->row_array();
        	            $emailBA = $ba['pengguna_email'];
        	            $namaBA = $ba['pengguna_nama'];
        	        
        	        $idDev = $worksDead->work_assignor_id;
        	        $dev = $this->db->query("SELECT * FROM pengguna where pengguna_id='$idDev'")->row_array();
        	            $emailDev = $dev['pengguna_email'];
        	            $namaDev = $dev['pengguna_nama'];
        	            
        	        $pm1 = $this->db->query("SELECT * FROM pengguna where pengguna_level='2'");
        	        $listPM = array();
        	        foreach($pm1->result() as $pm):
        	            array_push($listPM,$pm->pengguna_email);
        	        endforeach;
        	            
        	        $deadlineEmail = date("d M Y H:i A",strtotime($worksDead->work_deadline));
        	        
        	        $workNama = $worksDead->work_nama;
        	        $workDeskripsi = strip_tags($worksDead->work_deskripsi);
        	        
        	        $listAllReciepents = array($emailBA, $emailDev);
        	        
        	        $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://mail.bacotlu.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'notification@bacotlu.com',
                    'smtp_pass' => 'bacotlu123',
                    'mailtype'  => 'html',
                    'charset' => 'utf-8',);
                    $this->email->initialize($config);
                    $this->email->from('notification@bacotlu.com','Myta Notification');
                    $this->email->to($listAllReciepents,$listPM);
                    $this->email->subject("$namaDev Passing the Deadline");
                    $this->email->message("
                    <html>
                    <body
                        style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
                        <table
                            style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
                                        <center>
                                            <img style='margin:20px' src='https://myta.bacotlu.com/assets/images/business/$iconProject' width='120px'>
                                        </center>
                                        
                                        <center>
                                            <h2>$workNama</h2>
                                            <p style='font-size:14px;margin:0 0 6px 0;'>
                                                $workDeskripsi
                                            </p><br>
                                            <center>
                                                <img style='margin:20px' src='https://myta.bacotlu.com/theme/images/warning.gif' width='50px'>
                                            </center>
                                            <p style='font-size:14px;margin:0 0 6px 0;'>
                                                Deadline : <b>$deadlineEmail</b>, <br>Please be concern to finish the Task!
                                            </p><br>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='height:35px;'></td>
                                </tr>
                            </tbody>
                            <tfooter>
                                <tr>
                                    <td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
                                        <strong style='display:block;margin:0 0 10px 0;'>Assignor,</strong> $namaBA | Developer, $namaDev <br> Jakarta Barat, DKI
                                        Jakarta, IND<br><br>
                                        This email is automatic sent by system, please dont reply this message, Thankyou
                                    </td>
                                </tr>
                            </tfooter>
                        </table>
                    </body>
                    </html>");
                 $this->email->send();
    	        }
    	        echo '<center><div>Deadline Notif Sent</div></center>';
    	        
    	    }else{
    	        echo '<center><div>Deadline Nihil</div></center>';
    	    }
	        
	    }
	 
	 function export_word(){
	     if($this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='5' ||$this->session->userdata('akses')=='1'||$this->session->userdata('akses')=='3'||$this->session->userdata('akses')=='2' ){
    	     $kode = $this->input->post("xkode");
    	     $data = $this->db->query("SELECT * FROM work where work_id='$kode'")->row_array();
    	     $deskripsi = strip_tags($data['work_deskripsi'],'<img>');
    	     $nama = $data['work_nama'];
    	     $date = date('d-M-Y-H:i');
    	     $filename = $nama . uniqid () .$date. '.doc';
    	     header("Content-Type: application/vnd.ms-word");
             header("Expires: 0");
             header("Content-disposition: attachment; filename=\"$filename\"");
             echo $deskripsi;
	     }else{
	        redirect('permission'); 
	     }
     }
     


        
}
