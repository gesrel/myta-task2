<?php
class Comment extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != true) {
            $url = base_url('login');
            redirect($url);
        };
        $this->load->library('upload');
        $this->load->library('email');

    }

    function index(){
        
    }



    function set_userOnline(){
        $userId = $this->session->userdata('idadmin');
        $taskId = $this->input->post('kode');
        $this->db->query("INSERT INTO user_online (online_task_id, online_user_id) 
        values('$taskId','$userId')");
    }

    function unset_userOnline(){
        $userId = $this->session->userdata('idadmin');
        $taskId = $this->input->post('kode');
        $this->db->query("DELETE FROM user_online where online_task_id='$taskId' AND online_user_id='$userId'");
    }

    function check_online(){
        $userId = $this->session->userdata('idadmin');
        $taskId = $this->input->post('kode');

        $online = $this->db->query("SELECT * FROM user_online
        inner join pengguna on pengguna.pengguna_id=online_user_id
         where online_task_id='$taskId' AND online_user_id!='$userId'");

        if($online->num_rows() > 0){
            $listOnline = array();
            foreach($online->result() as $ol){
                $gambar = base_url().'assets/images/'.$ol->pengguna_photo;
                $userOnline = array(
                    'online_user' => $ol->pengguna_nama,
                    'online_user_gambar' => $gambar
                );
                array_push($listOnline,$userOnline);
            }
            echo json_encode($listOnline);
        }else{
            echo 'noOnline';
        }
        
    }

    function read_comment(){
        $kode = $this->input->post('kode');
        $userId = $this->session->userdata('idadmin');
        $this->db->query("UPDATE comment set comment_read='READ' where comment_user_id!='$userId' AND comment_task_id='$kode'");

    }

    function hit_newComment_dashboard(){
        $userId = $this->session->userdata('idadmin');
        $akses = $this->session->userdata('akses');
        if($akses === '4' || $akses === '5'){
            $comment = $this->db->query("SELECT * FROM comment 
            inner join pengguna on pengguna.pengguna_id=comment_user_id
            inner join attachment on attachment.attachment_comment_id=comment_id
            inner join work on work.work_id=comment.comment_task_id
            inner join kategori on kategori_id=work.work_kategori_id
            where comment_read='UNREAD' AND comment_user_id!='$userId' AND work_user_id='$userId'");
        }else{
            $comment = $this->db->query("SELECT * FROM comment 
            inner join pengguna on pengguna.pengguna_id=comment_user_id
            inner join attachment on attachment.attachment_comment_id=comment_id
            inner join work on work.work_id=comment.comment_task_id
            inner join kategori on kategori_id=work.work_kategori_id
            where comment_read='UNREAD' AND comment_user_id!='$userId' AND work_assignor_id='$userId'");
        }

        if($comment->num_rows() > 0){
            $response = array();
            foreach($comment->result() as $cmt){
                $attach1 = $cmt->attachment_file1;
                $gif = false;
                if(strpos($attach1, '.gif')){
                    $gif = true;
                    $attach1 = "<img width='30px' src='".base_url()."assets/images/attachment/".$attach1."'>";
                }
                $isi = substr($cmt->comment_isi, 0, 60);
                $response1 = array(
                    'task_id' => $cmt->work_id,
                    'task_nama' => $cmt->work_nama,
                    'kategori_nama' => $cmt->kategori_nama,
                    'comment_isi' => $isi,
                    'comment_user' => $cmt->pengguna_nama,
                    'comment_attachment1' => $attach1,
                    'comment_gif' => $gif
                );
                array_push($response, $response1);
                
            }           
            echo json_encode($response);
        }else{
            echo 'noNotif';
        }  
    }


    function hit_newComment(){
        $kode = $this->input->post('kode');
        date_default_timezone_set("Asia/Jakarta");
        $userId = $this->session->userdata('idadmin');
        $hasilComment = array();

			$comment = $this->db->query("SELECT * FROM comment 
			inner join pengguna on pengguna.pengguna_id=comment_user_id
			inner join attachment on attachment.attachment_comment_id=comment_id
            where comment_task_id='$kode' AND comment_read='UNREAD' AND comment_user_id!='$userId'");

			foreach($comment->result() as $com){
				$gambar = $com->pengguna_photo;	
				$gender = $com->pengguna_jenkel;
				if($gambar === '' || $gambar === null){
					if($gender === 'L'){
						$gambar = 'user_blank.png';
					}else{
						$gambar = 'user_blank2.png';
					}
				}
				$time = date("d M Y h:i A", strtotime($com->comment_created));
				$attach1 = $com->attachment_file1;
				$attach2 = $com->attachment_file2;
				$attach3 = $com->attachment_file3;
				$attach4 = $com->attachment_file4;

				if(strpos($attach1, '.PDF') || strpos($attach1, '.pdf')){
					$attach1 = "pdf.png";
				}
				if(strpos($attach2, '.PDF') || strpos($attach2, '.pdf')){
					$attach2 = "pdf.png";
				}
				if(strpos($attach3, '.PDF') || strpos($attach3, '.pdf')){
					$attach3 = "pdf.png";
				}
				if(strpos($attach4, '.PDF') || strpos($attach4, '.pdf')){
					$attach4 = "pdf.png";
				}
                
                if($attach1 == ""){
					$attach1 = '';
				}else{
					$attach1 = '<a class="img-link img-link-zoom-in img-lightbox" target="_blank" href="'.base_url().'assets/images/attachment/'.$com->attachment_file1.'"><img class="img-fluid" src="'.base_url().'assets/images/attachment/'.$attach1.'" alt=""></a>';
				}
				
				if($attach2 == ""){
					$attach2 = '';
				}else{
					$attach2 = '<a class="img-link img-link-zoom-in img-lightbox" target="_blank" href="'.base_url().'assets/images/attachment/'.$com->attachment_file2.'"><img class="img-fluid" src="'.base_url().'assets/images/attachment/'.$attach2.'" alt=""></a>';
				}

				if($attach3 == ""){
					$attach3 = '';
				}else{
					$attach3 = '<a class="img-link img-link-zoom-in img-lightbox" target="_blank" href="'.base_url().'assets/images/attachment/'.$com->attachment_file3.'"><img class="img-fluid" src="'.base_url().'assets/images/attachment/'.$attach3.'" alt=""></a>';
				}

				if($attach4 == ""){
					$attach4 = '';
				}else{
					$attach4 = '<a class="img-link img-link-zoom-in img-lightbox" target="_blank" href="'.base_url().'assets/images/attachment/'.$com->attachment_file4.'"><img class="img-fluid" src="'.base_url().'assets/images/attachment/'.$attach4.'" alt=""></a>';
				}
                $isi = substr($com->comment_isi, 0, 40);

				$hasilComment1 = array(
					'comment_created' => $time,
					'comment_user' => $com->pengguna_nama,
					'comment_user_gambar' => $gambar,
					'comment_isi' => $com->comment_isi,
					'comment_attach1' => $attach1 ,
					'comment_attach2' => $attach2 ,
					'comment_attach3' => $attach3 ,
                    'comment_attach4' => $attach4 ,
                    'comment_isi2' => $isi
				);
				array_push($hasilComment, $hasilComment1);
            }
			// $response['listComment'] = $hasilComment; 
			
			echo json_encode($hasilComment);
    }

    function tambah_comment(){
        $config['upload_path'] = './assets/images/attachment/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = true; //nama yang terupload nantinya
        $this->upload->initialize($config);
        
        $taskId = $this->input->post('taskId');
        $userId = $this->session->userdata('idadmin');
        $comment = $this->input->post('isi');
        $screen = $this->input->post('screen');

        $emoticon = $this->input->post('emoticon');

        
        $pengirim = $this->db->query("SELECT * FROM pengguna
        where pengguna_id='$userId'")->row_array();

        $kategori = $this->db->query("SELECT * FROM kategori
        inner join work on work.work_kategori_id=kategori.kategori_id
        inner join pengguna on pengguna.pengguna_id=kategori.kategori_tl_id
        where work.work_id='$taskId'
        ")->row_array();

        $batl = $this->db->query("SELECT * FROM pengguna
        inner join work on work.work_user_id=pengguna.pengguna_id
        where work.work_id='$taskId' AND
        pengguna_level IN ('4','5')")->row_array();

        $dev = $this->db->query("SELECT * FROM pengguna
        inner join work on work.work_assignor_id=pengguna.pengguna_id
        where work.work_id='$taskId' AND
        pengguna_level IN ('3')")->row_array();

        $namaTask = $kategori['work_nama'];
        $emailDev = $dev['pengguna_email'];
        $emailBATL = $batl['pengguna_email'];
        $emailTL = $kategori['pengguna_email'];
        $pengirim_nama = $pengirim['pengguna_nama'];
        $listAllReciepents = array($emailBATL, $emailDev);

        if($this->session->userdata('akses') === '3'){
            $listAllReciepents = array($emailBATL);
        }else if($this->session->userdata('akses') === '4' ||$this->session->userdata('akses') === '5' ){
            $listAllReciepents = array($emailDev);
        }

        if($this->upload->do_upload('attach1')){
            $file1 = $this->upload->data();
            $gambar1 = $file1['file_name'];
            if(strpos($gambar1, '.PDF') || strpos($gambar1, '.pdf')){
                $attach_email1 = "<a href='".base_url()."/assets/images/attachment/".$gambar1."' target='_blank'><img width='200px' src='".base_url()."/assets/images/attachment/pdf.png'></a>";
            }else{
                $attach_email1 = "<a href='".base_url()."/assets/images/attachment/".$gambar1."' target='_blank'><img width='200px' src='".base_url()."/assets/images/attachment/'".$gambar1."></a>";
            }
        }else{
            if($emoticon !== ''){
               $gambar1 = $emoticon;
               $attach_email1 = "<img width='80px' src='".base_url()."/assets/images/attachment/$emoticon'>";
            }else{
               $gambar1 = null;
            }
        }
        if($this->upload->do_upload('attach2')){
            $file2 = $this->upload->data();
            $gambar2 = $file2['file_name'];
            if(strpos($gambar2, '.PDF') || strpos($gambar2, '.pdf')){
                $attach_email2 = "<a href='".base_url()."/assets/images/attachment/".$gambar2."' target='_blank'><img width='200px' src='".base_url()."/assets/images/attachment/pdf.png'></a>";
            }else{
                $attach_email2 = "<a href='".base_url()."/assets/images/attachment/".$gambar2."' target='_blank'><img width='200px' src='".base_url()."/assets/images/attachment/'".$gambar2."></a>";
            }
        }else{
            $gambar2 = null;
            $attach_email2 = '';

        }
        if($this->upload->do_upload('attach3')){
            $file3 = $this->upload->data();
            $gambar3 = $file3['file_name'];
            if(strpos($gambar3, '.PDF') || strpos($gambar3, '.pdf')){
                $attach_email3 = "<a href='".base_url()."/assets/images/attachment/".$gambar3."' target='_blank'><img width='200px' src='".base_url()."/assets/images/attachment/pdf.png'></a>";
            }else{
                $attach_email3 = "<a href='".base_url()."/assets/images/attachment/".$gambar3."' target='_blank'><img width='200px' src='".base_url()."/assets/images/attachment/'".$gambar3."></a>";
            }
        }else{
            $gambar3 = null;
            $attach_email3 = '';

        }
        if($this->upload->do_upload('attach4')){
            $file4 = $this->upload->data();
            $gambar4 = $file4['file_name'];
            if(strpos($gambar4, '.PDF') || strpos($gambar4, '.pdf')){
                $attach_email4 = "<a href='".base_url()."/assets/images/attachment/".$gambar4."' target='_blank'><img width='200px' src='".base_url()."/assets/images/attachment/pdf.png'></a>";
            }else{
                $attach_email4 = "<a href='".base_url()."/assets/images/attachment/".$gambar4."' target='_blank'><img width='200px' src='".base_url()."/assets/images/attachment/'".$gambar4."></a>";
            }
        }else{
            $gambar4 = null;
            $attach_email4 = '';

        }
        
        if($comment === "" && $gambar1 === null && $gambar2 === null && $gambar3 === null && $gambar4 === null){
        }else{
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://mail.bacotlu.com',
                'smtp_port' => 465,
                'smtp_user' => 'notification@bacotlu.com',
                'smtp_pass' => 'bacotlu123',
                'mailtype'  => 'html',
                'charset' => 'utf-8',);
                $this->email->initialize($config);
                $this->email->from('notification@bacotlu.com','Myta My Task');
                $this->email->to($listAllReciepents);
                $this->email->subject("$pengirim_nama post new comment on $namaTask");
                $this->email->message("
                <html>
                <body
                    style='background-color:#ddd;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
                    <table
                        style='max-width:900px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px black;'>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
                                   <h2>$namaTask</h2>
                                    <p style='font-size:14px;margin:0 0 6px 0;'>
                                        $comment
                                    </p><br>
                                    $attach_email1 $attach_email2 $attach_email3 $attach_email4
                                </td>
                            </tr>
                            <tr>
                                <td style='height:35px;'></td>
                            </tr>
                        </tbody>
                        <tfooter>
                            <tr>
                                <td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
                                    <strong style='display:block;margin:0 0 10px 0;'>Regards,</strong> $pengirim_nama <br> Jakarta Barat, DKI
                                    Jakarta, IND<br><br>

                                    This email is automatic sent by system, please dont reply this message, Thankyou
                                </td>
                            </tr>
                        </tfooter>
                    </table>
                </body>
                </html>");
            // $this->email->send();
            $commentId = rand(111111,123456789);
            $this->db->query("INSERT INTO comment (comment_id,comment_task_id, comment_user_id,comment_isi) values
            ('$commentId','$taskId','$userId','$comment')");
    
            $this->db->query("INSERT INTO attachment(attachment_comment_id,attachment_file1, attachment_file2, attachment_file3, attachment_file4) values 
            ('$commentId','$gambar1','$gambar2','$gambar3','$gambar4')");
            
        }
        
        $this->session->set_userdata('succesComment', 'success_add_comment');
        $this->session->set_userdata('idtask', $taskId);
        if($screen === 'scrumboard'){
            redirect('admin/scrumboard');
        }else{
            redirect('admin/dashboard/project/'.$kategori['kategori_slug']);
        }
    }
}