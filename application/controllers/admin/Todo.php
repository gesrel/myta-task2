<?php
class Todo extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
		$this->load->library('upload');
	}


	function index(){
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='5' ){
			$kode = $this->session->userdata('idadmin');
			$userId = $this->session->userdata('idadmin');
				$userDB = $this->db->query("SELECT * FROM pengguna where pengguna_id='$userId'")->row_array();
				$x['dark'] = $userDB['pengguna_moto'];
			$x['work']=$this->db->query("SELECT * FROM work where work_user_id='$kode' AND work_status='A'");
			$this->load->view('admin/v_todo',$x);
		}elseif($this->session->userdata('akses') === '3'){
		    $kode = $this->session->userdata('idadmin');
			$x['work']=$this->db->query("SELECT * FROM work where work_assignor_id='$kode' AND work_status='A'");
			$this->load->view('admin/v_todo',$x);
		}else{
			redirect('permission');
		}
	}

}