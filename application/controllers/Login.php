<?php
class Login extends CI_Controller{
    function __construct(){
        parent:: __construct();
        $this->load->model('M_login','m_login');
        $this->load->library('user_agent');

    }
    function index(){
        $this->load->view('admin/v_login');
    }
    function auth(){
        $email=htmlspecialchars($this->input->post('email',TRUE),ENT_QUOTES);
        $password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);
        $u=$email;
        $p=$password;
        $cadmin=$this->m_login->cekadmin($u,$p);
        if($cadmin->num_rows() > 0){
         $this->session->set_userdata('masuk',true);
         $this->session->set_userdata('user',$u);
         $xcadmin=$cadmin->row_array();
         $mobile = false;
         $agent='';
         if ($this->agent->is_browser()){
            $agent = $this->agent->browser().' '.$this->agent->version();
         }elseif ($this->agent->is_robot()){
            $agent = $this->agent->robot();
         }elseif ($this->agent->is_mobile()){
            $agent = $this->agent->mobile();
            $mobile = true;
         }else{
            $agent = 'Unidentified Device';
         }
         
         

         if($xcadmin['pengguna_level']==1){
            $this->session->set_userdata('akses','1');
            $idadmin=$xcadmin['pengguna_id'];
            $user_nama=$xcadmin['pengguna_nama'];
            $user_telp=$xcadmin['pengguna_nohp'];
            $user_prior=$xcadmin['pengguna_prior'];
            
            $this->session->set_userdata('prior', $user_prior);
            $this->session->set_userdata('idadmin',$idadmin);
            $this->session->set_userdata('nama',$user_nama);
            $this->session->set_userdata('mobile', $mobile);
         }elseif($xcadmin['pengguna_level']==2){
            $this->session->set_userdata('akses','2');
            $idadmin=$xcadmin['pengguna_id'];
            $user_nama=$xcadmin['pengguna_nama'];
            $user_telp=$xcadmin['pengguna_nohp'];
            $user_prior=$xcadmin['pengguna_prior'];
            $this->session->set_userdata('mobile', $mobile);
            $this->session->set_userdata('prior', $user_prior);

            $this->session->set_userdata('idadmin',$idadmin);
            $this->session->set_userdata('nama',$user_nama);

         }elseif($xcadmin['pengguna_level']==3){
            $this->session->set_userdata('akses','3');
            $idadmin=$xcadmin['pengguna_id'];
            $user_nama=$xcadmin['pengguna_nama'];
            $user_telp=$xcadmin['pengguna_nohp'];
            $user_prior=$xcadmin['pengguna_prior'];
            $this->session->set_userdata('mobile', $mobile);
            $this->session->set_userdata('prior', $user_prior);

            $this->session->set_userdata('idadmin',$idadmin);
            $this->session->set_userdata('nama',$user_nama);

        }elseif($xcadmin['pengguna_level']==4){
            $this->session->set_userdata('akses','4');
            $idadmin=$xcadmin['pengguna_id'];
            $user_nama=$xcadmin['pengguna_nama'];
            $user_telp=$xcadmin['pengguna_nohp'];
            $user_prior=$xcadmin['pengguna_prior'];
           $this->session->set_userdata('mobile', $mobile);
            $this->session->set_userdata('prior', $user_prior);

            $this->session->set_userdata('idadmin',$idadmin);
            $this->session->set_userdata('nama',$user_nama);
        }elseif($xcadmin['pengguna_level']==5){
            $this->session->set_userdata('akses','5');
            $idadmin=$xcadmin['pengguna_id'];
            $user_nama=$xcadmin['pengguna_nama'];
            $user_telp=$xcadmin['pengguna_nohp'];
            $user_prior=$xcadmin['pengguna_prior'];
           $this->session->set_userdata('mobile', $mobile);
            $this->session->set_userdata('prior', $user_prior);

            $this->session->set_userdata('idadmin',$idadmin);
            $this->session->set_userdata('nama',$user_nama);
        }else{
            redirect('login/gagallogin2');

        }

        }
        
        if($this->session->userdata('masuk')==true){
            $agent='';
         if ($this->agent->is_browser()){
            $agent = $this->agent->browser().' '.$this->agent->version();
         }elseif ($this->agent->is_robot()){
            $agent = $this->agent->robot();
         }elseif ($this->agent->is_mobile()){
            $agent = $this->agent->mobile();
         }else{
            $agent = 'Unidentified Device';
         }
            $user_nama = $this->session->userdata('nama');
            $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$user_nama','Login aplikasi dengan menggunakan $agent')");
            redirect('login/berhasillogin');
        }else{
            redirect('login/gagallogin');
        }
    }
        function berhasillogin(){
            echo $this->session->set_flashdata('bannerLogin','true');
            redirect('admin/dashboard');
        }
        function gagallogin(){
            $url=base_url('login');
            echo $this->session->set_flashdata('msg','<div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button> Wrong Email or Password!</div>');
            redirect($url);
        }
        function gagallogin2(){
            $url=base_url('login');
            echo $this->session->set_flashdata('msg','<div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button> Terjadi Kesalahan!</div>');
            redirect($url);
        }
        function logout(){
            $this->session->sess_destroy();
            $user_nama = $this->session->userdata('nama');
            $this->db->query("INSERT INTO log (log_user, log_deskripsi) VALUES ('$user_nama','Logout dari aplikasi')");
            $url=base_url('login');
            redirect($url);
        }
}